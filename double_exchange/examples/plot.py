import numpy as np, h5py, time, sys
sys.path.append('../')
from numba_wrapper import *
sys.path.append('../../plot_manager')
from plot2d import plotfig


e = np.linspace(-5,5,100)
mu = 0
temp = 1000
filling = fermi_fun(e, mu, temp)

cspin = h5py.File("results/01.hdf5","r")["cspin"][:]
print(cspin.shape)
trange = np.arange(0,100,0.1)

data = dict();values = dict()
data['x_plot'] = trange; data['y_plot'] = cspin[:,0,:]
values['xlabel1'] = r'$\mathrm{\epsilon/\mu}$'
values['ylabel1'] = r'$\mathrm{n}$'
sf = np.array([[0,3]])
plotfig(data, 'ff', values, sf=sf, fs=8, lw=0.5)
