import numpy as np, h5py, time, sys
from scipy import linalg
sys.path.append('../')
from square_lattice import Lattice
from hamiltonian_core import Op
from evolve_step import rk4_rho, Henk_Lindblad
from constants import SIG_X, SIG_Y, SIG_Z, UNIT, KB, HBAR
from numba_wrapper import *
sys.path.append('../../plot_manager')
from plot2d import plotfig
nm = '03'
start = time.time()
#reading input parameters------------------------------
Lx	= 8
Ly	= 8
t0      = 0.5
JH      = 2
temp    = 300
mu      = -2.0
g_l     = 0.1

dtheta   = 0.1*np.pi/180
espins = np.zeros(3)
#building the system-----------------------------------
lat = Lattice(Lx=Lx,Ly=Ly, layers=1)
ham = Op(lat, t0=t0, tj=JH)
evolve = Henk_Lindblad(gamma=1.0)
#defining initial cspin config with temp effect--------
spins = np.zeros((lat.Ns,3))
theta = 0.1*np.pi/180
np.random.seed(10)
for i in np.arange(lat.Ns):
    dphi    = 2*np.pi*np.random.rand(1)
    spins[i,:] = (np.sin(theta) * np.cos(dphi),
                   np.sin(theta) * np.sin(dphi),
                   np.cos(theta)) 
ham.cspins.s = spins
Ht = ham.create_H()
#print(np.round(Ht.real,2))
#-------------------------------------------------------------------
#finding initial state
wt,vt = np.linalg.eigh(Ht)
e_exp = np.diag(fermi_fun(wt, mu, temp))
rho_e = e_exp 
rho_s = s_basis(rho_e, vt)
print(ham.check_equal(rho_e, e_basis(rho_s, vt)))

#rho = np.outer(vt[:,0], vt[:,0].conj())
#rho = calc_rho_vdv(wt,vt,temp)
#rho = calc_rho(wt,vt, temp)
rhot = rho_s
#equilibrium measurements of current printed
bb = [0, 1]
print(bb, ham.spin_curr_rho(bb, rhot, Ht, 'all'), np.trace(rhot))
print(ham.cspins.jsd_to_llg, ham.cspins.g_lambda, ham.cspins.jexc)

trange = np.arange(0,100,0.1)
csave = np.zeros((len(trange),lat.Ns,3))


omega = 1.54/0.6582119
Atx = 0.12*np.cos(omega*trange) * np.exp(-(trange-50)**2 /(2*10*10) )
Aty = 0.12*np.sin(omega*trange) * np.exp(-(trange-50)**2 /(2*10*10) )

#espins = ham.rho_spinden(rhot)
#print(espins[0,:])
#ham.cspins.llg(espins, 0.0)
for t_ind, t_now in enumerate(trange):
    #measurements
    csave[t_ind,:] = ham.cspins.s
    #evolution
    #find new hamiltonian and bring Ht, rhot to new energy basis
    Ht = ham.create_H(spins = ham.cspins.s, Atx=Atx[t_ind], Aty=Aty[t_ind])
    wt, vt = np.linalg.eigh(Ht)
    rho_e = e_basis(rhot, vt)
    Ht_e = e_basis(Ht, vt)
    #evolve in new energy basis with new wt
    rho_e = evolve.rk4_open(rho_e, Ht_e, wt)
    #bring back to site basis for calculation and LLG
    rhot = s_basis(rho_e, vt)
    #find espin density and do LLG with that
    espins = ham.rho_spinden(rhot)
    ham.cspins.llg(espins, t_now)

fsave = h5py.File('results/'+nm+'.hdf5',"w")
fsave.create_dataset("cspin", data = csave, 
             dtype=np.float64)
fsave.create_dataset("Atx", data = Atx, 
             dtype=np.float64)
fsave.close()

data = dict();values = dict()
data['x_plot'] = trange; data['y_plot'] = csave[:,0,:]
values['xlabel1'] = r'$\mathrm{S^{\alpha}_i}$'
values['ylabel1'] = r'$\mathrm{Time~(fs)}$'
Cl = ['C0','C2','C3','k']
sf = np.array([[0,3]])
plotfig(data, 'm_'+nm, values, sf=sf, Cl=Cl,fs=8, lw=0.5)
print('time taken: ',time.time()-start)

"""
data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = csave[:,2:3]/Nconf
values['xlabel'] = r'$\mathrm{Time}$'
values['ylabel'] = r'$\mathrm{M_z}$'
plotfig(data, '1', values, ylim=(-1.2,1.2),fs=8, lw=0.5)
f.close()
with open('results/'+nm+'out.txt', "w") as fref:
    out_str = str(time.time()-start)
    fref.write(out_str)
fref.close()
"""
