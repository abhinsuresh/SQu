import numpy as np
import sys
from numba_wrapper import vector, unit_vector, flatten_list 
from kwant_systems import make_ribbon
from index_core import *
from spins import Spins
from constants import SIG_X, SIG_Y, SIG_Z
from plot_core import plotfig, plot_video 


class Lattice:

    def __init__(self,
                    Lx = 3,
                    Ly = 3,
                    layers = 2,
                    PBC = 0,
                    dtype = np.complex64
                ):
        self.Lx = Lx
        self.Ly = Ly
        self.layers = layers
        self.Ns = self.Lx*self.Ly
        self.dtype = dtype
        self.PBC = PBC
        self.bond_len = dict()
        self.pos_atoms = [] 
        ###
        self.generate()
        return

    def generate(self):
        """generates all the hopping indices required
        """
        #Sn
        self.intra()
        self.ribbon = make_ribbon(length=self.Lx, width=self.Ly)
        #self.pos = np.array([site.pos for site in self.ribbon.sites])
         
    def intra(self):
        """Find the intra layer hopping list for triangular plane lattice
        and extend for bilayer
        -Base hopping in TB Hamiltonian
        Constructs
        ----------
        self.hopx_list
        self.hopy_list
        """
        intra(self)

    #---------------------------------------------------------------
    # basic math functions
    #---------------------------------------------------------------

    def bond_v(self, i, j):
        """find the bond direction from bond indices
        """
        return vector(self.pos_atoms[i], self.pos_atoms[j])
    
    def bond_uv(self, i, j):
        """find the bond direction from bond indices
        """
        return unit_vector(self.pos_atoms[i], self.pos_atoms[j])
    def rotate_sample(self, theta):
        """rotate the sample by theta radian using z-axis 
        rotation matrix
        """
        mat = np.array([[np.cos(theta),-np.sin(theta),0],
                        [np.sin(theta), np.cos(theta),0],
                        [0            , 0            ,1]])
        for i in range(len(self.pos_atoms)):
            self.pos_atoms[i,:] = mat @ self.pos_atoms[i,:]

    #---------------------------------------------------------------
    # plot functions
    #---------------------------------------------------------------
    def plotfig(self, ham, fig_name, values=dict(), espins=np.ones((9,3)), 
                Gs=dict(), **kwargs):		
        plotfig(self, ham, fig_name, values=dict(), espins=np.ones((9,3)), 
                Gs=Gs, **kwargs)	
        
    def plot_video(self, ham, times, plot_y, t_plot, fig_name, t_now, values=dict(), 
                  Gs=dict(), **kwargs):		
        plot_video(self, ham, times, plot_y, t_plot, fig_name, t_now, values=dict(), 
                  Gs=Gs, **kwargs)	
