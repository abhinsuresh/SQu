import numpy as np
import sys
from spins import Spins
from constants import SIG_X, SIG_Y, SIG_Z
from numba_wrapper import flatten_list

class Op:
    def __init__(self,
                    lat,
                    dtype = np.complex64,
                    #all terms in TB ham with positive sign
                    t0 = -1,    #intra hopping
                    tj = 0.0,  #(also copied to llg)
                    tso = 0.0,   # not part of balents ham
                    dt = 0.1,
                    #llg parameters follow same as marko
                    g_lambda = 0.01,
                ):
        self.lat = lat
        self.dtype = dtype
        self.t0 = t0
        self.tj = tj
        self.tso = tso
        self.dt = dt
        self.g_lambda = g_lambda
        self.verbose = False
        self.generate()
        return
    def generate(self):
        """generate cspins object with llg included
        The JH interaction between the classical spin and
        quantum electrons is ferromangetic (negative in Ham with JH>0)
        that implies heff sign should be positive (heff = -dHclc/dS)
        """
        if self.tj < 0: print("\n!!! wrong sign convention to LLG !!!\n")
        self.cspins = Spins(self.lat.ribbon,
                            dt          = self.dt, 
                            jsd_to_llg  = self.tj, #(+ve in Heff(i.e. -ve in Hcl), -ve in Hele)
                            g_lambda    = self.g_lambda)
    def create_H(self, spins=None, Atx=0, Aty=0):
        """Hamiltonian size if Pt assumes to have bilayer Mn3Sn
        """
        #setting spin direction, accept new cspin or take initial 
        if spins is None:
            spins = self.cspins.s
            if self.verbose: print('assigning default cspins values')
        #initialising the hamiltonian with zeros
        H = np.zeros((self.lat.layers*2*self.lat.Ns,
                      self.lat.layers*2*self.lat.Ns), dtype=self.dtype)
        #-------------------------------------------------------------------
        fx = np.exp(-1j*Atx)
        fy = np.exp(-1j*Aty)
        #-------------------------------------------------------------------
        #adding kin hop
        for ind, [x,y] in enumerate(self.lat.hopx_list):
            H[2*x:2*(x+1), 2*y:2*(y+1)] -= self.t0 * fx * np.eye(2)
        for ind, [x,y] in enumerate(self.lat.hopy_list):
            H[2*x:2*(x+1), 2*y:2*(y+1)] -= self.t0 * fy * np.eye(2)
        #-------------------------------------------------------------------
        H = H + H.conj().T
        #-------------------------------------------------------------------
        #adding spin coupling
        for ind in range(self.lat.Lx*self.lat.Ly):
            H[2*ind:2*(ind+1), 2*ind:2*(ind+1)] -= self.tj * \
                             (SIG_X*spins[ind,0] +
                              SIG_Y*spins[ind,1] +
                              SIG_Z*spins[ind,2])
        return H

    def spin_curr_rho(self, bond, rho, H, spin='all'):
        """find the bond spin current value from rho and H
        """
        l = bond[0]
        k = bond[1]
        #1st term
        rholk = rho[2*l:2*(l+1), 2*k:2*(k+1)]
        Hkl   =   H[2*k:2*(k+1), 2*l:2*(l+1)]

        #2nd term
        rhokl = rho[2*k:2*(k+1), 2*l:2*(l+1)]
        Hlk   =   H[2*l:2*(l+1), 2*k:2*(k+1)]
        
        curr_op = 1j*( rholk @ Hkl - rhokl @ Hlk )*2*np.pi
        cc = np.trace(curr_op)
        cx = np.trace(curr_op @ SIG_X)
        cy = np.trace(curr_op @ SIG_Y)
        cz = np.trace(curr_op @ SIG_Z)
        return np.array([cc, cx, cy, cz])

    def rho_spinden(self, rho):
        """find the espins for lattice
        """
        espins = np.zeros((len(self.cspins.s),3))
        for ind in range(len(rho)//2):
            espins[ind,0] = np.trace(rho[2*ind:2*(ind+1), 2*ind:2*(ind+1)] @ SIG_X)
            espins[ind,1] = np.trace(rho[2*ind:2*(ind+1), 2*ind:2*(ind+1)] @ SIG_Y)
            espins[ind,2] = np.trace(rho[2*ind:2*(ind+1), 2*ind:2*(ind+1)] @ SIG_Z)
        return espins

    def check_hermitian(self, a, rtol=1e-05, atol=1e-08):
        return np.allclose(a, a.conj().T, rtol=rtol, atol=atol)
    
    def check_equal(self, a, b, rtol=1e-05, atol=1e-05):
        return np.allclose(a, b, rtol=rtol, atol=atol)
    
    def check_comm(self, a, b, rtol=1e-05, atol=1e-05):
        return np.allclose(a@b, b@a, rtol=rtol, atol=atol)
