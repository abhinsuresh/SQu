import numpy as np
from numba_wrapper import vector, unit_vector, flatten_list 
def intra(lat):
    """Find the intra layer hopping list for triangular plane lattice
    and extend for bilayer
    -Base hopping in TB Hamiltonian and jsd term hop
    Constructs
    ----------
    lat.hopx_list
    lat.bonds and lat.bon_len -> dict()
    """
    _hopx = []
    _hopy = []
    for i in range(lat.Ly):
        _hopx.append(range(lat.Lx*i, lat.Lx*i + lat.Lx, 1))
    for i in range(lat.Lx):
        _hopy.append(range(i, lat.Lx*(lat.Ly-1) + i + 1, lat.Lx))

    lat.hopx_list = []
    lat.hopy_list = []
    for ele in _hopx:
        lat.hopx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
        if lat.PBC: lat.hopx_list.append( [[ele[0], ele[-1]]])
    lat.hopx_list = flatten_list(lat.hopx_list)

    for ele in _hopy:
        lat.hopy_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
        if lat.PBC: lat.hopy_list.append( [[ele[0], ele[-1]]])
    lat.hopy_list = flatten_list(lat.hopy_list)
 
    #making np.array and exteding for bilayer    
    lat.hopx_list  = np.array(lat.hopx_list, dtype=np.int8)
    lat.hopy_list  = np.array(lat.hopy_list, dtype=np.int8)
    if lat.layers == 2:
        lat.hopx_list  = np.append(lat.hopx_list,  lat.hopx_list  + lat.Lx*lat.Ly, axis=0)
        lat.hopy_list = np.append(lat.hopy_list, lat.hopy_list + lat.Lx*lat.Ly, axis=0)

    lat.hop_list = np.append(lat.hopx_list, lat.hopy_list, axis=0)
    #periodic boundary condition

    lat.bonds = lat.hop_list
    lat.bond_len['intra'] = len(lat.hop_list)
