import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
from scipy.sparse import eye
from scipy.sparse import kron

def create_mu(sys, mu=1):
    #access the operators to create number operators
    OP = sys.op
    NM = sys.names
    L = OP[NM[0]+'0'].shape[0]
    H = coo_matrix((L,L),dtype=sys.dtype)
    for i in range(sys.N):
        for nm in NM:
            H = H + mu*OP[nm+str(i)].getH() @ OP[nm+str(i)]
    return H

def hubbard(sys, t=-1, periodic=0):
    #access the operators to create number operators
    OP = sys.op
    NM = sys.names
    L = OP[NM[0]+'0'].shape[0]
    H = coo_matrix((L,L),dtype=sys.dtype)
    for i in range(sys.Ns-1):
        for nm in NM:
            H = H + t*OP[nm+str(i)].getH() @ OP[nm+str(i+1)]
    H = H + H.getH()
    return H

def heisenberg(spin, J=0.1, Jz=0.1, periodic=0):
    #access the operators to create number operators
    OP = spin.op
    NM = spin.names[:3] # only getting x,y and z 
    L = OP[NM[0]+'0'].shape[0]
    H = coo_matrix((L,L),dtype=spin.dtype)
    for i in range(spin.N-1):
        for nm in NM[:2]:
            H = H + J*OP[nm+str(i)] @ OP[nm+str(i+1)]
        H = H + Jz*OP[NM[2]+str(i)] @ OP[NM[2]+str(i+1)]
    return H

