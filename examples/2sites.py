import numpy as np
from scipy.sparse import coo_matrix
from construct_op import extend_to_n_sites
from construct_op import create_spin
from verifications import spin_check
from verifications import c_check
import scipy.sparse.linalg as ssl
import scipy.linalg as sl
from timeit import default_timer as timer

names=['Cup', 'Cdn']
N = 2
J = -0.1
Jz = -0.1001

op = extend_to_n_sites(N=N)

sx0, sy0, sz0, N0 = create_spin(op, site = 0)
sx1, sy1, sz1, N1 = create_spin(op, site = 1)
spin_check(sx0, sy0, sz0)
spin_check(sx1, sy1, sz1)

#constrcuting heisenberg hamiltonian
H = coo_matrix((4**N, 4**N), dtype=np.float64)

H = H + J   * (sx0@sx1 + sy0@sy1) 
H = H + Jz*(sz0@sz1) 
#H = H + 0.1*(op['Cup0'].getH()@op['Cup0'])


ed, ev = sl.eigh(H.toarray())
#ed, ev = ssl.eigsh(H.asfptype(), k = 3, which='SA')
print('Energy values: ',ed[:5])
i = 0
print('Energy   sz0 sz1')
print('{0: 1.3f} {1: 1.2f} {2: 1.2f}'.format(
            ev[:,i] @ H @ ev[:,i],
            ev[:,i] @ sz0 @ ev[:,i],
            ev[:,i] @ sz1 @ ev[:,i]))

