import numpy as np
import sys
from scipy.sparse import eye
from scipy.sparse import kron
import scipy.linalg as la

sys.path.append('../')
from fermionic import Fermionic
from fermionic import Spin
from hamiltonian import create_mu
from hamiltonian import heisenberg

#parameters
N = 4

#electronic system creation
sys = Fermionic(N=N, kronD=1)
print(sys.op['Cup0'].shape)
op_name = sys.names

Hn = create_mu(sys)
w,v = la.eigh(Hn.toarray())
print(w[-10:])


#spin system creation
spin = Spin(S=1, N = 4, kronD=1)
op = spin.op
print(spin.Sz.toarray())
spin.kron_iden(1)

HJ = heisenberg(spin,J=0,Jz=0.1)
w,v = la.eigh(HJ.toarray())
print(w[:5])
s = 0
print(v[:,s].conj() @ op['Sz0'] @ v[:,s])
print(v[:,s].conj() @ op['Sz1'] @ v[:,s])
print(v[:,s].conj() @ op['Sz2'] @ v[:,s])
print(v[:,s].conj() @ op['Sz3'] @ v[:,s])

S = spin.Sy
I4 = eye(3)
re = kron(kron(kron(I4,I4),I4),S)
oc = 'Sy3'

if not np.allclose(op[oc].toarray(), re.toarray()):
    raise ValueError('bleh')
else:
    print('on your face')
