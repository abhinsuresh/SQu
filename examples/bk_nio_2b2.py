import numpy as np
from scipy.sparse import coo_matrix
from construct_op import extend_to_n_sites
from construct_op import create_spin
from verifications import spin_check
from verifications import c_check
from scipy.sparse import eye
import scipy.sparse.linalg as ssl
import scipy.linalg as sl
from timeit import default_timer as timer

names=['Cup', 'Cdn']
N = 4
J = 0
Jz = 0.2
t = 1.0
U = 8.0
mu = 9.5
Ue = 6.0
Jh = 1.0

op = extend_to_n_sites(N=N)



#constructing the hamiltonian
H = coo_matrix((4**N, 4**N), dtype=np.float64)
Hk = coo_matrix((4**N, 4**N), dtype=np.float64)
I  = eye(4**N)

sx0, sy0, sz0, N0 = create_spin(op, site = 0)
sx1, sy1, sz1, N1 = create_spin(op, site = 1)
sx2, sy2, sz2, N2 = create_spin(op, site = 2)
sx3, sy3, sz3, N3 = create_spin(op, site = 3)

#heisenberg
#H = H + J   * (sx0 @ sx2 + sx1 @ sx3) 
#H = H + J   * (sy0 @ sy2 + sy1 @ sy3) 
H = H + Jz*(sz0@sz2 + sz1@sz3) 

Cup0 = op['Cup0'] @ (I - op['Cdn0'].getH()@op['Cdn0'])
Cup1 = op['Cup1'] @ (I - op['Cdn1'].getH()@op['Cdn1'])
Cup2 = op['Cup2'] @ (I - op['Cdn2'].getH()@op['Cdn2'])
Cup3 = op['Cup3'] @ (I - op['Cdn3'].getH()@op['Cdn3'])

Cdn0 = op['Cdn0'] @ (I - op['Cup0'].getH()@op['Cup0'])
Cdn1 = op['Cdn1'] @ (I - op['Cup1'].getH()@op['Cup1'])
Cdn2 = op['Cdn2'] @ (I - op['Cup2'].getH()@op['Cup2'])
Cdn3 = op['Cdn3'] @ (I - op['Cup3'].getH()@op['Cup3'])


#kinetic part
Hk = Hk + t * (Cup0.getH()@Cup2 + Cdn0.getH()@Cdn2
             + Cup1.getH()@Cup3 + Cdn1.getH()@Cdn3)
Hk = Hk + Hk.getH()

#Hk = Hk + t * (op['Cup0'].getH()@op['Cup2'] + op['Cdn0'].getH()@op['Cdn2']
#             + op['Cup1'].getH()@op['Cup3'] + op['Cdn1'].getH()@op['Cdn3'])
#Hk = Hk + Hk.getH()

H = H + Hk

#intra-orbital
H = H + U * (Cup0.getH()@Cup0 @ Cdn0.getH()@Cdn0 
           + Cup1.getH()@Cup1 @ Cdn1.getH()@Cdn1 
           + Cup2.getH()@Cup2 @ Cdn2.getH()@Cdn2 
           + Cup3.getH()@Cup3 @ Cdn3.getH()@Cdn3)

#onsite
H = H - mu * (Cup0.getH()@Cup0 + Cdn0.getH()@Cdn0 
            + Cup1.getH()@Cup1 + Cdn1.getH()@Cdn1 
            + Cup2.getH()@Cup2 + Cdn2.getH()@Cdn2 
            + Cup3.getH()@Cup3 + Cdn3.getH()@Cdn3)
             
#inter-orbital
H = H + (Ue - Jh) * (Cup0.getH()@Cup0 @ Cup1.getH()@Cup1 
                   + Cdn0.getH()@Cdn0 @ Cdn1.getH()@Cdn1 
                   + Cup2.getH()@Cup2 @ Cup3.getH()@Cup3 
                   + Cdn2.getH()@Cdn2 @ Cdn3.getH()@Cdn3) 

H = H + Ue * (Cup0.getH()@Cup0 @ Cdn1.getH()@Cdn1 
            + Cdn0.getH()@Cdn0 @ Cup1.getH()@Cup1 
            + Cup2.getH()@Cup2 @ Cdn3.getH()@Cdn3 
            + Cdn2.getH()@Cdn2 @ Cup3.getH()@Cup3)

 

ed, ev = sl.eigh(H.toarray())
#ed, ev = ssl.eigsh(H.asfptype(), k = 3, which='SA')
print('Energy values: {:1.3f} {:1.3f} {:1.3f} {:1.3f} {:1.3f}'.format(*ed[:5]))
i = 0
print('Energy   sz0 sz1 sz2 sz3')
print('{0: 1.2f} {1: 1.2f} {2: 1.2f} {3: 1.2f} {4: 1.2f}'.format(
            ev[:,i] @ H @ ev[:,i],
            ev[:,i] @ sz0 @ ev[:,i],
            ev[:,i] @ sz1 @ ev[:,i],
            ev[:,i] @ sz2 @ ev[:,i],
            ev[:,i] @ sz3 @ ev[:,i]))

