import numpy as np
from scipy.sparse import coo_matrix
from construct_op import extend_to_n_sites
import scipy.sparse.linalg as ssl
import scipy.linalg as sl
from timeit import default_timer as timer

start = timer()
N = 8
t = -1.0
op = extend_to_n_sites(N=N)

H = coo_matrix((4**N, 4**N), dtype=np.float64)

names=['Cup', 'Cdn']
for nm in names:
    for i in np.arange(N):
        #print(i%N, (i+1)%N)
        #H = H + t * (np.dot(op[nm+str(i%N)].getH(), op[nm+str((i+1)%N)]))
        H = H + t * (op[nm+str(i%N)].getH() @ op[nm+str((i+1)%N)])
H = H + H.getH()
ed, ev = ssl.eigsh(H.asfptype(), k = 10, which='SA')
print(H.toarray().shape)
print(ed)

time_o = '{:1.3f}'.format(timer()-start)
print('time taken is '+ time_o + ' s')
