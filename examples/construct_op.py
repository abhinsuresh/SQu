import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import eye
from scipy.sparse import kron

def fermionic():
    """A function to create basic fermionic operators, supports
    two blocks for up and down spin, also returns the parity
    operator in the same dimension of 4 by 4.
    
    Parameters
    ----------
    
    Returns
    -------
    Cup : scipy.sparse.csr_matrix
    Cdn : scipy.sparse.csr_matrix
    P1 : scipy.sparse.csr_matrix
    """
    dtype = np.int8
    row = np.array([0, 1, 2, 3])
    col = np.array([0, 1, 2, 3])
    data = np.array([1, -1, -1, 1])
    P1 = csr_matrix((data, (row,col)), shape=((4,4)), dtype=dtype)

    row = np.array([0, 2])
    col = np.array([1, 3])
    data = np.array([1, 1])
    Cup = csr_matrix((data, (row,col)), shape=((4,4)), dtype=dtype)

    row = np.array([0, 1])
    col = np.array([2, 3])
    data = np.array([1, -1])
    Cdn = csr_matrix((data, (row,col)), shape=((4,4)), dtype=dtype)    
    
    return Cup, Cdn, P1


def extend_to_n_sites(N = 1, names=['Cup', 'Cdn']):
    """A function to extend to multiple sites basis.
    
    Parameters
    ----------
    N : int
        Total number of sites in the system
    names : names of two blocks, up and down.
    
    Returns
    -------
    result : dictionary
        A dict containing Cup and Cdn of all sites. Can be 
        accessed by 'Cup[0-(N-1)]'. In the order Cup all sites
        followed by Cdn all sites
    """
    nums = ['{0:1.0f}'.format(i) for i in np.arange(N)]    
    result = dict()    
    Cup, Cdn, P1 = fermionic()
    I4 = eye(4)
    
    for ind in np.arange(N):
        op = eye(1)
        for n_site in np.arange(N):
            
            if n_site == 0 and ind == 0:
                op = kron(op, Cup)
                
            elif n_site < ind:
                op = kron(op, P1)
                
            elif n_site == ind:
                op = kron(op, Cup)
                
            elif n_site > ind:
                op = kron(op, I4)
            
        result[names[0]+nums[ind]] = op.tocsr()
        
    for ind in np.arange(N):
        op = eye(1)
        for n_site in np.arange(N):
            
            if n_site == 0 and ind == 0:
                op = kron(op, Cdn)
                
            elif n_site < ind:
                op = kron(op, P1)
                
            elif n_site == ind:
                op = kron(op, Cdn)
                
            elif n_site > ind:
                op = kron(op, I4)
                
        result[names[1]+nums[ind]] = op.tocsr()
        
    return result

def create_spin(op, site = 0):
    """A function to create spin operators for corresponding
    sites.
    
    Parameters
    ----------
    op : dict of scipy.sparse.csr_matrices
        c-operators
    site : int
        corresponding site
    
    Returns
    -------
    sx : scipy.sparse.csr_matrix
        Spin x operator in sz basis
    sy : scipy.sparse.csr_matrix
        Spin y operator in sz basis
    sz : scipy.sparse.csr_matrix
        Spin z operator in sz basis
    N : scipy.sparse.csr_matrix
        Number operator for the corresponding site
    """
    Cup = op['Cup' + str(site)]
    Cdn = op['Cdn' + str(site)]

    sx =      (Cdn.getH()@Cup + Cup.getH()@Cdn)/2;
    sy = (-1j*(Cup.getH()@Cdn - Cdn.getH()@Cup))/2;
    sz =      (Cup.getH()@Cup - Cdn.getH()@Cdn)/2;

    N = Cup.getH()*Cup + Cdn.getH()*Cdn;
    
    return sx, sy, sz, N
