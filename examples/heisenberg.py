import numpy as np
import sys
sys.path.append('../')
from scipy.sparse import coo_matrix
from construct_op import extend_to_n_sites
from construct_op import create_spin
from verifications import spin_check
from verifications import c_check
import scipy.sparse.linalg as ssl
import scipy.linalg as sl
from timeit import default_timer as timer

names=['Cup', 'Cdn']
N = 4
J = 0.1
Jz = 0.2

op = extend_to_n_sites(N=N)

sx0, sy0, sz0, N0 = create_spin(op, site = 0)
sx1, sy1, sz1, N1 = create_spin(op, site = 1)
sx2, sy2, sz2, N2 = create_spin(op, site = 2)
sx3, sy3, sz3, N3 = create_spin(op, site = 3)
spin_check(sx1, sy1, sz1)
spin_check(sx2, sy2, sz2)
spin_check(sx3, sy3, sz3)
spin_check(sx0, sy0, sz0)
c_check(op, site = 3)

#constrcuting heisenberg hamiltonian
H = coo_matrix((4**N, 4**N), dtype=np.float64)

H = H + J   * (sx0 @ sx1 + sx1 @ sx2 + sx2 @ sx3 + sx3 @ sx0) 
H = H + J   * (sy0 @ sy1 + sy1 @ sy2 + sy2 @ sy3 + sy3 @ sy0) 
H = H + Jz*(sz0@sz1 + sz1@sz2 + sz2@sz3 + sz3@sz0) 


ed, ev = sl.eigh(H.toarray())
#ed, ev = ssl.eigsh(H.asfptype(), k = 3, which='SA')
print('Energy values: ',ed[:10])
i = 0
print('Energy   sz0 sz1 sz2 sz3')
print('{0: 1.2f} {1: 1.2f} {2: 1.2f} {3: 1.2f} {4: 1.2f}'.format(
            ev[:,i] @ H @ ev[:,i],
            ev[:,i] @ sz0 @ ev[:,i],
            ev[:,i] @ sz1 @ ev[:,i],
            ev[:,i] @ sz2 @ ev[:,i],
            ev[:,i] @ sz3 @ ev[:,i]))

'''

ed, ev = ssl.eigsh(H.asfptype(), k = 3, which='SA')
print(ed[:3])
print('E:   {:1.3f}'.format(ev[:,0].conj() @ H @ ev[:,0]))
print('Sz1: {:1.3f}'.format(ev[:,0].conj() @ sz0 @ ev[:,0]))
print('Sz2: {:1.3f}'.format(ev[:,0].conj() @ sz1 @ ev[:,0]))
print('Sz3: {:1.3f}'.format(ev[:,0].conj() @ sz2 @ ev[:,0]))
print('Sz4: {:1.3f}'.format(ev[:,0].conj() @ sz3 @ ev[:,0]))

'''
'''
H = coo_matrix((4**N, 4**N), dtype=np.float64)
for nm in names:
    for i in np.arange(N):
        #print(i%N, (i+1)%N)
        H = H + t * (np.dot(op[nm+str(i%N)].getH(), op[nm+str((i+1)%N)]))
H = H + H.getH()


ed, ev = ssl.eigsh(H.asfptype(), k = 10, which='SA')
print(H.toarray().shape)
print(ed)
'''
