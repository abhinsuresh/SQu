import numpy as np
from scipy.sparse import coo_matrix
from construct_op import extend_to_n_sites
from construct_op import create_spin
from verifications import spin_check
from verifications import c_check
from create_H import tj_ham
from scipy.sparse import eye
import scipy.sparse.linalg as ssl
import scipy.linalg as sl
from timeit import default_timer as timer

names=['Cup', 'Cdn']
N = 4
J = 0.0
Jz = 0.2
t = 1.0
U = 8.0
mu = 9.5
Ue = 6.0
Jh = 1.0

op = extend_to_n_sites(N=N)

#constructing the hamiltonian
H, sz0, sz1, sz2, sz3 =  tj_ham(op, N, J, Jz, t, U, mu, Ue, Jh,
                                return_sz = True)


ed, ev = sl.eigh(H.toarray())
#ed, ev = ssl.eigs(H.toarray(), k = 3, which='SR')
print('Energy values: {:1.3f} {:1.3f} {:1.3f}'.format(*ed[:3]))
#print('Energy values: {:1.3f} {:1.3f} {:1.3f} {:1.3f} {:1.3f}'.format(*ed[:2]))
i = 0
print('Energy   sz0 sz1 sz2 sz3')
print('{0: 1.2f} {1: 1.2f} {2: 1.2f} {3: 1.2f} {4: 1.2f}'.format(
            ev[:,i].conj() @ H @ ev[:,i],
            ev[:,i].conj() @ sz0 @ ev[:,i],
            ev[:,i].conj() @ sz1 @ ev[:,i],
            ev[:,i].conj() @ sz2 @ ev[:,i],
            ev[:,i].conj() @ sz3 @ ev[:,i]))

