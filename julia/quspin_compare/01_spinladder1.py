import numpy as np # general math functions
import time
from quspin.operators import hamiltonian # operators
from quspin.basis import spin_basis_general # spin basis constructor

no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)

def spin_op(basis, site=0, s='z'):
    sigma = [[s, [[1.0, site]]]]
    return hamiltonian(sigma,[],dtype=np.float64,basis=basis,**no_checks)
#parameters
Lx,Ly = 6,2
Jxy=0.1
Jzz=0.5
Bz = 0.01

#deriverd parameters
N2d = Lx*Ly
s = np.arange(N2d)
x = s%Lx 
y = s//Lx 

subsystem=[0, 1, 4, 5]

#setting the basis
basis = spin_basis_general(N2d, pauli=0)
print('size:', basis.Ns)

#measurables
sz0 = spin_op(basis,0,'z')
    
J_xy=[[Jxy,i,i+1] for i in range(Lx-1)] + \
     [[Jxy,i+Lx,i+Lx+1] for i in range(Lx-1)] + \
     [[Jxy,i,i+Lx] for i in range(Lx)] 
J_zz=[[Jzz,i,i+1] for i in range(Lx-1)] + \
     [[Jzz,i+Lx,i+Lx+1] for i in range(Lx-1)] + \
     [[Jzz,i,i+Lx] for i in range(Lx)] 

static=[["xx",J_xy],["yy",J_xy],["zz",J_zz],['z', [[-Bz, 0]]]]

H=hamiltonian(static,[],dtype=np.float64,basis=basis,**no_checks)
print(subsystem)
E,V= H.eigh()
print('Ei using eigh: ',E[0])
print('Half system entropy: ', basis.ent_entropy(V[:,0], sub_sys_A=subsystem)['Sent_A'])
print('Spin-z at site 0: ', spin_op(basis,0,'z').expt_value(V[:,0]))
print('Spin-z at site 1,4: ', spin_op(basis,4,'z').expt_value(V[:,0]))
