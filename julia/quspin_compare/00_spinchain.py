import numpy as np # general math functions
import time
from quspin.operators import hamiltonian # operators
from quspin.basis import spin_basis_general # spin basis constructor

no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)

def spin_op(basis, site=0, s='z'):
    sigma = [[s, [[1.0, site]]]]
    return hamiltonian(sigma,[],dtype=np.float64,basis=basis,**no_checks)
#parameters
Lx = 10
Jxy= 1.0
Jzz= 2.0
Bz = 0.01

#deriverd parameters

subsystem=[0,1,2,3,4]

#setting the basis
basis = spin_basis_general(Lx, pauli=0)
print('size:', basis.Ns)

#measurables
sz0 = spin_op(basis,0,'z')
    
J_xy=[[Jxy,i,i+1] for i in range(Lx-1)]
J_zz=[[Jzz,i,i+1] for i in range(Lx-1)]

static=[["xx",J_xy],["yy",J_xy],["zz",J_zz],['z', [[-Bz, 0]]]]

H=hamiltonian(static,[],dtype=np.float64,basis=basis,**no_checks)
print(subsystem)
E,V= H.eigh()
print('Ei using eigh: ',E[0])
print('Spin-z at site 0: ', spin_op(basis,0,'z').expt_value(V[:,0]))
print('Spin-z at site 1: ', spin_op(basis,1,'z').expt_value(V[:,0]))
print('Spin-z at site 2 ', spin_op(basis,2,'z').expt_value(V[:,0]))
print('Spin-z at site 3: ', spin_op(basis,3,'z').expt_value(V[:,0]))
print('Half system entropy: ', basis.ent_entropy(V[:,0], sub_sys_A=subsystem, density=False)['Sent_A'])
