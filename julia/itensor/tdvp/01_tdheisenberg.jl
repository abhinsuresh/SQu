using ITensors, Random, ITensorTDVP, HDF5
using DifferentialEquations
using LinearAlgebra
include("measure.jl")
include("solvers.jl")
include("observers.jl")
Random.seed!(1234)

function H_build(J, Jz, Bz, Bx, By)
    os = OpSum()
    for j in 1:(N - 1)
        os += Jz, "Sz", j, "Sz", j + 1
        os += J, 0.5, "S+", j, "S-", j + 1
        os += J, 0.5, "S-", j, "S+", j + 1
    end
    os += Bz, "Sz", 1
    os += Bx, "Sx", 1
    os += By, "Sy", 1
    return os
end

outputlevel = 1
N  = 8
J  = 0.1
Jz = 0.2
Bz = 0.01
Bx = 0.2
dt = 0.1
Ω = 0.05/0.658211

sites = siteinds("S=1/2", N)
f = [t -> 1, t -> sin(Ω * t)]

H1 = H_build(J, Jz, Bz, 0, 0)
H2 = H_build(0, 0, 0, Bx, 0)
Hv = [H1, H2]
H = [MPO(H0, sites) for H0 in Hv]


Hini = MPO(H_build(J, Jz, Bz, 0, 0), sites)
Sy = MPO(H_build(0,0,0,0,1), sites)
Sz = MPO(H_build(0,0,1,0,0), sites)
ψ₀ = randomMPS(sites, 10)

nsweeps = 10
maxdim = [10, 20, 100, 100, 200]
cutoff = [1E-11]
tsteps = 1000
#sweeps = Sweeps(nsweeps)
#maxdim!(sweeps,10,20,40,60,100)
#cutoff!(sweeps,1E-11)

energy, ψ = dmrg(Hini, ψ₀; nsweeps, maxdim, mindim=10, cutoff)
println("Final energy = ", energy)
println("spin expecvalues: ",expect(ψ,"Sz")[1:5])
println("energy", dot(ψ', Hini, ψ))
println("Sz", dot(ψ', Sz, ψ))
println("Sy", dot(ψ', Sy, ψ))
#println("spin expecvalues: ",expect(psi,"Sy")[1:5])
SvN = Measure.ent(ψ, 4)
println("entropy = ", SvN)
spinz = zeros(tsteps,N)
spinx = zeros(tsteps,N)
spiny = zeros(tsteps,N)
psi_t = complex.(ψ)
#Ht = MPO(H_build(J, Jz, Bz, Bx, 0), sites)

time_step = 0.1
time_stop = 1
maxdim = 100
mindim = 4
cutoff = 1e-8
nsite = 2
tol = 1e-15
ode_alg = Tsit5()
ode_kwargs = (; reltol=tol, abstol=tol)

function ode_solver(H, time_step, psi_t; kwargs...)
  return ode_solver(
    -im * TimeDependentSum(f, H),
    time_step,
    psi_t;
    solver_alg=ode_alg,
    ode_kwargs...,
    kwargs...,
  )
end
println(expect(psi_t, "Sx")[1])
println(expect(psi_t, "Sz")[1])
println(real(dot(psi_t', Sy, psi_t)))

obs = Observer(
  "steps" => step, "times" => current_time, "psis" => return_state, 
  "Sz" => measure_sz, "Sy" => measure_sy, "Sx" => measure_sx,
)

psi_f = tdvp(ode_solver, H, time_stop, psi_t; 
            time_step, maxdim, mindim, cutoff, nsite, outputlevel,
            (observer!)=obs)

res = results(obs)
spinx = res["Sx"]
spiny = res["Sy"]
spinz = res["Sz"]
println(size(spinz))
f = h5open("results/01.hdf5", "w")
    write(f, "spinx", spinx)
    write(f, "spiny", spiny)
    write(f, "spinz", spinz)
close(f)
#psi_t = tdvp(ode_solver, H, time_stop, psi_t; time_step, maxdim, mindim, cutoff, nsite, outputlevel)

#println(expect(psi_t, "Sx")[1])
#println(expect(psi_t, "Sz")[1])
#println(real(dot(psi_t', Sy, psi_t)))

"""
for i in 1:tst
    #observables
    global spinx[i,:] = expect(psi_t, "Sx")
    global spinz[i,:] = expect(psi_t, "Sz")
    global spiny[i,1] = real(dot(psi_t', Sy, psi_t))
    #Ht = MPO(H_build(J, Jz, Bz, sin(Ω * (tsteps-1)/10)*Bx, 0), sites)
    #evolution by one step
    #global psi_t = tdvp(Ht, -im*0.1, psi_t; time_step=-im*0.1,
    #             cutoff=1e-6, maxdim=100, mindim=10, outputlevel=1, normalize=true)
    tdvp(ode_solver, H, time_stop, ψ₀; time_step, maxdim, cutoff, nsite, outputlevel)
end


#println(spinz[:,1])
#println(spinx[:,1])
f = h5open("results/td_evolve.hdf5", "w")
    write(f, "spinx", spinx)
    write(f, "spiny", spiny)
    write(f, "spinz", spinz)
close(f)
"""
