using ITensors, Random, ITensorTDVP, HDF5, Observers
include("measure.jl")
include("observers.jl")
Random.seed!(1234)

function H_build(J, Jz, Bz, Bx, By)
    os = OpSum()
    for j in 1:(N - 1)
        os += Jz, "Sz", j, "Sz", j + 1
        os += J, 0.5, "S+", j, "S-", j + 1
        os += J, 0.5, "S-", j, "S+", j + 1
    end
    os += Bz, "Sz", 1
    os += Bx, "Sx", 1
    os += By, "Sy", 1
    return os
end
    
N  = 8
J  = -0.1
Jz = -0.2
Bz = 0.01
Bx = 0.4
dt = 0.1

sites = siteinds("S=1/2", N)
H = MPO(H_build(J, Jz, Bz, 0, 0), sites)
Sy = MPO(H_build(0,0,0,0,1), sites)
Sz = MPO(H_build(0,0,1,0,0), sites)
psi0 = randomMPS(sites, 10)

nsweeps = 10
maxdim = [10, 20, 100, 100, 200]
cutoff = [1E-11]
tsteps = 1000
#sweeps = Sweeps(nsweeps)
#maxdim!(sweeps,10,20,40,60,100)
#cutoff!(sweeps,1E-11)

energy, psi = dmrg(H, psi0; nsweeps, maxdim, cutoff)
println("Final energy = ", energy)
println("spin expecvalues: ",expect(psi,"Sz")[1:5])
println("energy", dot(psi', H, psi))
println("Sz", dot(psi', Sz, psi))
println("Sy", dot(psi', Sy, psi))
#println("spin expecvalues: ",expect(psi,"Sy")[1:5])
SvN = Measure.ent(psi, 4)
println("entropy = ", SvN)
spinz = zeros(tsteps,N)
spinx = zeros(tsteps,N)
spiny = zeros(tsteps,N)

psi_t = psi
Ht = MPO(H_build(J, Jz, Bz, Bx, 0), sites)

psi_f = tdvp(Ht, -im*100, psi_t; time_step=-im*0.1,
  cutoff=1e-12, maxdim=100, mindim=10, outputlevel=1,
  normalize=true, (observer!)=obs,)

res = results(obs)
steps = res["steps"]
times = res["times"]
psis = res["psis"]
spinx = res["Sx"]
spiny = res["Sy"]
spinz = res["Sz"]

f = h5open("results/06.hdf5", "w")
    write(f, "spinx", spinx)
    write(f, "spiny", spiny)
    write(f, "spinz", spinz)
close(f)
