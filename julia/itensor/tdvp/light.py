import sys, numpy as np, h5py, os

sys.path.append('../../../plot_manager')
from plot2d import plotfig

data = dict();values=dict()
trange = np.arange(0,100,0.05)

t0 = 50
s = 10
zmax = 0.12
w = 1.54/0.6582119


E = zmax * np.cos(w * trange) * np.exp(-(trange-t0)**2 /(2*s*s))

result = np.zeros((len(trange),1))

result[:,0] = E
#result[:,0] = h5py.File("gg.hdf5")["light"][:-1]
result[:,0] = h5py.File("gg_hphi.hdf5")["A"][:]

data['x_plot'] = trange; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time}$';
values['ylabel1'] = r'$\mathrm{light~pulse}$'
Cl = ['k','C3']
Gs = dict()

sf = np.array([[0,1]])
plotfig(data, 'light', values, Cl=Cl, lw=0.4, sf=sf, minor=1, fs=6)
