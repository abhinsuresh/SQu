import sys, numpy as np, h5py, os

sys.path.append('../../../plot_manager')
from plot2d import plotfig

data = dict();values=dict()

Ns = 8
trange = np.arange(0,100,0.1)
File = "01.hdf5"
spinx = h5py.File("results/"+File,"r")["spinx"][:].transpose()
spiny = h5py.File("results/"+File,"r")["spiny"][:].transpose()
spinz = h5py.File("results/"+File,"r")["spinz"][:].transpose()
print(spinz.shape)
result = np.zeros((len(trange),3))
result[:,0] = spinx[:]
result[:,1] = spiny[:]
result[:,2] = spinz[:]

data['x_plot'] = trange; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time}$';
values['ylabel1'] = r'$\mathrm{S^\alpha_1}$'
Cl = ['C0','C2','C3']
Gs = dict()

sf = np.array([[0,3]])
plotfig(data, 'dmrg_01', values, Cl=Cl, lw=0.4, sf=sf, minor=1, fs=6)
