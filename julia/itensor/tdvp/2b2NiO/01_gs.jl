using ITensors, MKL, HDF5, ITensorTDVP
include("wrapper.jl")

#constants of this code
Ny = 4
# Parameters of the Hamiltonian
Nx  = 2
t   = 1
U   = 8
Jex = 0.1
Jz  = Jex*10
Up  = 6
JH  = 1
gamma = 1
Bz  = 0.1
N   = Nx*Ny
A   = 0
#Definition of the site type
sites = siteinds("Electron", N; conserve_qns=true, conserve_sz=true)
# Define the Hund Hamiltonian on this lattice
H = MPO(NiO.H_build(Nx,t,U,Jex,Jz,Up,JH,gamma, Bz, A),sites)
println("contructed the Hamiltonian")
# Initialize the wavefunction as random MPS with same number of electons
state = NiO.nstate(N, Nx)
psi0 = randomMPS(sites,state,10)
println("contructed the random MPS")
# DMRG parameters maxdim has to be increased gradually
# noise has to be decreased gradually
# cutoff has to be increased gradually
sweeps = Sweeps(30)
maxdim!(sweeps,100,200,200,400,400,800, 800, 1200,1200,1200,1600,1600, 1600, 2000, 2000)
noise!(sweeps,1E-5,1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-8, 1E-8, 1E-9, 1E-11, 1E-12, 0)
cutoff!(sweeps,1E-9)
#@show sweeps

#DMRG calculation
println("time to do dmrg")
@time energy,psi = dmrg(H,psi0,sweeps)
spinz = expect(psi,"Sz")
println("spin value at site 1: ", spinz[1])
println("spin value at site 2: ", spinz[2])
println("spin value at site 3: ", spinz[3])
println("spin value at site 4: ", spinz[4])
f = h5open("gsdmrg.h5","w")
write(f, "psi", psi)
close(f)
