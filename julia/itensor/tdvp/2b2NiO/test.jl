tsteps = 100
N = 10
spinz = zeros(tsteps,N)
spinx = zeros(tsteps,N)
psi = 0
for ind in 1:tsteps
    #observables
    spinz[ind,:] = psi*ones(N)
    spinx[ind,:] = ones(N)
    global psi = psi + 5
end
println(spinz[1,1])
println(spinz[100,1])
println(psi)

using HDF5
for i = 1:10
    f = h5open("gg.h5","cw")
    write(f,"ind_"*string(i), i)
    close(f)
end
