#set of observer functions
using ITensors, ITensorTDVP, Observers

function step(; sweep, bond, half_sweep)
  if bond == 1 && half_sweep == 2
    return sweep
  end
  return nothing
end

function current_time(; current_time, bond, half_sweep)
  if bond == 1 && half_sweep == 2
    return current_time
  end
  return nothing
end

function measure_sz(; psi, bond, half_sweep)
  if bond == 1 && half_sweep == 2
    return expect(psi, "Sz"; sites=1)
    #return expect(psi, "Sz")
  end
  return nothing
end

function return_state(; psi, bond, half_sweep)
  if bond == 1 && half_sweep == 2
    return psi
  end
  return nothing
end

