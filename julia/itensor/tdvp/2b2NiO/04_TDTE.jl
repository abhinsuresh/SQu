using ITensors, HDF5, ITensorTDVP
using DifferentialEquations
using LinearAlgebra
include("td_wrapper.jl")
include("observers.jl")

#light pulse constants and vector potential
ħ  = 0.6582119
ω  = 1.54/ħ
dt = 0.1
tf = 100
σ  = 10
t₀ = 50
z₀ = 0.12
tspan = range(0,tf, step=dt)
At = z₀ * cos.(ω * tspan ) .* exp.( -1*((tspan .- t₀).^2)/(2*σ^2) )
println("made light file")
#constants of this code
Ny = 4
# Parameters of the Hamiltonian
Nx  = 2
t   = 1
U   = 8
Jex = 0.1
Jz  = 0.1*10
Up  = 6
JH  = 1
gamma = 1
Bz  = 0.1
N   = Nx*Ny
A   = 0
#---------------------------------------
sites = siteinds("Electron", N; conserve_qns=true, conserve_sz=true)
H0 = MPO(NiO.H_fixed(Nx,t,U,Jex,Jz,Up,JH,gamma, Bz),sites)
println("made H0")
Hx1 = MPO(NiO.H_x1(Nx,t),sites)
Hx2 = MPO(NiO.H_x2(Nx,t),sites)
println("made Hx")
Hini = Hx1 + Hx2 + H0
println("added Hx")
#Ht = MPO(NiO.H_build(Nx,t,U,Jex,Jz,Up,JH,gamma, Bz, A),sites)

state = NiO.nstate(N, Nx)
psi0 = randomMPS(sites,state,10)
sweeps = Sweeps(30)
maxdim!(sweeps,100,200,200,400,400,800, 800, 1200,1200,1200,1600,1600, 1600, 2000, 2000)
noise!(sweeps,1E-5,1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-8, 1E-8, 1E-9, 1E-11, 1E-12, 0)
cutoff!(sweeps,1E-9)
#DMRG calculation
println("time to do dmrg")
@time energy,psi = dmrg(Hini,psi0,sweeps)

@time spinz = expect(psi,"Sz")
println("spin value at site 1: ", spinz[1])
println("spin value at site 2: ", spinz[2])
println("spin value at site 3: ", spinz[3])
println("spin value at site 4: ", spinz[4])

f = [t -> 1, t -> exp( im * z₀*cos(ω*t)*exp(-1*((t-t₀)^2)/(2*σ^2))), 
             t -> exp(-im * z₀*cos(ω*t)*exp(-1*((t-t₀)^2)/(2*σ^2)))]
H = [H0, Hx1, Hx2]
flush(stdout)
time_step = 0.1
time_stop = 100
maxdim = 100
mindim = 4
cutoff = 1e-8
nsite = 2
tol = 1e-15
ode_alg = Tsit5()
outputlevel = 1
ode_kwargs = (; reltol=tol, abstol=tol)

function ode_solver(H, time_step, psi; kwargs...)
  return ode_solver(
    -im * TimeDependentSum(f, H),
    time_step,
    psi;
    solver_alg=ode_alg,
    ode_kwargs...,
    kwargs...,
  )
end

obs = Observer(
  "steps" => step, "times" => current_time, "psis" => return_state, 
  "Sz" => measure_sz,
)

psi_f = tdvp(ode_solver, H, time_stop, psi; 
            time_step, maxdim, mindim, cutoff, nsite, outputlevel,
            (observer!)=obs)

res = results(obs)
spinz = res["Sz"]
println(size(spinz))
f = h5open("results/05.h5", "w")
    write(f, "spinz", spinz)
close(f)
