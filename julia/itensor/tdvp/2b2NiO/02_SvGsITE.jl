using ITensors, MKL, HDF5, ITensorTDVP
include("wrapper.jl")

#light pulse constants and vector potential
ħ  = 0.6582119569
ω  = 1.54/ħ
dt = 0.1
tf = 100
σ  = 10
t₀ = 50
z₀ = 0.12
tspan = range(0,tf, step=dt)
At = z₀ * cos.(ω * tspan ) .* exp.( -1*((tspan .- t₀).^2)/(2*σ^2) )
println("made light file")
#constants of this code
Ny = 4
# Parameters of the Hamiltonian
Nx  = 2
t   = 1
U   = 8
Jex = 0.1
Jz  = 0.2
Up  = 6
JH  = 1
gamma = 1
Bz  = 0.1
N   = Nx*Ny
A   = 0

println("reading h5 file")
fo = h5open("gsdmrg.h5","r")
psi = read(fo, "psi", MPS)
close(fo)
println("read h5 file")

sites = siteinds("Electron", N; conserve_qns=true, conserve_sz=true)
Ht = MPO(NiO.H_build(Nx,t,U,Jex,Jz,Up,JH,gamma, Bz, At[10]),sites)
#psit = tdvp(Ht, -im*0.1, psi; time_step=-im*0.1,
#            cutoff=1e-6, maxdim=500, outputlevel=1, normalize=true)
"""
@time spinz = expect(psi,"Sz")
println("spin value at site 1: ", spinz[1])
println("spin value at site 2: ", spinz[2])
println("spin value at site 3: ", spinz[3])
println("spin value at site 4: ", spinz[4])


sites = siteinds("Electron", N; conserve_qns=true, conserve_sz=true)

println("start time evolution")
tsteps = 1000
spinz = zeros(tsteps, N)
for ind in 1:tsteps
    println("current step ", ind)
    Ht = MPO(NiO.H_build(Nx,t,U,Jex,Jz,Up,JH,gamma, Bz, At[ind]),sites)
    #observables
    spinz[ind,:] = expect(psi, "Sz")
    println("spin values", spinz[ind,:])
    #evolution by one step
    global psi = tdvp(Ht, -im*0.1, psi; time_step=-im*0.1,
                 cutoff=1e-6, maxdim=500, outputlevel=1, normalize=true)
    fs = h5open("states/02_states.h5","cw")
        write(fs, "psit_"*string(ind), psi)
    close(fs)
end

f = h5open("results/02spin.h5","w")
    write(f, "spinz", spinz)
close(f)
"""
