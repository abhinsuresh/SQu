module NiO
    using ITensors
    function H_x1(Nx, t)
      os = OpSum()
      for n in 1:(Nx-1)
        # x-Hopping L = 1, α = 1 [1,5,9,13]
        os += -t,  "Cdagup", 4*n-3, "Cup", 4*n+1
        os += -t,  "Cdagdn", 4*n-3, "Cdn", 4*n+1
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 2, α = 1 [2,6,10,14]
        os += -t, "Cdagup", 4*n-2, "Cup", 4*n+2
        os += -t, "Cdagdn", 4*n-2, "Cdn", 4*n+2
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 1, α = 2 [3,7,11,15]
        os += -t, "Cdagup", 4*n-1, "Cup", 4*n+3
        os += -t, "Cdagdn", 4*n-1, "Cdn", 4*n+3
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 2, α = 2 [4,8,12,16]
        os += -t,  "Cdagup", 4*n,   "Cup", 4*n+4
        os += -t,  "Cdagdn", 4*n,   "Cdn", 4*n+4
      end
      return os
    end

    function H_x2(Nx, t)
      os = OpSum()
      for n in 1:(Nx-1)
        # x-Hopping L = 1, α = 1 [1,5,9,13]
        os += -t,  "Cdagup", 4*n+1, "Cup", 4*n-3
        os += -t,  "Cdagdn", 4*n+1, "Cdn", 4*n-3
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 2, α = 1 [2,6,10,14]
        os += -t, "Cdagup", 4*n+2, "Cup", 4*n-2
        os += -t, "Cdagdn", 4*n+2, "Cdn", 4*n-2
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 1, α = 2 [3,7,11,15]
        os += -t, "Cdagup", 4*n+3, "Cup", 4*n-1
        os += -t, "Cdagdn", 4*n+3, "Cdn", 4*n-1
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 2, α = 2 [4,8,12,16]
        os += -t,  "Cdagup", 4*n+4,   "Cup", 4*n
        os += -t,  "Cdagdn", 4*n+4,   "Cdn", 4*n
      end
      return os
    end
    
    function H_fixed(Nx, t, U, Jex, Jz, Up, JH, gamma, Bz)
      os = OpSum()
      for n in 1:(Nx-1)
        # x-Hopping L = 1, α = 1 [1,5,9,13]
        os += Jz, "Sz", 4*n-3, "Sz", 4*n+1
        os += 0.5*Jex, "S-", 4*n-3, "S+", 4*n+1
        os += 0.5*Jex, "S+", 4*n-3, "S-", 4*n+1
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 2, α = 1 [2,6,10,14]
        os += Jz, "Sz", 4*n-2, "Sz", 4*n+2
        os += 0.5*Jex, "S-", 4*n-2, "S+", 4*n+2
        os += 0.5*Jex, "S+", 4*n-2, "S-", 4*n+2
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 1, α = 2 [3,7,11,15]
        os += Jz, "Sz", 4*n-1, "Sz", 4*n+3
        os += 0.5*Jex, "S-", 4*n-1, "S+", 4*n+3
        os += 0.5*Jex, "S+", 4*n-1, "S-", 4*n+3
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 2, α = 2 [4,8,12,16]
        os += Jz, "Sz", 4*n, "Sz", 4*n+4
        os += 0.5*Jex, "S-", 4*n, "S+", 4*n+4
        os += 0.5*Jex, "S+", 4*n, "S-", 4*n+4
      end
      for n in 1:Nx
        # y-Hopping α = 1
        os += -t, "Cdagup", 4*n-3, "Cup", 4*n-2
        os += -t, "Cdagup", 4*n-2, "Cup", 4*n-3
        os += -t, "Cdagdn", 4*n-3, "Cdn", 4*n-2
        os += -t, "Cdagdn", 4*n-2, "Cdn", 4*n-3
        os += Jz, "Sz", 4*n-3, "Sz", 4*n-2
        os += 0.5*Jex, "S-", 4*n-3, "S+", 4*n-2
        os += 0.5*Jex, "S+", 4*n-3, "S-", 4*n-2
      end
      for n in 1:Nx
        # y-Hopping α = 2
        os += -t, "Cdagup", 4*n-1, "Cup", 4*n
        os += -t, "Cdagup", 4*n, "Cup", 4*n-1
        os += -t, "Cdagdn", 4*n-1, "Cdn", 4*n
        os += -t, "Cdagdn", 4*n, "Cdn", 4*n-1
        os += Jz, "Sz", 4*n-1, "Sz", 4*n
        os += 0.5*Jex, "S-", 4*n-1, "S+", 4*n
        os += 0.5*Jex, "S+", 4*n-1, "S-", 4*n
      end
      # Hund terms L = 1
      for n in 1:Nx
        os += Up-JH, "Nup", 4*n-3, "Nup", 4*n-1
        os += Up-JH, "Ndn", 4*n-3, "Ndn", 4*n-1
        os += Up, "Nup", 4*n-3, "Ndn", 4*n-1
        os += Up, "Ndn", 4*n-3, "Nup", 4*n-1
        os += gamma*JH, "H1", 4*n-3, "H1dag", 4*n-1
        os += gamma*JH, "H1", 4*n-1, "H1dag", 4*n-3
        os += gamma*JH, "H2", 4*n-3, "H2dag", 4*n-1
        os += gamma*JH, "H2", 4*n-1, "H2dag", 4*n-3
      end
      # Hund terms L = 2
      for n in 1:Nx
        os += Up-JH, "Nup", 4*n-2, "Nup", 4*n
        os += Up-JH, "Ndn", 4*n-2, "Ndn", 4*n
        os += Up, "Nup", 4*n-2, "Ndn", 4*n
        os += Up, "Ndn", 4*n-2, "Nup", 4*n
        os += gamma*JH, "H1", 4*n-2, "H1dag", 4*n
        os += gamma*JH, "H1", 4*n, "H1dag", 4*n-2
        os += gamma*JH, "H2", 4*n-2, "H2dag", 4*n
        os += gamma*JH, "H2", 4*n, "H2dag", 4*n-2
      end
      # On-site repulsion
      for n in 1:Nx
        os += U, "Nupdn", 4*n-3
        os += U, "Nupdn", 4*n-2
        os += U, "Nupdn", 4*n-1
        os += U, "Nupdn", 4*n
      end 
      os += Bz, "Sz", 1
      return os
    end

    function nstate(N, Nx)
        state = ["Emp" for n in 1:N]
        for i in 1:Nx
          state[4*i-3] = (isodd(i) ? "Up" : "Dn")
          state[4*i-2] = (isodd(i) ? "Dn" : "Up")
          state[4*i-1] = (isodd(i) ? "Dn" : "Up")
          state[4*i]   = (isodd(i) ? "Dn" : "Up")
        end
        return state
    end

    function ent(psi::MPS, b::Int)
        s = siteinds(psi)
        orthogonalize!(psi, b)
        U,S,V = svd(psi[b], (linkind(psi, b-1), s[b]))
        SvN = 0.0
        for n in 1:dim(S, 1)
            p = S[n,n]^2
            #if p > 1E-12
            SvN -= p * log(p)
            #end
        end
      return SvN
    end
    function H_build(Nx, t, U, Jex, Jz, Up, JH, gamma, Bz, A)
      os = OpSum()
      fx = exp(im * A)
      for n in 1:(Nx-1)
        # x-Hopping L = 1, α = 1 [1,5,9,13]
        os += -t, fx,  "Cdagup", 4*n-3, "Cup", 4*n+1
        os += -t, fx', "Cdagup", 4*n+1, "Cup", 4*n-3
        os += -t, fx,  "Cdagdn", 4*n-3, "Cdn", 4*n+1
        os += -t, fx', "Cdagdn", 4*n+1, "Cdn", 4*n-3
        os += Jz, "Sz", 4*n-3, "Sz", 4*n+1
        os += 0.5*Jex, "S-", 4*n-3, "S+", 4*n+1
        os += 0.5*Jex, "S+", 4*n-3, "S-", 4*n+1
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 2, α = 1 [2,6,10,14]
        os += -t, fx,  "Cdagup", 4*n-2, "Cup", 4*n+2
        os += -t, fx', "Cdagup", 4*n+2, "Cup", 4*n-2
        os += -t, fx,  "Cdagdn", 4*n-2, "Cdn", 4*n+2
        os += -t, fx', "Cdagdn", 4*n+2, "Cdn", 4*n-2
        os += Jz, "Sz", 4*n-2, "Sz", 4*n+2
        os += 0.5*Jex, "S-", 4*n-2, "S+", 4*n+2
        os += 0.5*Jex, "S+", 4*n-2, "S-", 4*n+2
      end
      for n in 1:Nx
        # y-Hopping α = 1
        os += -t, "Cdagup", 4*n-3, "Cup", 4*n-2
        os += -t, "Cdagup", 4*n-2, "Cup", 4*n-3
        os += -t, "Cdagdn", 4*n-3, "Cdn", 4*n-2
        os += -t, "Cdagdn", 4*n-2, "Cdn", 4*n-3
        os += Jz, "Sz", 4*n-3, "Sz", 4*n-2
        os += 0.5*Jex, "S-", 4*n-3, "S+", 4*n-2
        os += 0.5*Jex, "S+", 4*n-3, "S-", 4*n-2
      end

      for n in 1:(Nx-1)
        # x-Hopping L = 1, α = 2 [3,7,11,15]
        os += -t, fx,  "Cdagup", 4*n-1, "Cup", 4*n+3
        os += -t, fx', "Cdagup", 4*n+3, "Cup", 4*n-1
        os += -t, fx,  "Cdagdn", 4*n-1, "Cdn", 4*n+3
        os += -t, fx', "Cdagdn", 4*n+3, "Cdn", 4*n-1
        os += Jz, "Sz", 4*n-1, "Sz", 4*n+3
        os += 0.5*Jex, "S-", 4*n-1, "S+", 4*n+3
        os += 0.5*Jex, "S+", 4*n-1, "S-", 4*n+3
      end
      for n in 1:(Nx-1)
        # x-Hopping L = 2, α = 2 [4,8,12,16]
        os += -t, fx,  "Cdagup", 4*n,   "Cup", 4*n+4
        os += -t, fx', "Cdagup", 4*n+4, "Cup", 4*n
        os += -t, fx,  "Cdagdn", 4*n,   "Cdn", 4*n+4
        os += -t, fx', "Cdagdn", 4*n+4, "Cdn", 4*n
        os += Jz, "Sz", 4*n, "Sz", 4*n+4
        os += 0.5*Jex, "S-", 4*n, "S+", 4*n+4
        os += 0.5*Jex, "S+", 4*n, "S-", 4*n+4
      end
      for n in 1:Nx
        # y-Hopping α = 2
        os += -t, "Cdagup", 4*n-1, "Cup", 4*n
        os += -t, "Cdagup", 4*n, "Cup", 4*n-1
        os += -t, "Cdagdn", 4*n-1, "Cdn", 4*n
        os += -t, "Cdagdn", 4*n, "Cdn", 4*n-1
        os += Jz, "Sz", 4*n-1, "Sz", 4*n
        os += 0.5*Jex, "S-", 4*n-1, "S+", 4*n
        os += 0.5*Jex, "S+", 4*n-1, "S-", 4*n
      end

      # Hund terms L = 1
      for n in 1:Nx
        os += Up-JH, "Nup", 4*n-3, "Nup", 4*n-1
        os += Up-JH, "Ndn", 4*n-3, "Ndn", 4*n-1
        os += Up, "Nup", 4*n-3, "Ndn", 4*n-1
        os += Up, "Ndn", 4*n-3, "Nup", 4*n-1
        os += gamma*JH, "H1", 4*n-3, "H1dag", 4*n-1
        os += gamma*JH, "H1", 4*n-1, "H1dag", 4*n-3
        os += gamma*JH, "H2", 4*n-3, "H2dag", 4*n-1
        os += gamma*JH, "H2", 4*n-1, "H2dag", 4*n-3
      end

      # Hund terms L = 2
      for n in 1:Nx
        os += Up-JH, "Nup", 4*n-2, "Nup", 4*n
        os += Up-JH, "Ndn", 4*n-2, "Ndn", 4*n
        os += Up, "Nup", 4*n-2, "Ndn", 4*n
        os += Up, "Ndn", 4*n-2, "Nup", 4*n
        os += gamma*JH, "H1", 4*n-2, "H1dag", 4*n
        os += gamma*JH, "H1", 4*n, "H1dag", 4*n-2
        os += gamma*JH, "H2", 4*n-2, "H2dag", 4*n
        os += gamma*JH, "H2", 4*n, "H2dag", 4*n-2
      end

      # On-site repulsion
      for n in 1:Nx
        os += U, "Nupdn", 4*n-3
        os += U, "Nupdn", 4*n-2
        os += U, "Nupdn", 4*n-1
        os += U, "Nupdn", 4*n
      end 
      os += Bz, "Sz", 1
      return os
    end
    
    # New operators needed for the Hund terms
    ITensors.op(::OpName"H1",::SiteType"Electron")=
      [
         0.0 0.0 0.0 0.0
         0.0 0.0 0.0 0.0
         0.0 0.0 0.0 0.0
         1.0 0.0 0.0 0.0
      ]

    ITensors.op(::OpName"H1dag",::SiteType"Electron")=
      [
         0.0 0.0 0.0 1.0
         0.0 0.0 0.0 0.0
         0.0 0.0 0.0 0.0
         0.0 0.0 0.0 0.0
      ]

    ITensors.op(::OpName"H2",::SiteType"Electron")=
      [
         0.0 0.0 0.0 0.0
         0.0 0.0 1.0 0.0
         0.0 0.0 0.0 0.0
         0.0 0.0 0.0 0.0
      ]

    ITensors.op(::OpName"H2dag",::SiteType"Electron")=
      [
         0.0 0.0 0.0 0.0
         0.0 0.0 0.0 0.0
         0.0 1.0 0.0 0.0
         0.0 0.0 0.0 0.0
      ]
end
