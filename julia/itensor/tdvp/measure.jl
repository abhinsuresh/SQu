module Measure
    using ITensors

    function ent(psi::MPS, b::Int)
        s = siteinds(psi)
        orthogonalize!(psi, b)
        U,S,V = svd(psi[b], (linkind(psi, b-1), s[b]))
        SvN = 0.0
        for n in 1:dim(S, 1)
            p = S[n,n]^2
            #if p > 1E-12
            SvN -= p * log(p)
            #end
        end
      return SvN
    end
    function entropyvonneumann(psi::MPS, N::Int)
      s = siteinds(psi)
      SvN = fill(0.0, N)
      for b in 2:N-1
        orthogonalize!(psi, b)
        U,S,V = svd(psi[b], (linkind(psi, b-1), s[b]))
        for n in 1:dim(S, 1)
          p = S[n,n]^2
          SvN[b] -= p * log(p)
        end
        println("$(SvN[b])")
      end
      return SvN
    end
end
