import sys, numpy as np, h5py, os

sys.path.append('../../../../plot_manager')
from plot2d import plotfig

data = dict();values=dict()

Ns = 8
trange = np.arange(0,100,0.1)

spinz = h5py.File("results/01.h5","r")["spinz"][:]
print(spinz.shape)
result = np.zeros((len(trange),2))
result[:,0] = spinz[:,0]
result[:,1] = spinz[:,1]

data['x_plot'] = trange; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time}$';
values['ylabel1'] = r'$\mathrm{S^\alpha_1}$'
Cl = ['k','k']
Gs = dict()

sf = np.array([[0,1]])
plotfig(data, '2b2jl', values, Cl=Cl, lw=0.4, sf=sf, minor=1, fs=6)
