using ITensors, MKL, HDF5, ITensorTDVP
include("wrapper.jl")

#light pulse constants and vector potential
ħ  = 0.6582119569
ω  = 1.54/ħ
dt = 0.1
tf = 100
σ  = 10
t₀ = 50
z₀ = 0.12
tspan = range(0,tf, step=dt)
At = z₀ * cos.(ω * tspan ) .* exp.( -1*((tspan .- t₀).^2)/(2*σ^2) )
println("made light file")
#constants of this code
Ny = 4
# Parameters of the Hamiltonian
Nx  = 2
t   = 1
U   = 8
Jex = 0.1
Jz  = Jex*10
Up  = 6
JH  = 1
gamma = 1
Bz  = 0.1
N   = Nx*Ny
A   = 0

sites = siteinds("Electron", N; conserve_qns=true, conserve_sz=true)
H = MPO(NiO.H_build(Nx,t,U,Jex,Jz,Up,JH,gamma, Bz, A),sites)
state = NiO.nstate(N, Nx)
psi0 = randomMPS(sites,state,10)
sweeps = Sweeps(30)
maxdim!(sweeps,100,200,200,400,400,800, 800, 1200,1200,1200,1600,1600, 1600, 2000, 2000)
noise!(sweeps,1E-5,1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-8, 1E-8, 1E-9, 1E-11, 1E-12, 0)
cutoff!(sweeps,1E-9)
#DMRG calculation
println("time to do dmrg")
@time energy,psi = dmrg(H,psi0,sweeps)

@time spinz = expect(psi,"Sz")
println("spin value at site 1: ", spinz[1])
println("spin value at site 2: ", spinz[2])
println("spin value at site 3: ", spinz[3])
println("spin value at site 4: ", spinz[4])

flush(stdout)

println("start time evolution")
tsteps = 1000
spinz = zeros(N, tsteps)
for ind in 1:tsteps
    #observables
    spinz[:,ind] = expect(psi,"Sz")
    #evolution by one step
    println("current step ", ind)
    Ht = MPO(NiO.H_build(Nx,t,U,Jex,Jz,Up,JH,gamma, Bz, At[ind]),sites)
    global psi = tdvp(Ht, -im*0.1, psi; time_step=-im*0.1,
                 cutoff=1e-6, maxdim=500, outputlevel=1, normalize=true)
    flush(stdout)
end
fs = h5open("results/01.h5","w")
    write(fs, "spinz", spinz)
close(fs)
