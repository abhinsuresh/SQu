using DifferentialEquations
using ITensors
using ITensorTDVP
using KrylovKit
using LinearAlgebra
using Random

Random.seed!(1234)
include("models.jl")
include("solvers.jl")

n = 8
outputlevel = 1

J₁ = 1.0
J₂ = 1.0

N  = 8
J  = -0.1
Jz = -0.2
Bz = 0.01
Bx = 0.4
dt = 0.1
Ω = 0.05/0.6582119

time_step = 0.1
time_stop = 1.0

# nsite-update TDVP
nsite = 2

start_linkdim = 4

# TDVP truncation parameters
maxdim = 100
cutoff = 1e-8
tol = 1e-15

# ODE solver parameters
ode_alg = Tsit5()
ode_kwargs = (; reltol=tol, abstol=tol)

#ω1 = 0.1
#fv = [t -> 1, t -> cos(ω1 * t)]
#s = siteinds("S=1/2", n)
#H1 = heisenberg(n; J=J₁, J2=0.0)
#H2 = heisenberg(n; J=0.0, J2=J₂)
#Hv = [H1, H2]
#Hm = [MPO(Hi, s) for Hi in Hv]


fv = [t -> 1, t -> cos(Ω * t)]
sites = siteinds("S=1/2", N)
H1 = H_build(J, Jz, Bz, 0, 0)
H2 = H_build(0, 0, 0, Bx, 0)
Hv = [H1, H1]
Hm = [MPO(Hi, sites) for Hi in Hv]

#ψ1 = complex.(randomMPS(sites, j -> isodd(j) ? "↑" : "↓"; linkdims=start_linkdim))
#println("inner", dot(ψ1', 1, ψ1))

Hini = MPO(H_build(J, Jz, Bz, 0, 0), sites)
Sy = MPO(H_build(0,0,0,0,1), sites)
Sz = MPO(H_build(0,0,1,0,0), sites)
ψ₀ = randomMPS(sites, 10)
nsweeps = 10
maxdim = [10, 20, 100, 100, 200]
cutoff = [1E-11]
#sweeps = Sweeps(nsweeps)
#maxdim!(sweeps,10,20,40,60,100)
#cutoff!(sweeps,1E-11)

energy, ψ = dmrg(Hini, ψ₀; nsweeps, maxdim, cutoff)
println("Final energy = ", energy)
println("spin expecvalues: ",expect(ψ,"Sz")[1:5])
println("energy", dot(ψ', Hini, ψ))
println("Sz", dot(ψ', Sz, ψ))
println("Sy", dot(ψ', Sy, ψ))
#@show ψ1
#@show ψ
#println("inner", dot(ψ1',Hini, ψ))

println("Running TDVP with ODE solver")

ψt = complex.(ψ)

function ode_solver(Hm, time_step, ψt; kwargs...)
  return ode_solver(
    -im * TimeDependentSum(fv,Hm),
    time_step,
    ψt;
    solver_alg=ode_alg,
    ode_kwargs...,
    kwargs...,
  )
end

maxdim = 100
cutoff = 1e-8
ψ_ode = tdvp(ode_solver, Hm, time_stop, ψt; time_step, maxdim, cutoff, nsite, outputlevel)

println("Finished running TDVP with ODE solver")
