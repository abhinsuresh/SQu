import sys, numpy as np, h5py, os

sys.path.append('../../../plot_manager')
from plot2d import plotfig_2p

data = dict();values=dict()

Ns = 16
J  = 0.1
#file1

f = h5py.File(sys.argv[1],"r")
print("Final energy: ", f["energy"].value)
print("spin at site 1: ", f["spinz"][0])
print("spin at site 2: ", f["spinz"][1])
print("spin at site 3: ", f["spinz"][2])
print("spin at site 4: ", f["spinz"][3])
print("entropy: ", f["ent"].value)

