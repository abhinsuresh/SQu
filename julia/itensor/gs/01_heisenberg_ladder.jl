using ITensors
include("measure.jl")

function H_build(Jz, J, Bz, lattice)
    os = OpSum()
    for b in lattice
        os += Jz, "Sz", b.s1, "Sz", b.s2
        os += 0.5, J, "S+", b.s2, "S-", b.s1
        os += 0.5, J, "S-", b.s2, "S+", b.s1
        #println(b.s1," ",b.x1,":",b.y1, "\t",b.s2," ",b.x2,":",b.y2)
    end
    os += Bz, "Sz", 1
    return os
end

Nx = 4
Ny = 2
J = 0.1
Jz = 0.11
Bz = 0.001
N = Nx * Ny
# Create N spin-one degrees of freedom
sites = siteinds("S=1/2", N)
lattice = square_lattice(Nx, Ny; yperiodic = false)
os = H_build(Jz, J, Bz, lattice)
H = MPO(os, sites)
psi0 = randomMPS(sites, 10)
sweeps = Sweeps(10)
maxdim!(sweeps,100,200,200,400,400)
#noise!(sweeps,1E-3,1E-3,1E-3,1E-3, 1E-4, 1E-4, 1E-4, 1E-5, 1E-5, 1E-5, 1E-5, 1E-5, 1E-6, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-7, 1E-8, 1E-8, 1E-8, 1E-9, 1E-9, 1E-10, 1E-10, 1E-11, 1E-11, 0)
noise!(sweeps, 1E-8, 1E-8, 1E-8, 1E-9, 1E-9, 1E-10, 1E-10, 1E-11, 1E-11, 0)
cutoff!(sweeps,1E-8)
energy, psi = dmrg(H, psi0, sweeps)
println("Final energy = ", energy)
println("spin expecvalues: ",expect(psi,"Sz", sites=1))
println("spin expecvalues: ",expect(psi,"Sz", sites=2))
println("spin expecvalues: ",expect(psi,"Sz", sites=3))
println("spin expecvalues: ",expect(psi,"Sz", sites=4))
SvN = Measure.ent(psi, 4)
println("entropy = ", SvN)
#SvN2 = Measure.entropyvonneumann(psi,4)
#println("entropy = ", SvN2)
