using ITensors, MKL
#using ITensorTDVP
include("measure.jl")

function H_build(Nx, t, U, J, Jz, Bz, lattice)
  os = OpSum()
  for b in lattice
    # x-Hopping interaction for the first orbital first layer
    os += -t, "Cdagup", b.s1, "Cup", b.s2
    os += -t, "Cdagup", b.s2, "Cup", b.s1
    os += -t, "Cdagdn", b.s1, "Cdn", b.s2
    os += -t, "Cdagdn", b.s2, "Cdn", b.s1
  end
  for b in lattice
    os +=    Jz, "Sz", b.s1, "Sz", b.s2
    os += 0.5*J, "S+", b.s1, "S-", b.s2
    os += 0.5*J, "S-", b.s1, "S+", b.s2
  end
  # On-site repulsion
  for n in 1:Nx
    os += U, "Nupdn", 2*n-1
    os += U, "Nupdn", 2*n
  end 
  os += Bz, "Sz", 1
  return os
end
  
# Parameters of the Hamiltonian
Nx = 4
Ny = 2
t = 1
U = 8
J = 1
Jz = 2
Bz = 1
N = Nx*Ny
#Definition of the site type
sites = siteinds("Electron", N; conserve_qns=true)
#Definition of the lattice
lattice = square_lattice(Nx, Ny; yperiodic = false)
# Define the Hund Hamiltonian on this lattice
@time H = MPO(H_build(Nx, t, U, J, Jz, Bz, lattice),sites)

# Initialize the wavefunction as random MPS
state = ["Emp" for n in 1:N]
for i in 1:Nx
  state[2*i-1] = (isodd(i) ? "Up" : "Dn")
  state[2*i] = (isodd(i) ? "Dn" : "Up")
end
println("state:\n", state)
psi0 = randomMPS(sites,state,10)

# DMRG parameters maxdim has to be increased gradually
# noise has to be decreased gradually
# cutoff has to be increased gradually
sweeps = Sweeps(40)
#the following function takes the sweeps element as input
#and change the maxdim noise and cutoff parameter of the
#sweeps
maxdim!(sweeps,100,200,200,400,400,800)
noise!(sweeps,1E-3,1E-3,1E-3,1E-3, 1E-4, 1E-4, 1E-4, 1E-5, 1E-5, 1E-5, 1E-5, 1E-5, 1E-6, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-7, 1E-8, 1E-8, 1E-8, 1E-9, 1E-9, 1E-10, 1E-10, 1E-11, 1E-11, 0)
cutoff!(sweeps,1E-8)
#@show sweeps

# DMRG calculation
println("time to do dmrg")
@time energy,psi = dmrg(H,psi0,sweeps)
println("Final energy = ", energy)
println("spin expecvalues: ",expect(psi,"Sz", sites=1))
println("spin expecvalues: ",expect(psi,"Sz", sites=2))
println("spin expecvalues: ",expect(psi,"Sz", sites=3))
println("spin expecvalues: ",expect(psi,"Sz", sites=4))
SvN = Measure.ent(psi, 4)
println("entropy = ", SvN)
#SvN = Measure.ent(psi, 3)
#println("entropy = ", SvN)
#SvN = Measure.ent(psi, 4)
#println("entropy = ", SvN)
#SvN = Measure.ent(psi, 5)
#println("entropy = ", SvN)
