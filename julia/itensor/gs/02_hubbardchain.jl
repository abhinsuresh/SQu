using ITensors
include("measure.jl")

function H_build(Nx, t, U, J, Jz, Bz)
  os = OpSum()
  for n in 1:(Nx-1)
    # x-Hopping interaction for the first orbital first layer
    os += -t, "Cdagup", n, "Cup", n+1
    os += -t, "Cdagup", n+1, "Cup", n
    os += -t, "Cdagdn", n, "Cdn", n+1
    os += -t, "Cdagdn", n+1, "Cdn", n
    end
  for n in 1:(Nx-1)
    # x-Hopping interaction for the first orbital first layer
    os +=    Jz, "Sz", n, "Sz", n+1
    os += 0.5*J, "S+", n, "S-", n+1
    os += 0.5*J, "S-", n, "S+", n+1
    end
  # On-site repulsion
  for n in 1:Nx
    os += U, "Nupdn", n
  end 
  os += Bz, "Sz", 1
  return os
end
  
# Parameters of the Hamiltonian
Nx = 8
t = 1
U = 8
J = 0.1
Jz = 0.2
Bz = 0.001
#Definition of the site type
sites = siteinds("Electron", Nx; conserve_qns=true)
# Define the Hund Hamiltonian on this lattice
println("time to create the Hamiltonian")
@time H = MPO(H_build(Nx, t, U, J, Jz, Bz),sites)

# Initialize the wavefunction as random MPS
state = ["Emp" for n in 1:Nx]
for i in 1:Nx
  state[i] = (isodd(i) ? "Dn" : "Up")
end
println("state:\n", state)
println("time to generate random MPS")
@time psi0 = randomMPS(sites,state,10)

# DMRG parameters maxdim has to be increased gradually
# noise has to be decreased gradually
# cutoff has to be increased gradually
sweeps = Sweeps(12)
#the following function takes the sweeps element as input
#and change the maxdim noise and cutoff parameter of the
#sweeps
maxdim!(sweeps,100,200,200,400,400,800,1200,1200,1200,1600,1600,2000)
noise!(sweeps,1E-5,1E-6,1E-6,1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-8, 1E-9)
cutoff!(sweeps,1E-8)
#@show sweeps

# DMRG calculation
println("time to do dmrg")
@time energy,psi = dmrg(H,psi0,sweeps)
println("Final energy = ", energy)
println("spin expecvalues: ",expect(psi,"Sz", sites=1))
println("spin expecvalues: ",expect(psi,"Sz", sites=2))
println("spin expecvalues: ",expect(psi,"Sz", sites=3))
println("spin expecvalues: ",expect(psi,"Sz", sites=4))
SvN = Measure.ent(psi, 4)
println("entropy = ", SvN)
