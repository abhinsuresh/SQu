using ITensors
using ITensorTDVP
using DelimitedFiles

function entropyvonneumann(psi::MPS, N::Int)
  s = siteinds(psi)
  SvN = fill(0.0, N)
  for b in 2:N-1
    orthogonalize!(psi, b)
    U,S,V = svd(psi[b], (linkind(psi, b-1), s[b]))
    for n in 1:dim(S, 1)
      p = S[n,n]^2
      SvN[b] -= p * log(p)
    end
    println("$(SvN[b])")
  end
  return SvN
end

function H_build(Nx, t, U, Jex, Up, JH, gamma, Jan, B)
  os = OpSum()
  for n in 1:(Nx-1)
    # Hopping interaction for the first orbital
    os += -t, "Cdagup", 2*n-1, "Cup", 2*n+1
    os += -t, "Cdagup", 2*n+1, "Cup", 2*n-1
    os += -t, "Cdagdn", 2*n-1, "Cdn", 2*n+1
    os += -t, "Cdagdn", 2*n+1, "Cdn", 2*n-1
    os += -Jan, "Sz", 2*n-1, "Sz", 2*n+1
    os += -0.5*Jex, "S-", 2*n-1, "S+", 2*n+1
    os += -0.5*Jex, "S+", 2*n-1, "S-", 2*n+1
  end

  for n in 1:(Nx-1)
    # Hopping interaction for the second orbital
    os += -t, "Cdagup", 2*n, "Cup", 2*n+2
    os += -t, "Cdagup", 2*n+2, "Cup", 2*n
    os += -t, "Cdagdn", 2*n, "Cdn", 2*n+2
    os += -t, "Cdagdn", 2*n+2, "Cdn", 2*n
    os += -Jan, "Sz", 2*n, "Sz", 2*n+2
    os += -0.5*Jex, "S-", 2*n, "S+", 2*n+2
    os += -0.5*Jex, "S+", 2*n, "S-", 2*n+2
  end

  # Hund terms
  for n in 1:Nx
    os += Up-JH, "Nup", 2*n-1, "Nup", 2*n
    os += Up-JH, "Ndn", 2*n-1, "Ndn", 2*n
    os += Up, "Nup", 2*n-1, "Ndn", 2*n
    os += Up, "Ndn", 2*n-1, "Nup", 2*n
    os += gamma*JH, "H1", 2*n-1, "H1dag", 2*n
    os += gamma*JH, "H1", 2*n, "H1dag", 2*n-1
    os += gamma*JH, "H2", 2*n-1, "H2dag", 2*n
    os += gamma*JH, "H2", 2*n, "H2dag", 2*n-1
  end

  # On-site repulsion
  for n in 1:Nx
    os += U, "Nupdn", 2*n
    os += U, "Nupdn", 2*n-1
  end
  os += B, "Nup", 1
  os += B, "Nup", 2
  os += -B, "Ndn", 1
  os += -B, "Ndn", 2
  return os
end
# New operators needed for the Hund terms

ITensors.op(::OpName"H1",::SiteType"Electron")=
  [
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
     1.0 0.0 0.0 0.0
]

ITensors.op(::OpName"H1dag",::SiteType"Electron")=
  [
     0.0 0.0 0.0 1.0
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
  ]

ITensors.op(::OpName"H2",::SiteType"Electron")=
  [
     0.0 0.0 0.0 0.0
     0.0 0.0 1.0 0.0
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
  ]

ITensors.op(::OpName"H2dag",::SiteType"Electron")=
  [
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
     0.0 1.0 0.0 0.0
     0.0 0.0 0.0 0.0
  ]


# Parameters of the Hamiltonian
Ny = 2
Nx = 24
t = 1
U = 8
Jex = -0.1
Up = 6
JH = 1
gamma = 1
B=0.1

N = Nx*Ny

#Definition of the site type
sites = siteinds("Electron", N; conserve_qns=true)
#Definition of the lattice
lattice = square_lattice(Nx, Ny; yperiodic = false)

Jan = range(0.1,stop=10,length=100)

for j in Jan
  # Define the Hund Hamiltonian on this lattice
  H = MPO(H_build(Nx,t,U,Jex,Up,JH,gamma,j,B),sites)

  # Initialize the wavefunction as random MPS
  state = ["Emp" for n in 1:N]
  for i in 1:Nx
    state[2*i] = (isodd(i) ? "Up" : "Dn")
    state[2*i-1] = (isodd(i) ? "Dn" : "Up")
  end

  psi0 = randomMPS(sites,state,10)

  # DMRG parameters maxdim has to be increased gradually
  # noise has to be decreased gradually
  # cutoff has to be increased gradually
  sweeps = Sweeps(40)
  maxdim!(sweeps,100,200,200,400,400,800,1200,1200,1200,1600,1600,2000)
  noise!(sweeps,1E-5,1E-6,1E-6,1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-8, 1E-7, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-8, 1E-9)
  cutoff!(sweeps,1E-8, 1E-8, 1E-8, 1E-8, 1E-9, 1E-9, 1E-9)
  @show sweeps

  # DMRG calculation
  energy,psi = dmrg(H,psi0,sweeps)
  SvN = entropyvonneumann(psi,40)
  filename="T_"*string(j)*".txt"
  writedlm(filename,SvN)
  @show SvN
end

