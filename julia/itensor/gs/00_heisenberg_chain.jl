using ITensors, Random, MKL, Printf
include("measure.jl")

Random.seed!(1234)
@time begin
let
    N = 10
    # Create N spin-one degrees of freedom
    sites = siteinds("S=1/2", N)
    J = 1.0
    Jz = 2.0
    Bz = 0.01
    os = OpSum()
    for j in 1:(N - 1)
        os += Jz, "Sz", j, "Sz", j + 1
        os += J, 0.5, "S+", j, "S-", j + 1
        os += J, 0.5, "S-", j, "S+", j + 1
    end
    os += Bz, "Sz", 1

    H = MPO(os, sites)
    psi0 = randomMPS(sites, 10)
    nsweeps = 10
    maxdim = [10, 20, 100, 100, 200]
    cutoff = [1E-11]
    energy, psi = dmrg(H, psi0; nsweeps, maxdim, cutoff)
    @printf("Final energy = %.5f\n", energy)
    println("spin expecvalues: ",expect(psi,"Sz")[1:5])
    SvN = Measure.ent(psi, 5)
    @printf("entropy = %.5f\n", SvN)
end
end
