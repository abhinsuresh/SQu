using ITensors, MKL, HDF5
#using ITensorTDVP
include("measure.jl")

function H_build(Nx, t, U, Jex, Jz, Up, JH, gamma, Bz)
  os = OpSum()
  for n in 1:(Nx-1)
    # x-Hopping L = 1, α = 1 [1,5,9,13]
    os += -t, "Cdagup", 4*n-3, "Cup", 4*n+1
    os += -t, "Cdagup", 4*n+1, "Cup", 4*n-3
    os += -t, "Cdagdn", 4*n-3, "Cdn", 4*n+1
    os += -t, "Cdagdn", 4*n+1, "Cdn", 4*n-3
    os += -Jz, "Sz", 4*n-3, "Sz", 4*n+1
    os += -0.5*Jex, "S-", 4*n-3, "S+", 4*n+1
    os += -0.5*Jex, "S+", 4*n-3, "S-", 4*n+1
  end
  for n in 1:(Nx-1)
    # x-Hopping L = 2, α = 1 [2,6,10,14]
    os += -t, "Cdagup", 4*n-2, "Cup", 4*n+2
    os += -t, "Cdagup", 4*n+2, "Cup", 4*n-2
    os += -t, "Cdagdn", 4*n-2, "Cdn", 4*n+2
    os += -t, "Cdagdn", 4*n+2, "Cdn", 4*n-2
    os += -Jz, "Sz", 4*n-2, "Sz", 4*n+2
    os += -0.5*Jex, "S-", 4*n-2, "S+", 4*n+2
    os += -0.5*Jex, "S+", 4*n-2, "S-", 4*n+2
  end
  for n in 1:Nx
    # y-Hopping α = 1
    os += -t, "Cdagup", 4*n-3, "Cup", 4*n-2
    os += -t, "Cdagup", 4*n-2, "Cup", 4*n-3
    os += -t, "Cdagdn", 4*n-3, "Cdn", 4*n-2
    os += -t, "Cdagdn", 4*n-2, "Cdn", 4*n-3
    os += -Jz, "Sz", 4*n-3, "Sz", 4*n-2
    os += -0.5*Jex, "S-", 4*n-3, "S+", 4*n-2
    os += -0.5*Jex, "S+", 4*n-3, "S-", 4*n-2
  end

  for n in 1:(Nx-1)
    # x-Hopping L = 1, α = 2 [3,7,11,15]
    os += -t, "Cdagup", 4*n-1, "Cup", 4*n+3
    os += -t, "Cdagup", 4*n+3, "Cup", 4*n-1
    os += -t, "Cdagdn", 4*n-1, "Cdn", 4*n+3
    os += -t, "Cdagdn", 4*n+3, "Cdn", 4*n-1
    os += -Jz, "Sz", 4*n-1, "Sz", 4*n+3
    os += -0.5*Jex, "S-", 4*n-1, "S+", 4*n+3
    os += -0.5*Jex, "S+", 4*n-1, "S-", 4*n+3
  end
  for n in 1:(Nx-1)
    # x-Hopping L = 2, α = 2 [4,8,12,16]
    os += -t, "Cdagup", 4*n, "Cup", 4*n+4
    os += -t, "Cdagup", 4*n+4, "Cup", 4*n
    os += -t, "Cdagdn", 4*n, "Cdn", 4*n+4
    os += -t, "Cdagdn", 4*n+4, "Cdn", 4*n
    os += -Jz, "Sz", 4*n, "Sz", 4*n+4
    os += -0.5*Jex, "S-", 4*n, "S+", 4*n+4
    os += -0.5*Jex, "S+", 4*n, "S-", 4*n+4
  end
  for n in 1:Nx
    # y-Hopping α = 2
    os += -t, "Cdagup", 4*n-1, "Cup", 4*n
    os += -t, "Cdagup", 4*n, "Cup", 4*n-1
    os += -t, "Cdagdn", 4*n-1, "Cdn", 4*n
    os += -t, "Cdagdn", 4*n, "Cdn", 4*n-1
    os += -Jz, "Sz", 4*n-1, "Sz", 4*n
    os += -0.5*Jex, "S-", 4*n-1, "S+", 4*n
    os += -0.5*Jex, "S+", 4*n-1, "S-", 4*n
  end

  # Hund terms L = 1
  for n in 1:Nx
    os += Up-JH, "Nup", 4*n-3, "Nup", 4*n-1
    os += Up-JH, "Ndn", 4*n-3, "Ndn", 4*n-1
    os += Up, "Nup", 4*n-3, "Ndn", 4*n-1
    os += Up, "Ndn", 4*n-3, "Nup", 4*n-1
    os += gamma*JH, "H1", 4*n-3, "H1dag", 4*n-1
    os += gamma*JH, "H1", 4*n-1, "H1dag", 4*n-3
    os += gamma*JH, "H2", 4*n-3, "H2dag", 4*n-1
    os += gamma*JH, "H2", 4*n-1, "H2dag", 4*n-3
  end

  # Hund terms L = 2
  for n in 1:Nx
    os += Up-JH, "Nup", 4*n-2, "Nup", 4*n
    os += Up-JH, "Ndn", 4*n-2, "Ndn", 4*n
    os += Up, "Nup", 4*n-2, "Ndn", 4*n
    os += Up, "Ndn", 4*n-2, "Nup", 4*n
    os += gamma*JH, "H1", 4*n-2, "H1dag", 4*n
    os += gamma*JH, "H1", 4*n, "H1dag", 4*n-2
    os += gamma*JH, "H2", 4*n-2, "H2dag", 4*n
    os += gamma*JH, "H2", 4*n, "H2dag", 4*n-2
  end

  # On-site repulsion
  for n in 1:Nx
    os += U, "Nupdn", 4*n-3
    os += U, "Nupdn", 4*n-2
    os += U, "Nupdn", 4*n-1
    os += U, "Nupdn", 4*n
  end 
  os += -Bz, "Sz", 1
  return os
end

# New operators needed for the Hund terms
ITensors.op(::OpName"H1",::SiteType"Electron")=
  [
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
     1.0 0.0 0.0 0.0
  ]

ITensors.op(::OpName"H1dag",::SiteType"Electron")=
  [
     0.0 0.0 0.0 1.0
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
  ]

ITensors.op(::OpName"H2",::SiteType"Electron")=
  [
     0.0 0.0 0.0 0.0
     0.0 0.0 1.0 0.0
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
  ]

ITensors.op(::OpName"H2dag",::SiteType"Electron")=
  [
     0.0 0.0 0.0 0.0
     0.0 0.0 0.0 0.0
     0.0 1.0 0.0 0.0
     0.0 0.0 0.0 0.0
  ]

  
# Parameters of the Hamiltonian
Nx = 4
Ny = 4
t = 1
U = 8
Jex = -0.1
Jz  = Jex*parse(Float64,ARGS[1])
nm = ARGS[1]
Up = 6
JH = 1
gamma = 1
Bz = 0.1

println("Jz = ", Jz)
println("nm = ", nm)


N = Nx*Ny

#Definition of the site type
sites = siteinds("Electron", N; conserve_qns=true)
#Definition of the lattice
#@time lattice = square_lattice(Nx, Ny; yperiodic = false)
# Define the Hund Hamiltonian on this lattice
println("time to create the Hamiltonian")
@time H = MPO(H_build(Nx,t,U,Jex,Jz,Up,JH,gamma, Bz),sites)
# Initialize the wavefunction as random MPS
state = ["Emp" for n in 1:N]
for i in 1:Nx
  state[4*i-3] = (isodd(i) ? "Up" : "Dn")
  state[4*i-2] = (isodd(i) ? "Dn" : "Up")
  state[4*i-1] = (isodd(i) ? "Dn" : "Up")
  state[4*i]   = (isodd(i) ? "Dn" : "Up")
end
println("state:\n", state)

println("time to generate the random MPS")
@time psi0 = randomMPS(sites,state,10)

# DMRG parameters maxdim has to be increased gradually
# noise has to be decreased gradually
# cutoff has to be increased gradually
sweeps = Sweeps(20)
#the following function takes the sweeps element as input
#and change the maxdim noise and cutoff parameter of the
#sweeps
maxdim!(sweeps,100,200,200,400,400,800, 800, 1200,1200,1200,1600,1600, 1600, 2000, 2000)
noise!(sweeps,1E-5,1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-8, 1E-8, 1E-9, 1E-11, 1E-12, 0)
cutoff!(sweeps,1E-8)
#@show sweeps
# DMRG calculation
println("time to do dmrg")
@time energy,psi = dmrg(H,psi0,sweeps)
spinz = expect(psi,"Sz")
ent = Measure.ent(psi, 8)

f = h5open("results/jz"*nm*".hdf5", "w")
    write(f, "energy", energy)
    write(f, "spinz", spinz)
    write(f, "ent", ent)
close(f)
