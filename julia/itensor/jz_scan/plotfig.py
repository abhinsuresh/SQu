import sys, numpy as np, h5py, os

sys.path.append('../../../plot_manager')
from plot2d import plotfig_2p

data = dict();values=dict()

Ns = 16
J  = 0.1
#file1

def get_files(folder):
    f_nm = [filename for filename in os.listdir(folder) 
           if filename.startswith("jz")]
    f_num = [float(filename[2:][:-5]) for filename in os.listdir(folder) 
           if filename.startswith("jz")]
    sort_ind = np.argsort(f_num)
    names = [f_nm[i] for i in sort_ind]
    tval = np.array([f_num[i] for i in sort_ind])
    #print(names)
    print("tvalues:\t", tval)
    result = np.zeros((len(tval),2))
    result[:,1] = [h5py.File(folder+name,"r")["ent"].value for name in names]

    #print(h5py.File("results/jz02.hdf5","r")["spinz"][:].shape)
    #print(h5py.File("results/jz02.hdf5","r")["spinz"][:])
    for ind, name in enumerate(names):
        spinz = h5py.File(folder+name,"r")["spinz"]
        if ind == 0: print(np.round(spinz[:4],5))
        for n in range(4):
            result[ind,0] += (spinz[4*n] + spinz[4*n+2])*(-1)**n
            result[ind,0] += (spinz[4*n+1] + spinz[4*n+3])*(-1)**(n+1)
    result[:,0] = np.abs(result[:,0])/8
    return tval, result

tval, result = get_files("results/")

#for n in range(4):
    #print(4*n, 4*n+2, (-1)**n)
    #print(4*n+1, 4*n+3, (-1)**(n+1))
data['x_plot1'], data['y_plot1'] = get_files("tso_res/")  

data['x_plot'] = tval; data['y_plot'] = np.abs(result)
values['xlabel1'] = r'$\mathrm{J_z/J}$';
values['xlabel2'] = r'$\mathrm{J_z/J}$';
values['ylabel1'] = r'$\mathrm{|S_{\rm stag}|/N}$'
values['ylabel2'] = r'$\mathrm{Entanglement~Entropy}$'
Cl = ['k']*2
Gs = dict()
Gs['t']=0.95; Gs['b']=0.18; Gs['l']=0.1; Gs['r']=0.98
Gs['w']=0.4; Gs['h']=0.4

sf = np.array([[0,2],[2,4]])
plotfig_2p(data, '2', values, Cl=Cl, lw=0.4, minor=1, Gs=Gs, ax_twin=0, fs=6)
