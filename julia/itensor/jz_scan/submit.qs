#!/bin/bash
#SBATCH --job-name='nio'
#SBATCH --output=LOGFILE
#SBATCH --nodes=1
#SBATCH --mem=100G
#SBATCH --error=ERR
#SBATCH --time=05:00:00
#SBATCH --tasks-per-node=16
#SBATCH --partition=idle
#SBATCH --mail-user='abhins@udel.edu'
#SBATCH --mail-type=END,FAIL
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=16
vpkg_require julia
julia 02_nio.jl $1 > outlog/out_$1.txt 
