#!/bin/bash
#SBATCH --job-name='hund_spectral'
#SBATCH --output=LOGFILE
#SBATCH --nodes=1
#SBATCH --mem=10G
#SBATCH --error=ERR
#SBATCH --time=20:00:00
#SBATCH --tasks-per-node=8
#SBATCH --partition=idle
export OMP_NUM_THREADS=8
vpkg_require julia
julia Hund_spectral.jl > outlog/out_01.txt 
