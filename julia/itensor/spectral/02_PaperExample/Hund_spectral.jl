using ITensors
using ITensorGPU
using ITensorTDVP
using DelimitedFiles

gpu = cu

function H_build(Nx, t, U, V, J, w)
  os = OpSum()
  for n in 1:(Nx-1)
    # Hopping interaction for the first orbital
    os += -t, "Cdagup", 2*n-1, "Cup", 2*n+1
    os += -t, "Cdagup", 2*n+1, "Cup", 2*n-1
    os += -t, "Cdagdn", 2*n-1, "Cdn", 2*n+1
    os += -t, "Cdagdn", 2*n+1, "Cdn", 2*n-1
  end
  # Hubbard U
  for n in 1:Nx
    os +=      U, "Nup", 2*n-1, "Ndn", 2*n-1
    os += -0.5*U, "Nup", 2*n-1
    os += -0.5*U, "Ndn", 2*n-1
  end
  # Hubbard V repulsion
  for n in 1:Nx-1
    os +=  V, "Nupdn", 2*n-1, "Nupdn", 2*n+1
    os +=  V, "Nupdn", 2*n-1
    os +=  V, "Nupdn", 2*n+1
  end
  
  # Probe-internal orbital coupling
  for n in 1:Nx
    os += J, "Cdagup", 2*n-1, "Cup", 2*n
    os += J, "Cdagup", 2*n, "Cup", 2*n-1
  end

  # Gate Voltage terms
  for n in 1:Nx
    os += w, "Nup", 2*n
    os += 100, "Ndn", 2*n
  end
 
  return os
end

function Compute_spectral(C, Nx, k)
  # Compute momentum resolved spectral function from the measurements in the probe virtual sites
  a=0  
  for i in 1:Nx
    for j in 1:Nx
      a += C[2*i,2*j]*exp(im*k*(i-j)) 
    end
  end
  a /= Nx
  return real(a)
end

function Compute_seg(C, Nx, kmin, kmax, kstep)
  # Iter momentum resolved spectral function for several momentum values
  K=range(kmin,stop=kmax,length=kstep)
  X = zeros(length(K))
  @show X
  iter=1
  for i in K
    X[iter]=Compute_spectral(C,Nx,i)
    iter+=1
  end
  return X
end


  
# Parameters of the Hamiltonian
Ny = 2
Nx = 10
t = 1
U = 8
V = 5
# Parameters of the probe part of the system
J = 0.1
w = 100

N = Nx*Ny

#Definition of the site type
sites = siteinds("Electron", N; conserve_qns=true)
#Definition of the lattice
lattice = square_lattice(Nx, Ny; yperiodic = false)

# Define the Hund Hamiltonian on this lattice
H = MPO(H_build(Nx,t,U,V,J,w),sites)

# Initialize the wavefunction as random MPS
state = ["Emp" for n in 1:N]
for i in 1:Nx
  state[2*i] = (isodd(i) ? "Up" : "Dn")
  state[2*i-1] = (isodd(i) ? "Dn" : "Up")
end

psi0 = randomMPS(sites,state,10)

# DMRG parameters maxdim has to be increased gradually
# noise has to be decreased gradually
# cutoff has to be increased gradually
sweeps = Sweeps(40)
maxdim!(sweeps,100,200,200,400,400,800,1200,1200,1200,1600,1600,2000)
noise!(sweeps,1E-5,1E-6,1E-6,1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-6, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-7, 1E-8,1E-9)
cutoff!(sweeps,1E-8)
#@show sweeps

# DMRG calculation
energy,psi = dmrg(H,psi0,sweeps)

#spectral function calculation
w = range(-10,10,step=0.2)

# real time evolution parameters
ttotal = 6
tau = 0.1
cutoff = 1e-6
maxdim = 2000

for V_g in w
  @show V_g
  H1 = MPO(H_build(Nx,t,U,V,J,V_g),sites)

  # Real time evolution through TDVP algorithm
  psi_f = tdvp(H1, -im * ttotal, psi;
               time_step = -im*tau,
               cutoff, maxdim,
	           outputlevel = 1,
               normalize = true)

  # computation of the correlation matrix
  c= correlation_matrix(psi_f,"Cdagup","Cup")
  
  # computation of the spectral function
  A = Compute_seg(c, Nx, -pi, pi, 100)

  filename="results/T_"*string(V_g)*".txt"
  writedlm(filename,A)
end
