Julia lessons
-------------

00. Learn how to reduce number of allocations
    reduce number of loop 
    a1. by assigning  applying functions to a whole column rather 
    than iterating over them, or passing them as vectors to a 
    function which will loop over them

01. In a script if I try to modify/redefine a previously defined varible
    inside a for loop, the previously defined varible might not get updated
    but a new local varibale might be created, to avoid that use global
    inside for loop or any other scope blocks, if thats what I wanted.


Itensor lessons
---------------

00. itensor dmrg fails when the energy of the excited state follows closely
    with the gs energy, the way to curcumvent that is to increase the magnetic
    filed value to 0.1 (which works for 2by2 system with two orbitals when
    checked with keldysh/squbit/dmrg_test/04_2b2...py and julia/itensor/test)
    also now tested for 4b2 system with two orbtials (Jz~J) takes an hour to
    compute


Notes
-----

01. String defined with "" only

02. a = "abhin_01.hdf5"
    a[7:end][1:end-5]
    $ 01

03. let ... end
    defines the scope of a defined varibale
    each for/if starts a new scope which can
    inherit previous scope as well

04. python exported hdf5 file will get permuted in shape
    when opened in julia (1,124) -> (124, 1)
    but matrix will elements won't get reordered
    and be a complete mess

05. Verified entanglement entropy for spin system with
    julia itensor dmrg algorithm

06. Verified GS energy for 2 x 2 system with 2 orbital and
    all terms in NiO project (t,U,U',JH,gJH).
    Also for 4 x 2 system with 2 orbital, dmrg is faster
    than julia
    dmrg in bloch
    dmrg energy after 10 iterations(1600 link dim): 34.23893138 (Err=1.74e-6)
    dmrg energy after 19 iterations (2000) :34.238871 (no multi threading)
    multithreading doesn't seems to improve the calculation for GS and ent
    (took 15 mins to find GS and entropy)
    hphi energy : 34.238830  (darwin/06_multorb)
    (took and 1 hour with 4 nodes 64 omp threads)

07. Entropy calculation in julia and python using quspin is not matching
    as of now for multiorbital case:
    E_dmrg = 17.519353163, ent = 1.906514
    E_squbit = 17.5193526, ent = 2.16717
    will test for chain now

08. Entropy and gs Energy calculation matched for a chain of 8 sites
    but not for ladder systems-> (fixed, density=False in quspin, numbering in squbit)

09. For spin chain and ladder Energy and spin expectation and entropy values matched 
    now onto hubbard ladder

10. jz_scan with Bz=0.1 verified with hphi even for Jz/J = 1, lower values of Bz=0.01 
    and 0.01 has less difference berween gs and first excited state
