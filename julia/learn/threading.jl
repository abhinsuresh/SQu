using BenchmarkTools
using Base.Threads
print(nthreads(),"\n")
function single(len=1000::Int64)
    a = zeros(len,len,2)
    @threads for i = 1:len        
        for j = 1:len                 
            a[i,j,1] = i^2 + j^2               
            a[i,j,2] = threadid()
        end                              
    end
    return a 
end        
function mult(len=1000::Int64)
    a = zeros(len,len,2)
    @threads for i = 1:len        
        @threads for j = 1:len                 
            a[i,j,1] = i^2 + j^2               
            a[i,j,2] = threadid()
        end                              
    end
    return a
end        
as = single(10000)                      
#time am = mult(10000)              
display(as[1:5,1:5,1])
display(as[1:2000:10000,1:2000:10000,2])
print("\n")
#display(am[1:5,1:5,1])
