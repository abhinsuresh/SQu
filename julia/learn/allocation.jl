@time begin 
    a = zeros(100,200)
    #a = Matrix{Float64}(undef,100,200)
    for j in range(1,200)
        for i in range(1,100)
            a[i,j] += (i^2 + j^3)/pi
        end
    end
    #println(a[1:5,1])
end 
