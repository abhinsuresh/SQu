module Updnlist
    function sum_to(n)
        ls = []
        for i = 0:Int64(n/2) + 1
            ls = [ls; [[i, n-i]]]
            if i != n-i
                ls = [ls; [[n-i,i]]]
            end
        end
        return ls
    end
end

