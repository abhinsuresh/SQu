import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
from scipy.sparse import eye
from scipy.sparse import kron

class Fermionic:

    def __init__(self,
                 N = 1,
                 kronD = 1,
                 dtype = np.complex128,
                 names = ['Cup', 'Cdn']):
        self.N = N
        self.kronD = kronD
        self.dtype= dtype
        self.names = names
        self.sp_names = ['sx', 'sy', 'sz']
        self.extended_op = self.extend_to_nsites()
        self.op = self.kron_iden(kronD)
        self.se = self.create_spin_op(kronD)
        return

    def basic_op(self):
        row = np.array([0, 1, 2, 3])
        col = np.array([0, 1, 2, 3])
        data = np.array([1, -1, -1, 1])
        P1 = csr_matrix((data, (row,col)), shape=((4,4)), dtype=self.dtype)

        row = np.array([0, 2])
        col = np.array([1, 3])
        data = np.array([1, 1])
        Cup = csr_matrix((data, (row,col)), shape=((4,4)), dtype=self.dtype)

        row = np.array([0, 1])
        col = np.array([2, 3])
        data = np.array([1, -1])
        Cdn = csr_matrix((data, (row,col)), shape=((4,4)), dtype=self.dtype)    
        
        return Cup, Cdn, P1


    def extend_to_nsites(self):
        """A function to extend to multiple sites basis.
        
        Parameters
        ----------
        N : int
            Total number of sites in the system
        names : names of two blocks, up and down.
        
        Returns
        -------
        result : dictionary
            A dict containing Cup and Cdn of all sites. Can be 
            accessed by 'Cup[0-(N-1)]'. In the order Cup all sites
            followed by Cdn all sites
        """
        nums = ['{0:1.0f}'.format(i) for i in np.arange(self.N)]    
        result = dict()    
        Cup, Cdn, P1 = self.basic_op()
        I4 = eye(4)
        
        for ind in np.arange(self.N):
            op = eye(1)
            for n_site in np.arange(self.N):
                
                if n_site == 0 and ind == 0:
                    op = kron(op, Cup)
                    
                elif n_site < ind:
                    op = kron(op, P1)
                    
                elif n_site == ind:
                    op = kron(op, Cup)
                    
                elif n_site > ind:
                    op = kron(op, I4)
                
            result[self.names[0]+nums[ind]] = op.tocsr()
            
        for ind in np.arange(self.N):
            op = eye(1)
            for n_site in np.arange(self.N):
                
                if n_site == 0 and ind == 0:
                    op = kron(op, Cdn)
                    
                elif n_site < ind:
                    op = kron(op, P1)
                    
                elif n_site == ind:
                    op = kron(op, Cdn)
                    
                elif n_site > ind:
                    op = kron(op, I4)
                    
            result[self.names[1]+nums[ind]] = op.tocsr()
            
        return result

    def create_spin(self, site = 0, op=None, return_N=0):
        """A function to create spin operators for corresponding
        sites.
        
        Parameters
        ----------
        op : dict of scipy.sparse.csr_matrices
            c-operators
        site : int
            corresponding site
        
        Returns
        -------
        sx : scipy.sparse.csr_matrix
            Spin x operator in sz basis
        sy : scipy.sparse.csr_matrix
            Spin y operator in sz basis
        sz : scipy.sparse.csr_matrix
            Spin z operator in sz basis
        N : scipy.sparse.csr_matrix
            Number operator for the corresponding site
        """
        if op == None:
            op = self.op
        Cup = op['Cup' + str(site)]
        Cdn = op['Cdn' + str(site)]

        sx =      (Cdn.getH()@Cup + Cup.getH()@Cdn)/2;
        sy = (-1j*(Cup.getH()@Cdn - Cdn.getH()@Cup))/2;
        sz =      (Cup.getH()@Cup - Cdn.getH()@Cdn)/2;

        N = Cup.getH()*Cup + Cdn.getH()*Cdn;
        if return_N:
            return sx, sy, sz, N
        else:
            return sx, sy, sz
    
    def create_spin_op(self, kron_size=1):
        result = dict()
        #print(self.op)
        nm = self.sp_names
        for site in range(self.N):
            result[nm[0]+str(site)],\
            result[nm[1]+str(site)],\
            result[nm[2]+str(site)] = self.create_spin(site,self.op)
        return result

    def kron_iden(self, size):
        """Kron indentity to the last
        """
        result = dict()
        names = list(self.extended_op.keys())
        for key in names:
            result[key] = kron(self.extended_op[key], eye(size))
        return result


class Spin:

    def __init__(self,
                 S = 1/2,   #spin value
                 N = 1,     #number of sites
                 kronD = 1,
                 dtype = np.complex128,
                 names = ['Sx', 'Sy', 'Sz', 'Sp', 'Sm']):
        self.S = S
        self.N = N
        self.dim = int(2*self.S + 1)
        self.kronD = kronD
        self.dtype = dtype
        self.names = names
        self.extended_op = self.extend_to_nsites()
        self.op = self.kron_iden(kronD) 
        return

    def basic_op(self):
        """creates sigma matrix
        """
        row_x = []; col_x = []; data_x = []
        row_y = []; col_y = []; data_y = []
        row_z = range(self.dim)
        col_z = range(self.dim)
        data_z = -1*np.arange(-self.S,self.S+1,1)
        self.Sz = coo_matrix((data_z, (row_z,col_z)), 
            shape=((self.dim,self.dim)), dtype=self.dtype)
        for i in range(self.dim-1):
            row_x.append(i); row_x.append(i+1)
            col_x.append(i+1); col_x.append(i)
            data_x.append(0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
            data_x.append(0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
            data_y.append(-1j*0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
            data_y.append(1j*0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
        self.Sx = coo_matrix((data_x, (row_x,col_x)), 
            shape=((self.dim,self.dim)), dtype=self.dtype)
        self.Sy = coo_matrix((data_y, (row_x,col_x)), 
            shape=((self.dim,self.dim)), dtype=self.dtype) 

        self.Sp = (self.Sx + 1j*self.Sy)/2
        self.Sm = (self.Sx - 1j*self.Sy)/2
        return

    def extend_to_nsites(self):
        """extend spin matrix to n sites
        """
        result = dict()    
        sites = self.N
        I4 = eye(self.dim)
        self.basic_op() 
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sx)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sx)
                elif n_site > ind:
                    op = kron(op, I4)
            result['Sx'+str(ind)] = op.tocsr()

        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sy)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sy)
                elif n_site > ind:
                    op = kron(op, I4)
            result['Sy'+str(ind)] = op.tocsr()
            
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sz)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sz)
                elif n_site > ind:
                    op = kron(op, I4)
            result['Sz'+str(ind)] = op.tocsr()

        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sp)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sp)
                elif n_site > ind:
                    op = kron(op, I4)
            result['Sp'+str(ind)] = op.tocsr()

        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sm)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sm)
                elif n_site > ind:
                    op = kron(op, I4)
            result['Sm'+str(ind)] = op.tocsr()
        return result

    def kron_iden(self,size):
        """Kron electronic identity to extended system
        """
        result = dict()
        names = list(self.extended_op.keys())
        for key in names:
            result[key] = kron(eye(size),self.extended_op[key])
        return result
