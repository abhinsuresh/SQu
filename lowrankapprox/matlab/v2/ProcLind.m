%
% Postprocessing of results from Lindblad solver
%
%
load abhin_evolve;
%%
Tf = T(it-1);
% Compute trace
tr_rho_final = trace_sepformat(U0,S0,V0);
[ri,~,~,~] = rank_truncation_sparse(Y0,tol,1,10);
[rf,Urho,Srho,Vrho] = rank_truncation_sparse(rhof2,tol,1,10);
rfa=r1;
%[rfa,~,~,~] = rank_truncation_sparse(Y1,tol,1,10);
%
% Compute error
irf=[1:min(rfa,rf)];
err=dist_sepformat(Urho(:,irf),Srho(irf,irf),Vrho(:,irf),...
    U0,S0,V0);
%%
figure(1);
subplot(2,2,3);
[I,J] = find(abs(Y1)>1.0e-3);
Y1s=sparse(size(Y1));
Y1s(I,J) = Y1(I,J);
spy(abs(Y1s));

Ysol = expm(-iunit*Tf*H)*Y0*expm(iunit*Tf*H);

fprintf(1,'Approximation properties\n');
fprintf(1,'system size n=%d  max effective dimension: %d\n',n,r1);
fprintf(1,'Trace preservation:\n')
fprintf(1,'Trace Y0     :  %6.5f t=0\n',tr_rho_init);
fprintf(1,'Trace Yapprox:  %6.5f t=%3.2f\n',tr_rho_final,Tf);
fprintf(1,'Trace Yexact :  %6.5f t=%3.2f\n',trace(Ysol),Tf);
fprintf(1,'Error ||Y1 - rho|| = %6.5e  tol = %6.5e\n', ...
    norm(Y1-Ysol),tol);
%%
fprintf(1,'Rank of approximation with TOL = %e\n',tol);
fprintf(1,'Initial effective dimension   : %d\n',ri);
fprintf(1,'Final effective dimension     : %d\n',rfa);
fprintf(1,'rhof Abhin effective dimension: %d\n',rf);

figure(2);clf
plot(tmeas,real(sz1meas),'-b*'); hold on;
leg(1)=plot(trho,real(sz1),'-b','LineWidth',2); hold on;

plot(tmeas,real(sz2meas),'-r*'); hold on;
leg(2)=plot(trho,real(sz2),'-r','LineWidth',2); hold on;

title('Observables');
xlabel('t');
ylabel('Tr(A\rho)');
legend(leg,'Sz1','Sz2');


%%
figure(3);clf
subplot(2,1,1);
plot(tmeas,real(sz1meas),'-b*'); hold on;
plot(tmeas,real(sz2meas),'-r*'); hold on;

subplot(2,1,2);
leg(1)=plot(trho,real(sz1),'-b','LineWidth',2); hold on;
leg(2)=plot(trho,real(sz2),'-r','LineWidth',2); hold on;
title('Observables');
xlabel('t');
ylabel('Tr(A\rho)');
legend(leg,'Sz1','Sz2');