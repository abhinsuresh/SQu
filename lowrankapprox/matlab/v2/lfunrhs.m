function Drhs = lfunrhs(U,S,V,H,Zp,Zm,Zpm,Zmp,g,flag_lindblad)
%
% Evaluates right hand side of the equation
%
% Integrate the system 
% dL/dt = F(t,U L^T(t))^T U
%
% U,S,V - rho in separated approx
% H     - Hamiltonian
% Li    - Lindbladians
%


iunit = 1i;
L = V*S';
[n,r]=size(L);
Drhs = -iunit*L*(U'* (H*U)) + iunit*H*L;

if flag_lindblad
%
% First Lindblad term
term1 = zeros(n,r);
for i = 1:8
    term1 = term1 + g*( (Zp{i}*L) * (U' * (Zm{i}*U)) ...
                      + (Zm{i}*L) * (U' * (Zp{i}*U)));
end
term1 = term1 -0.5*g*( L*(U' * (Zmp*U)) + Zmp*L ...
                     + L*(U' * (Zpm*U)) + Zpm*L);
Drhs = Drhs + term1;
%
% Second Lindblad term
end

end