function Drhs = kfunrhs(U,S,V,H,Zp,Zm,Zpm,Zmp,g,flag_lindblad)
%
% Evaluates right hand side of the equation
%
% Integrate the system 
%   dK/dt = F(t,K(t) V^T) V
%
% U,S,V - rho in separated approx
% H     - Hamiltonian
% Li    - Lindbladians
%


iunit = 1i;
K = U*S;
[n,r]=size(K);
Drhs = -iunit*H*K +iunit*K*(V'*(H*V));

if flag_lindblad
%
% First Lindblad term
term1 = zeros(n,r);
for i = 1:8
    term1 = term1 + g*(  (Zp{i}*K) * (V' * (Zm{i}*V)) ...
                       + (Zm{i}*K) * (V' * (Zp{i}*V)) );

end
term1 = term1 - 0.5*g*(  Zmp*K + K*(V' * (Zmp*V)) ...
                       + Zpm*K + K*(V' * (Zpm*V)) );
Drhs = Drhs + term1;
%
% Second Lindblad term
end

end