function tr = trace_sepformat(U,S,V)
%
% Computes trace of a matrix in the separated format
% A = U*S*V'
%
% where 
%   U = n x r
%   S = r x r
%   V = n x r
%
% Use the identity Tr(AB) = Tr(BA)
% if r<<n it avoids building large matrix and then trace it
%
tr = trace(V'*(U*S));
end

