function Drhs = sfunrhs(U,S,V,H,Zp,Zm,Zpm,Zmp,g,flag_lindblad)
%
% Evaluates right hand side of the equation
%
% Integrate the system 
% dShat/dt = Uhat^T F(t,Uhat Shat(t) Vhat^T) Vhat
%
% U,S,V - rho in separated approx
% H     - Hamiltonian
% Li    - Lindbladians
%
%flag_lindblad=1;

iunit = 1i;
[n,r]=size(S);
Drhs = -iunit*U'*(H*(U*S)) + iunit*S*(V'*(H*V));

if flag_lindblad
%
% First Lindblad term
term1 = zeros(n,r);
for i = 1:8
    term1 = term1 + g*( (U' * (Zp{i}*U)) * (S*(V' * (Zm{i}*V))) ...
                      + (U' * (Zm{i}*U)) * (S*(V' * (Zp{i}*V))));
end
term1 = term1 - 0.5*g*( (U' * (Zpm*U)) * (S*(V' * (Zpm*V)))...
                       +(U' * (Zmp*U)) * (S*(V' * (Zmp*V))));
Drhs = Drhs + term1;
%
% Second Lindblad term

end

end