clc;
clear all;

%H_ = load('Ham.mat');
%rho_ = load('Rho.mat');
H_ = load('H.mat');
rho_ = load('rho.mat');
pop = load('pop.mat');
op = load('op.mat');

%op1_ = load('op.mat');



H = H_.H;
rho = rho_.rho;
%pop = pop_.pop;
%op = op_.op;

Zp = load('Zp.mat');
Zm = load('Zm.mat');
Zmp = load('Zmp.mat');
Zpm = load('Zpm.mat');
Zp = Zp.Zp;
Zm = Zm.Zm;
Zmp = Zmp.Zmp;
Zpm = Zpm.Zpm;

%measurements
%energy = trace(H*rho)
%sz0 = trace(rho*op.sz0)
%sz7 = trace(rho*op.sz7)


%{
Zpm = sparse(length(rho), length(rho));
for ind = 1:8
    si = num2str(ind-1);
    Zpm = Zpm + pop.("szpm"+si);
    
end
Zmp = sparse(length(rho), length(rho));
for ind = 1:8
    si = num2str(ind-1);
    Zmp = Zmp + pop.("szmp"+si);
    
end

for ind = 1:8
    si = num2str(ind-1);
    Zp{ind} = pop.("szp"+si);
    Zm{ind} = pop.("szm"+si);
end

%}

% save('Zp.mat','Zp')
% save('Zm.mat','Zm')
% save('Zpm.mat','Zpm')
% save('Zmp.mat','Zmp')
%{
term1 = sparse(n,r);
for i = 1:8
    term1 = term1 +  g*( (Zp{i}*K)*((V'*Zm{i})*V) ...
                       + (Zm{i}*K)*((V'*Zp{i})*V) );
end
term1 = term1 - g*0.5*( Zmp*K + K*(V'*(Zmp*V)) ...
                      - Zpm*K + K*(V'*(Zpm*V)) );

%}




%{
rho_s = trace_A(rho, 220, 256);


log_neg = zeros(5,1);
entropy = get_entropy(rho_s)
[~,log_neg(1)] = negativity(full(rho_s), 16);

[~,log_neg(2)] = negativity(full(rho_s), 16);
log_neg

szexp = trace(rho_s * op1_.sz1)

ssum = 0
parfor ind = 1:100
    if mod(ind,10) == 0
        ind
        ssum = ssum + ind
    end
end
%}

