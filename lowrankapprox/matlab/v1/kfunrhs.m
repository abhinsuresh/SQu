function Drhs = kfunrhs(U,S,V,H,Li,g1,g2)
%
% Evaluates right hand side of the equation
%
% Integrate the system 
%   dK/dt = F(t,K(t) V^T) V
%
% U,S,V - rho in separated approx
% H     - Hamiltonian
% Li    - Lindbladians
%
flag_lindblad=1;

iunit = 1i;
K = U*S;
[n,r]=size(K);
Drhs=zeros(n,r);
Drhs = -iunit*H*K +iunit*K*(V'*(H*V));

if flag_lindblad
%
% First Lindblad term
term1 = zeros(n,r);
for ind = 1:8
    si = num2str(ind-1);
    term1 = term1 + g1 * ((Li.("szp"+si)*K)*(V'*(Li.("szm"+si)*V)) ...
          - 0.5*(Li.("szmp"+si)*K + K*(V'*(Li.("szmp"+si)*V))));
    term1 = term1 + g1 * ((Li.("szm"+si)*K)*(V'*(Li.("szp"+si)*V))...
          - 0.5*(Li.("szpm"+si)*K + K*(V'*(Li.("szpm"+si)*V))));
end
Drhs = Drhs + term1;
%
% Second Lindblad term
term1 = zeros(n,r);
for ind = 1:7
    si = num2str(ind-1);
    sj = num2str(ind);
    term1 = term1 + g2*(Li.("szp"+sj)*K*(V'*(Li.("szm"+si)*V)) ...
           - 0.5*(Li.("szmp"+si+sj)*K + K*(V'*(Li.("szmp"+si+sj)*V))));
    term1 = term1 + g2*(Li.("szm"+sj)*K*(V'*(Li.("szp"+si)*V)) ...
           - 0.5*(Li.("szpm"+si+sj)*K + K*(V'*(Li.("szpm"+si+sj)*V))));
end
Drhs = Drhs + term1;
end

end