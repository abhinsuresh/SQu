% Implementation of adaptive low-rank application
% for dY/dt = F(t,Y(t))
clc;
clear all;
iunit = 1.0i;
prtfreq=70;

tol = 1.0e-6;
Tfinal = 0.2;
dt = 0.1;
%
% Setting the problem
% 
% Hamiltonian
%H = load("H.mat");
%Y0= load("rho.mat");
load H;
load rho;

 op_ = load('op.mat');
 Sz0 = op_.sz0;
 Sz1 = op_.sz1;
 clear op_;
 pop = load('pop.mat'); 
 gamma = 0.01;
 g = 0;
 trace(rho*Sz0)
  trace(rho*Sz1)
 
 
%%
% Initial condition
% Y0
Y0 = rho;
[n,nH]=size(H);

[r0,U0,S0,V0] = rank_truncation_sparse(Y0,tol,1,10);
tr_rho_init = trace_sepformat(U0,S0,V0);

fprintf(1,'Initial rank r=%d  (tolerance %e)\n',r0,tol);
fprintf(1,'Trace of initial condition: %e\n',tr_rho_init);
%
% Main time stepping loop
%
it = 1;

t=0;
T(it) = t;
it=it+1;

ind=1;

tic;
%
while t< Tfinal
 %%   
    %
    % Kstep
    %
    % Integrate the system dK/dt = F(t,K(t) V0)^T)V0
    K0 = U0*S0;
    Drhs = kfunrhs(U0,S0,V0,H,pop,gamma,g); 
    %Drhs = -iunit*H*K0 +iunit*K0*(V0'*(H*V0));
    
    %euler function
    K1 = K0 + dt * Drhs;
    fprintf(1,'dk trace = %e\n',trace(Drhs*V0'));
   
    %%
    % Adjust the rank
    [Uhat,~] = qr([K1,U0],0);
    M = Uhat'*U0;
    
    %
    % Lstep
    %
    % Integrate the system dL/dt = F(t,U0 L^T(t))^T U0
    L0 = V0*S0';
    Drhs = lfunrhs(U0,S0,V0,H,pop,gamma,g);
    %Drhs = -iunit*V0*(S0'*((U0'*H')*U0))+iunit*H'*(V0*S0');
    L1 = L0 + dt * Drhs;
    fprintf(1,'dl trace = %e\n',trace(U0*Drhs'));

    %%
    % Ajust rank
    [Vhat,~] = qr([L1,V0],0);
    N = Vhat'*V0;
    
    %%
    % Update S
    %
    %Shat0 = M*S0*N';
    %Drhs = rhsfun(t,Uhat*Shat0*Vhat',H);
    %S1 = Shat0 + dt * Uhat'*Drhs*Vhat;
    
    Shat0 = M*S0*N';
    Drhs = sfunrhs(Uhat,Shat0,Vhat,H,pop,gamma,g);
    %Drhs = -iunit*(Uhat'*(H*Uhat))*Shat0 + iunit*(Shat0*Vhat')*(H*Vhat);
    S1 = Shat0 + dt * Drhs;
    fprintf(1,'ds trace = %e\n',trace(Drhs));
    %
    % Get a new rank
    %[r1,P,Sigma,Q] = rank_truncation_sparse(S1,tol,1,10);
    [r1,P,Sigma,Q] = rank_truncation(S1,tol,1);

    %
    % Set new approximation spaces
    U0 = Uhat*P;
    V0 = Vhat*Q;
    S0 = Sigma;
    fprintf(1,'rho trace = %e\n\n',trace(U0*S0*V0'));
 
    if rem(it,prtfreq) == 0
        Y1 = U0*S0*V0';
        %fprintf(1,'[%d] t= %5.4f rank r=%d trace = %e\n',it,t,r1,...
        %    trace(Y1));
        tr = trace_sepformat(U0,S0,V0);
        fprintf(1,'[%d] t= %5.4f rank r=%d Tr=%5.4f\n',it,t,r1,tr);
        sz1meas(ind) = trace(Y1 * Sz0);
        sz2meas(ind) = trace(Y1 * Sz1);
        tmeas(ind) = t;
        ind=ind+1;
    end
    %
    % Time Step
    T(it) = t;
    t = t+dt;    
    it = it +1;
    
end

toc
%}
fprintf(1,'Solver finished, run ProcLind.m for plotting\n');
