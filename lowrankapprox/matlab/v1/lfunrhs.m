function Drhs = lfunrhs(U,S,V,H,Li,g1,g2)
%
% Evaluates right hand side of the equation
%
% Integrate the system 
% dL/dt = F(t,U L^T(t))^T U
%
% U,S,V - rho in separated approx
% H     - Hamiltonian
% Li    - Lindbladians
%
flag_lindblad=1;

iunit = 1i;
L = V*S';
[n,r]=size(L);
Drhs=zeros(n,r);
Drhs = -iunit*L*((U'*H)*U)+iunit*H*L;

if flag_lindblad
%
% First Lindblad term
term1 = zeros(n,r);
for ind = 1:8
    si = num2str(ind-1);
    term1 = term1 + g1 * ((Li.("szp"+si))'*(L*(U'*(Li.("szm"+si)*U))') ...
          - 0.5*(L*((U'*Li.("szmp"+si))*U)' + (Li.("szmp"+si))'*L));
    term1 = term1 + g1 * ((Li.("szm"+si))'*(L*(U'*(Li.("szp"+si)*U))')...
          - 0.5*(L*((U'*Li.("szpm"+si))*U)' + (Li.("szpm"+si))'*L));
 end
Drhs = Drhs + term1;
%
% Second Lindblad term
term1 = zeros(n,r);
for ind = 1:7
    si = num2str(ind-1);
    sj = num2str(ind);
    term1 = term1 + g2*((Li.("szp"+sj))'*(L*(U'*(Li.("szm"+si)*U))') ...
           - 0.5*(L*((U'*Li.("szmp"+si+sj))*U)' + (Li.("szmp"+si+sj))'*L));
    term1 = term1 + g2*((Li.("szm"+sj))'*(L*(U'*(Li.("szp"+si)*V))') ...
           - 0.5*(L*((U'*Li.("szpm"+si+sj))*U)' + (Li.("szpm"+si+sj))'*L));
end
Drhs = Drhs + term1;
end

end