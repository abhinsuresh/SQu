function [r,P,Sigma,Q] = rank_truncation_sparse(A,tol,truncate,nsingval)
%
% find the rank of approximation ||A - Approx|| < tol
%
[m,n] = size(A);
if truncate
    [P,Sigma,Q] = svds(A,nsingval);
    s = diag(Sigma);
else
    s = svds(A,nsingval);
    P = [];
    Sigma = [];
    Q = [];
end
srem = sqrt(cumsum(s.^2,'reverse'));
isx = find(srem>tol);
if ~isempty(isx)
    r = isx(end);
else
    r=n;
end
if truncate
    P = P(:,1:r);
    Q = Q(:,1:r);
    Sigma = Sigma(1:r,1:r);
end

end

