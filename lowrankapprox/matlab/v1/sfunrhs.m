function Drhs = sfunrhs(U,S,V,H,Li,g1,g2)
%
% Evaluates right hand side of the equation
%
% Integrate the system 
% dShat/dt = Uhat^T F(t,Uhat Shat(t) Vhat^T) Vhat
%
% U,S,V - rho in separated approx
% H     - Hamiltonian
% Li    - Lindbladians
%
flag_lindblad=1;

iunit = 1i;
[n,r]=size(S);
Drhs=zeros(n,r);
Drhs = -iunit*((U'*H)*U)*S + iunit*S*(V'*(H*V));

if flag_lindblad
%
% First Lindblad term
term1 = zeros(n,r);
for ind = 1:8
    si = num2str(ind-1);
    term1 = term1 + g1 * (U'*(Li.("szp"+si)*U)*S*(V'*(Li.("szm"+si)*V))) ...
          - 0.5*g1*(((U'*Li.("szmp"+si))*U)*S + S*((V'*Li.("szmp"+si))*V));
    term1 = term1 + g1 * (U'*(Li.("szm"+si)*U)*S*(V'*(Li.("szp"+si)*V)))...
          - 0.5*g1*(((U'*Li.("szpm"+si))*U)*S + S*((V'*Li.("szpm"+si))*V));
 end
Drhs = Drhs + term1;
%
% Second Lindblad term
term1 = zeros(n,r);
for ind = 1:7
    si = num2str(ind-1);
    sj = num2str(ind);
    term1 = term1 + g2*(U'*(Li.("szp"+sj)*U)*S*(V'*(Li.("szm"+si)*V))) ...
           - 0.5*g2*(((U'*Li.("szmp"+si+sj))*U)*S + S*((V'*Li.("szmp"+si+sj))*V));
    term1 = term1 + g2*(U'*(Li.("szm"+sj)*U)*S*(V'*(Li.("szp"+si)*V))) ...
           - 0.5*g2*(((U'*Li.("szpm"+si+sj))*U)*S + S*((V'*Li.("szpm"+si+sj))*V));
end
Drhs = Drhs + term1;
end

end