clc;
clear all;

H_ = load('H.mat');
op_ = load('op.mat');
rho_ = load('rho.mat');
pop_ = load('pop.mat');

gamma = 0;
g = 0;
tic;

%H = sparse(full(H_.H));
%rho = sparse(full(rho_.rho));


H = H_.H;
rho = rho_.rho;
%save('H.mat')

Sz0 = op_.sz0;
Sz1 = op_.sz1;


%{
a = fieldnames(op_);
b = fieldnames(pop_);
for i = 1:length(a)
    op.(a{i}) = sparse(full(op_.(a{i})));
end
for i = 1:length(b)
    pop.(b{i}) = sparse(full(pop_.(b{i})));
end
save('op.mat','-struct', 'op');
save('pop.mat','-struct','pop');
%}


%checking some ground state properties
trace(rho * Sz0)
trace(rho * Sz1)
trace(rho * H)


%time evolution
tspan = [0 70];

%
%calling the ode45 integrator by reshaping the matrix into a vector
[trho, rho_vec] = ode45(@(trho, rho) rk4_rho(trho, rho, H, pop_, gamma, g), tspan, rho);

%measurement of spin expectation values of first and 2nd site
sz1 = zeros(size(trho));
sz2 = zeros(size(trho));
for ind = 1: length(trho)
    sz1(ind) = trace(reshape(rho_vec(ind,:),[256,256]) * Sz0);
    sz2(ind) = trace(reshape(rho_vec(ind,:),[256,256]) * Sz1);
end
%plotting the results
plot(trho, sz1, '-o')
hold on;
plot(trho, sz2, '-o')
toc

%reshaping function and calling rk4_solver
function drho = rk4_rho(trho, rho, H, pop_, gamma, g)
    rho_m = reshape(rho, [256,256]);
    drho_m = rk4_solver(trho, rho_m, H, pop_, gamma, g);
    drho = reshape(drho_m, [65536,1]);

end
%rk4_solver (currently lindblad turned off, uncomment the two terms to apply full terms)
function rho_prime = rk4_solver(trho, rho_m, H, pop_, gamma, g)
    rho_prime = -1j*(H*rho_m - rho_m*H);% + lindblad_1(rho_m, pop_, gamma) + lindblad_2(rho_m, pop_, g);
 
end
%lindblad term1
function term1 = lindblad_1(rho, pop_, gamma)
    term1 = 0*speye(length(rho));
    for ind = 1:8
        si = num2str(ind-1);
        term1 = term1 + gamma * (pop_.("szp"+si)* rho *pop_.("szm"+si) ...
                        - 0.5*(pop_.("szmp"+si)*rho + rho*pop_.("szmp"+si)));
        term1 = term1 + gamma * (pop_.("szm"+si)* rho * pop_.("szp"+si)...
                        - 0.5*(pop_.("szpm"+si)*rho + rho*pop_.("szpm"+si)));
    end
end
%lindblad term2
function term2 = lindblad_2(rho, pop_, g)
    term2 = 0*speye(length(rho));
    for ind = 1:7
        si = num2str(ind-1);
        sj = num2str(ind);
        term2 = term2 + g * (pop_.("szp"+sj)* rho * pop_.("szm"+si) ...
                        - 0.5*(pop_.("szmp"+si+sj)*rho + rho*pop_.("szmp"+si+sj)));
        term2 = term2 + g * (pop_.("szm"+sj)* rho * pop_.("szp"+si) ...
                        - 0.5*(pop_.("szpm"+si+sj)*rho + rho*pop_.("szpm"+si+sj)));
    end
end

%}






