"""Python program to convert ris passed as an argument to bib format
which can be copied into the qttg.bib file, the jounral name might 
be in full format and can be editted later to the abbrevation
$python ris_tocite.py ref.ris
output -> citationkey.bib file in the same directory
"""
import sys
from ref_wrapper import *
jf_flag = True
verbose = False
pe_flag = False   # include page end if it exists
get_alist = False
get_bibfile = False
get_citetxt = True
#--------------------------------------------------------------------
if sys.argv[1].endswith('.ris') or sys.argv[1].endswith('.RIS'):
    if verbose: print('ris file')
else: 
    print('not ris file, only accepts .ris file as input'); exit()
with open(sys.argv[1], 'r') as fname:
    data = fname.read().split('\n')
    #if verbose: print('splitting based on \\n')

info = extract_from_ris(data)
#out, key = get_bibitem(info)
#print(info)
print(get_bibitem(info))

"""
#--------------------------------------------------------------------
#--------------------------------------------------------------------

#--------------------------------------------------------------------
#creating author list for new Phys Rev B standards
#print(len(info['author']))
#print(info)
out = ""
for author in info['author'][:10]:
    l_name, f_name = author.split(',')
    f_name = f_name.strip()
    l_name = l_name.strip()
    #print(f_name)
    if len(f_name.split(" ")) > 1:
        for name in f_name.split(" "):
            out += name[0]+'.~'
    elif len(f_name)>0: 
        out += f_name+'.~'
        out = out[:-1] + ' '
        out += l_name + ', '
    elif len(f_name)==0 and len(l_name)>1:
        for name in l_name.split(" ")[:-1]:
            out += name[0]+'.~'
        out += l_name.split(" ")[-1] + ', '
    print(out)
if len(info['author']) > 10:
    out += "{\em et al.}"
else: print("need to add and before the last author")
if get_alist:
    with open(info['author'][0].split(',')[0]+info['year']+".txt", "w") as fout:
        fout.write(out)
#print(out)

from get_bbl import *    
out = ""
key=info['author'][0].split(',')[0]+info['year']
info = generate_dict(key, bib_text=out_bib)
#print(info)
if info is not None: 
    info["journal"] = get_abbr(info["journal"])
    out += generate_bibitem(info, key) + "\n\n"
if get_citetxt:
    with open("add.txt", "a") as fout:
        fout.write(out)

"""
