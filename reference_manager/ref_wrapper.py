def extract_from_ris(data, jf_flag=True, pe_flag=False, verbose=False):
    """Extract info dict from ris file
    Parameters
    ----------
    data: string
        This is RIS file split by line '\n'
    jf_flag: bool. 
            Default is True
    pe_flag: bool
        Include page end. Default is False
    Returns
    -------
    info: which has key to each element to fill the
          Converts to author format: last name, first name ini. middle name ini.
    """
    info = dict()
    info['author'] = []
    for string in data:
        if string.startswith('AU') or string.startswith('A1'):
            author = string[6:]
            out = ""
            l_name, f_name = author.split(',')
            f_name = f_name.strip()
            l_name = l_name.strip()
            if len(f_name) == 0:
                #format from IOPScience
                out += l_name.split(" ")[-1] + ', '
                for name in l_name.split(" ")[:-1]:
                    out += name[0] +'. '
                author = out
                #print(author)
            info['author'].append(author)
        if string.startswith('VL'):
            info['volume'] = string[6:]
        if string.startswith('SP'):
            info['page_start'] = string[6:]
        if string.startswith('EP'):
            info['page_end'] = string[6:]
        if string.startswith('TI') or string.startswith('T1'):
            info['title'] = string[6:]
        if string.startswith('PY') or string.startswith('Y1') or string.startswith('DA'):
            info['year'] = string[6:][:4] 
        if 'year' not in info.keys() and string.startswith('Y2'):
            info['year'] = string[6:][:4] 
        if string.startswith('JA'):
            info['journal'] = string[6:]
            jf_flag = False      # if abbrevation exist use that, else use full and modify later
        if (string.startswith('JO') or string.startswith('JF') or
            string.startswith('T2')) and jf_flag:
            info['journal'] = get_abbr(string[6:])
        if string.startswith('UR'):
            info['url'] = string[6:]
        if string.startswith('DO'):
            info['doi'] = string[6:]
    
    info['key'] = info['author'][0].split(',')[0]+info['year']
    #--------------------------------------------------------------------
    #testing if all required items were extracted from ris file
    if all(item in info.keys() for item in ['author','title','journal',
                                            'volume','page_start','year']):
        if verbose: print('All relevant details transferred')
        return info
    else:
        print('Some missing information to complete the bib file \
               key for author,title,journal,volume,page,year not found or \
               in a differernt two letter code')
        return None

def get_bibitem(info, href=True):
    """
    Parameters
    ----------
    info: output from ris_to_cite
         dict file with keys to cite
    href: bool. 
         Default is True
    Returns
    -------
    out: string file to copied into the paper tex file
         Converts to author format: last name, first name ini. middle name ini.
    """
    author_type = 1
    if "url" not in info.keys(): href= False
    out = r"\bibitem{"+info['key']+"}\n"
    a_len = len(info["author"])
    a_end = -1
    if a_len > 10: a_end = 9
    if len(info["author"][0].split(",")) > 1: author_type = 2

    if author_type == 1:
        #author format "first-name middle-name last-name"
        for author in info["author"][:a_end]:
            for name in author.split(" ")[:-1]:
                out += name[0] + ".~"
            out += author.split(" ")[-1] + ", "

        #no comma if only two authors
        if a_len == 2:
            out = out[:-2] + " "

        #adding last author after and
        out = out + "and "
        author = info["author"][a_end]
        for name in author.split(" ")[:-1]:
            out += name[0] + ".~"
        out += author.split(" ")[-1] + ", "
        if a_end ==9:
            out = out[:-2] + " "
            out = out + "\emph{et al}, "
    

    if author_type == 2:
        #author format "last-name, first-name middle-name"
        for author in info["author"][:a_end]:
            for name in author.split(",")[1:]:
                for sub_name in name.strip().split(" "):
                    out += sub_name[0] + ".~"
            out += author.split(",")[0] + ", "

        #no comma if only two authors
        if a_len == 2:
            out = out[:-2] + " "

        #adding last author after and
        out = out + "and "
        author = info["author"][a_end]
        for name in author.split(",")[1:]:
            for sub_name in name.strip().split(" "):
                out += sub_name[0] + ".~"
        out += author.split(",")[0] + ", "
        if a_end ==9:
            out = out[:-2] + " "
            out = out + "\emph{et al}, "
    
    #adding title 
    out += info["title"] + ", "
    if not href: out += info["journal"] + " "
    if href: out +=r"\href{"+ info["url"] +"}{"+ info["journal"] +" "
    if "volume" in info.keys(): out += r"{\bf " + info["volume"] + r"}" + ", "
    if "page_start" in info.keys(): out += info["page_start"].split(" ")[0] + " "
    out += r"(" + info["year"] + r")"
    if href: out += "}."
    else: out +="."
    
    return out

def get_abbr(journal, verbose=False):
    abbr_list = open("JAbbrPhysics.csv","r").read().split("\n")
    jour = [item.split(";")[0] for item in abbr_list[:-1]]
    abbr = [item.split(";")[1] for item in abbr_list[:-1]]
    #print(jour[:5], abbr[:5])
    if not (journal in jour or journal in abbr):
        print("!!! name and abbr not found for: "+ journal +"!!!")
        return journal
    elif journal in jour:
        if verbose: print(journal, "->", abbr[jour.index(journal)])
        return abbr[jour.index(journal)]
    else: return journal


def generate_bibfile_item(info, verbose=False, write_tofile=False): 
    #creating the output bib
    out_bib = "@Article{" + info['author'][0].split(',')[0] + info['year'] + ",\n"
    out_bib = out_bib + "  author\t= {" 
    for author in info['author']:
        out_bib = out_bib + author + " and "
    out_bib = out_bib[:-5]+"},\n"
    out_bib = out_bib + "  title\t\t= {" + info['title'] + "},\n"
    out_bib = out_bib + "  journal\t= {" + info['journal'] + "},\n"
    out_bib = out_bib + "  volume\t= {" + info['volume'] + "},\n"
    out_bib = out_bib + "  pages\t\t= {" + info['page_start'] 
    if "page_end" in info.keys() and pe_flag: out_bib += "-" + info['page_end'] + "},\n"    
    else: out_bib += "},\n"
    out_bib = out_bib + "  year\t\t= {" + info['year'] + "},\n"
    if 'url' in info.keys(): out_bib = out_bib + "  url\t\t= {" + info['url'] + "},\n"
    if 'doi' in info.keys(): out_bib = out_bib + "  doi\t\t= {" + info['doi'] + "},\n"
    out_bib = out_bib + "}"
    key = info['author'][0].split(',')[0]+info['year']
    if verbose: print("key: ", key)
    if write_tofile:
        with open(key+".bib", "w") as fout:
            fout.write(out_bib)
    return out_bib, key
