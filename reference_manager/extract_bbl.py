import sys
from get_bbl import *

key_list = get_cite_list(sys.argv[1])
#print(key_list)
#key = key_list[25]
#info = generate_dict(key)
#print(info["journal"])
#get_abbr(info["journal"])
#out = generate_bibitem(info)
#print(out)

out = ""
for key_ind, key in enumerate(key_list):
    #print(key_ind, key)
    info = generate_dict(key)
    if info is not None: 
        info["journal"] = get_abbr(info["journal"])
        out += generate_bibitem(info, key) + "\n\n"

fout = open("out.bbl", "w")
fout.write(out)
fout.close()
