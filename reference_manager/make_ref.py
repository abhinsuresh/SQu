"""Python program to convert ris passed as an argument to bib format
which can be copied into the qttg.bib file, the jounral name might 
be in full format and can be editted later to the abbrevation
$python ris_tocite.py ref.ris
output -> citationkey.bib file in the same directory
"""
import sys
from ref_wrapper import *
verbose = False
#--------------------------------------------------------------------
if sys.argv[1].endswith('.ris') or sys.argv[1].endswith('.RIS'):
    if verbose: print('ris file')
else: 
    print('not ris file, only accepts .ris file as input'); exit()
with open(sys.argv[1], 'r') as fname:
    data = fname.read().split('\n')
    #if verbose: print('splitting based on \\n')
info = extract_from_ris(data)
#out, key = get_bibitem(info)
#print(info)
out = get_bibitem(info)
with open("add.txt", "a") as fout:
    fout.write(out + "\n\n")

