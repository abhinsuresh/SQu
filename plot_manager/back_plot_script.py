#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
from rcparams import rc_update
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MaxNLocator

val = dict();val['x_plot']='x_plot';val['y_plot']='y_plot'
for i in range(10): 
    val['ylabel'+str(i+1)]=r'$\mathrm{f(x)}$'
    val['xlabel'+str(i+1)]=r'$\mathrm{x}$'
Cl = ['k', 'C0', 'C2', 'C3', 'C4', 'C5', 'C6']*100
Ls = ['-', '-', '-', '-', '-', '-', '-']*100
Mk = ['', '', '', '', '', '', '']*100
Ps = [1]*500; Pf = [None]*500; Pi = [0]*500
Tx = ['']*100
Lg = ['']*100
Alpha = [1]*500
sf = np.array([[0,1],[1,2],[2,3],[3,4],[5,6],[7,8],[9,10],[11,12]])
Gs1p = dict(); Gs2p = dict(); Gs4p = dict()
Gs1p['t']=0.9; Gs1p['b']=0.2; Gs1p['l']=0.3; Gs1p['r']=0.7
Gs1p['w']=0.4; Gs1p['h']=0.4
Gs2p['t']=0.9; Gs2p['b']=0.2; Gs2p['l']=0.15; Gs2p['r']=0.98
Gs2p['w']=0.4; Gs2p['h']=0.4
Gs4p['t']=0.95; Gs4p['b']=0.1; Gs4p['l']=0.15; Gs4p['r']=0.98
Gs4p['w']=0.45; Gs4p['h']=0.35
Lpx = [1,1,1,1]*100
Lpy = [1,1,1,1]*100
Lg = ['','']*10

#------------------------------------------------------------
#------------------------------------------------------------

def plotfig(data, fig_name, values=val,
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim = None, Cl=Cl, Ls=Ls, Mk=Mk, Ps=Ps, Pf=None, ylog=0, 
            Tx=Tx, Gs=Gs1p, Lg=Lg, sf=sf, Alpha=Alpha, ft='.pdf',
            errorbar=False, **kwargs):		

    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    values = {**val, **values}
    #------------------------------------------------------------
    f = data
    plot_x = f[values['x_plot']]; plot_y = f[values['y_plot']]
    if Pf==None: Pf = [len(plot_x)]*500
    if len(plot_y.shape) == 1:
        plot_y = plot_y.reshape((-1,1))
    #print(plot_x, plot_y)
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 1, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0])
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel1'], labelpad=1)
    ax1.set_ylabel(values['ylabel1'], labelpad=3)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    #Alpha = [1,1,1,0.7,0.7,0.7]
    for i in range(sf[0,0], sf[0,1]):
        ax1.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i], label=Lg[i], alpha=Alpha[i])
    #i=2
    #ax1.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
    #marker=Mk[i], color=Cl[i], ls=Ls[i], label=Lg[i], alpha=Alpha[i])
    
    #ax1.plot(plot_x, plot_y, marker=Mk[i], color=Cl[i], ls=Ls[i], label=Lg[i])
    #ax1.plot(f['x_plot2'], f['y_plot2'],color='C2', label=Lg[4])
    if ax_twin:
        ax2 = ax1.twinx()
        i = 1
        ax2.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i], label=Lg[i])
    if errorbar: 
        for i in range(sf[0,0], sf[0,1]):
            j = i + sf[0,1]
            """
            ax1.fill_between(plot_x[Pi[i]:Pf[i]:Ps[i]], 
                             plot_y[:,i][Pi[i]:Pf[i]:Ps[i]] - plot_y[:,j][Pi[j]:Pf[j]:Ps[j]],
                             plot_y[:,i][Pi[i]:Pf[i]:Ps[i]] + plot_y[:,j][Pi[j]:Pf[j]:Ps[j]],
                             color=Cl[j], ls=Ls[j], alpha=Alpha[j])
            """
            ax1.errorbar(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
                         yerr = plot_y[:,j][Pi[j]:Pf[j]:Ps[j]],
                         color=Cl[j], ls=Ls[j], alpha=Alpha[j])
        
    #------------------------------------------------------------
    if legend:
        ax1.legend(bbox_to_anchor=(1, 0.84), ncol=2,frameon=False,
                handlelength = 0.7,columnspacing = 0.5, fontsize=6,
                loc='lower right') 
    #ax1.text(0.2,0.99, r'$\mathrm{|\mathbf{k}|=\pi/2}$',color='k',transform=ax1.transAxes, va ='top', fontsize=7)
    #ax1.text(0.6,0.99, r'$\mathrm{|\mathbf{k}|=\pi/6}$',color='k',transform=ax1.transAxes, va ='top', fontsize=7, alpha=0.8)
    #------------------------------------------------------------
    if xlim is not None:
        ax1.set_xlim(xlim) 
    if ylim is not None:
        ax1.set_ylim(ylim)
    if ylog: ax1.set_yscale('log')
    #ax1.set_xticks([64, 256, 1024])
    #------------------------------------------------------------
    plt.savefig('results/fig_' + fig_name + ft)

#------------------------------------------------------------
#------------------------------------------------------------

def plotfig_2p(data, fig_name, values=val,
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim=None, Cl=Cl, Ls=Ls, Mk=Mk, Ps=Ps, Pi=Pi, Pf=Pf, 
            Tx=Tx, Gs=Gs2p, Lg=Lg, sf=sf, errorbar=False, 
            Lpx=Lpx, Lpy=Lpy, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    values = {**val, **values}
    #------------------------------------------------------------
    f = data; fs=8#fs = kwargs['fs']
    plot_x = f[values['x_plot']]; plot_y = f[values['y_plot']]
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 2, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0]); ax2 = plt.subplot(gs[0,1])
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel1'], labelpad=1)
    ax1.set_ylabel(values['ylabel1'], labelpad=3)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    alpha = [1, 1]*100
    ax1.xaxis.set_major_locator(MaxNLocator(4)) 
    ax2.xaxis.set_major_locator(MaxNLocator(4)) 
    for i in range(sf[0,0], sf[0,1]):
        ax1.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i], label=Lg[i])
    #------------------------------------------------------------
    ax2.set_xlabel(values['xlabel2'], labelpad=1)
    ax2.set_ylabel(values['ylabel2'], labelpad=3)
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    for i in range(sf[1,0], sf[1,1]):
        ax2.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i], label=Lg[i])
    #plot_x1 = f['x_plot1']; plot_y1 = f['y_plot1']
    #ax2.plot(plot_x1, plot_y1[:,0], marker=Mk[i], color='k', ls=Ls[i], alpha=0.3)
    #ax2.plot(plot_x1, plot_y1[:,1], marker=Mk[i], color='k', ls=Ls[i])
    #ax2.set_xlim([0,1])
    #------------------------------------------------------------
    if ax_twin: 
        ax3 = ax2.twinx()
        ax3.set_xlabel(values['xlabel3'], labelpad=1)
        ax3.set_ylabel(values['ylabel3'], labelpad=3)
        ax3.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
        for i in range(sf[2,0], sf[2,1]):
            ax3.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
            marker=Mk[i], color=Cl[i], ls=Ls[i])
        ax2.set_ylim([0.8,1]); ax3.set_ylim([-1,-0.8])
    #------------------------------------------------------------
    if xlim is not None:
        ax1.set_xlim(xlim); ax2.set_xlim(xlim) 
    if ylim is not None:
        ax1.set_ylim(ylim); ax2.set_ylim([0,4.3e-1]); 
    #------------------------------------------------------------
    x = 0.89; y = 1.13
    ax1.text(x, y, r'$\mathrm{(a)}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{(b)}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    if legend:
        ax1.legend(bbox_to_anchor=(1, 0), ncol=1,frameon=False,
                handlelength = 0.7,columnspacing = 0.5, fontsize=6,
                loc='lower right') 
    #ax1.text(0.68,0.99, r'$\mathrm{q=\pi/2}$',color='k',transform=ax1.transAxes, va ='top', fontsize=7)
    #ax2.text(0.69,0.99, r'$\mathrm{q=\pi/6}$',color='k',transform=ax2.transAxes, va ='top', fontsize=7)
    y1 =0.7; ys=0.13
    #ax1.text(0.23,y1, r"$-U'=J_{\rm H}=\gamma=0$",color='k',transform=ax1.transAxes, va ='top', fontsize=7)
    #ax1.text(0.52,y1-ys, r"$J=0.1~{\rm eV}$",color='k',transform=ax1.transAxes, va ='top', fontsize=7)
    #ax1.text(0.25,y1-2*ys, r"$g\mu_{\rm B}B_z=10^{-3}~{\rm eV}$",color='k',transform=ax1.transAxes, va ='top', fontsize=7)
    #ax1.text(0.47,0.85, r"$-t_{\rm SO}=0~{\rm eV}$",color='k',transform=ax1.transAxes, va ='top', fontsize=7)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + fig_name + '.pdf')

#------------------------------------------------------------
#------------------------------------------------------------

def plotfig_4p(data, fig_name, values=val, text=False, legend=False,
               xlim=None, ax_twin=False, ylim=None, Cl=Cl, Ls=Ls, 
               Mk=Mk, Pi=Pi, Ps=Ps, Pf=Pf, Tx=Tx, Gs=Gs4p, Lg=Lg, 
               sf=sf, Lpx=Lpx, Lpy=Lpy, errorbar=False, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, np_v=2, **kwargs)
    values = {**val, **values}
    #------------------------------------------------------------
    f = data; fs = kwargs['fs']
    plot_x = f[values['x_plot']]; plot_y = f[values['y_plot']]
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(2, 2, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0]); ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[1,0]); ax4 = plt.subplot(gs[1,1])
    rm_ax = 0; nticks = 3; nyticks = 3
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel1'], labelpad=Lpx[0]); 
    ax1.set_ylabel(values['ylabel1'], labelpad=Lpy[0])
    ax1.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax1.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    if rm_ax: ax1.set_xticks([])
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    for i in range(sf[0,0], sf[0,1]):
        ax1.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax2.set_xlabel(values['xlabel2'], labelpad=Lpx[1]);
    ax2.set_ylabel(values['ylabel2'], labelpad=Lpy[1])
    ax2.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax2.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    if rm_ax : ax2.set_xticks([]); ax2.set_yticks([])
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    for i in range(sf[1,0], sf[1,1]):
        ax2.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax3.set_xlabel(values['xlabel3'], labelpad=Lpx[2]);
    ax3.set_ylabel(values['ylabel3'], labelpad=Lpy[2])
    ax3.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax3.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    ax3.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    for i in range(sf[2,0], sf[2,1]):
        ax3.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax4.set_xlabel(values['xlabel4'], labelpad=Lpx[3]);
    ax4.set_ylabel(values['ylabel4'], labelpad=Lpy[3])
    ax4.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax4.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    if rm_ax: ax4.set_yticks([])
    ax4.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    for i in range(sf[3,0], sf[3,1]):
        ax4.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    x = 0.01; y = 0.97
    ax1.text(x, y, r'$\mathrm{(a)}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{(b)}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    ax3.text(x, y, r'$\mathrm{(c)}$',color='k',transform=ax3.transAxes, va ='top', fontsize=fs)
    ax4.text(x, y, r'$\mathrm{(d)}$',color='k',transform=ax4.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    if 0:
        ax1.set_ylim((-1.5,1.5)); ax2.set_ylim((-1.5,1.5))
        ax3.set_ylim((0,1.3));
        #ax5.set_ylim((-0.75,0.75))
        ax1.set_xticklabels([]); ax2.set_xticklabels([])
    if xlim is not None:
        ax1.set_xlim(xlim); ax3.set_xlim(xlim) 
        ax2.set_xlim(xlim); ax4.set_xlim(xlim) 
    if ylim is not None:
        ax1.set_ylim(ylim)
    if text:
        ax1.text(0.5, 1.15, Tx[0], color='k', transform=ax1.transAxes, va ='top', fontsize=fs)
        ax2.text(0.5, 1.15, Tx[1], color='k', transform=ax2.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    #------------------------------------------------------------
    plt.savefig('results/fig_' + fig_name + '.pdf')


#------------------------------------------------------------
#------------------------------------------------------------

def plotfig_8p(data, fig_name, values=val,
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim=None, Cl=Cl, Ls=Ls, Mk=Mk, Pi=Pi, Ps=Ps, Pf=Pf, 
            Tx=Tx, Gs=Gs2p, Lg=Lg, sf=sf, Lpx=Lpx, Lpy=Lpy,
            errorbar=False, plot_pulse=0, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, np_v=2, np_h=2, **kwargs)
    values = {**val, **values}
    number = fig_name
    #------------------------------------------------------------
    f = data
    fs = kwargs['fs']
    plot_x = f[values['x_plot']]
    plot_y = f[values['y_plot']]
    xlabel1 = values['xlabel1']; xlabel2 = values['xlabel2']
    ylabel1 = values['ylabel1']; ylabel2 = values['ylabel2']
    xlabel3 = values['xlabel3']; xlabel4 = values['xlabel4']
    ylabel3 = values['ylabel3']; ylabel4 = values['ylabel4']
    xlabel5 = values['xlabel5']; xlabel6 = values['xlabel6']
    ylabel5 = values['ylabel5']; ylabel6 = values['ylabel6']
    xlabel7 = values['xlabel7']; xlabel8 = values['xlabel8']
    ylabel7 = values['ylabel7']; ylabel8 = values['ylabel8']
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(2, 4, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0]); ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[1,0]); ax4 = plt.subplot(gs[1,1])
    ax5 = plt.subplot(gs[0,2]); ax6 = plt.subplot(gs[0,3])
    ax7 = plt.subplot(gs[1,2]); ax8 = plt.subplot(gs[1,3])
    rm_ax = 1; nticks = 4
    #------------------------------------------------------------
    ax1.set_xlabel(xlabel1, labelpad=Lpx[0]); 
    ax1.set_ylabel(ylabel1, labelpad=Lpy[0])
    ax1.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[0,0], sf[0,1]):
        ax1.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], 
        color=Cl[i], ls=Ls[i])
    if plot_pulse:
        ylim = ax1.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*1.3 + (ylim[1]-ylim[0])*0.8
        ax1.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C1')
        ax1.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    ax2.set_xlabel(xlabel2, labelpad=Lpx[1]);
    ax2.set_ylabel(ylabel2, labelpad=Lpy[1])
    ax2.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[1,0], sf[1,1]):
        ax2.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    if plot_pulse:
        ylim = ax2.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*1.3 + (ylim[1]-ylim[0])*0.8
        ax2.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C1')
        ax2.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    ax3.set_xlabel(xlabel3, labelpad=Lpx[2]);
    ax3.set_ylabel(ylabel3, labelpad=Lpy[2])
    ax3.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax3.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    for i in range(sf[2,0], sf[2,1]):
        ax3.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    ax3.yaxis.set_major_locator(MaxNLocator(4)) 
    if plot_pulse:
        ylim = ax3.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*1.3 + (ylim[1]-ylim[0])*0.8
        ax3.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C1')
        ax3.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    ax4.set_xlabel(xlabel4, labelpad=Lpx[3]);
    ax4.set_ylabel(ylabel4, labelpad=Lpy[3])
    ax4.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax4.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[3,0], sf[3,1]):
        ax4.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    ax4.yaxis.set_major_locator(MaxNLocator(3)) 
    if plot_pulse:
        ylim = ax4.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*1.3 + (ylim[1]-ylim[0])*0.8
        ax4.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C1')
        ax4.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    ax5.set_xlabel(xlabel5, labelpad=Lpx[4]);
    ax5.set_ylabel(ylabel5, labelpad=Lpy[4])
    ax5.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax5.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[4,0], sf[4,1]):
        ax5.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    if plot_pulse:
        ylim = ax5.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*1.3 + (ylim[1]-ylim[0])*0.8
        ax5.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C1')
        ax5.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    ax6.set_xlabel(xlabel6, labelpad=Lpx[5]);
    ax6.set_ylabel(ylabel6, labelpad=Lpy[5])
    ax6.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax6.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[5,0], sf[5,1]):
        ax6.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    if plot_pulse:
        ylim = ax6.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*1.3 + (ylim[1]-ylim[0])*0.8
        ax6.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C1')
        ax6.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    ax7.set_xlabel(xlabel7, labelpad=Lpx[6]);
    ax7.set_ylabel(ylabel7, labelpad=Lpy[6])
    ax7.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax7.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[6,0], sf[6,1]):
        ax7.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    if plot_pulse:
        ylim = ax7.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*1.3 + (ylim[1]-ylim[0])*0.8
        ax7.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C1')
        ax7.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    ax8.set_xlabel(xlabel8, labelpad=Lpx[7]);
    ax8.set_ylabel(ylabel8, labelpad=Lpy[7])
    ax8.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax8.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[7,0], sf[7,1]):
        ax8.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    if plot_pulse:
        ylim = ax8.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*1.3 + (ylim[1]-ylim[0])*0.8
        ax8.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C1')
        ax8.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    if xlim is not None:
        ax1.set_xlim(xlim); ax2.set_xlim(xlim) 
        ax3.set_xlim(xlim); ax4.set_xlim(xlim) 
        ax5.set_xlim(xlim); ax6.set_xlim(xlim) 
        ax7.set_xlim(xlim); ax8.set_xlim(xlim)
    if rm_ax: 
        ax1.set_xticklabels([]); ax2.set_xticklabels([])
        ax5.set_xticklabels([]); ax6.set_xticklabels([])
    #------------------------------------------------------------
    if legend:
        #ax1.plot([],[],color='k',ls='-',label=r'$\mathrm{N_e = 1}$')
        #ax1.plot([],[],color='k',ls='--',marker=Mk[1], label=r'$\mathrm{Ne = 3}$')
        ax1.plot([],[],color='k',ls='-',marker=Mk[0],label=Lg[0])
        ax1.plot([],[],color='k',ls='',marker=Mk[1],label=Lg[1])
        ax1.legend(bbox_to_anchor=(0.8, 0.15), ncol=1,frameon=False,
                handlelength = 0.5,columnspacing = 0.2, fontsize=6,
                loc='center') 
        ax5.plot([],[],color='k',ls='-',marker=Mk[0],label=Lg[2])
        ax5.plot([],[],color='k',ls='',marker=Mk[-1],label=Lg[3])
        ax5.legend(bbox_to_anchor=(0.76, 0.15), ncol=1,frameon=False,
                handlelength = 0.5,columnspacing = 0.2, fontsize=6,
                loc='center') 
    #------------------------------------------------------------
    if text:
        ax1.text(0.01, 1.1, Tx[0], color='k', transform=ax1.transAxes, va ='top', fontsize=fs)
        ax2.text(0.01, 1.1, Tx[1], color='k', transform=ax2.transAxes, va ='top', fontsize=fs)
        ax5.text(0.01, 1.1, Tx[2], color='k', transform=ax5.transAxes, va ='top', fontsize=fs)
        ax6.text(0.01, 1.1, Tx[3], color='k', transform=ax6.transAxes, va ='top', fontsize=fs)
    
    x = 0.01; y = 0.98
    #ax1.text(0.5, 0.78, r'$\mathrm{Light\ Pulse}$',color='C1',transform=ax1.transAxes, va ='top', fontsize=fs)            
    ax1.text(x, y, r'$\mathrm{(a)}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{(b)}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    ax3.text(x, y, r'$\mathrm{(c)}$',color='k',transform=ax3.transAxes, va ='top', fontsize=fs)
    ax4.text(x, y, r'$\mathrm{(d)}$',color='k',transform=ax4.transAxes, va ='top', fontsize=fs)

    ax5.text(x, y, r'$\mathrm{(e)}$',color='k',transform=ax5.transAxes, va ='top', fontsize=fs)
    ax6.text(x, y, r'$\mathrm{(f)}$',color='k',transform=ax6.transAxes, va ='top', fontsize=fs)
    ax7.text(x, y, r'$\mathrm{(g)}$',color='k',transform=ax7.transAxes, va ='top', fontsize=fs)
    ax8.text(x, y, r'$\mathrm{(h)}$',color='k',transform=ax8.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + number + '.pdf')

#------------------------------------------------------------
#------------------------------------------------------------

def plotfig_6p(data, fig_name, values=val,
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim=None, Cl=Cl, Ls=Ls, Mk=Mk, Pi=Pi, Ps=Ps, Pf=Pf, 
            Tx=Tx, Gs=Gs2p, Lg=Lg, sf=sf, Lpx=Lpx, Lpy=Lpy,
            errorbar=False, plot_pulse=0, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, np_v=2, np_h=2, **kwargs)
    values = {**val, **values}
    number = fig_name
    #------------------------------------------------------------
    f = data
    fs = kwargs['fs']
    plot_x = f[values['x_plot']]
    plot_y = f[values['y_plot']]
    xlabel1 = values['xlabel1']; xlabel2 = values['xlabel2']
    ylabel1 = values['ylabel1']; ylabel2 = values['ylabel2']
    xlabel3 = values['xlabel3']; xlabel4 = values['xlabel4']
    ylabel3 = values['ylabel3']; ylabel4 = values['ylabel4']
    xlabel5 = values['xlabel5']; xlabel6 = values['xlabel6']
    ylabel5 = values['ylabel5']; ylabel6 = values['ylabel6']
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(2, 3, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0]); ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[0,2]); ax4 = plt.subplot(gs[1,0])
    ax5 = plt.subplot(gs[1,1]); ax6 = plt.subplot(gs[1,2])
    rm_ax = 0; nticks = 5
    #------------------------------------------------------------
    ax1.set_xlabel(xlabel1, labelpad=Lpx[0]); 
    ax1.set_ylabel(ylabel1, labelpad=Lpy[0])
    ax1.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax1.yaxis.set_major_locator(MaxNLocator(3)) 
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[0,0], sf[0,1]):
        ax1.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], 
        color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax2.set_xlabel(xlabel2, labelpad=Lpx[1]);
    ax2.set_ylabel(ylabel2, labelpad=Lpy[1])
    ax2.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax2.yaxis.set_major_locator(MaxNLocator(3)) 
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[1,0], sf[1,1]):
        ax2.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax3.set_xlabel(xlabel3, labelpad=Lpx[2]);
    ax3.set_ylabel(ylabel3, labelpad=Lpy[2])
    ax3.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax3.yaxis.set_major_locator(MaxNLocator(3)) 
    ax3.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[2,0], sf[2,1]):
        ax3.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax4.set_xlabel(xlabel4, labelpad=Lpx[3]);
    ax4.set_ylabel(ylabel4, labelpad=Lpy[3])
    ax4.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax4.yaxis.set_major_locator(MaxNLocator(nticks)) 
    ax4.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[3,0], sf[3,1]):
        ax4.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    ax4.yaxis.set_major_locator(MaxNLocator(3)) 
    #------------------------------------------------------------
    ax5.set_xlabel(xlabel5, labelpad=Lpx[4]);
    ax5.set_ylabel(ylabel5, labelpad=Lpy[4])
    ax5.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax5.yaxis.set_major_locator(MaxNLocator(3)) 
    ax5.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[4,0], sf[4,1]):
        ax5.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax6.set_xlabel(xlabel6, labelpad=Lpx[5]);
    ax6.set_ylabel(ylabel6, labelpad=Lpy[5])
    ax6.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax6.yaxis.set_major_locator(MaxNLocator(3)) 
    ax6.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[5,0], sf[5,1]):
        ax6.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    if 0:
        ax1.set_ylim((-0.75,0.75)); ax2.set_ylim((-0.75,0.75))
        ax4.set_ylim((-0.75,0.75)); ax5.set_ylim((-0.75,0.75))
    if xlim is not None:
        ax1.set_xlim(xlim); ax2.set_xlim(xlim) 
        ax3.set_xlim(xlim); ax4.set_xlim(xlim) 
        ax5.set_xlim(xlim); ax6.set_xlim(xlim) 
    if 1: 
        ax1.set_xticklabels([]); ax2.set_xticklabels([])
        ax3.set_xticklabels([])
    if 0:
        ax1.set_yscale('log'); ax2.set_yscale('log')
        ax3.set_yscale('log'); ax4.set_yscale('log')
        ax5.set_yscale('log'); ax6.set_yscale('log')
    if 0:
        #fig3
        #ax1.set_ylim((-1,1.8)); ax2.set_ylim((-1,1.8)); ax3.set_ylim((-0.6,0.9)); 
        #ax5.set_ylim((-1.1,1)); ax6.set_ylim((-1.1,1));
        #fig4
        ax1.set_ylim((-2,2.5)); ax2.set_ylim((-2,2.5)); ax3.set_ylim((-1.2,1.6)); 
        ax4.set_ylim((-1.1,1));ax5.set_ylim((-1.1,1));# ax6.set_ylim((-1.1,1));
        #fig8
        #ax1.set_ylim((-1.7,4)); ax2.set_ylim((-1.7,4)); ax3.set_ylim((-1.7,4)); 
        #ax5.set_ylim((-1.2,1)); ax6.set_ylim((-1.2,1));
    if plot_pulse:
        ylim = ax1.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*0.5 + (ylim[1]-ylim[0])*0.6
        ax1.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C0', alpha=1)
        ylim = ax2.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*0.5 + (ylim[1]-ylim[0])*0.6
        ax2.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C0', alpha=1)
        ylim = ax3.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*0.5 + (ylim[1]-ylim[0])*0.6
        ax3.plot(plot_x[Pi[i]:Pf[i]:Ps[i]], pulse, 'C0', alpha=1)
    #------------------------------------------------------------
    x = 0.01; y = 0.98
    #ax1.text(0.5, 0.78, r'$\mathrm{Light\ Pulse}$',color='C1',transform=ax1.transAxes, va ='top', fontsize=fs)            
    ax1.text(x, y, r'$\mathrm{(a)}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{(b)}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    ax3.text(x, y, r'$\mathrm{(c)}$',color='k',transform=ax3.transAxes, va ='top', fontsize=fs)
    ax4.text(x, y, r'$\mathrm{(d)}$',color='k',transform=ax4.transAxes, va ='top', fontsize=fs)

    ax5.text(x, y, r'$\mathrm{(e)}$',color='k',transform=ax5.transAxes, va ='top', fontsize=fs)
    ax6.text(x, y, r'$\mathrm{(f)}$',color='k',transform=ax6.transAxes, va ='top', fontsize=fs)
    if text:
        ax1.text(0.02, 1.1, Tx[0], color='k', transform=ax1.transAxes, va ='top', fontsize=fs)
        ax2.text(0.02, 1.1, Tx[1], color='k', transform=ax2.transAxes, va ='top', fontsize=fs)
        ax3.text(0.02, 1.1, Tx[2], color='k', transform=ax3.transAxes, va ='top', fontsize=fs)
        #ax4.text(0.5, 1.1, Tx[3], color='k', transform=ax4.transAxes, va ='top', fontsize=fs)
        #ax5.text(0.5, 1.1, Tx[4], color='k', transform=ax5.transAxes, va ='top', fontsize=fs)
        #ax6.text(0.5, 1.1, Tx[5], color='k', transform=ax6.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + number + '.pdf')
#------------------------------------------------------------
#------------------------------------------------------------

def plotfig_4p1(data, fig_name, values=val,
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim=None, Cl=Cl, Ls=Ls, Mk=Mk, Pi=Pi, Ps=Ps, Pf=Pf, 
            Tx=Tx, Gs=Gs2p, Lg=Lg, sf=sf, Lpx=Lpx, Lpy=Lpy,
            errorbar=False, plot_pulse=0, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, np_v=2, **kwargs)
    values = {**val, **values}
    number = fig_name
    ps = 0.3747
    #------------------------------------------------------------
    f = data
    fs = kwargs['fs']
    plot_x = f[values['x_plot']]
    plot_y = f[values['y_plot']]
    xlabel1 = values['xlabel1']; xlabel2 = values['xlabel2']
    ylabel1 = values['ylabel1']; ylabel2 = values['ylabel2']
    xlabel3 = values['xlabel3']; xlabel4 = values['xlabel4']
    ylabel3 = values['ylabel3']; ylabel4 = values['ylabel4']
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(2, 2, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0]); ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[1,0]); ax4 = plt.subplot(gs[1,1])
    rm_ax = 0; nticks = 7
    #------------------------------------------------------------
    ax1.set_xlabel(xlabel1, labelpad=Lpx[0]); 
    ax1.set_ylabel(ylabel1, labelpad=Lpy[0])
    ax1.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[0,0], sf[0,1]):
        ax1.plot(1000*plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], 
        color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax2.set_xlabel(xlabel2, labelpad=Lpx[1]);
    ax2.set_ylabel(ylabel2, labelpad=Lpy[1])
    ax2.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[1,0], sf[1,1]):
        ax2.plot(1000*plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    #------------------------------------------------------------
    ax3.set_xlabel(xlabel3, labelpad=Lpx[2]);
    ax3.set_ylabel(ylabel3, labelpad=Lpy[2])
    ax3.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax3.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[2,0], sf[2,1]):
        ax3.plot(plot_x[Pi[i]:Pf[i]:Ps[i]]/ps, plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    ax3.yaxis.set_major_locator(MaxNLocator(3)) 
    if plot_pulse:
        ylim = ax3.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*0.5 + (ylim[1]-ylim[0])*0.0
        ax3.plot(plot_x[Pi[i]:Pf[i]:Ps[i]]/ps, pulse, 'k', ls='--', lw=0.2)
        ax3.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    ax4.set_xlabel(xlabel4, labelpad=Lpx[3]);
    ax4.set_ylabel(ylabel4, labelpad=Lpy[3])
    ax4.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax4.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[3,0], sf[3,1]):
        ax4.plot(plot_x[Pi[i]:Pf[i]:Ps[i]]/ps, plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    ax4.yaxis.set_major_locator(MaxNLocator(3)) 
    if plot_pulse:
        ylim = ax4.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*0.5 + (ylim[1]-ylim[0])*0.0
        ax4.plot(plot_x[Pi[i]:Pf[i]:Ps[i]]/ps, pulse, 'k', ls='--', lw=0.2)
        ax4.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    if xlim is not None:
        ax1.set_xlim((0,100)); ax2.set_xlim((0,100)) 
        ax4.set_xlim(xlim); ax3.set_xlim(xlim) 
    if 0: 
        ax1.set_xticklabels([]); ax2.set_xticklabels([])
        ax3.set_xticklabels([])
    if 1:
        ax1.set_yscale('log'); ax2.set_yscale('log')
        ax3.set_yscale('log'); ax4.set_yscale('log')
    #------------------------------------------------------------
    x = 0.01; y = 0.98
    #ax1.text(0.5, 0.78, r'$\mathrm{Light\ Pulse}$',color='C1',transform=ax1.transAxes, va ='top', fontsize=fs)            
    ax1.text(x, y, r'$\mathrm{(a)}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{(b)}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    ax3.text(x, y, r'$\mathrm{(c)}$',color='k',transform=ax3.transAxes, va ='top', fontsize=fs)
    ax4.text(x, y, r'$\mathrm{(d)}$',color='k',transform=ax4.transAxes, va ='top', fontsize=fs)
    if text:
        ax1.text(0.01, 1.15, Tx[0], color='k', transform=ax1.transAxes, va ='top', fontsize=fs)
        ax2.text(0.01, 1.15, Tx[1], color='k', transform=ax2.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + number + '.pdf')

#------------------------------------------------------------
#------------------------------------------------------------

def plotfig_6p1(data, fig_name, values=val,
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim=None, Cl=Cl, Ls=Ls, Mk=Mk, Pi=Pi, Ps=Ps, Pf=Pf, 
            Tx=Tx, Gs=Gs2p, Lg=Lg, sf=sf, Lpx=Lpx, Lpy=Lpy,
            errorbar=False, plot_pulse=0, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, np_v=2, np_h=2, **kwargs)
    values = {**val, **values}
    number = fig_name
    ps = 0.3747 # setting the omega_0
    alpha = [1,1]*6
    #------------------------------------------------------------
    f = data
    fs = kwargs['fs']
    plot_x = f[values['x_plot']]
    plot_y = f[values['y_plot']]
    xlabel1 = values['xlabel1']; xlabel2 = values['xlabel2']
    ylabel1 = values['ylabel1']; ylabel2 = values['ylabel2']
    xlabel3 = values['xlabel3']; xlabel4 = values['xlabel4']
    ylabel3 = values['ylabel3']; ylabel4 = values['ylabel4']
    xlabel5 = values['xlabel5']; xlabel6 = values['xlabel6']
    ylabel5 = values['ylabel5']; ylabel6 = values['ylabel6']
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(2, 3, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0]); ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[0,2]); ax4 = plt.subplot(gs[1,0])
    ax5 = plt.subplot(gs[1,1]); ax6 = plt.subplot(gs[1,2])
    rm_ax = 0; nticks = 8; nticks1 = 6
    #------------------------------------------------------------
    ax1.set_xlabel(xlabel1, labelpad=Lpx[0]); 
    ax1.set_ylabel(ylabel1, labelpad=Lpy[0])
    ax1.xaxis.set_major_locator(MaxNLocator(nticks1)) 
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[0,0], sf[0,1]):
        ax1.plot(1000*plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], 
        color=Cl[i], ls=Ls[i], alpha=alpha[i])
    #------------------------------------------------------------
    ax2.set_xlabel(xlabel2, labelpad=Lpx[1]);
    ax2.set_ylabel(ylabel2, labelpad=Lpy[1])
    ax2.xaxis.set_major_locator(MaxNLocator(nticks1)) 
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[1,0], sf[1,1]):
        ax2.plot(1000*plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], 
        color=Cl[i], ls=Ls[i], alpha=alpha[i])
    #------------------------------------------------------------
    ax3.set_xlabel(xlabel3, labelpad=Lpx[2]);
    ax3.set_ylabel(ylabel3, labelpad=Lpy[2])
    ax3.xaxis.set_major_locator(MaxNLocator(nticks1)) 
    ax3.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[2,0], sf[2,1]):
        ax3.plot(1000*plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i],
        color=Cl[i], ls=Ls[i], alpha=alpha[i])
    ax3.yaxis.set_major_locator(MaxNLocator(4)) 
    #------------------------------------------------------------
    #------------------------------------------------------------
    ax4.set_xlabel(xlabel4, labelpad=Lpx[3]);
    ax4.set_ylabel(ylabel4, labelpad=Lpy[3])
    ax4.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax4.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[3,0], sf[3,1]):
        ax4.plot(plot_x[Pi[i]:Pf[i]:Ps[i]]/ps, plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i],
        color=Cl[i], ls=Ls[i], alpha=alpha[i])
    ax4.yaxis.set_major_locator(MaxNLocator(3)) 
    if plot_pulse:
        ylim = ax4.get_ylim()
        pulse = plot_y[:,sf[-1,-1]][Pi[i]:Pf[i]:Ps[i]]\
              * (ylim[1]-ylim[0])*0.5 + (ylim[1]-ylim[0])*0.0
        ax4.plot(plot_x[Pi[i]:Pf[i]:Ps[i]]/ps, pulse, 'k', ls='--', lw=0.2)
        ax4.set_ylim((ylim[0], ylim[1]+(ylim[1]-ylim[0])*0.42))
    #------------------------------------------------------------
    ax5.set_xlabel(xlabel5, labelpad=Lpx[4]);
    ax5.set_ylabel(ylabel5, labelpad=Lpy[4])
    ax5.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax5.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[4,0], sf[4,1]):
        ax5.plot(plot_x[Pi[i]:Pf[i]:Ps[i]]/ps, plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i],
        color=Cl[i], ls=Ls[i], alpha=alpha[i])
    #------------------------------------------------------------
    ax6.set_xlabel(xlabel6, labelpad=Lpx[5]);
    ax6.set_ylabel(ylabel6, labelpad=Lpy[5])
    ax6.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax6.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[5,0], sf[5,1]):
        ax6.plot(plot_x[Pi[i]:Pf[i]:Ps[i]]/ps, plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i],
        color=Cl[i], ls=Ls[i], alpha=alpha[i])
    #------------------------------------------------------------
    if xlim is not None:
        ax1.set_xlim((0,30)); ax2.set_xlim((0,30)) 
        ax3.set_xlim((0,30)); 
        ax4.set_xlim(xlim) 
        ax5.set_xlim(xlim); ax6.set_xlim(xlim) 
    if 0: 
        ax1.set_xticklabels([]); ax2.set_xticklabels([])
        ax3.set_xticklabels([])
    if 1:
        ax1.set_yscale('log'); ax2.set_yscale('log')
        ax3.set_yscale('log'); ax4.set_yscale('log')
        ax5.set_yscale('log'); ax6.set_yscale('log')
    if 1:
        ll = 1e-10; ul = 1e-3
        ax1.set_ylim((ll,ul)); ax2.set_ylim((ll,ul))
        ax3.set_ylim((ll,ul))
    #------------------------------------------------------------
    x = 0.89; y = 0.98
    #ax1.text(0.5, 0.78, r'$\mathrm{Light\ Pulse}$',color='C1',transform=ax1.transAxes, va ='top', fontsize=fs)            
    ax1.text(x, y, r'$\mathrm{(a)}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{(b)}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    ax3.text(x, y, r'$\mathrm{(c)}$',color='k',transform=ax3.transAxes, va ='top', fontsize=fs)
    ax4.text(x, y, r'$\mathrm{(d)}$',color='k',transform=ax4.transAxes, va ='top', fontsize=fs)

    ax5.text(x, y, r'$\mathrm{(e)}$',color='k',transform=ax5.transAxes, va ='top', fontsize=fs)
    ax6.text(x, y, r'$\mathrm{(f)}$',color='k',transform=ax6.transAxes, va ='top', fontsize=fs)
    if text:
        ax1.text(0.02, 1.12, Tx[0], color='k', transform=ax1.transAxes, va ='top', fontsize=fs)
        ax2.text(0.02, 1.12, Tx[1], color='k', transform=ax2.transAxes, va ='top', fontsize=fs)
        ax3.text(0.02, 1.12, Tx[2], color='k', transform=ax3.transAxes, va ='top', fontsize=fs)
    #ax1.text(0.3, 0.95, r'$\mathrm{-Mn_3Sn\ SOC\ off}$', color='grey', transform=ax1.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + number + '.pdf')

#------------------------------------------------------------
#------------------------------------------------------------

def plotfig_2p1(data, fig_name, values=val,
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim=None, Cl=Cl, Ls=Ls, Mk=Mk, Pi=Pi, Ps=Ps, Pf=Pf, 
            Tx=Tx, Gs=Gs2p, Lg=Lg, sf=sf, Lpx=Lpx, Lpy=Lpy,
            errorbar=False, plot_pulse=0, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    values = {**val, **values}
    number = fig_name
    ps = 0.3747
    #------------------------------------------------------------
    f = data
    fs = kwargs['fs']
    plot_x = f[values['x_plot']]
    plot_y = f[values['y_plot']]
    xlabel1 = values['xlabel1']; xlabel2 = values['xlabel2']
    ylabel1 = values['ylabel1']; ylabel2 = values['ylabel2']
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 2, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0]); ax2 = plt.subplot(gs[0,1])
    rm_ax = 0; nticks = 10
    #------------------------------------------------------------
    ax1.set_xlabel(xlabel1, labelpad=Lpx[0]); 
    ax1.set_ylabel(ylabel1, labelpad=Lpy[0])
    ax1.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[0,0], sf[0,1]):
        ax1.plot(1000*plot_x[Pi[i]:Pf[i]:Ps[i]], plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], 
        color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax2.set_xlabel(xlabel2, labelpad=Lpx[1]);
    ax2.set_ylabel(ylabel2, labelpad=Lpy[1])
    ax2.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[1,0], sf[1,1]):
        ax2.plot(plot_x[Pi[i]:Pf[i]:Ps[i]]/ps, plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    #------------------------------------------------------------
    if xlim is not None: ax1.set_xlim((0,100)); ax2.set_xlim(xlim) 
    if 1: ax1.set_yscale('log'); ax2.set_yscale('log')
    #------------------------------------------------------------
    x = 0.01; y = 0.98
    #ax1.text(0.5, 0.78, r'$\mathrm{Light\ Pulse}$',color='C1',transform=ax1.transAxes, va ='top', fontsize=fs)            
    ax1.text(x, y, r'$\mathrm{(a)}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{(b)}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + number + '.pdf')

#------------------------------------------------------------
#------------------------------------------------------------

def hist_2p(data, s1, s2, fig_name, values=val,
            text=False, legend=False, xlim=None,
            ylim=None, Cl=Cl, Ls=Ls, Mk=Mk, 
            Tx=Tx, Gs=Gs2p, errorbar=False, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    values = {**val, **values}
    number = fig_name
    fs = kwargs['fs']
    #------------------------------------------------------------
    f = data
    plot_x = f[values['x_plot']]
    plot_y = f[values['y_plot']]
    xlabel1 = values['xlabel1']; xlabel2 = values['xlabel2']
    ylabel1 = values['ylabel1']; ylabel2 = values['ylabel2']
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 2, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0]); ax2 = plt.subplot(gs[0,1])
    #------------------------------------------------------------
    ax1.set_xlabel(xlabel1, labelpad=1); ind = 0
    ax1.set_ylabel(ylabel1, labelpad=3)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax1.plot(plot_x[:,ind], plot_y[:,ind], marker=Mk[ind], color=Cl[ind], ls=Ls[ind])
    ax1.hist(s1, bins=100, histtype=u'step', range=(0,5), density=1, 
    linewidth=0.4, color='C1')
    ax1.yaxis.set_major_locator(MaxNLocator(3)) 
    #------------------------------------------------------------
    ax2.set_xlabel(xlabel2, labelpad=1); ind = 1
    ax2.set_ylabel(ylabel2, labelpad=3)
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax2.plot(plot_x[:,ind], plot_y[:,ind], marker=Mk[ind], color=Cl[ind], ls=Ls[ind])
    ax2.hist(s2, bins=100, histtype=u'step', range=(0,5), density=1, 
    linewidth=0.4, color='C1', label=r'\mathrm{}')
    ax2.yaxis.set_major_locator(MaxNLocator(3)) 
    #------------------------------------------------------------
    if xlim is not None:
        ax1.set_xlim(xlim) 
        ax2.set_xlim(xlim) 
    if ylim is not None:
        ax1.set_ylim(ylim)
        ax2.set_ylim(ylim) 
    x = 0.01; y = 0.98
    ax1.text(x, y, r'$\mathrm{(a)}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{(b)}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    x = 0.78; y = 0.98
    ax1.text(x, y, r'$\mathrm{FM}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{AFM}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + number + '.pdf')

#------------------------------------------------------------
#------------------------------------------------------------
#------------------------------------------------------------
#------------------------------------------------------------





def plotfig_4p_old(data, fig_name, values=val,
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim=None, Cl=Cl, Ls=Ls, Mk=Mk, Ps=Ps, Pf=Pf, 
            Tx=Tx, Gs=Gs2p, Lg=Lg, sf=sf, errorbar=False, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, np_v=2, **kwargs)
    values = {**val, **values}
    #------------------------------------------------------------
    f = data; fs = kwargs['fs']
    plot_x = f[values['x_plot']]; plot_y = f[values['y_plot']]
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(2, 2, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0]); ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[1,0]); ax4 = plt.subplot(gs[1,1])
    rm_ax = 0; nticks = 2; nyticks = 3
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel1'], labelpad=1); 
    ax1.set_ylabel(values['ylabel1'], labelpad=3)
    ax1.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax1.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    if rm_ax: ax1.set_xticks([])
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[0,0], sf[0,1]):
        ax1.plot(plot_x[:Pf[i]:Ps[i]], plot_y[:,i][:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax2.set_xlabel(values['xlabel2'], labelpad=1);
    ax2.set_ylabel(values['ylabel2'], labelpad=3)
    ax2.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax2.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    if rm_ax : ax2.set_xticks([]); ax2.set_yticks([])
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-3,3))
    for i in range(sf[1,0], sf[1,1]):
        ax2.plot(plot_x[:Pf[i]:Ps[i]], plot_y[:,i][:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax3.set_xlabel(values['xlabel3'], labelpad=1);
    ax3.set_ylabel(values['ylabel3'], labelpad=3)
    ax3.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax3.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    ax3.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    for i in range(sf[2,0], sf[2,1]):
        ax3.plot(plot_x[:Pf[i]:Ps[i]], plot_y[:,i][:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    ax4.set_xlabel(values['xlabel4'], labelpad=1);
    ax4.set_ylabel(values['ylabel4'], labelpad=-3)
    ax4.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax4.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    if rm_ax: ax4.set_yticks([])
    ax4.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    for i in range(sf[3,0], sf[3,1]):
        ax4.plot(plot_x[:Pf[i]:Ps[i]], plot_y[:,i][:Pf[i]:Ps[i]], 
        marker=Mk[i], color=Cl[i], ls=Ls[i])
    #------------------------------------------------------------
    x = 0.01; y = 0.97
    ax1.text(x, y, r'$\mathrm{(a)}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
    ax2.text(x, y, r'$\mathrm{(b)}$',color='k',transform=ax2.transAxes, va ='top', fontsize=fs)
    ax3.text(x, y, r'$\mathrm{(c)}$',color='k',transform=ax3.transAxes, va ='top', fontsize=fs)
    ax4.text(x, y, r'$\mathrm{(d)}$',color='k',transform=ax4.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    if xlim is not None:
        ax1.set_xlim(xlim); ax3.set_xlim(xlim) 
        ax2.set_xlim(xlim); ax4.set_xlim(xlim) 
    if ylim is not None:
        ax1.set_ylim(ylim)
    #------------------------------------------------------------
    if ax_twin: 
        i = i + 1
        ax5 = ax4.twinx()
        ax5.plot(plot_x, plot_y[:,i], marker=Mk[i], color=Cl[i], ls=Ls[i])
        ax5.yaxis.set_major_locator(plt.MaxNLocator(2))
        ax4.yaxis.set_major_locator(plt.MaxNLocator(3))
        ax5.set_ylim((0,0.25))
        ax4.set_ylim((-0.23,0.23))
        #ax1.set_ylim((-0.05, 0.03))
        #ax3.set_ylim((-1.005,-0.959))
        ax5.set_ylabel(r'$\mathrm{|\langle \hat{\bf s}_e \rangle|}$',
                        labelpad=8, rotation=270)
        ax4.plot(plot_x, np.zeros(len(plot_x)), ls=':', color='grey')
        ax3.yaxis.set_major_locator(plt.MaxNLocator(3))        
        #ax4.set_xticks([-0.1,0,0.1])
    #------------------------------------------------------------
    if legend:
        #ax1.plot([],[],color='k',ls='-',label=r'$\mathrm{N_e = 1}$')
        #ax1.plot([],[],color='k',ls='--',marker=Mk[1], label=r'$\mathrm{Ne = 3}$')
        ax1.plot([],[],color='k',ls='-',marker=Mk[0],label=Lg[0])
        ax1.plot([],[],color='k',ls='',marker=Mk[1],label=Lg[1])
        ax1.legend(bbox_to_anchor=(0.5, 0.88), ncol=1,frameon=False,
                handlelength = 1,columnspacing = 0.8, fontsize=6,
                loc='center') 
    #------------------------------------------------------------
    if text:
        if False:
            ax1.text(0.3, 1.12, Tx[0], color='k', transform=ax1.transAxes, va ='top', fontsize=fs)
            ax2.text(0.3, 1.12, Tx[1], color='k', transform=ax2.transAxes, va ='top', fontsize=fs)
            ax3.text(0.3, 1.1, Tx[0], color='k', transform=ax3.transAxes, va ='top', fontsize=fs)
            ax4.text(0.3, 1.12, Tx[1], color='k', transform=ax4.transAxes, va ='top', fontsize=fs)
            ax2.text(1.1, 1.1, Tx[2], color='k', transform=ax2.transAxes, va ='top', fontsize=fs, rotation=270)
            ax4.text(1.1, 0.98, Tx[3], color='k', transform=ax4.transAxes, va ='top', fontsize=fs, rotation=270)
            
            ax1.text(0.02, 0.4, r'$\mathrm{\alpha=}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
            ax1.text(0.2, 0.4, r'$\mathrm{x}$',color='r',transform=ax1.transAxes, va ='top', fontsize=fs)
            ax1.text(0.3, 0.4, r'$\mathrm{y}$',color='b',transform=ax1.transAxes, va ='top', fontsize=fs)
            ax1.text(0.4, 0.4, r'$\mathrm{z}$',color='k',transform=ax1.transAxes, va ='top', fontsize=fs)
     
        
       
        if False: 
            ax1.text(0, 1.25, Tx[0], color='k', transform=ax1.transAxes, va ='top', fontsize=fs)
            ax2.text(0, 1.25, Tx[1], color='k', transform=ax2.transAxes, va ='top', fontsize=fs)
            ax1.text(0.05, 0.5, r'$\mathrm{S=1}$', color=Cl[0], transform=ax1.transAxes, va ='top', fontsize=fs)
            ax1.text(0.05, 0.4, r'$\mathrm{S=5/2}$', color=Cl[2], transform=ax1.transAxes, va ='top', fontsize=fs)
            ax3.text(0.05, 0.3, r'$\mathrm{N_e = 1}$', color='k', transform=ax3.transAxes, va ='top', fontsize=fs)
            ax3.text(0.05, 0.2, r'$\mathrm{S = 5/2}$', color='k', transform=ax3.transAxes, va ='top', fontsize=fs)
            
            ax4.text(0.1, 0.5, r'$\mathrm{i = 1}$', color='C2', transform=ax4.transAxes, va ='top', fontsize=fs)
            ax4.text(0.1, 0.4, r'$\mathrm{i = 2}$', color='C3', transform=ax4.transAxes, va ='top', fontsize=fs)
            ax4.text(0.1, 0.3, r'$\mathrm{i = 3}$', color='b', transform=ax4.transAxes, va ='top', fontsize=fs)
            ax4.text(0.1, 0.2, r'$\mathrm{i = 4}$', color='C1', transform=ax4.transAxes, va ='top', fontsize=fs)
            ax4.text(0.1, 0.1, r'$\mathrm{Electron}$', color='c', transform=ax4.transAxes, va ='top', fontsize=fs)
        if True:
            ax1.text(0.2, 1.25,r'$\mathrm{g\mu_B|B_i^z|=0.6\ eV\ J=0.1\ eV\ J_{sd}=0.1\ eV\ S=1 \hbar}$' , 
            color='k', transform=ax1.transAxes, va ='top', fontsize=fs)
            #ax4.text(0.05, 0.48, r'$\mathrm{N_e = 1}$', color='C1', transform=ax4.transAxes, va ='top', fontsize=6)
            #ax4.text(0.5, 0.48, r'$\mathrm{N_e = 3}$', color='C2', transform=ax4.transAxes, va ='top', fontsize=6)
            ax4.text(0.2, 0.1, r'$\mathrm{Electron}$', color='c', transform=ax4.transAxes, va ='top', fontsize=6)
            ax4.text(0.05, 0.98, r'$\mathrm{N_e = 1}$', color='C1', transform=ax4.transAxes, va ='top', fontsize=6)
            ax4.text(0.5, 0.98, r'$\mathrm{N_e = 3}$', color='C2', transform=ax4.transAxes, va ='top', fontsize=6)
            
        #ax2.text(1.07, 1.1, Tx[2], color='k', transform=ax2.transAxes, va ='top', fontsize=fs)
        #ax4.text(1.07, 0.98, Tx[3], color='k', transform=ax4.transAxes, va ='top', fontsize=fs)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + fig_name + '.pdf')

#------------------------------------------------------------
#------------------------------------------------------------

def plotfig_old(data, fig_name, values=val, twin_axis=False,
            text=False, legend=False, xlim=None,
            ylim = None, Cl=Cl, Ls=Ls, Mk=Mk, 
            Tx = Tx, errorbar=False, hist=False, **kwargs):		
    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    values = {**val, **values}
    number = fig_name
    #------------------------------------------------------------
    f = data
    plot_x = f[values['x_plot']]; plot_y = f[values['y_plot']]
    print(plot_y.shape)
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 1, 
                           top=0.87, bottom=0.2, 
                           left=0.3, right=0.7, 
                           wspace=0, hspace=0)
    ax1 = plt.subplot(gs[0,0])
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel'], labelpad=1)
    ax1.set_ylabel(values['ylabel'], labelpad=3)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))

    if twin_axis:
        ax2 = ax1.twinx()
        #ax2.set_ylabel(r'$\mathrm{{\langle \hat{S^{\alpha}} \rangle}_2}$',
        #        rotation=270,labelpad=10) 
    else: ax2 = ax1 
    fss = 5
    if text:
        for ind in range(len(Tx)):
            ax1.text(0.5, 0.4-ind*0.1, Tx[ind], color=Cl[ind],
            transform=ax1.transAxes, va ='top', fontsize=fss)

    label1 = r'$\mathrm{t_{RSO} = 0\ eV}$' 
    label2 = r'$\mathrm{t_{RSO} = 1\ eV}$' 
    if not errorbar and not hist:
        if len(plot_y.shape) == 1 or 1 in plot_y.shape:
            ax1.plot(plot_x, plot_y, marker=Mk[0], color=Cl[0], ls=Ls[0])
        else:
            for ind in range(plot_y.shape[1]):
                ax1.plot(plot_x, plot_y[:,ind], marker=Mk[ind], color=Cl[ind], ls=Ls[ind])
        if legend:
            ax1.legend(bbox_to_anchor=(1, 0.45), ncol=1,frameon=False,
                    handlelength = 1,columnspacing = 0.2, fontsize=fss,
                    loc='lower right') 
        if xlim is not None:
            ax1.set_xlim(xlim) 
        if ylim is not None:
            ax1.set_ylim(ylim)
    elif errorbar:
        ax1.errorbar(plot_x, plot_y[:,0], yerr = plot_y[:,1],
                marker=Mk[0], color=Cl[0], ls=Ls[0], capsize=2, capthick=0.2)
        ax1.errorbar(plot_x, plot_y[:,2], yerr = plot_y[:,3],
                marker=Mk[1], color=Cl[1], ls=Ls[1], capsize=2, capthick=0.2)
        #ax1.plot(plot_x, plot_y[:,0], marker=Mk[2], color=Cl[2], ls=Ls[2])
        #ax1.plot(plot_x, plot_y[:,2], marker=Mk[3], color=Cl[3], ls=Ls[3])
    elif hist:
        ax1.hist(plot_x)
         
    plt.savefig('results/fig_' + number + '.pdf')
#------------------------------------------------------------
#------------------------------------------------------------
def plotfig_arrow(result, t_ind, fig_name, values=val,
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim = None, Cl=Cl, Ls=Ls, Mk=Mk, Ps=Ps, Pf=None, ylog=0, 
            Tx=Tx, Gs=Gs1p, Lg=Lg, sf=sf, Alpha=Alpha, ft='.pdf', **kwargs):		

    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 1, 
                           top=0.99, bottom=0.01, 
                           left=0.01, right=0.99, 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0])
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel1'], labelpad=1)
    ax1.set_ylabel(values['ylabel1'], labelpad=3)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-4,4))
    Sea = result[t_ind,0:3] 
    sea = result[t_ind,3:6] 

    #ax1.arrow(0,0,Sea[0],sea[1], width=0.05, fc='C0', ec='k',lw=1,alpha=0)
    #ax1.arrow(0,0,sea[0],Sea[1], width=0.05, fc='C1', ec='k',lw=1,alpha=0)
    
    import matplotlib.patches as mpatches
    ar1 = mpatches.FancyArrowPatch((0, 0), (sea[0], sea[1]),
                                 mutation_scale=10,fc='C0',lw=0.5)
    ar2 = mpatches.FancyArrowPatch((0, 0), (Sea[0], Sea[1]),
                                 mutation_scale=10,fc='C2',lw=0.5)
    ax1.add_patch(ar1) 
    ax1.add_patch(ar2) 
    ax1.axis('off') 
    ax1.set_xlim([-1,1]) 
    ax1.set_ylim([-1,1])
    #------------------------------------------------------------
    plt.savefig('results/fig_' + fig_name + ft)

#------------------------------------------------------------
#------------------------------------------------------------

def plotfig_c(data, panels, fig_name, values=val, text=False, legend=False,
            xlim=None, ax_twin=False, ylim=None, Cl=Cl, Ls=Ls, 
            Mk=Mk, Pi=Pi, Ps=Ps, Pf=Pf, Tx=Tx, Gs=Gs4p, Lg=Lg, 
            sf=sf, Lpx=Lpx, Lpy=Lpy, errorbar=False, **kwargs):
		
    from matplotlib import rcParams
    rc_update(rcParams, np_v=panels[0], **kwargs)
    values = {**val, **values}
    #------------------------------------------------------------
    f = data; fs = kwargs['fs']
    plot_x = f[values['x_plot']]; plot_y = f[values['y_plot']]

    #if len(plot_x.shape)==1: plot_x = np.array([plot_x]*plot_y.shape[1])
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(panels[0], panels[1], 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    
    pax = [[[i,j] for j in range(panels[1])] for i in range(panels[0])]    
    print(pax)
    ax = [[plt.subplot(gs[i,j]) for j in range(panels[1])]
                                for i in range(panels[0])]
    #print(ax[0][0]) 
    #print(ax[0][1]) 


    rm_ax = 0; nticks = 3; nyticks = 3
    N = 0
    for ai in range(panels[0]):
        for aj in range(panels[1]):
            print(ai, aj, N, chr(97+N))
            ax[ai][aj].set_xlabel(values['xlabel'+str(N+1)], labelpad=Lpx[N])
            ax[ai][aj].set_ylabel(values['ylabel'+str(N+1)], labelpad=Lpy[N])
            ax[ai][aj].xaxis.set_major_locator(MaxNLocator(nticks))
            ax[ai][aj].yaxis.set_major_locator(MaxNLocator(nyticks))
            ax[ai][aj].ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))

            ax[ai][aj].text(0.01, 0.97, r'$\mathrm{('+chr(97+N)+')}$',
                            color='k',transform=ax[ai][aj].transAxes, 
                            va ='top', fontsize=fs)

            for i in range(sf[N,0], sf[N,1]):
                ax[ai][aj].plot(plot_x[Pi[i]:Pf[i]:Ps[i]], 
                                plot_y[:,i][Pi[i]:Pf[i]:Ps[i]], 
                                marker=Mk[i], color=Cl[i], ls=Ls[i])
            N = N + 1
    
    #------------------------------------------------------------
    """
    if 1:
        ax1.set_ylim((-1.5,1.5)); ax2.set_ylim((-1.5,1.5))
        ax3.set_ylim((0,1.3));
        #ax5.set_ylim((-0.75,0.75))
        ax1.set_xticklabels([]); ax2.set_xticklabels([])
    if xlim is not None:
        ax1.set_xlim(xlim); ax3.set_xlim(xlim) 
        ax2.set_xlim(xlim); ax4.set_xlim(xlim) 
    if ylim is not None:
        ax1.set_ylim(ylim)
    if text:
        ax1.text(0.5, 1.15, Tx[0], color='k', transform=ax1.transAxes, va ='top', fontsize=fs)
        ax2.text(0.5, 1.15, Tx[1], color='k', transform=ax2.transAxes, va ='top', fontsize=fs)
    """
    #------------------------------------------------------------
    #------------------------------------------------------------
    plt.savefig('results/fig_' + fig_name + '.pdf')

