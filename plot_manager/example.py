import numpy as np
import sys

from plot2d import plotfig_c as plotfig_c

from plot2d import plotfig_4p as plotfig_4p


x = 0.1*np.arange(100).reshape(100,1)
y = np.sin(x)
y1 = np.sin(2*x)
y2 = np.sin(3*x)
y3 = np.sin(34*x)
#-----------------------
plot_y = y
plot_y = np.concatenate((plot_y, y1), axis=1)
plot_y = np.concatenate((plot_y, y2), axis=1)
plot_y = np.concatenate((plot_y, y3), axis=1)

plot_x = x
#-----------------------




sf = np.array([[0,1],[1,2],[2,3],[3,4]])


data = dict();values = dict()
#L = len(ps1)
#plot_y = ps1.reshape(L,1)

#plot_x = s1.reshape(L,1);
#plot_x = np.concatenate((plot_x, s2.reshape(L,1)), axis=1)

values['xlabel1'] = ''; values['xlabel2'] = r'$\mathrm{s}$' 
values['ylabel1'] = r'$\mathcal{P}(\mathrm{a})$'
values['ylabel2'] = r'$\mathcal{P}(\mathrm{b})$'
values['ylabel3'] = r'$\mathcal{P}(\mathrm{c})$'
values['ylabel4'] = r'$\mathcal{P}(\mathrm{d})$'
#values['ylabel2'] = ''
data['x_plot'] = plot_x; data['y_plot'] = plot_y

Gs = dict();
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.15; Gs['r']=0.9
Gs['w']=0.5; Gs['h']=0.4
panels = [1,2]
plotfig_c(data, panels, '1', values, Gs=Gs, sf=sf, fs=8)
#plotfig_4p(data, '1', values, Gs=Gs, sf=sf, fs=8)
