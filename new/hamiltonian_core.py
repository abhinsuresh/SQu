import numpy as np
import sys
from spins import Spins
from constants import SIG_X, SIG_Y, SIG_Z
from numba_wrapper import flatten_list

class Op:
    def __init__(self,
                    lat,
                    dtype = np.complex64,
                    #all terms in TB ham with positive sign
                    t0 = -1,    #intra Sn-Sn hopping
                    tj = -0.5,  #Sn-Mn-Sn jsd hopping(also copied to llg)
                    t1 = -1,    #inter Sn-Sn hopping
                    t_pt = -1,  #intra Pt hopping and Sn-Pt hopping
                    lambda_z = 0.5, #Sn-Sn rashba
                    tso = 0.0,   # not part of balents ham
                    tso_pt = 0.0, # extra added part
                    dt = 0.1,
                    make_square = 1, #if 0 makes TB sqaure lattice
                    #llg parameters follow same as marko
                    spin_dir = None,
                    jexc = -0.28,
                    g_lambda = 0.1,
                    ani = 0.0187,
                    dmi = -0.063,
                    dmi_prime = 0,
                ):
        self.lat = lat
        self.dtype = dtype
        self.t0 = t0
        self.tj = tj
        self.t1 = t1
        self.t_pt = t_pt
        self.lambda_z = lambda_z
        self.tso = tso
        self.tso_pt = tso_pt
        self.dt = dt
        self.msq = make_square
        self.spin_dir = spin_dir
        self.jexc = jexc
        self.g_lambda = g_lambda
        self.ani = ani
        self.dmi = dmi
        self.dmi_prime = dmi_prime
        self.verbose = False
        self.generate()
        return

    def ani_func(self, site_ind):
        """anisotropy function 
        """
        dir_ani = self.lat.dir_ani
        ani_norm = np.linalg.norm(np.array(dir_ani[site_ind]))
        return dir_ani[site_ind] / ani_norm

    def generate(self):
        """generate cspins object with llg included
        """
        self.ccoup = self.lat.ccoup
        self.cspins = Spins(self.lat.ribbon,
                            dt          = self.dt, 
                            jexc        = self.jexc,
                            g_lambda    = self.g_lambda,
                            jsd_to_llg  = -1*self.tj, #(diff in sign LLG)
                            ani_func    = self.ani_func, 
                            ani         = self.ani,
                            dmi         = self.dmi, 
                            dmi_prime   = self.dmi_prime)
        for i in np.arange(len(self.lat.ribbon.sites)):
            if self.spin_dir is not None:
                self.cspins.s[i] = self.spin_dir[i]
            else:
                self.cspins.s[i] = self.lat.cspin_dir[i]
            #self.cspins.s[i] = self.ani_func(i)
            

    def create_H(self, spins=None, alpha=0, lp_vec=np.zeros(3)):
        """Hamiltonian size if Pt assumes to have bilayer Mn3Sn
        """
        #setting spin direction, accept new cspin or take initial 
        if spins is None:
            spins = self.cspins.s
            if self.verbose: print('assigning default cspins values')
        #initialising the hamiltonian with zeros
        H = np.zeros((3*2*self.lat.Lx*self.lat.Ly, 
                      3*2*self.lat.Lx*self.lat.Ly), 
                                 dtype=self.dtype)
        #-------------------------------------------------------------------
        #adding spin coupling
        for sc, [x,y] in enumerate(self.lat.hopx_list):
            ind = self.lat.x_cup[sc]
            H[2*x:2*(x+1), 2*y:2*(y+1)] +=  \
                + self.tj * ( SIG_X*spins[ind,0] + SIG_Y*spins[ind,1] 
                            + SIG_Z*spins[ind,2])
        for sc, [x,y] in enumerate(self.lat.hopy1_list):
            ind = self.lat.y1_cup[sc]
            H[2*x:2*(x+1), 2*y:2*(y+1)] +=  \
                + self.tj * ( SIG_X*spins[ind,0] + SIG_Y*spins[ind,1] 
                            + SIG_Z*spins[ind,2])
        for sc, [x,y] in enumerate(self.lat.hopy2_list):
            ind = self.lat.y2_cup[sc]
            H[2*x:2*(x+1), 2*y:2*(y+1)] +=  \
                + self.tj * ( SIG_X*spins[ind,0] + SIG_Y*spins[ind,1] 
                            + SIG_Z*spins[ind,2])
        #-------------------------------------------------------------------
        #making default light factors
        h_fx  = np.ones(len(self.lat.dir_x),  dtype=np.complex128)
        h_fy1 = np.ones(len(self.lat.dir_y1), dtype=np.complex128)
        h_fy2 = np.ones(len(self.lat.dir_y2), dtype=np.complex128)
        h_fz = np.ones(len(self.lat.dir_z), dtype=np.complex128)
        #adding light only onto two layers inter layer hopping
        h_fx[:len(self.lat.dir_x)]   = \
            np.array([np.exp(-1j*np.dot(self.lat.dir_x[i],  lp_vec)) 
            for i in range(len(self.lat.dir_x))])
        h_fy1[:len(self.lat.dir_y1)] = \
            np.array([np.exp(-1j*np.dot(self.lat.dir_y1[i], lp_vec)) 
            for i in range(len(self.lat.dir_y1))])
        h_fy2[:len(self.lat.dir_y2)] = \
            np.array([np.exp(-1j*np.dot(self.lat.dir_y2[i], lp_vec)) 
            for i in range(len(self.lat.dir_y2))])
        h_fz[:len(self.lat.dir_z)] = \
            np.array([np.exp(-1j*np.dot(self.lat.dir_z[i], lp_vec)) 
            for i in range(len(self.lat.dir_z))])
        #-------------------------------------------------------------------
        #adding kin hop
        for ind, [x,y] in enumerate(self.lat.hopx_list):
            H[2*x:2*(x+1), 2*y:2*(y+1)] += self.t0 * np.eye(2) * h_fx[ind]
        for ind, [x,y] in enumerate(self.lat.hopy1_list):
            H[2*x:2*(x+1), 2*y:2*(y+1)] += self.t0 * np.eye(2) * h_fy1[ind] 
        for ind, [x,y] in enumerate(self.lat.hopy2_list):
            H[2*x:2*(x+1), 2*y:2*(y+1)] += self.t0 * np.eye(2) * h_fy2[ind]
        for ind, [x,y] in enumerate(self.lat.hop_inter):
            H[2*x:2*(x+1), 2*y:2*(y+1)] += self.t1 * np.eye(2) * h_fz[ind]
        #-------------------------------------------------------------------
        #adding spin orbit part hop_(x and y1) +ve hop_y2 -ve
        for [x,y] in np.append(self.lat.hopx_list, self.lat.hopy1_list, axis=0):
            H[2*x:2*(x+1), 2*y:2*(y+1)] += -1*1j*self.lambda_z * SIG_Z
        for [x,y] in self.lat.hopy2_list:
            H[2*x:2*(x+1), 2*y:2*(y+1)] +=  1*1j*self.lambda_z * SIG_Z
        #-------------------------------------------------------------------
        #adding hopping within in the Pt part
        if self.lat.ad_pt==1 and self.lat.layers==2:
            for [x,y] in self.lat.hopx_pt:
                H[2*x:2*(x+1), 2*y:2*(y+1)] += self.t_pt * np.eye(2) \
                                         - 1j*self.tso_pt*SIG_Y
            for [x,y] in self.lat.hopy_pt:
                H[2*x:2*(x+1), 2*y:2*(y+1)] += self.t_pt * np.eye(2) \
                                         + 1j*self.tso_pt*SIG_X
            #adding hopping between Pt and Sn
            for [x,y] in self.lat.hop_inter_pt:
                H[2*x:2*(x+1), 2*y:2*(y+1)] += self.t_pt * np.eye(2)
        #-------------------------------------------------------------------
        H = H + np.conjugate(H.transpose())

        return H

    def spin_curr_rho(self, bond, rho, H, spin='x'):
        """find the bond spin current value from rho and H
        """
        l = bond[0]
        k = bond[1]
        #1st term
        rholk = rho[2*l:2*(l+1), 2*k:2*(k+1)]
        Hkl = H[2*k:2*(k+1), 2*l:2*(l+1)]

        #2nd term
        rhokl = rho[2*k:2*(k+1), 2*l:2*(l+1)]
        Hlk = H[2*l:2*(l+1), 2*k:2*(k+1)]
        
        if spin=='x':
            curr1 = np.trace(rholk @ (SIG_X @ Hkl + Hkl @ SIG_X)) 
            curr2 = np.trace(rhokl @ (SIG_X @ Hlk + Hlk @ SIG_X)) 
            return 1j*(curr1 - curr2)/4
        if spin=='y':
            curr1 = np.trace(rholk @ (SIG_Y @ Hkl + Hkl @ SIG_Y)) 
            curr2 = np.trace(rhokl @ (SIG_Y @ Hlk + Hlk @ SIG_Y)) 
            return 1j*(curr1 - curr2)/4
        if spin=='z':
            curr1 = np.trace(rholk @ (SIG_Z @ Hkl + Hkl @ SIG_Z)) 
            curr2 = np.trace(rhokl @ (SIG_Z @ Hlk + Hlk @ SIG_Z)) 
            return 1j*(curr1 - curr2)/4
        if spin=='i':
            curr1 = np.trace(rholk @ Hkl) 
            curr2 = np.trace(rhokl @ Hlk) 
            return 1j*(curr1 - curr2)/2

        if spin=='z_old':
            curr1 = 1j*( rholk @ Hkl - rhokl @ Hlk )/2
            return np.trace(curr1 @ SIG_Z)

        if spin=='all':
            curr_op = 1j*( rholk @ Hkl - rhokl @ Hlk )*2*np.pi
            cc = np.trace(curr_op)
            cx = np.trace(curr_op @ SIG_X)
            cy = np.trace(curr_op @ SIG_Y)
            cz = np.trace(curr_op @ SIG_Z)
            return np.array([cc, cx, cy, cz])

    def rho_alpha_nn(self, rho):
        """find the espins for Mn3Sn
        """
        espins = np.zeros((len(self.cspins.s),3))
        
        for i in range(len(espins)):
            l = self.ccoup[i,0]
            k = self.ccoup[i,1]
            espins[i,0] = np.trace(rho[2*l:2*(l+1), 2*l:2*(l+1)] @ SIG_X) + \
                          np.trace(rho[2*k:2*(k+1), 2*k:2*(k+1)] @ SIG_X) 
            espins[i,1] = np.trace(rho[2*l:2*(l+1), 2*l:2*(l+1)] @ SIG_Y) + \
                          np.trace(rho[2*k:2*(k+1), 2*k:2*(k+1)] @ SIG_Y) 
            espins[i,2] = np.trace(rho[2*l:2*(l+1), 2*l:2*(l+1)] @ SIG_Z) + \
                          np.trace(rho[2*k:2*(k+1), 2*k:2*(k+1)] @ SIG_Z) 
        return espins

    def check_hermitian(self, a, rtol=1e-05, atol=1e-08):
        return np.allclose(a, a.conj().T, rtol=rtol, atol=atol)
    
    def check_equal(self, a, b, rtol=1e-05, atol=1e-06):
        return np.allclose(a, b, rtol=rtol, atol=atol)
    
    def check_comm(self, a, b, rtol=1e-05, atol=1e-05):
        return np.allclose(a@b, b@a, rtol=rtol, atol=atol)
