from numba import njit, prange, complex64, float32, complex128
import itertools
import numpy as np 
from scipy.special import expit

KB = 8.617333*1e-5   #eV K-1
HBAR = 0.6582119569

@njit(nopython=True)
def calc_rho(E,V, temp=0, verbose=False):
    """Calculate the canonical gibbs density matrix
    """
    temp = temp+1e-15    #to avoid divide by zero error
    beta = 1/(KB*temp)
    E = E - E[0]
    rho = np.zeros((len(E),len(E)), dtype=np.complex128)
    for ind in np.arange(len(E)):
        rho = rho + np.exp( -beta*E[ind]  ) * \
                    np.conjugate(V[:,ind].reshape((len(E), 1))) @ \
                                 V[:,ind].reshape((1, len(E)))
    
    return rho/( np.trace(rho) )

#@jit(nopython=True)
def calc_rho_vdv(E,V, temp=0, verbose=False):
    """Calculate the canonical gibbs density matrix
    """
    temp = temp+1e-15    #to avoid divide by zero error
    beta = 1/(KB*temp)
    E = E - E[0]
    e_exp = np.diag(np.exp(-beta * E))
    rho = V @ e_exp @ np.transpose(np.conjugate(V))
    
    return rho/( np.trace(rho) )



#@jit(nopython=True)
def sp_trace(a):
    row = a.tocoo().row
    col = a.tocoo().col
    L = len(row)
    trace = 0
    for i in range(L):
        if row[i] == col[i]:
            trace = trace + a.data[i]
    return trace

def fermi_fun(E, mu=0, temp=0):
    KB = 8.617333*1e-5   #eV K-1
    temp = temp+1e-15    #to avoid divide by zero error
    beta = 1/(KB*temp)
    _x = -1*beta*(E - mu)
    return expit(_x)
    

 
    
     
def flatten_list(_2d_list):
    """flatten a 2d list
    """
    flat_list = []
    for element in _2d_list:
        if type(element) is list:
            for item in element:
                flat_list.append(item)
        else:
            flat_list.append(element)
    return flat_list

def unit_vector(x2, x1):
    """Find the unit vector along x2 - x1
    """
    x1 = np.array(x1)
    x2 = np.array(x2)

    x = x2 - x1
    x_norm = np.linalg.norm(x)
    return x/x_norm

def vector(x2, x1):
    """Find the unit vector along x2 - x1
    """
    x1 = np.array(x1)
    x2 = np.array(x2)

    x = x2 - x1
    return x

def r_z(vec, th):
    """roatate the vector with z axis
    """
    vec
    mat = np.array([[ np.cos(th), np.sin(th)],\
                    [-np.sin(th), np.cos(th)]])
    return mat @ vec

def find_mid(array):
    res = 0
    for i in range(len(array)-1):
        if array[i] <= 0 and array[i+1] >= 0:
            res = i
    p = (array[res] + array[res+1])/2
    return p, res

def fbe(de):
    KB = 8.6173324e-5        # Boltzman constant [eV / K]
    beta = 1/(KB*0)
    return 1/( np.exp((de-0)*beta) - 1 )  
    
#@njit([complex64[:,:],float32[:]])
def lindblad(rho_t, w, temp=0, mu=0):
    """Linblad evolution in energy basis 0f H0
    """
    #diagonal part
    KB = 8.6173324e-5        # Boltzman constant [eV / K]
    temp = temp + 1e-15
    gamma_sc = 2e-4
    gamma_sf = 2e-6
    gamma_dp = 5e-2
    beta = 1/(KB*temp)
    rho_new = np.zeros(rho_t.shape)
    for n in range(rho_t.shape[0]):
        for m in range(rho_t.shape[0]):
            #term preserving pauli exclusion principle
            pi_nm = rho_t[m,m]*(1 - rho_t[n,n])
            #term proportional to sc and sf process both added
            fac = (gamma_sc + gamma_sf) * pi_nm
            #term accounting for de-/excitation factors 
            de = w[n] - w[m]
            if de > 0:
                fbe_de = 1/( np.exp((de-mu)*beta) - 1 )
                fac = fac * (fbe_de + 1)
                #daigonal part 1st term of sum
                rho_new[n,n] = rho_new[n,n] + fac*(rho_t[m,m])
                #non-digonal part 2nd term of sum
                rho_new[m,:] = rho_new[m,:] - 0.5*fac*(rho_t[m,:])
                rho_new[:,m] = rho_new[:,m] - 0.5*fac*(rho_t[:,m])
            elif de < 0:
                fbe_de = 1/( np.exp((de-mu)*beta) - 1 )
                fac = fac *  (fbe_de)
                #daigonal part 1st term of sum
                rho_new[n,n] = rho_new[n,n] + fac*(rho_t[m,m])
                #non-digonal part 2nd term of sum
                rho_new[m,:] = rho_new[m,:] - 0.5*fac*(rho_t[m,:])
                rho_new[:,m] = rho_new[:,m] - 0.5*fac*(rho_t[:,m])
            elif de == 0 and n==m:
                fac = gamma_dp
                #daigonal part 1st term of sum
                rho_new[n,n] = rho_new[n,n] + fac*(rho_t[m,m]) 
                #non-digonal part 2nd term of sum
                rho_new[m,:] = rho_new[m,:] - 0.5*fac*(rho_t[m,:])
                rho_new[:,m] = rho_new[:,m] - 0.5*fac*(rho_t[:,m])
    return rho_new

@njit()
def lindblad_t(rho_t, w, temp=0, mu=0):
    """Linblad evolution in energy basis 0f H0 with only one
    explicit sum loop doing the opreations, all gamma_nm
    could be saved before hand
    """
    KB = 8.6173324e-5        # Boltzman constant [eV / K]
    temp = temp + 1e-15
    gamma_sc = 2e-4
    gamma_sf = 2e-6
    gamma_dp = 5e-2
    beta = 1/(KB*temp)
    rho_new = np.zeros(rho_t.shape, dtype=np.complex128)
    gamma_nm = np.zeros(len(w), dtype=np.complex128)
    #print(KB, rho_new.dtype, rho_t.dtype, gamma_nm.dtype, np.diag(gamma_nm).dtype)
    #rho_new = rho_new - 0.5*(np.diag(gamma_nm)@rho_t + rho_t@np.diag(gamma_nm))
    for n in range(len(w)):
        gamma_nm = np.zeros(len(w), dtype=np.complex128)
        for m in range(rho_t.shape[0]):
            de = w[n] - w[m]
            pi_nm = rho_t[m,m]*(1 - rho_t[n,n])
            fac = (gamma_sc + gamma_sf) * pi_nm
            if m == n: 
                gamma_nm[m] = gamma_dp
            elif de > 0: 
                fbe_de = 1/( np.exp((de-mu)*beta) - 1 )
                gamma_nm[m] = fac * (fbe_de + 1)
            elif de < 0:
                fbe_de = 1/( np.exp((de-mu)*beta) - 1 )
                gamma_nm[m] = fac * (fbe_de)
        #other term
        rho_new = rho_new - 0.5*(np.diag(gamma_nm)@rho_t + rho_t@np.diag(gamma_nm))
        #diagonla term
        rho_new[n,n] = rho_new[n,n] + np.dot(gamma_nm, np.diag(rho_t))
    return rho_new

@njit()
def test(rho_t):
    """Linblad evolution in energy basis 0f H0
    """
    gamma_nm = np.zeros(rho_t.shape[0], dtype=complex64)
    fac = np.pi * 5 * 1j
    gamma_nm[3] = 4
    gamma_nm[4] = 4*fac
    print(fac)
    rho_new = np.zeros(rho_t.shape, dtype=complex64)
    #rho_new = np.diag(np.diag(rho_new) + 0.5*(np.diag(rho_t)))
    #rho_new[5,5] = rho_new[5,5] + 0.5*(rho_t[5,5])
    return rho_new
