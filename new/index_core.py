import numpy as np
from numba_wrapper import vector, unit_vector, flatten_list 
def intra_Sn(lat):
    """Find the intra layer hopping list for triangular plane lattice
    and extend for bilayer
    -Base hopping in TB Hamiltonian and jsd term hop
    Constructs
    ----------
    lat.hopx_list
    lat.hopy1_list
    lat.hopy2_list
    lat.bonds and lat.bon_len -> dict()
    self.ccoup
    self.sd_hop
    """
    _hopx = []
    _hopy = []
    for i in range(lat.Ly):
        _hopx.append(range(lat.Lx*i, lat.Lx*i + lat.Lx, 1))
    for i in range(lat.Lx):
        _hopy.append(range(i, lat.Lx*(lat.Ly-1) + i + 1, lat.Lx))

    lat.hopx_list = []
    lat.hopy1_list = []
    lat.hopy2_list = []
    for ele in _hopx:
        lat.hopx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
        if lat.PBC: lat.hopx_list.append( [[ele[0], ele[-1]]])
    lat.hopx_list = flatten_list(lat.hopx_list)

    for ele in _hopy:
        lat.hopy1_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
        if lat.PBC: lat.hopy1_list.append( [[ele[0], ele[-1]]])
    lat.hopy1_list = flatten_list(lat.hopy1_list)

    for ele in _hopy[1:]:
        lat.hopy2_list.append( [[ele[i], ele[i]+lat.Lx-1] for i in range(len(ele)-1)]  )
        if lat.PBC: lat.hopy2_list.append( [[ele[-1], ele[0]-1]])

    if lat.PBC:
        lat.hopy2_list.append( [[0,5]])
        lat.hopy2_list.append( [[3,8]])
        lat.hopy2_list.append( [[6,2]])
    lat.hopy2_list = flatten_list(lat.hopy2_list)
    
    #finding corresponding jsd terms for classical spins
    lat.ccoup = []
    lat.ccoup.append(lat.hopy2_list)
    lat.ccoup.append(lat.hopx_list[0:9:3])
    lat.ccoup.append(lat.hopx_list[1:9:3])
    lat.ccoup.append(lat.hopx_list[2:9:3])
    lat.ccoup.append(lat.hopy1_list)
    
    lat.ccoup = flatten_list(lat.ccoup)

    #making np.array and exteding for bilayer    
    lat.hopx_list   = np.array(lat.hopx_list, dtype=np.int8)
    lat.hopy1_list  = np.array(lat.hopy1_list, dtype=np.int8)
    lat.hopy2_list  = np.array(lat.hopy2_list, dtype=np.int8)
    lat.ccoup       = np.array(lat.ccoup, dtype=np.int8)
    if lat.layers == 2:
        lat.hopx_list  = np.append(lat.hopx_list,  lat.hopx_list  + lat.Lx*lat.Ly, axis=0)
        lat.hopy1_list = np.append(lat.hopy1_list, lat.hopy1_list + lat.Lx*lat.Ly, axis=0)
        lat.hopy2_list = np.append(lat.hopy2_list, lat.hopy2_list + lat.Lx*lat.Ly, axis=0)
        lat.ccoup      = np.append(lat.ccoup, lat.ccoup + lat.Lx*lat.Ly, axis=0)

    lat.hop_list = np.append(lat.hopx_list, lat.hopy1_list, axis=0)
    lat.hop_list = np.append(lat.hop_list, lat.hopy2_list, axis=0)

    #periodic boundary condition

    lat.bonds = lat.hop_list
    lat.bond_len['intra_sn'] = len(lat.hop_list)
    

def inter_Sn(lat):
    """Find the inter layer hopping elements of bilayer Sn, only
    exist in the presence of bilayer
    -Base hopping TB hamiltonian
    """
    lat.hop_inter = []
    ind = 0 
    for i in range(lat.Ly):
        for j in range(lat.Lx):
            lat.hop_inter.append([ind + 1*lat.Lx*lat.Ly, ind])
            if j != lat.Lx-1:
                lat.hop_inter.append([ind + 1*lat.Lx*lat.Ly, ind+1])
            if i != lat.Ly-1:
                lat.hop_inter.append([ind + 1*lat.Lx*lat.Ly, ind+lat.Lx])
            ind = ind + 1
    if lat.PBC:
        lat.hop_inter.append([11,0]);lat.hop_inter.append([14,3])
        lat.hop_inter.append([17,6])
        lat.hop_inter.append([15,0]);lat.hop_inter.append([16,1])
        lat.hop_inter.append([17,2])
    lat.hop_inter = np.array(lat.hop_inter)
    lat.bonds = np.append(lat.bonds, lat.hop_inter, axis = 0)
    lat.bond_len['inter_sn'] = len(lat.hop_inter)

def link_MnSn(lat):
    """Find the classical spin site index of Mn for
    corresponding hopx/y1/y2_list in order
    -Required for jsd part of TB hamiltonian
    Constructs
    ----------
    lat.x_cup  -> hopx_list
    lat.y1_cup -> hopy1_list
    lat.y2_cup -> hopy2_list
    """
    st = (lat.Lx)*(lat.Ly)
    _cup_x = []
    for i in range(lat.Ly):
        _cup_x.append(np.arange(st + i, (st+i) + lat.Ly*(lat.Lx), lat.Ly))
    _cup_x = np.array(_cup_x, dtype=np.int8)
        
    #print(hopy1_list)
    st = 2*(lat.Lx)*(lat.Ly)
    _cup_y1 = []
    for i in range(lat.Lx):
        _cup_y1.append(np.arange(st + i*(lat.Ly), st + (i+1)*(lat.Ly)))
    _cup_y1 = np.array(_cup_y1, dtype=np.int8)

    #print(hopy2_list)
    st = 0
    _cup_y2 = []
    for i in range(lat.Lx):
        _cup_y2.append(np.arange(st + i*(lat.Ly), st + (i+1)*(lat.Ly)))
    _cup_y2 = np.array(_cup_y2, dtype=np.int8)
    
    lat.x_cup  = np.array([j for sub in _cup_x for j in sub] )
    lat.y1_cup = np.array([j for sub in _cup_y1 for j in sub] ) 
    lat.y2_cup = np.array([j for sub in _cup_y2 for j in sub] ) 

    #extending to bilayer
    if lat.layers == 2:
        lat.x_cup  = np.append(lat.x_cup,  lat.x_cup  + len(lat.ribbon.sites)//2, axis=0)
        lat.y1_cup = np.append(lat.y1_cup, lat.y1_cup + len(lat.ribbon.sites)//2, axis=0)
        lat.y2_cup = np.append(lat.y2_cup, lat.y2_cup + len(lat.ribbon.sites)//2, axis=0)
    #used for inverse mapping
    lat.all_cup = np.append(lat.x_cup, lat.y1_cup, axis=0)
    lat.all_cup = np.append(lat.all_cup, lat.y2_cup, axis=0)

def pos_Sn(lat):
    """Find the position of Sn atoms for both layers 
    position of second relative to first is not real values, 
    but they don't appear in equations as i know it.
    -Base position TB Hamiltonian
    Constructs
    ----------
    lat.pos_Sn1/2
    """
    lat.pos_Sn1 = []
    for j in range(lat.Ly):
        for i in range(lat.Lx):
            lat.pos_Sn1.append([0.75 + 1*i + 0.5*j, 0.4330 + 0.8660*j, 0])
    lat.pos_Sn1 = np.array(lat.pos_Sn1)
    #list of all pos
    lat.pos_atoms = lat.pos_Sn1
    lat.pos_Sn2 = []
    #gave c/a = 0.7689
    for j in range(lat.Ly):
        for i in range(lat.Lx):
            lat.pos_Sn2.append([1.25 + 1*i + 0.5*j, 1.6*0.4330 + 0.8660*j, 0.7689])
    lat.pos_Sn2 = np.array(lat.pos_Sn2)
    #list of all pos
    lat.pos_Sn = np.append(lat.pos_Sn1, lat.pos_Sn2, axis=0)
    lat.pos_atoms = lat.pos_Sn

def dir_hop(lat):        
    """find the vector along the direction of kin hop
    -Required to use in TB Hamiltonian, like to add light
    """
    lat.dir_x = []
    for ind, hop in enumerate(lat.hopx_list):
        lat.dir_x.append(unit_vector(lat.pos_Sn[hop[1]], lat.pos_Sn[hop[0]]))
    lat.dir_y1 = []
    for ind, hop in enumerate(lat.hopy1_list):
        lat.dir_y1.append(unit_vector(lat.pos_Sn[hop[1]], lat.pos_Sn[hop[0]]))
    lat.dir_y2 = []
    for ind, hop in enumerate(lat.hopy2_list):
        ### SETTING SAME DIR FOR ALL Y2 BONDS
        lat.dir_y2.append(unit_vector(lat.pos_Sn[1], lat.pos_Sn[3]))

    lat.dir_bonds = lat.dir_x + lat.dir_y1 + lat.dir_y2 

def dir_hop_inter(lat):        
    """find the vector along the direction of kin hop
    -Required for TB Hamiltonian or to measure emission
    """
    lat.dir_z = []
    L = lat.Lx * lat.Ly
    for ind, hop in enumerate(lat.hop_inter):
        #lat.dir_z.append(vector(lat.pos_Sn[hop[0]-L], lat.pos_Sn[hop[1]]))
        if ind < 21:
            lat.dir_z.append(vector(lat.pos_Sn[hop[0]], lat.pos_Sn[hop[1]]))
        elif ind < 24:
            lat.dir_z.append(vector(lat.pos_Sn[hop[0]-1], lat.pos_Sn[hop[1]+2]))
        elif ind < 27:
            lat.dir_z.append(vector(lat.pos_Sn[hop[0]-5], lat.pos_Sn[hop[1]+4]))
    
    lat.dir_bonds = lat.dir_bonds + lat.dir_z
            

def dir_cspin(lat):
    """get the orientation of the cspins
    -Required to assign the initial direction of cspins
    """
    lat.cspin_dir = []
    if lat.domain == 0:
        #alpha+,theta=0
        d1 = [-0.500011,-0.86601905,0.]; d2 = [1.,0.,0.]
        d3 = [-0.500011,0.86601905,0.]
    if lat.domain == 1:
        #beta+,theta=2pi/3
        d1 = [-0.500011, 0.86601905,0.] 
        d2 = [-0.500011,-0.86601905,0.]; d3 = [1.,0.,0.]
    if lat.domain == 6:
        #v-,theta=pi/3
        d1 = [-1.,0.,0.]; d2 = [0.500011,-0.86601905,0.]
        d3 = [0.500011, 0.86601905,0.]
    #ARITA
    if lat.domain == 2:
        #v-,theta=pi/3
        d1 = [-1/2,-np.sqrt(3)/2,0.]; d2 = [1.,0.,0.]
        d3 = [-1/2, np.sqrt(3)/2,0.]

    for i in range((lat.Lx)*(lat.Ly)):
        lat.cspin_dir.append(d1)
    for i in range((lat.Lx)*(lat.Ly)):
        lat.cspin_dir.append(d2)
    for i in range((lat.Lx)*(lat.Ly)):
        lat.cspin_dir.append(d3)
    if lat.layers==2:
        for i in range((lat.Lx)*(lat.Ly)):
            lat.cspin_dir.append(d1)
        for i in range((lat.Lx)*(lat.Ly)):
            lat.cspin_dir.append(d2)
        for i in range((lat.Lx)*(lat.Ly)):
            lat.cspin_dir.append(d3)

    lat.cspin_dir = np.array(lat.cspin_dir)
    
def find_anivec(lat):
    """Calcualates the direction from Mn to corresponding Sn,
    or between Sn's
    -Used as anisotropy in LLG
    Constructs
    ----------
    lat.dir_ani 
    """
    # finding the direction of anisotropy
    lat.dir_ani = []
    sign = 1
    #BALENTS        # SET MANUALLY
    #0-2-1(family name order)
    _dir =  [unit_vector(lat.pos_Sn[3],lat.pos_Sn[1]),
             unit_vector(lat.pos_Sn[3],lat.pos_Sn[0]),
             unit_vector(lat.pos_Sn[1],lat.pos_Sn[0])]
    #ARITA          # SET MANUALLY
    #0-2-1(family name order)
    _dir = [[-1/2,-np.sqrt(3)/2,0],[-1/2,np.sqrt(3)/2,0],[1,0,0]]

    for ind, site in enumerate(lat.ribbon.sites):
        lat.dir_ani.append(_dir[int(site.family.name)])   
    lat.dir_ani = np.array(lat.dir_ani)

def hop_Pt(lat):
    """Find the hopping in Pt layer, assuming its the
    third layer
    -Required to construct TB Hamiltonian
    """
    _hopx = []
    _hopy = []
    for i in (np.arange(lat.Ly)):
        _hopx.append(range(lat.Lx*i + 2*lat.Lx*lat.Ly, 2*lat.Lx*lat.Ly + lat.Lx*i + lat.Lx, 1))
    for i in (2*lat.Lx*lat.Ly + np.arange(lat.Lx)):
        _hopy.append(range(i, lat.Lx*(lat.Ly-1) + i + 1, lat.Lx))

    lat.hopx_pt = []
    lat.hopy_pt = []
    for ele in _hopx:
        lat.hopx_pt.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
        if lat.PBC: lat.hopx_pt.append( [[ele[0], ele[-1]]] )
    lat.hopx_pt = flatten_list(lat.hopx_pt)
    
    for ele in _hopy:
        lat.hopy_pt.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
        if lat.PBC: lat.hopy_pt.append( [[ele[0], ele[-1]]] )
    lat.hopy_pt = flatten_list(lat.hopy_pt)
    #extending total bonds
    lat.bonds = np.append(lat.bonds, lat.hopx_pt, axis = 0)
    lat.bonds = np.append(lat.bonds, lat.hopy_pt, axis = 0)
    lat.bond_len['intra_pt'] = len(lat.hopx_pt) + len(lat.hopy_pt)

    lat.pos_pt = []
    for j in range(lat.Ly):
        for i in range(lat.Lx):
            lat.pos_pt.append([1.25 + 1*i + 0.5*j, 1.6*0.4330 + 0.8660*j, 0.7689*2])
    lat.pos_pt = np.array(lat.pos_pt)
    lat.pos_atoms = np.append(lat.pos_atoms, lat.pos_pt, axis = 0)
    
    lat.dir_bonds = lat.dir_bonds + lat.dir_x[:9] + lat.dir_y1[:9]
    
def inter_Pt(lat):
    """Find the inter layer hopping 2nd layer Sn and Pt
    -Required to construct TB Hamiltonian
    """
    lat.hop_inter_pt = []
    for i in range(lat.Lx*lat.Ly, 2*lat.Lx*lat.Ly):
        lat.hop_inter_pt = lat.hop_inter_pt + [[i, i + lat.Lx*lat.Ly]]
    #extending total bonds
    lat.bonds = np.append(lat.bonds, lat.hop_inter_pt, axis = 0)
    lat.bond_len['inter_pt'] = len(lat.hop_inter_pt)
    
    lat.dir_bonds = lat.dir_bonds + [[0,0,1]]*9

