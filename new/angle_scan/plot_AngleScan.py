import numpy as np, sys, h5py, time
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from field_wrapper import *
sys.path.append('../../plot_manager/')
from plot2d import plotfig_6p, plotfig_2p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
t_ini = time.time()
case = '01'
print('file prop:', sys.argv[1], case)
nm = sys.argv[1][11:][:-5]
print('file name: ',nm)


theta = np.arange(0,180,10)*np.pi/180
phi   = np.arange(0,360,20)*np.pi/180


#integration along bonds split
N = 10
#create the system
#-----------------
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1, domain=2)
#-------------------------------------------------------------------
curr    = h5py.File(sys.argv[1])['curr'][:,:,:-1]   
rho_f     = h5py.File(sys.argv[1])['rho_diag'][:,:-1]
#summing over spin indices for each site
rho = rho_f[::2,:] + rho_f[1::2,:]

#setting measuring values to zero
Poynting = np.zeros((len(theta),len(phi),3, curr.shape[2]-1))

pulse   = h5py.File(sys.argv[1])['pulse'][:]


for t_ind, tval in enumerate(theta):
    for p_ind, pval in enumerate(phi):
        r = 100*np.array([np.cos(pval)*np.sin(tval), np.sin(pval)*np.sin(tval),
                          np.cos(tval)])
        print("extracting light")
        E, B, S = extract_light(r, N, curr, rho, pulse, lat.pos_atoms,
                                lat.bonds, lat.dir_bonds, sys.argv[1], case, verbose=1)
        print("extracted light")
        Poynting[t_ind,p_ind,:,:] = S
        print(tval*180/np.pi, pval*180/np.pi, 'S:\t', S.shape)


with h5py.File('results/'+nm+'.hdf5', 'w') as f:
    f.create_dataset("S",data = Poynting, dtype=np.float64, compression="gzip")
f.close()

print(time.time()-t_ini)
