import h5py, numpy as np, sys, h5py, os
import matplotlib.pyplot as plt
sys.path.append('../')
sys.path.append('../../plot_manager/')
from plot2d import plotfig

verbose = 0
#--------------------------------------------------------------
#input power
hbar = 0.6582119569         #[eV fs]
c = 2.99792458*1e8          #[m/s]
mu0 = 1.256637062 * 1e-6    #[H/m]
kl = 5.59*1e-10             #[m]
kc = 1.602176634 * 1e-19    #[C]
ti = 3000
tf = 7000
dt = 0.1                    #[fs]
tspan = np.arange(ti,tf, dt)
if verbose: print("length: ", len(tspan))
W = 1.54/hbar               #[eV/eVfs]
#z = 0.12                   []
#A0 = z * hbar/(e a_0)
#E0 = dA0/dt            
A0 = hbar/(kl)              #[V fs / m]     # zterm to be added at end
E0 = A0/dt                  #[V/m]          # E0 not used further in code
t0 = 4000
s = 50
A = A0 * np.cos(W*tspan) * np.exp(-((tspan - t0)**2)/(2*s*s))
E = np.zeros(len(A))
E[1:] = np.array([(A[i]-A[i-1])/dt for i in range(1,len(A))])
if verbose: print(E.shape, A.shape)
B = E/c
S = (E * B)/mu0
k_in = dt*1e-15*(kl*kl*2*2)   
p_in = k_in * np.sum(S)               #[sum * dt * area] is in Energy Joule
print("total input S through sample: ", p_in)

#--------------------------------------------------------------
#getting file names, zval and then sorting them
f_num = [float(filename[2:][:-5]) for filename in os.listdir('poynting/.') 
       if filename.startswith("S_")]
f_nm = [filename[2:][:-5] for filename in os.listdir('poynting/.') 
       if filename.startswith("S_")]
sort_ind = np.argsort(f_num)
names = [f_nm[i] for i in sort_ind]
zval = [f_num[i]/100 for i in sort_ind]
if verbose: print(names)
if verbose: print(zval)

#--------------------------------------------------------------
#calculating output power
k_outA = 4*np.pi*(100*100*kl*kl) * dt*1e-15         #[S to Joules]
k_outdA = (100*kl*10*np.pi/180)**2 * (dt*1e-15)
p_out = np.zeros(len(zval))
for n_ind, nm in enumerate(names):
    S = h5py.File("poynting/S_"+nm+".hdf5")["S"]
    p_out[n_ind] = k_outA * np.sum(S)               #[summing over all angles]
print("output power: ", p_out[:])

eff = p_out/(p_in*zval)
print("efficiency: ", eff[:])

#--------------------------------------------------------------
#plotting
data = dict(); values = dict(); Gs = dict()
data['x_plot'] = zval; data['y_plot'] = eff.reshape(len(eff),1)
values['xlabel1'] = r'$z_{\rm max}$'; values['ylabel1'] = r'${\rm Efficiency}$'
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.1; Gs['r']=0.9
Gs['w']=0.5; Gs['h']=0.4
plotfig(data, 'n2', values, fs=8, minor=1, lw=0.4, Mk='o')
plt.close()

#--------------------------------------------------------------
x = range(-180,190,10)
y = range(0,190,10)
X,Y = np.meshgrid(x,y)
S = k_outdA * h5py.File("poynting/S_12.hdf5","r")["S"][:]/(p_in*0.12)
print(np.sum(S))
#Z = (S - S.mean(axis=0))/S.std(axis=0)
Z = S/np.max(S)
im = plt.pcolor(X,Y,Z)#shading='auto'
plt.colorbar(im)
plt.savefig("mesh_10.pdf")


