using HDF5, MKL, Base.Threads, LinearAlgebra
include("field_wrapper.jl")

#getting lattice parameters 
lattice     =  h5open("results/lattice.hdf5", "r")
pos_atoms   = lattice["pos_atoms"][]
#have to update indices to start from 1
bonds       = Int8.(lattice["bonds"][] .+ 1)
dir_bonds   = lattice["dir_bonds"][]
close(lattice)

#println(size(dir_bonds), size(pos_atoms), size(bonds))
#(3, 108)(3, 27)(2, 108)

#getting input curr and rho
data = h5open("results/08_12.hdf5","r")
#only getting charge current from all
curr = data["curr"][:,:,1]
rho_f = data["rho_diag"][]
close(data)
#summing spin up and dn
rho = rho_f[:,1:2:end] + rho_f[:,2:2:end]
rho_f = nothing

#print(size(curr)," ",size(rho_f),"\n")

#-----------------------------------------------------------
#constants
ki = 1/(2*π)
ki = ki/0.6582119569                #[e/fs]
ki = ki * 1.602176634 * 1e-4        #[A] or [C/s]
kc = 1.602176634 * 1e-19            #[C]

kl = 5.59*1e-10                     #[m]
μ₀ = 1.256637062 * 1e-6             #[H/m]
ϵ₀ = 8.8541878128 * 1e-12           #[F/m]
c  = 2.99792458 * 1e8
tr = kl/(c*1e-15*0.1)               #[s] 
NInt = 30
#-----------------------------------------------------------
#converting input to SI units
curr = curr * ki
rho = rho * kc
dcurr = zeros(size(curr[1:end-1,:]))
drho  = zeros(size(rho[1:end-1,:]))
println("curr, rho shape: ",size(curr), size(rho))

tf, bl = size(curr)
tf, rl = size(rho)
ti = 3000
tf = 8000

#-------------------------------------------------
#finding derivatives
@threads for b_ind in range(1,bl)
    for t_ind in range(ti,tf)
        dcurr[t_ind, b_ind] = (curr[t_ind+1,b_ind] - curr[t_ind,b_ind])/0.1
    end
end
@threads for r_ind in range(1,rl)
    for t_ind in range(ti,tf)
       drho[t_ind, r_ind] = (rho[t_ind+1,r_ind] - rho[t_ind,r_ind])/0.1
    end
end

#-------------------------------------------------
#inputs for calculation made in same size
rf(θ, ϕ) = 100*[cos(ϕ*π/180)*sin(θ*π/180), sin(ϕ*π/180)*sin(θ*π/180), cos(θ*π/180)]
I  = curr[1:end,:]
dI = dcurr
n = rho[1:end,:]
dn = drho
ph = range(-180,180,step=10)
th = range(0,180,step=10)
S = zeros(length(ph), length(th))
#-------------------------------------------------
#constant for each term
ec1 = kc/(4*π*ϵ₀ * kl * kl)  
ec2 = kc/(4*π*ϵ₀ * kl * c * 1e-16)
ec3 = 1/(4*π*ϵ₀ * c * c * 1e-16)
bc1 = μ₀ / (4*π * kl)
bc2 = μ₀ / (4*π * c * 1e-16)

r = rf(0,0)
println(r)
println(FInt.Jefimenko(pos_atoms, bonds, dir_bonds, n, dn, I, dI, r, NInt,
                                ti, tf, rl, bl, ec1, ec2, ec3, bc1, bc2, μ₀))
r = rf(180,0)
println(r)
println(FInt.Jefimenko(pos_atoms, bonds, dir_bonds, n, dn, I, dI, r, NInt,
                                ti, tf, rl, bl, ec1, ec2, ec3, bc1, bc2, μ₀))
    
