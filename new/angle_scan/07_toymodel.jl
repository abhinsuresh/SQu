using HDF5, MKL, Base.Threads, LinearAlgebra
include("field_wrapper.jl")

#getting lattice parameters 
#pos_atoms   = [-5 5; 0 0; 0 0]  
#pos_atoms   = [0.0 ; 0.0 ; 0.0]  
pos_atoms   = [0.0 0.0; 0.0 0.0; -100.0 100.0]  
bonds       = [1; 2]
dir_bonds   = [0.0, 0.0, 1.0]

#getting input curr and rho
tspan = range(0,100,step=0.1)
tl = length(tspan)
rl = 1
bl = 1
ti = 1
tf = 1
n   = fill(1,(tl,1))
dn  = fill(0,(tl,1))
I   = fill(1,(tl,1))
dI  = fill(0,(tl,1))

#-----------------------------------------------------------
#verified ki
ki = 1/(2*π)                        #[eγ/h → eγ/ħ]
ki = ki/0.6582119569                #[e/fs]
ki = ki * 1.602176634 * 1e-4        #[A or C/s after e to C and fs to s]
#verified kc
kc = 1.602176634 * 1e-19            #[C]

kl = 5.59*1e-10                     #[m]
μ₀ = 1.256637062 * 1e-6             #[H/m]
ϵ₀ = 8.8541878128 * 1e-12           #[F/m  or C/Vm]
c  = 2.99792458 * 1e8
tr = kl/(c*1e-15*0.1)               #[s] 
#-----------------------------------------------------------
#all currents and charage densities are in SI units after ki kc
n  = kc * n
dn = kc * dn
I  = ki * I
dI = ki * dI
#-------------------------------------------------
#inputs for calculation made in same size
R0 = 1
rf(θ, ϕ) = R0*[cos(ϕ*π/180)*sin(θ*π/180), sin(ϕ*π/180)*sin(θ*π/180), cos(θ*π/180)]
ph = range(0,360,step=10)
th = [90]
S = zeros(length(ph), length(th))
#-------------------------------------------------
#constant for each term
#each E term verified
ec1 = 1/(4*π*ϵ₀ * kl * kl)  
ec2 = 1/(4*π*ϵ₀ * kl * c * 1e-16)
ec3 = 1/(4*π*ϵ₀ * c * c * 1e-16)
#
bc1 = μ₀ / (4*π * kl)
bc2 = μ₀ / (4*π * c * 1e-16)

E = zeros(3,length(ph))
B = zeros(3,length(ph))
S = zeros(3,length(ph))
pos = zeros(3,length(ph))

NInt = 1000
#println(FInt.bf_t1(pos_atoms[:,1],pos_atoms[:,2], rf(90,0), 3000))
for (ind,ϕ) in enumerate(ph)
    r = rf(90, ϕ)
    pos[:,ind] = r
    E[:,ind], B[:,ind], S[:,ind] = FInt.Jefimenko(pos_atoms,bonds,dir_bonds,n,dn,I,dI,
                                          r,NInt,ti,tf,rl,bl,ec1,ec2,ec3,bc1,bc2,μ₀) 
end
fsave = h5open("toy.hdf5","w")
    write(fsave, "E", E)
    write(fsave, "B", B)
    write(fsave, "S", S)
    write(fsave, "pos", pos)
close(fsave)
#println(pos[:,1])
#println(I[1,1])
#println("E at 1: ", E[:,1])
#println("E coulomb: ", (1/(4*π*ϵ₀)) * n[1,1] * (1/(R0*R0*kl*kl)) )
println("B at R0: ", B[:,1])
println("B simple: ", (μ₀ * I[1,1] / (2*π*R0*kl)))
#println(pos_atoms[:,1],pos_atoms[:,2], rf(90,0), NInt)
