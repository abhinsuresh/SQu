using HDF5, MKL, Base.Threads, LinearAlgebra
include("field_wrapper.jl")
verbose = true


#getting lattice parameters 
lattice     =  h5open("results/lattice.hdf5", "r")
pos_atoms   = lattice["pos_atoms"][]
#have to update indices to start from 1
bonds       = Int8.(lattice["bonds"][] .+ 1)
dir_bonds   = lattice["dir_bonds"][]
close(lattice)

#println(size(dir_bonds), size(pos_atoms), size(bonds))
#(3, 108)(3, 27)(2, 108)

#getting input curr and rho
data = h5open("results/08_01.hdf5","r")
#only getting charge current from all
curr = data["curr"][:,:,1]
rho_f = data["rho_diag"][]
#pulse = data["pulse"][]
close(data)
#summing spin up and dn
rho = rho_f[:,1:2:end] + rho_f[:,2:2:end]

#print(size(curr)," ",size(rho_f),"\n")

#-----------------------------------------------------------
#constants
ki = 1/(2*π)
ki = ki/0.6582119569                #[e/fs]
ki = ki * 1.602176634 * 1e-4        #[A] or [C/s]
kc = 1.602176634 * 1e-19            #[C]

kl = 5.59*1e-10                     #[m]
μ₀ = 1.256637062 * 1e-6             #[H/m]
ϵ₀ = 8.8541878128 * 1e-12           #[F/m]
c  = 2.99792458 * 1e8
tr = kl/(c*1e-15*0.1)               #[s] 
Nint = 10
#-----------------------------------------------------------
#converting input to SI units
curr = curr * ki
rho = rho * kc
dcurr = zeros(size(curr[1:end-1,:]))
drho  = zeros(size(rho[1:end-1,:]))
println("curr, rho shape: ",size(curr), size(rho))

tf, bl = size(curr)
tf, rl = size(rho)
println("tf ", tf, " bl ", bl)
println("tf ", tf, " rl ", rl)

#-------------------------------------------------
#finding derivatives
@threads for b_ind in range(1,bl)
    for t_ind in range(1,tf-1)
        dcurr[t_ind, b_ind] = (curr[t_ind+1,b_ind] - curr[t_ind,b_ind])/0.1
    end
end
@threads for r_ind in range(1,rl)
    for t_ind in range(1,tf-1)
       drho[t_ind, r_ind] = (rho[t_ind+1,r_ind] - rho[t_ind,r_ind])/0.1
    end
end

#-------------------------------------------------
#inputs for calculation made in same size
ϕ = 0
θ = 0
rf(θ, ϕ) = 100*[cos(ϕ*π/180)*sin(θ*π/180), sin(ϕ*π/180)*sin(θ*π/180), cos(θ*π/180)]
I  = curr[1:end-1,:]
dI = dcurr
n = rho[1:end-1,:]
dn = drho
#output of the calculation
E = zeros(3, tf-1)
B = zeros(3, tf-1)
S = zeros(3, tf-1)

#constant for each term
#-------------------------------------------------
#finding electric field
#finding magnetic field
#finding poynting vector
ec1 = kc/(4*π*ϵ₀ * kl * kl)  
ec2 = kc/(4*π*ϵ₀ * kl * c * 1e-16)
ec3 = 1/(4*π*ϵ₀ * c * c * 1e-16)
bc1 = μ₀ / (4*π * kl)
bc2 = μ₀ / (4*π * c * 1e-16)
@time begin
r = rf(0,0)
@threads for t_ind in range(1,tf-1)
    for s_ind in range(1,rl)
        e1, sb1 = FInt.ef_t1(pos_atoms[:,s_ind], r)
        e2, sb2 = FInt.ef_t2(pos_atoms[:,s_ind], r)
        (t_ind - sb1) < 1 ? val1 = 0 : val1 =  n[t_ind-sb1]        
        (t_ind - sb2) < 1 ? val2 = 0 : val2 = dn[t_ind-sb2]
        E[:, t_ind] += ec1 * e1 * val1        
        E[:, t_ind] += ec2 * e2 * val2
    end
    for b_ind in range(1,bl)
        e3, sb3 = FInt.ef_t3(pos_atoms[:,bonds[1,b_ind]], dir_bonds[:,b_ind], r)
        (t_ind - sb3) < 1 ? val3 = 0 : val3 = dI[t_ind-sb3, b_ind]
        E[:, t_ind] += ec3 * e3 * val3
    end

    for b_ind in range(1,bl)
        B[:, t_ind] += bc1 * FInt.bf_t1(pos_atoms[:,bonds[1,b_ind]],
                                     pos_atoms[:,bonds[2,b_ind]],
                                     r, Nint) * I[t_ind, b_ind] 
        B[:, t_ind] += bc2 * FInt.bf_t2(pos_atoms[:,bonds[1,b_ind]],
                                     pos_atoms[:,bonds[2,b_ind]],
                                     r, Nint) * dI[t_ind, b_ind]
    end
    S[:, t_ind] = (1/μ₀) * cross(E[:, t_ind],B[:, t_ind])
end
r = rf(5,0)
@threads for t_ind in range(1,tf-1)
    for s_ind in range(1,rl)
        e1, sb1 = FInt.ef_t1(pos_atoms[:,s_ind], r)
        e2, sb2 = FInt.ef_t2(pos_atoms[:,s_ind], r)
        (t_ind - sb1) < 1 ? val1 = 0 : val1 =  n[t_ind-sb1]        
        (t_ind - sb2) < 1 ? val2 = 0 : val2 = dn[t_ind-sb2]
        E[:, t_ind] += ec1 * e1 * val1        
        E[:, t_ind] += ec2 * e2 * val2
    end
    for b_ind in range(1,bl)
        e3, sb3 = FInt.ef_t3(pos_atoms[:,bonds[1,b_ind]], dir_bonds[:,b_ind], r)
        (t_ind - sb3) < 1 ? val3 = 0 : val3 = dI[t_ind-sb3, b_ind]
        E[:, t_ind] += ec3 * e3 * val3
    end

    for b_ind in range(1,bl)
        B[:, t_ind] += bc1 * FInt.bf_t1(pos_atoms[:,bonds[1,b_ind]],
                                     pos_atoms[:,bonds[2,b_ind]],
                                     r, Nint) * I[t_ind, b_ind] 
        B[:, t_ind] += bc2 * FInt.bf_t2(pos_atoms[:,bonds[1,b_ind]],
                                     pos_atoms[:,bonds[2,b_ind]],
                                     r, Nint) * dI[t_ind, b_ind]
    end
    S[:, t_ind] = (1/μ₀) * cross(E[:, t_ind],B[:, t_ind])
end
end
