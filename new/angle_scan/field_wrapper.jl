module FInt
    using LinearAlgebra, Base.Threads, MKL
    kl  = 5.59*1e-10
    c   = 2.99792458*1e8
    t_r = kl/(c*1e-15*0.1)

    function ef_t1(rn::Vector{Float64}, r::Vector{Float64})
        dr = r - rn
        l_dr = norm(dr)
        sb = trunc(Int, t_r*l_dr)
        return dr/(l_dr^3), sb
    end
    
    function ef_t2(rn::Vector{Float64}, r::Vector{Float64})
        dr = r - rn
        l_dr = norm(dr)
        sb = trunc(Int, t_r*l_dr)
        return dr/(l_dr^2), sb
    end
    
    function ef_t3(r1::Vector{Float64}, r2::Vector{Float64}, 
                    r::Vector{Float64}, Nint=10)
        dt = 1/Nint
        val = [0.0, 0.0, 0.0]
        dl = (r2 - r1)*dt
        for tstep in range(1, Nint)
            l = r1 + tstep*dl
            rl = r - l
            val = val + dl/norm(rl)
        end
        #not taking different tdiff for each segment
        sb = trunc(Int,t_r*norm(r-(r2-r1)/2))
        return val, sb
    end

    function bf_t1(r1::Vector{Float64}, r2::Vector{Float64}, 
                     r::Vector{Float64}, Nint=10)
        dt = 1/Nint
        val = [0.0, 0.0, 0.0]
        dl = (r2 - r1)*dt
        for tstep in range(1, Nint)
            l = r1 + tstep*dl
            rl = r - l
            val = val + cross(dl,rl)/(norm(rl)^3)
        end
        #not taking different tdiff for each segment
        sb = trunc(Int,t_r*norm(r-(r2-r1)/2))
        return val, sb        
    end

    function bf_t2(r1::Vector{Float64}, r2::Vector{Float64}, 
                     r::Vector{Float64}, Nint=10)
        dt = 1/Nint
        val = [0.0, 0.0, 0.0]
        dl = (r2 - r1)*dt
        for tstep in range(1, Nint)
            l = r1 + tstep*dl
            rl = r - l
            val = val + cross(dl, rl)/(norm(rl)^2)
        end
        #not taking different tdiff for each segment
        sb = trunc(Int,t_r*norm(r-(r2-r1)/2))
        return val, sb
    end

    function Jefimenko(pos_atoms, bonds, dir_bonds, n, dn, I, dI, r, NInt,
                       ti, tf, rl, bl, ec1, ec2, ec3, bc1, bc2, μ₀)
        S = zeros(tf-ti+1)
        @threads for t_val in range(ti, tf)
            E = zeros(3)
            B = zeros(3)
            for s_ind in range(1,rl)
                e1, sb1 = ef_t1(pos_atoms[:,s_ind], r)
                e2, sb2 = ef_t2(pos_atoms[:,s_ind], r)
                #(t_val - sb1) < 1 ? val1 = 0 : val1 =  n[t_val-sb1, s_ind]        
                #(t_val - sb2) < 1 ? val2 = 0 : val2 = dn[t_val-sb2, s_ind]
                val1 = n[t_val, s_ind]
                val2 = dn[t_val, s_ind]
                E += ec1 * e1 * val1        
                E += ec2 * e2 * val2
            end
            for b_ind in range(1,bl)
                e3, sb3 = ef_t3(pos_atoms[:,bonds[1,b_ind]],
                                pos_atoms[:,bonds[2,b_ind]], r, NInt)
                #(t_val - sb3) < 1 ? val3 = 0 : val3 = dI[t_val-sb3, b_ind]
                val3 = dI[t_val, b_ind]
                E += ec3 * e3 * val3
            end

            for b_ind in range(1,bl)
                b1, sb1 = bf_t1(pos_atoms[:,bonds[1,b_ind]],
                                pos_atoms[:,bonds[2,b_ind]], r, NInt) 
                val1 = I[t_val, b_ind]
                b2, sb2 = bf_t2(pos_atoms[:,bonds[1,b_ind]],
                                pos_atoms[:,bonds[2,b_ind]], r, NInt)
                val2 = dI[t_val, b_ind]
                B += bc1 * b1 * val1
                B += bc2 * b2 * val2
            end
            S[t_val-ti+1] = dot((1/μ₀) * cross(E, B), r)
            #S[t_val-ti+1,:] = (1/μ₀) * cross(E, B)
            #S = (1/μ₀) * cross(E, B)
        end
        return sum(S)
    end

    function ToyJef(pos_atoms, bonds, dir_bonds, n, dn, I, dI, r, NInt,
                       ti, tf, rl, bl, ec1, ec2, ec3, bc1, bc2, μ₀)
        S = zeros(3)
        E = zeros(3)
        B = zeros(3)
        for s_ind in range(1,rl)
            e1, sb1 = ef_t1(pos_atoms[:,s_ind], r)
            e2, sb2 = ef_t2(pos_atoms[:,s_ind], r)
            #(t_val - sb1) < 1 ? val1 = 0 : val1 =  n[t_val-sb1, s_ind]        
            #(t_val - sb2) < 1 ? val2 = 0 : val2 = dn[t_val-sb2, s_ind]
            val1 = n[t_val, s_ind]
            val2 = dn[t_val, s_ind]
            E += ec1 * e1 * val1        
            E += ec2 * e2 * val2
        end
        for b_ind in range(1,bl)
            e3, sb3 = ef_t3(pos_atoms[:,bonds[1,b_ind]],
                            pos_atoms[:,bonds[2,b_ind]], r, NInt)
            #(t_val - sb3) < 1 ? val3 = 0 : val3 = dI[t_val-sb3, b_ind]
            val3 = dI[t_val, b_ind]
            E += ec3 * e3 * val3
        end
        for b_ind in range(1,bl)
            b1, sb1 = bf_t1(pos_atoms[:,bonds[1,b_ind]],
                            pos_atoms[:,bonds[2,b_ind]], r, NInt) 
            val1 = I[t_val, b_ind]
            b2, sb2 = bf_t2(pos_atoms[:,bonds[1,b_ind]],
                            pos_atoms[:,bonds[2,b_ind]], r, NInt)
            val2 = dI[t_val, b_ind]
            B += bc1 * b1 * val1
            B += bc2 * b2 * val2
        end
        Sdot = dot((1/μ₀) * cross(E, B), r)
        return E, B, S, Sdot
    end

end
