import numpy as np, sys, h5py, time
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from field_wrapper import *
sys.path.append('../../plot_manager/')
from plot2d import plotfig_6p, plotfig_2p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
t_ini = time.time()
case = '01'
print('file prop:', sys.argv[1], case)


theta = np.arange(0,180,10)*np.pi/180
phi   = np.arange(0,360,20)*np.pi/180


#integration along bonds split
N = 10
#create the system
#-----------------
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1, domain=2)
#-------------------------------------------------------------------
curr    = h5py.File(sys.argv[1])['curr'][:,:,:-1]   
rho_f     = h5py.File(sys.argv[1])['rho_diag'][:,:-1]
#summing over spin indices for each site
rho = rho_f[::2,:] + rho_f[1::2,:]

#setting measuring values to zero
Poynting = np.zeros((len(theta),len(phi),3, curr.shape[2]-1))

pulse   = h5py.File(sys.argv[1])['pulse'][:]


#r = 100*np.array([np.cos(pval)*np.sin(tval), np.sin(pval)*np.sin(tval),
#                  np.cos(tval)])
#E, B, S = extract_light(r, N, curr, rho, pulse, lat.pos_atoms,
#                        lat.bonds, lat.dir_bonds, sys.argv[1], case, verbose=1)
print(type(curr), type(rho), type(pulse), type(lat.pos_atoms), type(lat.bonds))

with h5py.File('results/lattice.hdf5', 'w') as f:
    f.create_dataset("pos_atoms",data = lat.pos_atoms, dtype=np.float64, compression="gzip")
    f.create_dataset("bonds",data = lat.bonds, dtype=np.float64, compression="gzip")
    f.create_dataset("dir_bonds",data = lat.dir_bonds, dtype=np.float64, compression="gzip")
f.close()

