import numpy as np
import time
import h5py
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import identity as eye
import sys
from constants import HBAR, KB
from numba_wrapper import lindblad as nlindblad
from numba_wrapper import lindblad_t as nlindblad_t
#HBAR = 1

class Henk_Lindblad:
    """Class implemented to call Lindblad evolution
    """
    def __init__(self,
                 w,
                 dt = 0.1, # fs 
                 gamma_sc=2*1e-4, # fs-1
                 gamma_sf=2*1e-6, # fs-1
                 gamma_dp=5*1e-2, # fs-1
                 mu = 0, # eV
                 temp = 0 # Kelvin
                ):
        self.w = w
        self.dt = dt
        self.mu = mu
        self.temp = temp + 1e-15
        self.gamma_sc = gamma_sc
        self.gamma_sf = gamma_sf
        self.gamma_dp = gamma_dp
        self.g_sum = gamma_sc + gamma_sf + gamma_dp
        self.serr = True
        return 
    
    def fbe(self, de):
        beta = 1/(KB*self.temp)
        return 1/( np.exp((de-self.mu)*beta) - 1 ) 
 
    def trace_test(self, rho_t):
        """Linblad evolution in energy basis 0f H0
        """
        rho_new = np.zeros(rho_t.shape, rho_t.dtype)
        fac = 1
        for n in range(rho_t.shape[0]):
            for m in range(rho_t.shape[0]):
                #daigonal part 1st term of sum
                rho_new[n,n] = rho_new[n,n] + fac*(rho_t[m,m])
                #non-digonal part 2nd term of sum
                rho_new[m,:] = rho_new[m,:] - 0.5*fac*(rho_t[m,:])
                rho_new[:,m] = rho_new[:,m] - 0.5*fac*(rho_t[:,m])
        return rho_new

    def lindblad(self, rho_t, w):
        """Linblad evolution in energy basis 0f H0
        """
        rho_new = np.zeros(rho_t.shape, rho_t.dtype)
        for n in range(rho_t.shape[0]):
            for m in range(rho_t.shape[0]):
                #term preserving pauli exclusion principle
                pi_nm = rho_t[m,m]*(1 - rho_t[n,n])
                #term proportional to sc and sf process both added
                fac = (self.gamma_sc + self.gamma_sf) * pi_nm
                #term accounting for de-/excitation factors 
                de = w[n] - w[m]
                #excitation
                if de > 0:
                    fac = fac * self.fbe(np.abs(de))
                    #daigonal part 1st term of sum
                    rho_new[n,n] = rho_new[n,n] + fac*(rho_t[m,m])
                    #non-digonal part 2nd term of sum
                    rho_new[m,:] = rho_new[m,:] - 0.5*fac*(rho_t[m,:])
                    rho_new[:,m] = rho_new[:,m] - 0.5*fac*(rho_t[:,m])
                #de-excitation
                elif de < 0:
                    fac = fac *  (self.fbe(np.abs(de)) + 1)
                    #daigonal part 1st term of sum
                    rho_new[n,n] = rho_new[n,n] + fac*(rho_t[m,m])
                    #non-digonal part 2nd term of sum
                    rho_new[m,:] = rho_new[m,:] - 0.5*fac*(rho_t[m,:])
                    rho_new[:,m] = rho_new[:,m] - 0.5*fac*(rho_t[:,m])
                #dephasing
                elif de == 0 or n==m:
                    fac = self.gamma_dp
                    #daigonal part 1st term of sum
                    rho_new[n,n] = rho_new[n,n] + fac*(rho_t[m,m]) 
                    #non-digonal part 2nd term of sum
                    rho_new[m,:] = rho_new[m,:] - 0.5*fac*(rho_t[m,:])
                    rho_new[:,m] = rho_new[:,m] - 0.5*fac*(rho_t[:,m])
        return rho_new

    def evolve_rho(self, rho_t, Ht, w):
        """Linblad evolution in energy basis 0f H0
        if self.g_sum > 1e-10:
            check = self.lindblad(rho_t, w)
            #checking if conserving trace, else saving full rho
            if np.abs(np.trace(check)) > 1e-6 :
                print("trace greater thatn 1e-6", np.trace(check))
                if self.serr:
                    hf = h5py.File('rho_err.h5', 'w')
                    hf.create_dataset('rho', data=rho_t)
                    hf.close()
                    self.serr = False 
            d_rho = 1j*(rho_t @ Ht - Ht @ rho_t)/HBAR + check
        else:
            d_rho = 1j*(rho_t @ Ht - Ht @ rho_t)/HBAR
        """
        #d_rho = 1j*(rho_t @ Ht - Ht @ rho_t)/HBAR + self.lindblad(rho_t, w)
        d_rho = 1j*(rho_t @ Ht - Ht @ rho_t)/HBAR + self.lindblad_t(rho_t, w)
        #d_rho = 1j*(rho_t @ Ht - Ht @ rho_t)/HBAR + nlindblad_t(rho_t, w, self.temp, self.mu)
        return d_rho
    
    def rk4_open(self, rho_t, Ht, w=None):
        """rk4_rho evolution in energy basis 0f H0
        """
        if w is None:
            w = self.w
        rho1 = self.evolve_rho(rho_t,            Ht, w)*self.dt
        rho2 = self.evolve_rho(rho_t + rho1*0.5, Ht, w)*self.dt
        rho3 = self.evolve_rho(rho_t + rho2*0.5, Ht, w)*self.dt
        rho4 = self.evolve_rho(rho_t + rho3,     Ht, w)*self.dt
        rho_new = rho_t + (rho1 + 2*rho2 + 2*rho3 + rho4)*(1/6)
        return rho_new 

    def lindblad_t(self, rho_t, w):
        """Linblad evolution in energy basis 0f H0 with only one
        explicit sum loop doing the opreations, all gamma_nm
        could be saved before hand
        """
        #diagonal part
        rho_new = np.zeros(rho_t.shape)
        for n in range(rho_t.shape[0]):
            gamma_nm = np.zeros(rho_t.shape[0])
            for m in range(rho_t.shape[0]):
                de = w[n] - w[m]
                pi_nm = rho_t[m,m]*(1 - rho_t[n,n])
                fac = (self.gamma_sc + self.gamma_sf) * pi_nm
                if m == n: 
                    gamma_nm[m] = self.gamma_dp
                elif de > 0: 
                    gamma_nm[m] = fac * (self.fbe(np.abs(de)))
                elif de < 0:
                    gamma_nm[m] = fac * (self.fbe(np.abs(de)) + 1)
            #diagonla term
            rho_new[n,n] = rho_new[n,n] + np.dot(gamma_nm, np.diag(rho_t))
            #other term
            rho_new = rho_new - 0.5*(np.diag(gamma_nm)@rho_t + rho_t@np.diag(gamma_nm))
        return rho_new



#normal rk4 closed system    
def evolve_rho(rho, Ht):
    rho_new = 1j*(rho @ Ht - Ht @ rho)/HBAR
    return rho_new
def rk4_rho(rho, Ht, dt):
    """a function to evolve the wave function in time
    H_dynamic should be a function of time if not []
    """ 
    rho1 = evolve_rho(rho,            Ht)*dt
    rho2 = evolve_rho(rho + rho1*0.5, Ht)*dt
    rho3 = evolve_rho(rho + rho2*0.5, Ht)*dt
    rho4 = evolve_rho(rho + rho3,     Ht)*dt
    rho = rho + (rho1 + 2*rho2 + 2*rho3 + rho4)*(1/6)
    return rho

#normal wavefunction evolution
def evolve(H_static, psi, dt, method='diag', time_dep=0):
    """a function to evolve the wave function in time
    if time independet, we return Ut and should be called outside
    the loop
    """
    size = H_static.shape[0]    
    if not time_dep:
        if method=='CN':
            Ut = sla.inv((eye(size) + 1j*dt*H_static/(2*HBAR)).tocsc()) @ \
                       (eye(size) - 1j*dt*H_static/(2*HBAR))
    else: 
        if method=='CN':
            Ht = H_static
            Ut = sla.inv(eye(size) + 1j*dt*Ht/(2*HBAR)) @ \
                        (eye(size) - 1j*dt*Ht/(2*HBAR))
            psi = Ut @ psi
            return psi


#def gpu_ode_integrator(H0=1, Ht=1, rho, time, op):
#    """gpu ode integrator
#    """
#    result = np.zeros
