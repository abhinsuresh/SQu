import numpy as np, time, h5py
from numba import njit, prange, complex64, float32, complex128


#unit convention
#-------------------------------------------------------------------
#if code multiplied by 2pi
ki  = 1/(2*np.pi)
ki  = ki/0.6582119569            #[e/fs]
ki  = ki*1.602176634 * 1e-4      #[A] or [C/s]
kc  = 1.602176634 * 1e-19        #[C]

kl  = 5.59*1e-10                  #[m] from Angstrom cgs=5.59 A
mu0 = 1.256637062*1e-6           #[H/m]
c   = 2.99792458*1e8             #[m/s]
ep0 = 8.8541878128*1e-12         #[F/m]
#kl/c = 1.8e-18 = 1e-2 * 0.1fs = 1e-2*dt
#if |r-l| * 1.8
t_r = kl/(c*1e-15*0.1)

def extract_light(r, N, curr, rho, pulse, pos_atoms, bonds, dir_bonds, 
                  nm='0', case='', p=None,b=None, verbose=False):
    #rr,tr = rho.shape
    #rho_new = np.zeros((rr//2, tr))
    #for t_ind in range(tr):
    #    for ind in range(rr//2):
    #        rho_new[ind, t_ind] = rho[2*ind, t_ind] + rho[2*ind + 1, t_ind]
    #if verbose: print(rho.shape, rho_new.shape)
    #rho = rho_new
    
    """
    Time retardation only applied for Electric field, now can be applied
    to magnetic field as well
    r: where to calculate
    N: steps in dividing the bond
    curr: current
    rho: charge density
    pulse: pulse 
    nm: name of the saved file
    """
    if p is None: p = [0,len(pos_atoms)]
    if b is None: b = [0,len(bonds)]
    if verbose: print(case)
    #bring the current and charge density to si unit
    curr    = curr   * ki
    rho     = rho * kc
    pulse   = pulse
    if verbose: print('shapes of input files', curr.shape, rho.shape, pulse.shape)
    #extracting the current and charge density values
    bl,tl   = curr.shape[1:]
    rr,tr   = rho.shape
    ti      = 0
    dt      = 0.1 # in fs
    tf      = 14000
    times   = np.arange(ti, tf , dt)
    dcurr   = np.zeros((bl,tl-1))
    drho    = np.zeros((rr,tr-1))
    #-------------------------------------------------------------------
    #finding the derivatives, shape of derivative is one less
    #changing unit of dt to s is done while calculating
    for b_ind in range(bl):
        for t_ind in range(tl-1):
            dcurr[b_ind, t_ind] = (curr[0, b_ind,t_ind+1] - curr[0, b_ind,t_ind])/dt
    for r_ind in range(rr):
        for t_ind in range(tr-1):
            drho[r_ind, t_ind] = (rho[r_ind,t_ind+1] - rho[r_ind,t_ind])/dt
    #-------------------------------------------------------------------
    #setting the derivatives
    t_span = times[:-1]
    I = curr[:,:,:-1]
    dI = dcurr
    n = rho[:,:-1]
    dn = drho
    E = np.zeros((3,len(t_span)))
    B = np.zeros((3,len(t_span)))
    S = np.zeros((3,len(t_span)))
    #-------------------------------------------------------------------
    #maybe plot all these quantities to see them
    if verbose: print('shape of quantities:\t',t_span.shape, I.shape, dI.shape, n.shape, dn.shape)
    #-------------------------------------------------------------------
    start = time.time()
    #finding electric field
    #constants for each term
    ec1 = (kc)/(4*np.pi*ep0 * kl*kl)
    ec2 = (kc)/(4*np.pi*ep0 * kl * c * 1e-16)
    ec3 = (1)/(4*np.pi*ep0 * c*c * 1e-16)
    if verbose: print(len(t_span), E.shape)
    for t_ind in range(len(t_span)):
        #summing over all sites
        for ind, site_pos in enumerate(pos_atoms[p[0]:p[1]]):
            e1, sb1 = ef_t1(site_pos, r)
            e2, sb2 = ef_t2(site_pos, r)
            val_1  =  n[ind, t_ind-sb1]
            val_2  = dn[ind, t_ind-sb2]
            if (t_ind-sb1) < 0: val_1 = 0
            if (t_ind-sb2) < 0: val_2 = 0
            E[:,t_ind] = E[:,t_ind] + ec1 * e1 * val_1
            E[:,t_ind] = E[:,t_ind] + ec2 * e2 * val_2
        #summing over all bonds
        for ind, bond in enumerate(bonds[b[0]:b[1]]):
            e3, sb3 = ef_nt3(pos_atoms[bond[0]], dir_bonds[ind], r)
            val_3 = dI[ind, t_ind-sb3]
            if (t_ind-sb3) < 0: val_3 = 0
            E[:,t_ind] = E[:,t_ind] + ec3 * e3 * val_3
    if verbose: print("time taken for E:\t", time.time()-start)
    #-------------------------------------------------------------------
    #finding magnetic field
    #constants for each term
    bc1 = mu0/(4*np.pi * kl)
    bc2 = mu0/(4*np.pi * c * 1e-16)

    for t_ind in range(len(t_span)):
        t_i = t_ind + int(ti/dt)
        #summing over all bonds
        for ind, bond in enumerate(bonds[b[0]:b[1]]):
            B[:,t_ind] = B[:,t_ind] + bc1 * bf_nt1(pos_atoms[bond[0]], 
                                            pos_atoms[bond[1]], 
                                            r, N)* I[0,ind,t_i] \
                                    + bc2 * bf_nt2(pos_atoms[bond[0]], 
                                            pos_atoms[bond[1]], 
                                            r, N)* dI[ind,t_i]
    if verbose: print("time taken for E + B:\t", time.time()-start)

    #finding S
    for t_ind in range(len(t_span)):
        S[:,t_ind] = (1/mu0) * np.cross(E[:,t_ind], B[:,t_ind])

    with h5py.File(nm, 'a') as f:
        f.create_dataset("E"+case,data = E, dtype=np.float64, compression="gzip")
        f.create_dataset("B"+case,data = B, dtype=np.float64, compression="gzip")
        f.create_dataset("S"+case,data = S, dtype=np.float64, compression="gzip")
    f.close()
    
    return E, B, S


#-------------------------------------------------------------------
def get_fft(t_span, fun, nm='filename'):
    """
    tspan: time step in fs 
    fun: function to get fft
    """
    #print('max frequency')
    #print('min frequency')

    dt = t_span[1] - t_span[0]
    N = t_span.size
    yif = np.fft.fft(fun)
    yf = (2/N)*np.abs(yif[0:N//2])**2
    xf = np.fft.fftfreq(N, dt)[:N//2]

    #normalize max value to 1
    m_val = np.max(yf)
    yf = yf/m_val

    return xf, yf


@njit()
def t_nsum(t_len, pos_atoms, bonds, r, I, dI):
    B = np.zeros((3, t_len), dtype=np.float64)
    for t_ind in range(t_len):
        for ind, bond in enumerate(bonds):
            r1 = pos_atoms[bond[0]]
            r2 = pos_atoms[bond[1]]
            N = 100.0
            dt = 1/N
            val1 = np.array([0.0, 0.0, 0.0])
            val2 = np.array([0.0, 0.0, 0.0])
            dl  = ((r2-r1)*dt)/np.linalg.norm(r2-r1)
            for tstep in np.arange(1, N, dtype=np.float64):
                l       = r1 + tstep * dt * (r2 - r1)
                l_rl    = np.linalg.norm(r - l)

                val1 = val1 + np.cross(dl, r - l)/(l_rl**3)
                val2 = val2 + np.cross(dl, r - l)/(l_rl**2)

            B[:,t_ind] =  B[:,t_ind] + val1*I[ind, t_ind] \
                                     + val2*dI[ind, t_ind]
    print(t_ind)
    return B

@njit()
def bf_nt1(r1, r2, r, N=100):
    """find the magnetic field term1 for a line charge
    """
    dt = 1/N    
    val = np.array([0.0, 0.0, 0.0])
    dl  = ((r2-r1)*dt)/np.linalg.norm(r2-r1)
    for tstep in np.arange(1, N, dtype=np.float64):
        l       = r1 + tstep * dt * (r2 - r1)
        rl      = r - l
        rl_l    = np.linalg.norm(rl)

        val = val + np.cross(dl, rl)/(rl_l**3)

    return val

@njit()
def bf_nt2(r1, r2, r, N=100):
    """find the magnetic field term2 for a line charge
    """

    dt = 1/N    

    val = np.array([0.0, 0.0, 0.0])
    dl  = ((r2-r1)*dt)/np.linalg.norm(r2-r1)
    for tstep in np.arange(1, N, dtype=np.float64):
        l       = r1 + tstep * dt * (r2 - r1)
        rl      = r - l
        l_rl    = np.linalg.norm(r - l)

        val = val + np.cross(dl, rl)/(l_rl**2)

    return val


#@njit()
def ef_t1(rn, r):
    """find the magnetic field term2 for a line charge
    """
    #rn = lat.pos_atoms[site]
    dr = r - rn
    l_dr = np.linalg.norm(dr)
    s_b = int(t_r*l_dr//1)
        
    return dr/(l_dr**3), s_b

#@njit()
def ef_t2(rn, r):
    """find the magnetic field term2 for a line charge
    """
    #rn = lat.pos_atoms[site]
    dr = r - rn
    l_dr = np.linalg.norm(dr)
    s_b = int(t_r*l_dr/1)

    return dr/(l_dr**2), s_b

#@njit()
def ef_nt3(ri, dir_r, r):
    """find the magnetic field term2 for a line charge
    """
    val = np.array([0.0, 0.0, 0.0])
    dl  = (dir_r)/np.linalg.norm(dir_r)

    l       = ri
    rl      = r - l
    l_rl    = np.linalg.norm(r - l)
    val = val + np.cross(dl, rl)/(l_rl)

    s_b = int(t_r*l_rl/1)
    return val, s_b

def ef_t3(lat, bond, r, N=100):
    """find the magnetic field term2 for a line charge
    """
    dt = 1/N    
    r1 = lat.pos_atoms[bond[0]]
    r2 = lat.pos_atoms[bond[1]]
    val = 0
    dl  = ((r2-r1)*dt)/np.linalg.norm(r2-r1)

    for tstep in range(1, N):
        l       = r1 + tstep * dt * (r2 - r1)
        rl      = r - l
        l_rl    = np.linalg.norm(r - l)
        val = val + np.cross(dl, rl)/(l_rl)
    return val



def test_bf_t1(r2, r1, r, N=100):
    """find the magnetic field term1 for a line current
    """
    
    dt = 1/N    

    val = 0
    dl  = ((r2-r1)*dt)/np.linalg.norm(r2-r1)
    for tstep in range(1, N):
        l       = r1 + tstep * dt * (r2 - r1)
        rl      = r - l
        rl_l    = np.linalg.norm(rl)

        val = val + np.cross(dl, rl)/(rl_l**3)
    return val

def test_ef_t1(ri, r, N=100):
    """find the electric field term1 for a point charge
    """
    
    dt = 1/N    

    val = 0
    for tstep in range(1, N):
        l       = r1 + tstep * dt * (r2 - r1)
        rl      = r - l
        rl_l    = np.linalg.norm(rl)

        val = val + np.cross(dl, rl)/(rl_l**3)
    return val

def bf_t1(lat, bond, r, N=100):
    """find the magnetic field term1 for a line charge
    """
    dt = 1/N    
    r1 = lat.pos_atoms[bond[0]]
    r2 = lat.pos_atoms[bond[1]]
    val = 0
    dl  = ((r2-r1)*dt)/np.linalg.norm(r2-r1)
    for tstep in range(1, N):
        l       = r1 + tstep * dt * (r2 - r1)
        rl      = r - l
        rl_l    = np.linalg.norm(rl)

        val = val + np.cross(dl, rl)/(rl_l**3)

    return val

def bf_t2(lat, bond, r, N=100):
    """find the magnetic field term2 for a line charge
    """

    dt = 1/N    

    r1 = lat.pos_atoms[bond[0]]
    r2 = lat.pos_atoms[bond[1]]
    val = 0
    dl  = ((r2-r1)*dt)/np.linalg.norm(r2-r1)

    for tstep in range(1, N):
        l       = r1 + tstep * dt * (r2 - r1)
        rl      = r - l
        l_rl    = np.linalg.norm(r - l)

        val = val + np.cross(dl, rl)/(l_rl**2)

    return val
