import numpy as np
import h5py
import sys
sys.path.append('../')
from field_wrapper import extract_light, get_fft
sys.path.append('../../plot_manager/')
from plot2d import plotfig_6p

nm = '4'
not_saved = 0
if not_saved:
    curr1 = h5py.File(sys.argv[1])['curr'][:]
    curr2 = h5py.File(sys.argv[2])['curr'][:]
    curr3 = h5py.File(sys.argv[3])['curr'][:]
    pulse = h5py.File(sys.argv[1])['pulse'][:]

    print('pulse shape', pulse.shape)
    dt = 0.1
    tf = int((curr1.shape[2]-1)*dt )
    print('tf', tf)
    times = np.arange(0, tf + dt, dt)
    print(sys.argv[1], nm)

    sx = np.zeros((len(times),3))
    sy = np.zeros((len(times),3))
    sz = np.zeros((len(times),3))
    ch = np.zeros((len(times),3))
    n1 = 81
    n2 = 90
    for i in range(len(times)):
        for j in range(n1,n2):
            ch[i,0] = ch[i,0] + curr1[0, j, i]
            sx[i,0] = sx[i,0] + curr1[1, j, i]
            sy[i,0] = sy[i,0] + curr1[2, j, i]
            sz[i,0] = sz[i,0] + curr1[3, j, i]
    for i in range(len(times)):
        for j in range(n1,n2):
            ch[i,1] = ch[i,1] + curr2[0, j, i]
            sx[i,1] = sx[i,1] + curr2[1, j, i]
            sy[i,1] = sy[i,1] + curr2[2, j, i]
            sz[i,1] = sz[i,1] + curr2[3, j, i]
    for i in range(len(times)):
        for j in range(n1,n2):
            ch[i,2] = ch[i,2] + curr3[0, j, i]
            sx[i,2] = sx[i,2] + curr3[1, j, i]
            sy[i,2] = sy[i,2] + curr3[2, j, i]
            sz[i,2] = sz[i,2] + curr3[3, j, i]

    plot_y = np.zeros((len(times),7))
    plot_x = times/1000
    plot_y[:,0:3] = ch[:,0:3]
    plot_y[:,3:6] = sz[:,0:3]
    plot_y[:,6] = pulse[:,0]
    #save results
    f = h5py.File('results/fig4.hdf5', 'w')
    f.create_dataset('plot_y', data=plot_y)
    f.create_dataset('plot_x', data=plot_x)
    f.close()
else:
    f = h5py.File('results/fig4.hdf5','r')
    plot_y = f['plot_y'][:]
    plot_x = f['plot_x'][:]
    f.close()

data = dict();values = dict()
data['x_plot'] = plot_x; data['y_plot'] = plot_y
values['xlabel1'] = values['xlabel2'] = values['xlabel3'] = \
values['ylabel2'] = values['ylabel3'] = values['ylabel5'] = \
values['ylabel6'] = ''
values['xlabel4'] = values['xlabel5'] = \
values['xlabel6'] = r'$\mathrm{Time\ (ps)}$'

values['ylabel1'] = r'$\mathrm{I_{Pt}\ (e\gamma/h)}$'
values['ylabel4'] = r'$\mathrm{I_{Pt}^{S_z}\ (e\gamma/h)}$'
sf = np.array([[0,1],[1,2],[2,3],[3,4],[4,5],[5,6]])
Cl = ['k', 'k', 'k', 'C3', 'C3', 'C3', 'C2']
Gs = dict();
Gs['t']=0.95; Gs['b']=0.11; Gs['l']=0.07; Gs['r']=0.99
Gs['w']=0.18; Gs['h']=0.1
Pi = [68000]*8
Pf = [72000]*8
Lpx = [1,1,1, 1,1,1]; Lpy = [1,1,1, 1,1,1]
Tx = [r'$\mathrm{LLG\ off\ and\ SOC\ off}$', r'$\mathrm{LLG\ on\ and\ SOC\ off}$',
      r'$\mathrm{LLG\ on\ and\ SOC\ on}$']
plotfig_6p(data, nm, values, sf=sf, Cl=Cl, Gs=Gs, 
           Lpx=Lpx, Lpy=Lpy, Tx=Tx, Pi=Pi, Pf=Pf, minor=1, 
           text=1, fs=9, lw=0.4, plot_pulse=1)
