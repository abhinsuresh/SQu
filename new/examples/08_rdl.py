import numpy as np, sys, os, h5py, time, datetime
import scipy.linalg as la
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from numba_wrapper import *
from constants import HBAR
from evolve_step import rk4_rho, Henk_Lindblad 
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
start = time.time()

nm = os.path.basename(__file__)[:2]
print("Evaluation file name:\t", os.path.basename(__file__), 'res:\t', nm)
print("Evaluation time:\t", datetime.datetime.now())
#input parameters
#-----------------
dt = 0.1
temp = 300
tf = 14000
dt = 0.1
m_step = 1
ad = 0
#light vector potential function
def vec(t, return_par=False):
    omega_l = 1.54/HBAR
    s = 50
    t_0 = 7000
    E0 = 0.12
    Et = E0*np.cos(omega_l*t) * np.exp(-(t - t_0)*(t - t_0)/(2*s*s))
    if return_par:
        return np.array([s, t_0, E0, omega_l, HBAR])
    else: return Et

theta_l = 0 * np.pi/180
lp_vec = np.array([np.cos(theta_l), np.sin(theta_l), 0])
inp =  vec(0, True)

#create the system
#-----------------
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1, domain=2)
ham = Op(lat, tso_pt=1.0, jexc=-0.0028, ani=-0.000187, 
         dmi=-0.00063, g_lambda=0.2)
print('t0:',ham.t0,'tj:',ham.tj,'t1:',ham.t1,'t_pt:',ham.t_pt,'l_z:',ham.lambda_z,'tso_pt:',ham.tso_pt)
cspin = ham.cspins
print('jexc:',cspin.jexc,'dmi:',cspin.dmi, 'ani:',cspin.ani,'jsd:',cspin.jsd_to_llg,"dmi':",cspin.dmi_prime, 'g_lambda:',cspin.g_lambda)
Ht = ham.create_H()
#-------------------------------------------------------------------
#finding initial state
wt,vt = np.linalg.eigh(Ht)
#transforming to energy basis
def e_basis(mat, v=vt):
    return v.conj().T @ mat @ v
#transforming to site basis
def s_basis(mat, v=vt):
    return v @ mat @ v.conj().T
e_exp = np.diag(fermi_fun(wt, 0, temp))
rho_e = e_exp 
rho_s = s_basis(rho_e)

#equilibrium measurements of current printed
bb = [3, 12]
print(bb, ham.spin_curr_rho(bb, rho_s, Ht, 'all'), np.trace(rho_s))

#setting the evolution and transformation
evolve = Henk_Lindblad(wt, temp=temp)

#time evolution
t_range = np.arange(0, tf + dt, dt)
lp_pulse = np.zeros((len(t_range)//m_step+ad,3))
espins = np.zeros((len(ham.cspins.s),3))
curr = np.zeros((4,len(lat.bonds), len(t_range)//m_step+ad))
rho_save =  np.zeros((len(wt), len(t_range)//m_step+ad))
save_spins = np.zeros((len(espins),3,len(t_range)//m_step+ad))
print(curr.shape, wt.shape)

t_c = 0
for t_ind, t_now in enumerate(t_range):    
    #measurements
    if t_ind % m_step == 0:
        lp_pulse[t_c,:] = lp_vec * vec(t_now)
        rho_save[:,t_c] = np.diag(rho_s)
        save_spins[:,:,t_c] = ham.cspins.s
        for c_ind in range(len(lat.bonds)):
            curr[:, c_ind, t_c] = \
            ham.spin_curr_rho(lat.bonds[c_ind], rho_s, Ht, 'all')
        t_c = t_c +  1

    #print time taken
    if t_ind%1000 == 0:
        print(t_ind, ':\t', '%0.5e'%np.trace(rho_e), time.time()-start)
 
    #time update the hamiltonian with pulse and spins
    pulse = lp_vec * vec(t_now)
    #Ht0 is H (updated spins alone)
    Ht0 = ham.create_H(spins=ham.cspins.s)
    #Ht is H + Vt(light part + updated spins)
    Ht  = ham.create_H(spins=ham.cspins.s, lp_vec=pulse)
    
    #briging back to site basis then turning back to new energy basis
    wt, vt = np.linalg.eigh(Ht0)
    rho_e  = e_basis(rho_s, vt)
    Ht_e   = e_basis(Ht, vt)
    
    #convert to energy basis and evolve
    rho_e = evolve.rk4_open(rho_e, Ht_e, wt)
    #bring back to site basis for measurements and next step
    rho_s = s_basis(rho_e, vt)
    espins = ham.rho_alpha_nn(rho_s)

    #update classical
    ham.cspins.llg(espins, t_now)

#-------------------------------------------------------------------
with h5py.File('results/'+nm+'.hdf5', 'w') as f:
    f.create_dataset("curr",data = curr, dtype=np.float64, compression="gzip")
    f.create_dataset("rho_diag",data = rho_save, dtype=np.float64, compression="gzip")
    f.create_dataset("cspins",data = save_spins, dtype=np.float64, compression="gzip")
    f.create_dataset("pulse",data = lp_pulse, dtype=np.float64, compression="gzip")
    f.create_dataset("light",data = inp, dtype=np.float64, compression="gzip")
f.close()
print(time.time()-start)
