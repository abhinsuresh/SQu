import numpy as np, sys, h5py, time
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from field_wrapper import *
sys.path.append('../../plot_manager/')
from plot2d import plotfig_6p, plotfig_2p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
start = time.time()
case = '01'
#print('file prop:', sys.argv[1], case)


#postion in which we are measuring
r = np.array([0,0,100])
N = 10
#create the system
#-----------------
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1, domain=2)
ham = Op(lat, tso_pt=0.0, jexc=-0.0028, ani=-0.000187, 
         dmi=-0.00063, g_lambda=0.01, lambda_z=0)
Ht = ham.create_H()
#-------------------------------------------------------------------
curr    = h5py.File(sys.argv[1])['curr'][:,:,:-1]   
rho_f     = h5py.File(sys.argv[1])['rho_diag'][:,:-1]
#summing over spin indices for each site
rho = rho_f[::2,:] + rho_f[1::2,:]

pulse   = h5py.File(sys.argv[1])['pulse'][:]

E, B, S = extract_light(r, N, curr, rho, pulse, lat.pos_atoms,
                        lat.bonds, lat.dir_bonds, sys.argv[1], case) 
                        #p=[9,27],b=[65,74])

#print(E.shape, B.shape)
