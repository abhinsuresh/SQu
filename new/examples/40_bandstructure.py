import numpy as np, sys, os, h5py, time, datetime
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from numba_wrapper import *
from constants import HBAR
from evolve_step import rk4_rho, Henk_Lindblad 
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p

nm = os.path.basename(__file__)[:2]
print("Evaluation file name:\t", os.path.basename(__file__), 'res:\t', nm)
print("Evaluation time:\t", datetime.datetime.now())
#input parameters
#-----------------
cspin = h5py.File('results/07.hdf5','r')['cspins'][:]
save = cspin[:,:,-2]
t_span = np.arange(0, 4000+0.1, 0.1)
#create the system
#-----------------
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1)
ham = Op(lat, spin_dir = cspin[:,:,60000])
Ht = ham.create_H()
w0 = np.linalg.eigvals(Ht)
print(w0[0],w0[-1])
diff = np.zeros((len(t_span),1))
for t_ind, t_now in enumerate(t_span):
    Ht = ham.create_H(spins = cspin[:,:,t_ind+60000])
    w = np.linalg.eigvals(Ht)
    diff[t_ind] = np.sum( 100*np.abs(w - w0)/np.abs(w0) ) 

data = dict();values = dict()
data['x_plot'] = (t_span+6000)/1000; data['y_plot'] = diff
values['xlabel1'] = r'$\mathrm{Time\ (ps)}$'
values['ylabel1'] = r'$\mathrm{\sum_i \frac{|\epsilon_i(t)-\epsilon_i(t_0)|}{|\epsilon_i(t_0)|}\ (\%)}$'
sf = np.array([[0,1]])
Cl = ['C0', 'C2', 'C3']
plot_1p(data, 'band', values, sf=sf, Cl=Cl, fs=7, lw=0.2)
