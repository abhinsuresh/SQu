import numpy as np
import h5py
import sys
sys.path.append('../')
from field_wrapper import extract_light, get_fft
sys.path.append('../../plot_manager/')
from plot2d import plotfig_6p1

nm = '5.1'
case = '01'
E1 = h5py.File(sys.argv[1])['E'+case]
E2 = h5py.File(sys.argv[2])['E'+case]
E3 = h5py.File(sys.argv[3])['E'+case]
pulse = h5py.File(sys.argv[1])['pulse'][:-2,:]

print('pulse shape', pulse.shape, E1.shape)
dt = 0.1
tf = 14000
print('tf', tf)
times = np.arange(0, tf, dt)[:-1]
print(sys.argv[1], nm)


ti = 60000
tf = 130000

fc_x1, fc_y1 = get_fft(times[ti:tf], E1[0,ti:tf])
fc_x2, fc_y2 = get_fft(times[ti:tf], E2[0,ti:tf])
fc_x3, fc_y3 = get_fft(times[ti:tf], E3[0,ti:tf])
#print('check', pulse[ti:tf,0].shape, E1[0,ti:tf].shape)

#fp_x, fp_y = get_fft(times[ti:tf], pulse[ti:tf,0])

print(fc_x1.shape, fc_y1.shape, fc_x2.shape, fc_y2.shape)


plot_y = np.zeros((len(fc_x1),7))
plot_x = fc_x1 
plot_y[:,0] = fc_y1
plot_y[:,1] = fc_y2
plot_y[:,2] = fc_y3
plot_y[:,3] = fc_y1
plot_y[:,4] = fc_y2
plot_y[:,5] = fc_y3
#plot_y[:,6] = fp_y

data = dict();values = dict()
data['x_plot'] = plot_x; data['y_plot'] = plot_y
values['xlabel1'] = values['xlabel2'] = values['xlabel3'] = \
r'$\mathrm{Frequency\ (THz)}$'

values['ylabel2'] = values['ylabel3'] = values['ylabel5'] = \
values['ylabel6'] = ''
values['xlabel4'] = values['xlabel5'] = \
values['xlabel6'] = r'$\mathrm{Frequency\ \Omega/\Omega_0}$'
#values['xlabel6'] = r'$\mathrm{Frequency\ (10^3\ THz)}$'

values['ylabel1'] = r'$\mathrm{FFT\ Power}$' + '\n' + r'$\mathrm{|E_x|^2\ (au)}$'
values['ylabel4'] = r'$\mathrm{FFT\ Power}$' + '\n' + r'$\mathrm{|E_x|^2\ (au)}$'
sf = np.array([[0,1],[1,2],[2,3],[3,4],[4,5],[5,6]])
Cl = ['C0']*12
Gs = dict();
Gs['t']=0.95; Gs['b']=0.11; Gs['l']=0.1; Gs['r']=0.99
Gs['w']=0.25; Gs['h']=0.35
Lpx = [1,1,1,1, 1,1,1,1]; Lpy = [1,1,1,1, 1,1,1,1]
Tx = [r'$\mathrm{LLG\ off\ and\ SOC\ off}$', r'$\mathrm{LLG\ on\ and\ SOC\ off}$',
      r'$\mathrm{LLG\ on\ and\ SOC\ on}$']
plotfig_6p1(data, nm, values, sf=sf, Cl=Cl, Gs=Gs, 
           Lpx=Lpx, Lpy=Lpy, Tx=Tx, text=1, fs=9, lw=0.4, 
           plot_pulse=0, xlim = (0,8))
