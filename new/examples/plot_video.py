import numpy as np
import scipy.linalg as la
import h5py, sys, time
sys.path.append('../')
from mn3sn_core import Lattice
from constants import *
from hamiltonian_core import Op
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#create the system
#-----------------
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1, domain=2, PBC=0)
#spins_eq = h5py.File('scl_eq.hdf5','r')['cspins'][:]
#ham = Op(lat, spin_dir=spins_eq)
#lat.plotfig(ham, 'sys_t0', lw=0.2, mew=0.5, mks=2.5, a_w=0.5)

cspins = h5py.File('results/07.hdf5','r')['cspins'][:]
d = 0
cx = np.zeros(len(cspins[0,0,:]));cy=np.zeros(len(cspins[0,0,:]));cz=np.zeros(len(cspins[0,0,:]))
for ma in range(54):
    cx = cx + cspins[ma,0,:]/54 - cspins[ma,0,0]*d
    cy = cy + cspins[ma,1,:]/54 - cspins[ma,1,0]*d
    cz = cz + cspins[ma,2,:]/54 - cspins[ma,2,0]*d


f = h5py.File('results/fig3.hdf5','r')
ch1 = f['plot_y'][:,1]
sz1 = f['plot_y'][:,4]
sx1 = f['plot_y'][:,7]
sy1 = f['plot_y'][:,8]
pulse = f['plot_y'][:,6]
times = f['plot_x'][:]
f.close()
#f = h5py.File('results/fig4.hdf5','r')
#ch2 = f['plot_y'][:,1]
#f.close()
    
data = np.zeros((len(times),8))
data[:,0] = pulse
data[:,1] = ch1
data[:,2] = sx1
data[:,3] = sy1
data[:,4] = sz1
data[:,5] = cx
data[:,6] = cy
data[:,7] = cz

t_ini = 73000

#t_index = 1000
tn = 1
for t_index in np.arange(0,12000)[::20]:
    
    t_plot = (t_ini+t_index)
    plot_y = data[t_ini:,:]
    plot_x = times[t_ini:]

    t_name = 'ap/%4.4d'%tn
    t_now = (t_ini+t_index)*0.1
    spin_eq = cspins[:,:,t_plot]
    ham = Op(lat, spin_dir=spin_eq, tso_pt=0.0)
    lat.plot_video(ham, plot_x, plot_y, t_index, fig_name=t_name, t_now=t_now, t_ini=t_ini,
                    lw=0.2, mew=0.2, mks=3, w=5, h =2, fs = 9)
    tn = tn + 1

#import os
#os.system("rsync -r test abhins@bloch.physics.udel.edu:projects/active/mn3sn/results/ap/.")
