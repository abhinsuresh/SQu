from mn3sn_core import Lattice
import numpy as np

from hamiltonian_core import Op
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1, domain=2, PBC=1)
ham = Op(lat)
lat.plotfig(ham, 'sys_2', lw=0.1, mew=0.5, mks=1.5, a_w=0.4, fs=2)

espins = np.zeros((len(ham.cspins.s),3))
ham.cspins.llg(espins,0)
#print(np.concatenate((np.array(lat.hopy2_list), np.array(lat.dir_y2)),axis=1))
#print(np.concatenate((np.array(lat.hop_inter), np.array(lat.dir_z)),axis=1))
#print(len(lat.hop_inter))

#print(len(lat.dir_x), len(lat.dir_y1), len(lat.dir_y2), len(lat.dir_z))
print(lat.bond_len)
#print(np.linalg.norm(lat.dir_y2[10]))
print(np.array(lat.bonds[:18]))
#print(lat.dir_bonds[-10])
