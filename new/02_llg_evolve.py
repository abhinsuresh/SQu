"""A code to run LLG alone in Mn3Sn to verfiy the LLG ground state
of the system with default parameters taken from base paper 
multiplied by some factor to increase the speed of the evolution.
Also save the final configuration of LLG as scl_eq file, if needed
to load as initial value for running with electronic dof, and plot
the configuration and dynamics as function of time
"""
import numpy as np, sys
from mn3sn_core import Lattice
from hamiltonian_core import Op
sys.path.append('../plot_manager/')
from plot2d import plotfig

tf = 1000
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1, domain=2)
#ham = Op(lat)
ham = Op(lat, tso_pt=0.0, jexc=-0.0028,
         ani=-0.000187, dmi=-0.00063, g_lambda=0.01)

print('t0:',ham.t0,'tj:',ham.tj,'t1:',ham.t1,'t_pt:',ham.t_pt,'l_z:',ham.lambda_z,'tso_pt:',ham.tso_pt)
cspin = ham.cspins
print('jexc:',cspin.jexc,'dmi:',cspin.dmi, 'ani:',cspin.ani,'jsd:',cspin.jsd_to_llg,"dmi':",cspin.dmi_prime)

times = np.arange(0, tf + ham.dt, ham.dt)
espins = np.zeros((len(ham.cspins.s),3))
#-------------------------------------------------------------------
with open('scl.txt', "w") as fref:
    fref.write('#reference data printed\n')      
fref.close()
save_spins = np.zeros((len(espins),3,len(times)))

with open('scl.txt', "a") as f:
    for t_ind, t_now in enumerate(times):
        out_str = '{:5.2e} '.format(t_now)
        for s in ham.cspins.s:
            out_str += '{0: 1.3e} {1: 1.3e} {2: 1.3e} '.format(*s)
        out_str += '\n'
        f.write(out_str)
        save_spins[:,:,t_ind] = ham.cspins.s
        ham.cspins.llg(espins, t_now)
f.close()
lat.plotfig(ham, 'sys_tf', lw=0.1, mew=0.5, mks=1.5, a_w=0.4, fs=2)

#-------------------------------------------------------------------
data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = save_spins[:3,0,:].T
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{{\bf M}_1}$'
sf = np.array([[0,3]])
Cl = ['C0', 'C2', 'C3']
plotfig(data, 'llg_ev', values, sf=sf, Cl=Cl, fs=8, lw=0.2)
