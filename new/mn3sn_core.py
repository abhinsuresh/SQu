import numpy as np
import sys
from numba_wrapper import vector, unit_vector, flatten_list 
from kwant_systems import make_kagome_bilayer_3d
from index_core import *
from spins import Spins
from constants import SIG_X, SIG_Y, SIG_Z
from plot_core import plotfig, plot_video 


class Lattice:

    def __init__(self,
                    Lx = 3,
                    Ly = 3,
                    layers = 2,
                    ad_pt = 1,
                    domain = 2, # earlier 6 was only option
                    PBC = 1,
                    dtype = np.complex64
                ):
        self.Lx = Lx
        self.Ly = Ly
        self.layers = layers
        self.ad_pt = ad_pt
        self.domain = domain
        self.dtype = dtype
        self.PBC = PBC
        self.hop_inter = []
        self.hopx_pt = []
        self.hopy_pt = []
        self.hop_inter_pt = []
        self.bond_len = dict()
        self.pos_atoms = []
    
        ###
        self.generate()
        return

    def generate(self):
        """generates all the hopping indices required
        """
        #Sn
        self.intra_Sn()
        self.inter_Sn()
        self.ribbon = make_kagome_bilayer_3d(self.Lx, self.Ly, self.PBC)
        self.link_MnSn()
        self.pos_Sn()
        self.dir_hop()
        self.dir_hop_inter()
        if self.ad_pt and self.layers==2:
            self.hop_Pt()
            self.inter_Pt()
        #Mn
        self.dir_cspin()
        self.find_anivec()
        self.pos_Mn = np.array([site.pos for site in self.ribbon.sites])
         
    def intra_Sn(self):
        """Find the intra layer hopping list for triangular plane lattice
        and extend for bilayer
        -Base hopping in TB Hamiltonian
        Constructs
        ----------
        self.hopx_list  -> intra Sn hopping
        self.hopy1_list -> "
        self.hopy2_list -> "
        self.bonds and self.bond_len -> dict()
        self.ccoup      -> to get dir_ani
        self.sd_hop     -> for llg
        """
        intra_Sn(self)

    def inter_Sn(self):
        """Find the inter layer hopping elements of bilayer Sn, only
        exist in the presence of bilayer
        -Base hopping TB hamiltonian
        Constructs
        ----------
        self.hop_inter  -> inter Sn hopping
        """
        inter_Sn(self)

    def link_MnSn(self):
        """Find the classical spin site index of Mn for
        corresponding hopx/y1/y2_list in order
        -Required for jsd part of TB hamiltonian
        Constructs
        ----------
        self.x_cup  -> jsd of hopx_list
        self.y1_cup -> jsd of hopy1_list
        self.y2_cup -> jsd of hopy2_list
        """
        link_MnSn(self)

    def pos_Sn(self):
        """Find the position of Sn atoms for both layers 
        position of second relative to first is not real values, 
        but they don't appear in equations as i know it.
        -Base position TB Hamiltonian
        Constructs
        ----------
        self.pos_Sn1/2
        """
        pos_Sn(self)

    def dir_hop(self):        
        """find the vector along the direction of kin hop
        -Required to use in TB Hamiltonian, like to add light
        these are constant value for each object
        Constructs
        ----------
        self.dir_x
        self.dir_y1
        self.dir_y2
        self.dir_bonds
        """
        dir_hop(self)

    def dir_hop_inter(self):        
        """find the vector along the direction of kin hop
        -Required for TB Hamiltonian or to measure emission
        Constructs
        ----------
        self.dir_z  - direction inter Sn hopping
        """
        dir_hop_inter(self)

    def dir_cspin(self):
        """get the orientation of the cspins
        -Required to assign the initial direction of cspins
        Constructs
        ----------
        self.cspin_dir
        """
        dir_cspin(self)
        
    def find_anivec(self):
        """Calcualates the direction from Mn to corresponding Sn,
        or between Sn's
        -Used as anisotropy in LLG
        Constructs
        ----------
        self.dir_ani    -> anisotropy direction for LLG
        """
        find_anivec(self)

    def hop_Pt(self):
        """Find the hopping in Pt layer, assuming its the
        third layer
        -Required to construct TB Hamiltonian
        Constructs
        ----------
        self.hopx_pt
        self.hopy_pt
        """
        hop_Pt(self)
        
    def inter_Pt(self):
        """Find the inter layer hopping 2nd layer Sn and Pt
        -Required to construct TB Hamiltonian
        Constructs
        ----------
        self.hop_inter_pt
        """
        inter_Pt(self)


    #---------------------------------------------------------------
    # basic math functions
    #---------------------------------------------------------------

    def bond_v(self, i, j):
        """find the bond direction from bond indices
        """
        return vector(self.pos_atoms[i], self.pos_atoms[j])
    
    def bond_uv(self, i, j):
        """find the bond direction from bond indices
        """
        return unit_vector(self.pos_atoms[i], self.pos_atoms[j])
    def rotate_sample(self, theta):
        """rotate the sample by theta radian using z-axis 
        rotation matrix
        """
        mat = np.array([[np.cos(theta),-np.sin(theta),0],
                        [np.sin(theta), np.cos(theta),0],
                        [0            , 0            ,1]])
        for i in range(len(self.pos_atoms)):
            self.pos_atoms[i,:] = mat @ self.pos_atoms[i,:]

    #---------------------------------------------------------------
    # plot functions
    #---------------------------------------------------------------
    def plotfig(self, ham, fig_name, values=dict(), espins=np.ones((9,3)), 
                Gs=dict(), **kwargs):		
        plotfig(self, ham, fig_name, values=dict(), espins=np.ones((9,3)), 
                Gs=Gs, **kwargs)	
        
    def plot_video(self, ham, times, plot_y, t_plot, fig_name, t_now, values=dict(), 
                  Gs=dict(), **kwargs):		
        plot_video(self, ham, times, plot_y, t_plot, fig_name, t_now, values=dict(), 
                  Gs=Gs, **kwargs)	
