import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
from scipy.sparse import eye
from scipy.sparse import kron

def spin_check(sx, sy, sz):
    """A function to verify spin commutation realtions
    """
    if ( ((sx@sy - sy@sx) != 1j*sz).nnz==0 and
         ((sy@sz - sz@sy) != 1j*sx).nnz==0 and
         ((sz@sx - sx@sz) != 1j*sy).nnz==0     ):
        print('statisfies spin commutation realtion')
    else:
        print('sad life')
    
def c_check(op, site = 0):
    """A function to verify c-operator anticommutation realtions
    """
    Cup = op['Cup' + str(site)]
    Cdn = op['Cdn' + str(site)]
    d = Cup.toarray().shape[0]
    zero = coo_matrix((d, d), dtype=np.float64).tocsr()
    
    if ( ((Cup@Cup.getH() + Cup.getH()@Cup) != eye(d)).nnz==0 and
         ((Cdn@Cdn.getH() + Cdn.getH()@Cdn) != eye(d)).nnz==0 and
         ((Cup@Cdn.getH() + Cdn.getH()@Cup) != zero).nnz==0 and
         ((Cdn@Cup.getH() + Cup.getH()@Cdn) != zero).nnz==0 and
         ((Cup@Cdn + Cdn@Cup) != zero).nnz==0 and
         ((Cup.getH()@Cdn.getH() + 
           Cdn.getH()@Cup.getH()) != zero).nnz==0 ):
        print('Stisfies c commutation realtions')
    else:
        print('another sad life')
    
    if len(op)//2 > 1 :
        Cup0 = op['Cup0']; Cdn0 = op['Cdn0']
        Cup1 = op['Cup0']; Cdn1 = op['Cdn1'].getH()
        if ( ((Cup0@Cup1 + Cup1@Cup0) != zero).nnz==0  and
             ((Cdn0@Cdn1 + Cdn1@Cdn0) != zero).nnz==0  ):
            print('Two sites anticommute as well')
        else:
            print('No..., not this again')
        


