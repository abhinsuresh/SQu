#using MKL
using MAT
include("../julia/solver_wrapper.jl")

println("Heff read time")
@time Heff = matread("../inputs_sv/Heff.mat")["Heff"]

psi1 = transpose(matread("../inputs_sv/psi1.mat")["psi1"]);

psi1f = collect(psi1)

println("\nHeff * psi1")
@time Heff * psi1;
@time Heff * psi1;

println("\nHeff * psi1f")
@time Heff * psi1f;
@time Heff * psi1f;

println("\nrk4(psi1f, Heff)")
@time rk4(psi1f, Heff, 0.1)
#@time rk4(psi1f, Heff, 0.1)

println("\nrk4(psi1, Heff)")
@time rk4(psi1, Heff, 0.1)
#@time rk4(psi1, Heff, 0.1)

