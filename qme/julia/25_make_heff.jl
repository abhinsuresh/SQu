using MKL
using MAT

@time H0 = collect(matread("../inputs_sv/H.mat")["H"])
flush(stdout)
@time L1 = matread("../inputs_sv/L1.mat")["L1"]
flush(stdout)
@time L2 = matread("../inputs_sv/L2.mat")["L2"]
flush(stdout)
@time L3 = matread("../inputs_sv/L3.mat")["L3"]
flush(stdout)
@time L4 = matread("../inputs_sv/L4.mat")["L4"]
flush(stdout)
@time L5 = matread("../inputs_sv/L5.mat")["L5"]
flush(stdout)
@time L6 = matread("../inputs_sv/L6.mat")["L6"]
flush(stdout)
@time L7 = matread("../inputs_sv/L7.mat")["L7"]
flush(stdout)
@time L8 = matread("../inputs_sv/L8.mat")["L8"]

Heff = H0 .- 0.5*im*(L1'*L1 .+ L2'*L2 .+ L3'*L3 .+ L4'*L4 .+ 
                     L5'*L5 .+ L6'*L6 .+ L7'*L7 .+ L8'*L8)

#@time Heff[abs.(Heff) .< 1e-15] .= 0
#flush(stdout)
@time matwrite("../inputs_sv/Heff8.mat",Dict("Heff"=>Heff))
