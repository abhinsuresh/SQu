using BenchmarkTools
include("solver_wrapper.jl")

function nsum(psi, B=1, dt=0.1)
    r1 = 0
    #psi1 = zeros(length(psi))
    for i in 1:11
        rk4_test!(psi, B, dt) 
        #rk4_test!(psi, psi1, B, dt) 
    end
    #println("psi1 \n", psi1)
    return psi
end
N = 5
psi = ones(N)
println("psi ", psi)
@time psi = nsum(psi)
println("psi ", psi)
