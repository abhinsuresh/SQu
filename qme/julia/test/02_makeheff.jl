using MKL
using MAT

H0 = collect(matread("H.mat")["H"])
@time L1 = matread("L1.mat")["L1"]
L2 = matread("L2.mat")["L2"]
L3 = matread("L3.mat")["L3"]
L4 = matread("L4.mat")["L4"]

Heff = H0 .- 0.5*im*(L1'*L1 .+ L2'*L2 .+ L3'*L3 .+ L4'*L4) 

#@time Heff[abs.(Heff) .< 1e-15] .= 0
#flush(stdout)
@time matwrite("Heff.mat",Dict("Heff"=>Heff))
