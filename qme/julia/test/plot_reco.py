import sys
import numpy as np, h5py
from stat_wrapper import *
sys.path.append('../../')
from density_core import *
sys.path.append('../../../plot_manager/')
from plot2d import plotfig


times = np.arange(0,30,1)
res1 = h5py.File("results/res_10_100a.hdf5")["r"][:].T + \
       1j*h5py.File("results/res_10_100a.hdf5")["i"][:].T
res2 = h5py.File("results/res_10_100b.hdf5")["r"][:].T + \
       1j*h5py.File("results/res_10_100b.hdf5")["i"][:].T


print(res1[:,0,0].shape, res2.shape)
out = np.zeros((len(times), 100))

for n_ind in range(10):
    for t_ind, t in enumerate(times):
        rhof = 0.5*(res1[:,t_ind:t_ind+1,n_ind] @ res1[:,t_ind:t_ind+1,n_ind].conj().T +
                    res2[:,t_ind:t_ind+1,n_ind] @ res2[:,t_ind:t_ind+1,n_ind].conj().T)
        rhos = partial_trace_A(rhof,120,16)
        out[t_ind, n_ind] = logarithmic_negativity(rhos,m=4)
        """
        rhos1 = partial_psi_trace(res1[:,t_ind,n_ind],120)
        rhos2 = partial_psi_trace(res2[:,t_ind,n_ind],120)
        out[t_ind, n_ind] = 0.5*(logarithmic_negativity(rhos1,m=4) + \
                            logarithmic_negativity(rhos2,m=4))
        """


l_neg = mean(out)


result = np.zeros((len(times),1))
result[:,0] = l_neg 

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{\langle \hat{S}_z^i \rangle}$'
values['ylabel2'] = r'$\mathrm{Log\ Neg}$'
sf = np.array([[0,4],[4,5]])
plotfig(data, 'traj_10', values)
