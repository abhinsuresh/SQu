using MKL
using MAT, LinearAlgebra
include("solver_wrapper.jl")
include("density_wrapper.jl")
t_i = time()
H   = collect(matread("H.mat")["H"])
rho = collect(matread("rho.mat")["rho"])
#psi1 = transpose(collect(matread("psi1.mat")["psi2"]))
#rho = psi1 * psi1'

Se  = matread("Se.mat")
L1   = collect(matread("L1.mat")["L1"])
L2   = collect(matread("L2.mat")["L2"])
L3   = collect(matread("L3.mat")["L3"])
L4   = collect(matread("L4.mat")["L4"])

L_op = [L1, L2, L3, L4]

Lplus = -im*H - 0.5*sum([L'*L for L in L_op])
Lminus = im*H - 0.5*sum([L'*L for L in L_op])

#Lplus = -im*H - 0.5*(L_op'*L_op)
#Lminus = im*H - 0.5*(L_op'*L_op)

println("time readin and making L+ ", time()-t_i)

tf  = 30
dt  = 0.1
tstep = 1
times = range(0,tf,step=dt)

function tevolve(rho, H, Se, Lplus, Lminus, L_op, times, dt)
    ob = zeros(length(times)÷10,5)
    Ie = I(120)
    Op1 = kron(Ie,collect(Se["sz0"]))
    Op2 = kron(Ie,collect(Se["sz1"]))
    Op3 = kron(Ie,collect(Se["sz2"]))
    Op4 = kron(Ie,collect(Se["sz3"]))
    for (t_ind, t) in enumerate(times)
        @time rho = rk4_rho_open(rho, Lplus, Lminus, L_op, dt)
        #println(real(tr(rho*Op)))
        if t_ind%10==0
            ob[t_ind÷10,1] = real(tr(rho*Op1))
            ob[t_ind÷10,2] = real(tr(rho*Op2))
            ob[t_ind÷10,3] = real(tr(rho*Op3))
            ob[t_ind÷10,4] = real(tr(rho*Op4))
            rhos = partial_trace_A(rho,120,16)
            ob[t_ind÷10,5] = real(logarithmic_negativity(rhos,4))
        end
    end
    return ob
end

@time result = tevolve(rho, H, Se, Lplus, Lminus, L_op, times, dt)
matwrite("results/res_03.mat", Dict("ob"=>result))
