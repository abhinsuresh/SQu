using MKL
using MAT, Base.Threads
using LinearAlgebra, HDF5
start = time()

include("solver_wrapper.jl")
#include("density_wrapper.jl")

psi0 = vec(collect(matread("4k_mat/psi0.mat")["psi0"]))
H0   = collect(matread("4k_mat/H0.mat")["H0"])
m_op = collect(matread("4k_mat/m.mat")["m"])
L1   = collect(matread("4k_mat/L1.mat")["L1"])
L2   = collect(matread("4k_mat/L2.mat")["L2"])
L3   = collect(matread("4k_mat/L3.mat")["L3"])
L4   = collect(matread("4k_mat/L4.mat")["L4"])

L5   = collect(matread("4k_mat/L5.mat")["L5"])
L6   = collect(matread("4k_mat/L6.mat")["L6"])
L7   = collect(matread("4k_mat/L7.mat")["L7"])
L8   = collect(matread("4k_mat/L8.mat")["L8"])
"""
L9   = collect(matread("4k_mat/L9.mat")["L9"])
L10   = collect(matread("4k_mat/L10.mat")["L10"])
"""

L_op = [L1, L2, L3, L4, L5, L6, L7, L8]#, L9, L10]
#L_op = [L1]
#println("read of inputs ", time()-start)
heff = H0 .- im*0.5*(L1'*L1 .+ L2'*L2 .+ L3'*L3 .+ L4'*L4 .+
                     L5'*L5 .+ L6'*L6 .+ L7'*L7 .+ L8'*L8)# .+
                     #L9'*L9 .+ L10'*L10)

tf  = 500
dt  = 0.1
times = range(0,tf,step=dt)

function tevolve(psi0, heff, L_op, m_op, times, dt)
    ob = zeros(length(times)÷10)
    psi = psi0
    #if note defined outside the loop, r1 will only exist in
    #the local scope of if condition
    r1 = 0
    draw = true
    for (t_ind, t) in enumerate(times) 
        if t_ind%10== 0
            psim = psi/norm(psi)
            ob[t_ind÷10] = real((psim' * m_op * psim)[1,1])
        end
        if draw
            r1 = rand(1)[1]
        end
        psi = rk4(psi, heff, dt)
        dp = norm(psi)^2
        draw = false

        if dp <= r1
            dpm = [norm(L * psi)^2 for L in L_op]
            dps = cumsum(dpm)
            dps = dps/(last(dps))
            r2 = rand(1)[1]
            op_ind = 1
            for idx in 1:length(dps)
                if r2 > dps[idx]
                    op_ind = idx + 1
                end
            end
            psi = L_op[op_ind] * psi
            psi = psi/norm(psi)
            draw = true
        end
    end
    return ob
end


Ntraj = 500
function sum_trajectories(psi0, heff, L_op, m_op, times, dt, Ntraj)
    ob = zeros(Ntraj, length(times)÷10)
    @threads for n_ind in 1:Ntraj
        #println("time for ", n_ind)
        #@time ob[n_ind,:] = tevolve(psi0, heff, L_op, m_op, times, dt)
        ob[n_ind,:] = tevolve(psi0, heff, L_op, m_op, times, dt)
    end
    return ob
end

print("intial m", real(psi0' * m_op * psi0))

@time expect = sum_trajectories(psi0, heff, L_op, m_op, times, dt, Ntraj)


fsave = h5open("results/res_08_500.hdf5","w")
write(fsave, "expect", expect)
close(fsave)
