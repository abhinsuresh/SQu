import sys
import numpy as np
import mat73
sys.path.append('../../../plot_manager/')
from plot2d import plotfig_2p


times = np.arange(0,30,1)
result = np.zeros((len(times),4))
result = mat73.loadmat("results/res_03.mat")["ob"]

#result2 = mat73.loadmat("res_03.mat")["ob"]
#result[:,0] = result1[:,0]
#result[:,1] = result2[:,0]
#result[:,2] = result1[:,1]
#result[:,3] = result2[:,1]

print(result.shape, times.shape)

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{\langle \hat{S}_z^3 \rangle}$'
values['ylabel2'] = r'$\mathrm{Log\ Neg}$'
sf = np.array([[0,4],[4,5]])
plotfig_2p(data, 'master_1', values, sf=sf)
