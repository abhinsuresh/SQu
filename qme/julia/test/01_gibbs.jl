using MKL
using MAT, LinearAlgebra
include("solver_wrapper.jl")
include("density_wrapper.jl")

H   = collect(matread("H.mat")["H"])
rho = collect(matread("rho.mat")["rho"])
Se  = matread("Se.mat")
#L1   = matread("../gibbs_mat/L1.mat")["L1"]

tf  = 30
dt  = 0.1
tstep = 1
times = range(0,tf,step=dt)

function tevolve(rho, H, Se, times, dt)
    ob = zeros(length(times)÷10,5)
    Ie = I(120)
    Op1 = kron(Ie,collect(Se["sz0"]))
    Op2 = kron(Ie,collect(Se["sz1"]))
    Op3 = kron(Ie,collect(Se["sz2"]))
    Op4 = kron(Ie,collect(Se["sz3"]))
    for (t_ind, t) in enumerate(times)
        @time rho = rk4_rho(rho, H, dt)
        #@time rho = rk4_rho(rho, H, dt)
        #println(real(tr(rho*Op)))
        if t_ind%10==0
            ob[t_ind÷10,1] = real(tr(rho*Op1))
            ob[t_ind÷10,2] = real(tr(rho*Op2))
            ob[t_ind÷10,3] = real(tr(rho*Op3))
            ob[t_ind÷10,4] = real(tr(rho*Op4))
            rhos = partial_trace_A(rho,120,16)
            ob[t_ind÷10,5] = real(logarithmic_negativity(rhos,4))
        end
    end
    return ob
end

#@time result = tevolve(rho, H, Se, times, dt)
result = tevolve(rho, H, Se, times, dt)
matwrite("res_01.mat", Dict("ob"=>result))
