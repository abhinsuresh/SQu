using MKL
using MAT, Base.Threads
using LinearAlgebra, HDF5
start = time()

include("solver_wrapper.jl")
include("density_wrapper.jl")

#heff = collect(matread("../gibbs_mat/Heff.mat")["Heff"])
psi1 = vec(collect(matread("psi1.mat")["psi1"]))
psi2 = vec(collect(matread("psi2.mat")["psi2"]))
H0   = collect(matread("H.mat")["H"])
L1   = collect(matread("L1.mat")["L1"])
L2   = collect(matread("L2.mat")["L2"])
L3   = collect(matread("L3.mat")["L3"])
L4   = collect(matread("L4.mat")["L4"])
Se   = matread("Se.mat")

L_op = [L1, L2, L3, L4]
#println("read of inputs ", time()-start)
heff = H0 .- im*0.5*(L1'*L1 .+ L2'*L2 .+ L3'*L3 .+ L4'*L4)
#heff = H0 .- im*0.5*(L1'*L1)


function tevolve(psi0, heff, L_op, m_op, times, dt)
    ob = zeros(Complex, length(psi0), length(times)÷10)
    psi = psi0
    #if note defined outside the loop, r1 will only exist in
    #the local scope of if condition
    r1 = 0
    draw = true
    for (t_ind, t) in enumerate(times) 
        if draw
            r1 = rand(1)[1]
        end
        psi = rk4(psi, heff, dt)
        dp = norm(psi)^2
        draw = false

        if dp <= r1
            dpm = [norm(L * psi)^2 for L in L_op]
            dps = cumsum(dpm)
            dps = dps/(last(dps))
            r2 = rand(1)[1]
            op_ind = 1
            for idx in 1:length(dps)
                if r2 > dps[idx]
                    op_ind = idx + 1
                end
            end
            psi = L_op[op_ind] * psi
            psi = psi/norm(psi)
            draw = true
        end
        if t_ind%10 == 0
            psim = psi/norm(psi)
            ob[:, t_ind÷10] = psim
        end
    end
    return ob
end

function sum_trajectories(psi0, heff, L_op, m_op, times, dt, Ntraj)
    ob = zeros(Complex, length(psi0), length(times)÷10, Ntraj)
    @threads for n_ind in 1:Ntraj
        #println("time for ", n_ind)
        #@time ob[:,:, n_ind] = tevolve(psi0, heff, L_op, m_op, times, dt)
        ob[:,:, n_ind] = tevolve(psi0, heff, L_op, m_op, times, dt)
    end
    return ob
end

tf  = 30
dt  = 0.1
times = range(0,tf,step=dt)
m_op = [kron(I(120),Se["sz0"]), kron(I(120),Se["sz1"]), 
        kron(I(120),Se["sz2"]), kron(I(120),Se["sz3"])]

Ntraj = 100

@time expect = sum_trajectories(psi2, heff, L_op, m_op, times, dt, Ntraj)
println(size(expect),typeof(expect))
#matwrite("results/10a.mat",Dict("expect"=>expect))
fsave = h5open("results/res_10_100b.hdf5","w")
write(fsave, "r", real(expect))
write(fsave, "i", imag(expect))
close(fsave)

