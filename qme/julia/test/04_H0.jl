using MKL
using MAT, LinearAlgebra
include("solver_wrapper.jl")
include("density_wrapper.jl")

H   = collect(matread("H0.mat")["H"])
rho = collect(matread("rho.mat")["rho"])
Se  = matread("Se.mat")
#L1   = matread("../gibbs_mat/L1.mat")["L1"]

tf  = 30
dt  = 0.1
tstep = 1
times = range(0,tf,step=dt)

function tevolve(rho, H, Se, times, dt)
    ob = zeros(length(times)÷10,2)
    Ie = I(120)
    Op = kron(Ie,collect(Se["sz2"]))
    for (t_ind, t) in enumerate(times)
        @time rho = rk4_rho(rho, H, dt)
        #println(real(tr(rho*Op)))
        if t_ind%10==0
            ob[t_ind÷10,1] = real(tr(rho*Op))
            rhos = partial_trace_A(rho,120,16)
            ob[t_ind÷10,2] = real(logarithmic_negativity(rhos,4))
        end
    end
    return ob
end

@time result = tevolve(rho, H, Se, times, dt)
matwrite("res_04.mat", Dict("ob"=>result))
