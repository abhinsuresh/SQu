import numpy as np
import h5py
import sys
from numba import njit

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

data = dict();values = dict()


#@njit
def mean(data):
    s  = data.shape[0]
    Nc = data.shape[1]
    avg = np.zeros(s)
    for i in range(s):
        for j in range(Nc):
            avg[i] = avg[i] + data[i,j]  
    return avg/Nc
#@njit
def std(data, mean):
    Nc = data.shape[1]
    #print(Nc)
    s = len(mean)
    sd = np.zeros(s)
    for i in range(s):
        for j in range(Nc):
            sd[i] = sd[i] + (mean[i]-data[i,j])**2
        sd[i] = np.sqrt(sd[i]/Nc)
    return sd
            
Ntraj = 500
res  = h5py.File("results/res_09_"+str(Ntraj)+".hdf5")["expect"][:]
#res  = h5py.File("res_08_12sites"+str(Ntraj)+"traj.hdf5")["expect"][:]
print(res.shape)
fig_name = "8sites"+str(Ntraj)
avg = mean(res)

print("1")
std = std(res, avg)
print("1")

_time = np.arange(0,500,1)


L = len(_time)
plot_y = np.zeros((len(_time),3))
plot_y[:,0] = avg
plot_y[:,1] = std/np.sqrt(Ntraj)
plot_y[:,2] = np.ones(len(_time))*0.283
print(plot_y.shape)

plot_x = _time; 
#values['ylabel'] = r'$\mathrm{{\langle {\hat{S}^x}_6 \rangle} - {\langle \hat{S}^x_2 \rangle}}$'
values['ylabel'] = r'$\mathrm{m_z}$'
values['xlabel'] = r'$\mathrm{Time\ (fs)}$'

    
data['x_plot'] = plot_x; data['y_plot'] = plot_y
Cl = ['k', 'grey', 'C0']
Ls = ['-', '-', '--', '--']
Mk = ['', '', '', '']
sf = np.array([[0,1]])
Ps = [2]*4
Alpha = [1,0.5,1]
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{m}$'
plotfig(data, fig_name, values, sf=sf, Cl=Cl, errorbar=True, Ps=Ps,
        fs=8, lw=0.4, Alpha=Alpha)
