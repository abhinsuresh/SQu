import sys
import numpy as np, h5py
from stat_wrapper import *
sys.path.append('../../../plot_manager/')
from plot2d import plotfig_2p


times = np.arange(0,30,0.5)
result = np.zeros((len(times),4))
res1 = h5py.File("results/res_07_100a.hdf5")["expect"][:].T
res2 = h5py.File("results/res_07_100b.hdf5")["expect"][:].T


out = np.zeros((len(times),5,200))
out[:,:,:100] = res1
out[:,:,100:200] = res2

print(res1.shape)
sz1 = mean(out[:,0,:])
sz2 = mean(out[:,1,:])
sz3 = mean(out[:,2,:])
sz4 = mean(out[:,3,:])
l_neg = mean(out[:,4,:])


result = np.zeros((len(times),5))
result[:,0] = sz1 
result[:,1] = sz2 
result[:,2] = sz3 
result[:,3] = sz4 
result[:,4] = l_neg 

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{\langle \hat{S}_z^i \rangle}$'
values['ylabel2'] = r'$\mathrm{Log\ Neg}$'
sf = np.array([[0,4],[4,5]])
plotfig_2p(data, 'traj_07_100', values, sf=sf)
