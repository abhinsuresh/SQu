import numpy as np


def avg(sdata, T = 100):
    L = len(sdata) - 1
    l = L//T
    avg = np.zeros(l)
    for i in range(l):
        avg[i] = np.sum(sdata[i*T: (i+1)*T])
    return avg/T

#@njit
def mean(data):
    s  = data.shape[0]
    Nc = data.shape[1]
    avg = np.zeros(s)
    for i in range(s):
        for j in range(Nc):
            avg[i] = avg[i] + data[i,j]  
    return avg/Nc
#@njit
def std(data, mean):
    Nc = data.shape[1]
    #print(Nc)
    s = len(mean)
    sd = np.zeros(s)
    for i in range(s):
        for j in range(Nc):
            sd[i] = sd[i] + (mean[i]-data[i,j])**2
        sd[i] = np.sqrt(sd[i]/Nc)
    return sd
