function dpsi(psit, Heff)
    return -im * (Heff * psit)
end

function rk4(psi, Heff, dt)
   psi1 = dpsi(psi,            Heff)*dt
   psi2 = dpsi(psi + 0.5*psi1, Heff)*dt
   psi3 = dpsi(psi + 0.5*psi2, Heff)*dt
   psi4 = dpsi(psi + psi3,     Heff)*dt
   return psi .+ (psi1 .+ 2*psi2 .+ 2*psi3 .+ psi4)/6
end

#---------------------------------------------------
#---------------------------------------------------
function drho(rho, H)
    return -im * (rho*H - H*rho)
end

function rk4_rho(rho, H, dt)
   rho1 = drho(rho,            H)*dt
   rho2 = drho(rho + 0.5*rho1, H)*dt
   rho3 = drho(rho + 0.5*rho2, H)*dt
   rho4 = drho(rho + rho3,     H)*dt
   return rho .+ (rho1 .+ 2*rho2 .+ 2*rho3 .+ rho4)/6
end

function drho_open(rho, Lplus, Lminus, L_op)
    #return Lplus*rho .+ rho*Lminus .+ L_op*rho*L_op'
    return Lplus*rho .+ rho*Lminus .+ sum([L*rho*L' for L in L_op])
end

function rk4_rho_open(rho, Lplus, Lminus, L_op, dt)
   rho1 = drho_open(rho,            Lplus, Lminus, L_op)*dt
   rho2 = drho_open(rho + 0.5*rho1, Lplus, Lminus, L_op)*dt
   rho3 = drho_open(rho + 0.5*rho2, Lplus, Lminus, L_op)*dt
   rho4 = drho_open(rho + rho3,     Lplus, Lminus, L_op)*dt
   return rho .+ (rho1 .+ 2*rho2 .+ 2*rho3 .+ rho4)/6
end
