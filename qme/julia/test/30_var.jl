using MKL
using MAT, Base.Threads
using LinearAlgebra, HDF5
include("solver_wrapper.jl")

#Heff = matread("4k_mat/Heff.mat")["Heff"]
psi0 = vec(collect(matread("../4k_mat/psi0.mat")["psi0"]))
H0   = collect(matread("../4k_mat/H0.mat")["H0"])
m_op = collect(matread("../4k_mat/m.mat")["m"])
L1   = collect(matread("../4k_mat/L1.mat")["L1"])
L2   = collect(matread("../4k_mat/L2.mat")["L2"])
L3   = collect(matread("../4k_mat/L3.mat")["L3"])
L4   = collect(matread("../4k_mat/L4.mat")["L4"])
L_op = [L1, L2, L3, L4]

heff = H0 .- im*0.5*(L1'*L1 .+ L2'*L2 .+ L3'*L3 .+ L4'*L4)

tf  = 1000
dt  = 0.1
tstep = 1
times = range(0,tf,step=dt)

function tevolve(psi0, heff, L_op, m_op, times, dt)
    ob = zeros(length(times))
    psi = psi0
    #if note defined outside the loop, r1 will only exist in
    #the local scope of if condition
    r1 = 0
    draw = true
    for (t_ind, t) in enumerate(times) 
        if draw
            r1 = rand(1)[1]
        end
        psi = rk4(psi, heff, dt)
        dp = norm(psi)^2
        draw = false

        if dp <= r1
            dpm = [norm(L * psi)^2 for L in L_op]
            dps = cumsum(dpm)
            dps = dps/(last(dps))
            r2 = rand(1)[1]
            op_ind = 1
            for idx in 1:length(dps)
                if r2 > dps[idx]
                    op_ind = idx + 1
                end
            end
            psi = L_op[op_ind] * psi
            psi = psi/norm(psi)
            draw = true
        end
        psim = psi/norm(psi)
        #ob[t_ind] = ob[t_ind] + real((psim' * m_op * psim)[1,1])
        ob[t_ind] = real((psim' * m_op * psim)[1,1])
    end
    return ob
end

function sum_trajectories(psi0, heff, L_op, m_op, times, dt, Nlist)
    ob = zeros(length(Nlist), length(times))
    #println("Nlist ", Nlist)
    for n_ind in Nlist
        #ob[n_ind,:] = ob[n_ind,:] + tevolve(psi0, heff, L_op, m_op, times, dt)
        ob[n_ind,:] = tevolve(psi0, heff, L_op, m_op, times, dt)
    end
    return ob
end


function monte_carlo(psi0, heff, L_op, m_op, times, dt, Ntraj)
    traj_partitions = Iterators.partition(1:Ntraj, Ntraj÷nthreads())
    tasks = map(traj_partitions) do traj_list
        @spawn sum_trajectories(psi0, heff, L_op, m_op, times, dt, traj_list)
        end
    #println(size(fetch.(tasks)[1]))
    ob = fetch.(tasks)[1]
    return ob
end


Ntraj = 500
@time expect = monte_carlo(psi0, heff, L_op, m_op, times, dt, Ntraj)
#println(size(expect))
fsave = h5open("variance_500.hdf5","w")
write(fsave, "expect", expect)
close(fsave)



