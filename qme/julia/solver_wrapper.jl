function dpsi(psit, Heff)
    return -im * (Heff * psit)
end

function rk4(psi, Heff, dt)
   psi1 = dpsi(psi,            Heff)*dt
   psi2 = dpsi(psi + 0.5*psi1, Heff)*dt
   psi3 = dpsi(psi + 0.5*psi2, Heff)*dt
   psi4 = dpsi(psi + psi3,     Heff)*dt
   return psi .+ (psi1 .+ 2*psi2 .+ 2*psi3 .+ psi4)/6
end

#---------------------------------------------------
#---------------------------------------------------
function rk4!(psi, Heff, dt)
   psi1 = dpsi(psi,            Heff)*dt
   psi2 = dpsi(psi + 0.5*psi1, Heff)*dt
   psi3 = dpsi(psi + 0.5*psi2, Heff)*dt
   psi4 = dpsi(psi + psi3,     Heff)*dt
   psi .= psi .+ (psi1 + 2*psi2 + 2*psi3 + psi4)/6
end


#function rk4_test!(psi, psi1, B, dt)
function rk4_test!(psi, B, dt)
   psi1 = psi*B
   psi2 = psi*B
   psi  .= psi .+ (psi1 .+ psi2)*0.5/1
end

"""
function dpsi(psit, Heff, eps=1e-10, make_sparse=false)
    if make_sparse
        dp = -im * (Heff * psit)
        dp[abs.(dp) .< eps] .= 0
        println("made sparse")
        return sparse(dp)
    else
        return -im * (Heff * psit)
    end 
end
function rk4_test!(psi_p, B, dt)
   psi_p[:,2] .= psi_p[:,1]*B
   psi_p[:,3] .= psi_p[:,1]*B
   psi_p[:,4] .= psi_p[:,1]*B
   psi_p[:,5] .= psi_p[:,1]*B
   psi_p[:,1] .= psi_p[:,1] .+ (psi_p[:,2] .+ psi_p[:,3] .+ psi_p[:,4] .+ psi_p[:,5])*dt/6
end

function solver(t, y, heff)
    return -im * (heff * y)
end

function solver!(y, heff)
    y = -im * (heff * y)
end

function ensemble_average_parallel(psi, heff, m_op, times, dt, Ntraj)
    traj = range(1,Ntraj)
    blocks = Iterators.partition(traj, Ntraj÷nthreads())
    tasks = map(blocks) do block
        @spawn sum_trajectories(psi, heff, m_op, times, dt, length(block))
        end
    block_sum = fetch.(tasks)
    println(size(block_sum))
end
"""
