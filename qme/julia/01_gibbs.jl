using MAT, Base.Threads
using LinearAlgebra, HDF5
using SparseArrays
using BenchmarkTools
include("solver_wrapper.jl")

Heff = matread("../gibbs_mat/Heff.mat")["Heff"]
psi0 = matread("../gibbs_mat/psi0.mat")["psi0"]
m_op = matread("../gibbs_mat/m.mat")["m"]

L1   = matread("../gibbs_mat/L1.mat")["L1"]
L2   = matread("../gibbs_mat/L2.mat")["L2"]
L3   = matread("../gibbs_mat/L3.mat")["L3"]
L4   = matread("../gibbs_mat/L4.mat")["L4"]

tf  = 1000
dt  = 0.1
tstep = 1
times = range(0,tf,step=dt)

function tevolve(psi, heff, L1, m_op, times, dt)
    draw = true
    ob = zeros(length(times))
    for (t_ind, t) in enumerate(times)
        
        if draw
            r1 = rand(1)[1]
        end
        psi = rk4(psi, heff, dt)
        dp = norm(psi)^2
        draw = false
        if dp <= r1
            psi = L1 * psi
            psi = psi/norm(psi)
            draw = true
        end
        psit = psi/norm(psi)
        ob[t_ind] = real((psi' * m_op * psi)[1,1])
    end
    return ob
end

function sum_trajectories(psi0, heff, L1, m_op, times, dt, nblock)
    #println(threadid()," and len" ,nblock)
    expect = zeros(length(times))
    for n_ind in range(1,nblock)
        expect = expect + tevolve(psi0, heff, L1, m_op, times, dt)
    end
    return expect
end


function ensemble_average(psi0, heff, L1, m_op, times, dt, Ntraj)
    expect =  sum_trajectories(psi0, heff, L1, m_op, times, dt, Ntraj)
    println(size(expect))
    fsave = h5open("expect.hdf5","w")
    write(fsave, "expect", expect/Ntraj)
    close(fsave)
end

Ntraj = 100
@time ensemble_average(psi0, Heff, L1, m_op, times, dt, Ntraj)
#@btime tevolve(psi0, Heff, times, dt)



