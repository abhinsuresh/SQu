import sys, h5py, numpy as np
sys.path.append('../../plot_manager/')
from plot2d import plotfig


times = np.arange(0,1000+0.1,0.1)
result = np.zeros((len(times),2))
result[:,0] = h5py.File("expect.hdf5","r")["expect"][:]
gibbs_m = 0.2838673841804142
result[:,1] = gibbs_m*np.ones(len(times))
print(result.shape)
print(times.shape)
data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{m}$'
sf = np.array([[0,2]])
plotfig(data, 'm_julia', values, sf=sf)
