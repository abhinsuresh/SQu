import numpy as np
import h5py
import sys
from numba import njit

sys.path.append('../../plot_manager/')
from plot2d import plotfig
scale_spin = 2

data = dict();values = dict()

def avg(sdata, T = 100):
    L = len(sdata) - 1
    l = L//T
    avg = np.zeros(l)
    for i in range(l):
        avg[i] = np.sum(sdata[i*T: (i+1)*T])
    return avg/T

#@njit
def mean(data):
    s  = data.shape[0]
    Nc = data.shape[1]
    avg = np.zeros(s)
    for i in range(s):
        for j in range(Nc):
            avg[i] = avg[i] + data[i,j]  
    return avg/Nc
#@njit
def std(data, mean):
    Nc = data.shape[1]
    #print(Nc)
    s = len(mean)
    sd = np.zeros(s)
    for i in range(s):
        for j in range(Nc):
            sd[i] = sd[i] + (mean[i]-data[i,j])**2
        sd[i] = np.sqrt(sd[i]/Nc)
    return sd
            

res  = h5py.File("variance_2000.hdf5")["expect"]
fig_name = "var_2000"
avg = mean(res)

std = std(res, avg)

_time = np.arange(0,1000+0.1,0.1)


L = len(_time)
plot_y = np.zeros((len(_time),3))
plot_y[:,0] = avg
plot_y[:,1] = std
plot_y[:,2] = np.ones(len(_time))*0.283867384


plot_x = _time; 
#values['ylabel'] = r'$\mathrm{{\langle {\hat{S}^x}_6 \rangle} - {\langle \hat{S}^x_2 \rangle}}$'
values['ylabel'] = r'$\mathrm{m_z}$'
values['xlabel'] = r'$\mathrm{Time\ (fs)}$'

    
data['x_plot'] = plot_x; data['y_plot'] = plot_y
Cl = ['k', 'grey', 'C0']
Ls = ['-', '-', '--', '--']
Mk = ['', '', '', '']
sf = np.array([[0,1]])
Ps = [50]*4
Alpha = [1,0.5,1]
plotfig(data, fig_name, values, sf=sf, Cl=Cl, errorbar=True, Ps=Ps,
        fs=8, lw=0.4, Alpha=Alpha)
