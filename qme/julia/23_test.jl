using MAT, LinearAlgebra
using SparseArrays

H0  = matread("../inputs_sv/H.mat")["H"]
rho = matread("../inputs_sv/rho.mat")["rho"]

println("L1 read time")
@time L1  = matread("../inputs_sv/L1.mat")["L1"]
println("type L1", typeof(L1))

println("making small ele zero", typeof(L1))
@time L1[abs.(L1) .< 1e-10] .= 0
L1s =  sparse(L1)
println("type L1s", typeof(L1s))

psi1 = transpose(matread("../inputs_sv/psi1.mat")["psi1"])
psi2 = transpose(matread("../inputs_sv/psi2.mat")["psi2"])

println("energy ", tr(H0 * rho))

psi1f = collect(psi1)

println("L1 psi")
@time L1 * psi1

println("L1 psif")
@time L1 * psi1f

println("L1s psi")
@time L1s * psi1

println("L1s psif")
@time L1s * psi1f

for i in 1:length(times)
    if i%10 == 0
        ob[i÷10] = i
    end
end
