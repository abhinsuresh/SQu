using LinearAlgebra
using BlockArrays
function partial_psi_trace(psi, dim_A)
    dim_B = length(psi)÷dim_A
    rho_B = zeros(Complex, dim_B, dim_B)
    
    for i in 1:dim_A
        rho_B .= rho_B .+ (psi[(i-1)*dim_B+1:i*dim_B] *
                           psi[(i-1)*dim_B+1:i*dim_B]')
    end
    return rho_B
end

function partial_transpose(rho, dim_B = 2)
   L = dim_B*dim_B
   rho_pt = zeros(Complex, L, L)
   for i in 1:dim_B
      for j in 1:dim_B
            rho_pt[(i-1)*dim_B+1: i*dim_B, (j-1)*dim_B+1: j*dim_B] =(
     transpose(rho[(i-1)*dim_B+1: i*dim_B, (j-1)*dim_B+1: j*dim_B]))
        end
   end

    return rho_pt
    
end

function logarithmic_negativity(rho, dim_B)
    rho_pt = partial_transpose(rho, dim_B)

    #w_neg = eigvals(rho_pt)
    #neg   = sum(w_neg[w_neg .< 0])

    norm_rho = rho_pt' * rho_pt
    norm_rho[abs.(norm_rho) .< 1e-10] .= 0
    w = eigvals(norm_rho) .+ 0*im
    log_neg = log.(sum(sqrt.(w)))./log(2)
    return log_neg
end

function partial_transpose_new!(rho, d1, d2)
    idx = [d2 for i = 1:d1]
    blkm = PseudoBlockArray(rho, idx, idx)
    bfm = Array{eltype(rho)}(undef, d2, d2)
    trm = Array{eltype(rho)}(undef, d2, d2)
    for i = 1:d1
        for j = 1:d1
            getblock!(bfm, blkm, i, j);
            transpose!(trm, bfm)
            setblock!(blkm, trm, i, j)
        end
    end
    rho
end

function partial_trace_A(rho, m=2, n=2)
    L = m*n
    rho_B = zeros(Complex,n,n)
    for i in 1:n
        for j in 1:n
            m_sum = 0
            for k in 1:m
                m_sum = m_sum + rho[i+n*(k-1),j+n*(k-1)]
            end
            rho_B[i,j] = m_sum
        end
    end
    return rho_B
end
