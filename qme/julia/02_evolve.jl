using MAT, Base.Threads
using LinearAlgebra, HDF5
using SparseArrays, DifferentialEquations
using BenchmarkTools
include("solver_wrapper.jl")

Heff = matread("../gibbs_mat/Heff.mat")["Heff"]
psi0 = matread("../gibbs_mat/psi0.mat")["psi0"]
m_op = matread("../gibbs_mat/m.mat")["m"]

L1   = matread("../gibbs_mat/L1.mat")["L1"]
L2   = matread("../gibbs_mat/L2.mat")["L2"]
L3   = matread("../gibbs_mat/L3.mat")["L3"]
L4   = matread("../gibbs_mat/L4.mat")["L4"]

tf  = 1000
dt  = 1
tstep = 1
times = range(0,tf,step=dt)


function sum_trajectories(psi0, heff, L1, m_op, times, dt, Ntraj)
    ob = zeros(length(times))
    f(y,h,t) = -im *(h * y) 
    prob = ODEProblem(f, psi0, (0,1), Heff)
    for n_ind in range(1,Ntraj)
        psi = psi0
        draw = true
        for (t_ind, t) in enumerate(times)
            
            if draw
                r1 = rand(1)[1]
            end
            psi = solve(prob, save_on=false, save_start=false)[:,:,1]
            dp = norm(psi)^2
            darw = false

            if dp <= r1
                psi = L1 * psi
                psi = psi/norm(psi)
                prob = ODEProblem(f, psi, (0,1), Heff)
                draw = true
            end
            psit = psi/norm(psi)
            ob[t_ind] = ob[t_ind] + real((psi' * m_op * psi)[1,1])
        end
    end
    return ob
end


Ntraj = 100
expect =  sum_trajectories(psi0, Heff, L1, m_op, times, dt, Ntraj)
fsave = h5open("expect.hdf5","w")
write(fsave, "expect", expect/Ntraj)
close(fsave)

#@btime tevolve(psi0, Heff, times, dt)



