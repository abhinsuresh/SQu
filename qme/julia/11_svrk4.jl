using MKL
using MAT, Base.Threads#, HDF5
using LinearAlgebra
using SparseArrays
include("solver_wrapper.jl")
include("density_wrapper.jl")


#H0  = matread("../inputs_sv/H.mat")["H"]

println("L1 read time")
@time L1  = matread("../inputs_sv/L1.mat")["L1"]
#flush(stdout)
#L1[abs.(L1) .< 1e-15] .= 0
#L1 = sparse(L1)

#making it sparse
#println("L2 read time")
#L2  = matread("../inputs_sv/L2.mat")["L2"]
#L2[abs.(L2) .< 1e-15] .= 0
#L2 = sparse(L2)

#println("L3 read time")
#L3  = matread("../inputs_sv/L3.mat")["L3"]
#L3[abs.(L3) .< 1e-15] .= 0
#L3 = sparse(L3)

#println("L4 read time")
#L4  = matread("../inputs_sv/L4.mat")["L4"]
#L4[abs.(L4) .< 1e-15] .= 0
#L4 = sparse(L4)

psi1 = vec(collect(transpose(matread("../inputs_sv/psi1.mat")["psi1"])))
psi2 = vec(collect(transpose(matread("../inputs_sv/psi2.mat")["psi2"])))

@time Heff  = matread("../inputs_sv/Heff8.mat")["Heff"]
#println("making Heff")
#heff  = H0 .- im*0.5*(L1'*L1)
#heff = H0 .- im*0.5*(L1'*L1 .+ L2'*L2 .+ L3'*L3 .+ L4'*L4)

#L_op = [L1]
L_op = [L1]



function find_Li(psi, L_op)
    dpm = [norm(L * psi)^2 for L in L_op]
    dps = cumsum(dpm)
    dps = dps/(last(dps))
    r2 = rand(1)[1]
    op_ind = 1
    for idx in 1:length(dps)
        if r2 > dps[idx]
            op_ind = idx + 1
        end
    end
    return op_ind
end

function tevolve(psi0, heff, L_op, m_op, times, dt, n_ind, m_step)
    ob = zeros(length(times)÷m_step)
    psi = psi0
    #if note defined outside the loop, r1 will only exist in
    #the local scope of if condition
    r1 = 0
    draw = true
    for (t_ind, t) in enumerate(times) 
        if draw
            r1 = rand(1)[1]
        end
        println("rk4 at t ", t_ind, "of traj ", n_ind)
        psi = rk4(psi, heff, dt)
        dp = norm(psi)^2
        draw = false

        if dp <= r1
            #println("Jump at t", t_ind, "of traj ", n_ind)
            op_ind = find_Li(psi, L_op)
            psi = L_op[op_ind] * psi
            psi = psi/norm(psi)
            draw = true
        end
        if t_ind%m_step == 0
            #println("measure at t ", t_ind, "of traj ", n_ind)
            psim = psi/norm(psi)
            rhos = partial_psi_trace(psim, 260)
            l_neg = logarithmic_negativity(rhos, 16)
            ob[t_ind÷m_step] = ob[t_ind÷m_step] + real(l_neg)
        end
    end
    return ob
end

function sum_trajectories(psi0, heff, L_op, m_op, times, dt, Nlist, m_step)
    ob = zeros(length(times)÷m_step)
    println("Nlist ", Nlist)
    for n_ind in Nlist
        println("currently running traj ", n_ind)
        ob = ob + tevolve(psi0, heff, L_op, m_op, times, dt, n_ind, m_step)
    end
    return ob
end


function monte_carlo(psi0, heff, L_op, m_op, times, dt, Ntraj, m_step)
    ob = zeros(length(times)÷m_step)
    
    traj_partitions = Iterators.partition(1:Ntraj, Ntraj÷nthreads())
    tasks = map(traj_partitions) do traj_list
        @spawn sum_trajectories(psi0, heff, L_op, m_op, times, dt, traj_list, m_step)
        end
    #println(size(fetch.(tasks)[1]))
    ob = sum(fetch.(tasks))
    return ob
end


Ntraj = 500
m_step = 10
m_op = 1
tf  = 50
dt  = 0.1
times = range(0,tf,step=dt)

@time expect = monte_carlo(psi1, Heff, L_op, m_op, times, dt, Ntraj, m_step)
matwrite("psi1_neg.mat",Dict("neg"=>expect/Ntraj))
#fsave = h5open("sv.hdf5","w")
#write(fsave, "expect", expect/Ntraj)
#close(fsave)




