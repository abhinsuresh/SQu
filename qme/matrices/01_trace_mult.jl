using MAT, Base.Threads
using LinearAlgebra
using SparseArrays
#using BenchmarkTools


fH = matopen("H.mat")
frho = matopen("rho.mat")

H = read(fH, "H")
rho = read(frho, "rho")

println("energy of the system ", tr(H * rho))


function mat_product_a(block,A,B,m)
    lsum = 0
    println(threadid()," and len" ,length(block))
    for ind in block
        for k in 1:m
            lsum = lsum + A[ind,k]*rho[k,ind]
        end
    end
    return lsum
end

function mat_trace(A, B)
    trace = 0
    m,n = size(A)
    sum_list = 1:m
    blocks = Iterators.partition(sum_list, length(sum_list)÷nthreads())
    tasks = map(blocks) do block
            @spawn mat_product_a(block,A,B,m)
            end
    block_sum = fetch.(tasks)
    #println(partition_sums)
    return sum(block_sum)
end
@time begin
    println("Energy through component: ", mat_trace(H,rho))
end
