using Base.Threads
println(nthreads())
function sum_single(a)
    s = 0
    for i in a
        s += i
    end
    s
end

sum_single(1:10000)


function sum_multi(a)
    chunks = Iterators.partition(a, length(a)÷nthreads())
    tasks = map(chunks) do chunk
            @spawn sum_single(chunk)
    end
    chunk_sums = fetch.(tasks)
    sum_single(chunk_sums)
end

println(sum_multi(1:10000))
