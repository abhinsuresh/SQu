using MAT, Base.Threads
using LinearAlgebra
using SparseArrays
#using BenchmarkTools


fH = matopen("H.mat")
frho = matopen("rho.mat")

H = read(fH, "H")
rho = read(frho, "rho")

println("energy of the system ", tr(H * rho))

#m,n = size(H)
#will go out of memory to construct the total matrix
#L = Matrix{Float64}(I, m,m)

function mat_product(sum_list,A,B,m)
    lsum = 0
    for ind in sum_list
        for k in 1:m
            lsum = lsum + A[ind,k]*rho[k,ind]
        end
    end
    return lsum
end

function mat_trace(A, B)
    trace = 0
    m,n = size(A)
    sum_list = 1:m
    sum_partitions = Iterators.partition(sum_list, length(sum_list)÷nthreads())
    tasks = map(sum_partitions) do s_list
            @spawn mat_product(s_list,A,B,m)
            end
    partition_sums = fetch.(tasks)
    #println(partition_sums)
    return sum(partition_sums)
end
@time begin
    println("Energy through component: ", mat_trace(H,rho))
end

"""
global energy = 0
@threads for i in 1:m
    for k in 1:m
        global energy = energy + H[i,k]*rho[k,i]
    end
end
print("energy", energy)

drho = spzeros(m,m)
drho[1,1] = 1
drho[1,2] = 1
drho[1,3] = 1
for j = 1:10
    drho[1,j] = drho[1,j] + 1
end
println(nnz(drho))
for j = 1:m
    for k = 1:n
        for l = 1:n
            drho[j,k] = drho[j,k] + ( H[j,l]*rho[l,k] 
                                  +   rho[j,l]*H[l,k])
        end
    end
end
"""
