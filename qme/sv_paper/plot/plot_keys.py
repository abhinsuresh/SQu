import numpy as np
import h5py
import sys
from numba import njit
from scipy.io import savemat

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

data = dict();values = dict()


#@njit
def mean(data):
    s  = data.shape[0]
    Nc = data.shape[1]
    avg = np.zeros(s)
    for i in range(s):
        for j in range(Nc):
            avg[i] = avg[i] + data[i,j]  
    return avg/Nc
#@njit
def std(data, mean):
    Nc = data.shape[1]
    #print(Nc)
    s = len(mean)
    sd = np.zeros(s)
    for i in range(s):
        for j in range(Nc):
            sd[i] = sd[i] + (mean[i]-data[i,j])**2
        sd[i] = np.sqrt(sd[i]/Nc)
    return sd
            
out = np.zeros((50,201))

N = 300
n = str(N)

res  = h5py.File(n+"_0.12/09_"+n+"_1.hdf5")
num = len(res.keys())
ind = 0
for k_item in res.keys():
    out[:,ind] = res[k_item]
    ind = ind + 1

res  = h5py.File(n+"_0.12/09_"+n+"_2.hdf5")
for k_item in res.keys():
    out[:,ind] = res[k_item]
    ind = ind + 1

res  = h5py.File(n+"_0.12/09_"+n+"_3.hdf5")
for k_item in res.keys():
    out[:,ind] = res[k_item]
    ind = ind + 1

res  = h5py.File(n+"_0.12/09_"+n+"_4.hdf5")
for k_item in res.keys():
    out[:,ind] = res[k_item]
    ind = ind + 1

Ntraj = ind
out = out[:,:ind]
print(Ntraj)

print(out.shape)
res = out/4
fig_name = n+"_0.12_"+str(Ntraj)

avg = mean(res)

savemat(n+'avg.mat',{'avg':avg})

print("1")
std = std(res, avg)
savemat(n+'std.mat',{'std':std})
print("1")

_time = np.arange(0,50,1)/0.6582119569


L = len(_time)
plot_y = np.zeros((len(_time),3))
plot_y[:,0] = avg
plot_y[:,1] = std/np.sqrt(Ntraj)
plot_y[:,2] = np.ones(len(_time))*0
print(plot_y.shape)

plot_x = _time; 
#values['ylabel'] = r'$\mathrm{{\langle {\hat{S}^x}_6 \rangle} - {\langle \hat{S}^x_2 \rangle}}$'
values['ylabel'] = r'$\mathrm{m_z}$'
values['xlabel'] = r'$\mathrm{Time\ (fs)}$'

    
data['x_plot'] = plot_x; data['y_plot'] = plot_y
Cl = ['k', 'grey', 'C0']
Ls = ['-', '-', '--', '--']
Mk = ['', '', '', '']
sf = np.array([[0,1]])
Ps = [1]*4
Alpha = [1,0.5,1]
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{Log~Neg}$'
plotfig(data, fig_name, values, ylim=(0,0.13),sf=sf, Cl=Cl, errorbar=True, Ps=Ps,
        fs=8, lw=0.4, Alpha=Alpha)
