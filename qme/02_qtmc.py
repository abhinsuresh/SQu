import sys, numpy as np, time
import scipy.linalg as la
from scipy.io import savemat
import qutip as qt

from spin_core import Spin
from evolve_step import Lindblad
from master_equation import *
sys.path.append('../plot_manager/')
from plot2d import plotfig

#parameters
N       = 4
S       = 1/2
eta     = 1
gamma   = 0.02 * eta
Bz      = 1 * eta
omega0  = 2 * eta
temp    = 400
T       = 1
Lambda  = 50*T
verbose = True

start = time.time()
spin = Spin(S=S, L = N, kronD=1, sc = 1)
Se = spin.op

H0   = -1*spin.heisenberg(J=1*eta,Jz=1*eta) + -1*spin.field_op(Bz,'sz')

#L, Ld, Lp = get_Lexact(H0, Se['sx3'], gamma, BathSpectral, omega0, T, Lambda)
w,v = la.eigh(H0.toarray())
m_op = spin.field_op(1,'sz')/N

#jump operator

#function
omega = np.arange(-10*Lambda,10*Lambda,0.01)
Sw = OmicSpectral(omega, omega0, Lambda)
Jw = BathSpectral(omega, omega0, T, Lambda)

#----------------------------
"""
#summation
t_span = np.arange(-0.5,0.5,0.001)
n_trunc = 20
cn  = get_cn(Jw, omega, t_span, n_trunc=n_trunc, ctype=np.complex64, plot=True)

L = np.zeros(Xs.shape, dtype=np.complex64)
for ind in range(n_trunc):
    L = L + cn[ind] * get_adHs(H0, Xs, ind)

#if verbose: print('L finished in:\t',time.time()-start)
L = L * np.sqrt(gamma)
if verbose: print("trace of L", np.trace(L))
Ld = L.T.conj()
Lp = Ld @ L
#print("L mat:\n", np.round(L[:5,:5],3))
"""
#----------------------------
#full diag
def e_basis(mat, v):
    return v.conj().T @ mat @ v
def s_basis(mat, v):
    return v @ mat @ v.conj().T

Ediff = (w[0] - w).reshape(-1,1)
for ind in range(1,len(w)):
    Ediff = np.append(Ediff,(w[ind]-w).reshape(-1,1),axis=1)
#limiting case
Ediff = Ediff + 1e-14
La = np.sqrt(2*np.pi*gamma*BathSpectral(Ediff,omega0,T,Lambda))

L1b = e_basis(Se['sx3'], v)
L1e = La * L1b
L1 = s_basis(L1e, v)
L1d = L1.T.conj()
L1p = L1d @ L1

L2b = e_basis(Se['sx2'], v)
L2e = La * L2b
L2  = s_basis(L2e, v)
L2d = L2.T.conj()
L2p = L2d @ L2

L3b = e_basis(Se['sx1'], v)
L3e = La * L3b
L3  = s_basis(L3e, v)
L3d = L3.T.conj()
L3p = L3d @ L3

L4b = e_basis(Se['sx0'], v)
L4e = La * L4b
L4  = s_basis(L4e, v)
L4d = L4.T.conj()
L4p = L4d @ L4
#----------------------------

w,v = la.eigh(H0.toarray())
psi = v[:,0].reshape((-1,1))

tf  = 1000
dt  = 0.1
times = np.arange(0, tf+dt, dt)
result = np.zeros((len(times),2))

print("initial m: ", (psi.conj().T @ m_op @ psi)[0,0])

Hob = qt.Qobj(H0)
Psiob = qt.Qobj(psi)
op1 = qt.Qobj(m_op)
L1ob = qt.Qobj(L1)
L2ob = qt.Qobj(L2)
L3ob = qt.Qobj(L3)
L4ob = qt.Qobj(L4)


mc = qt.mcsolve(Hob, Psiob, times, [L1ob,L2ob,L3ob,L4ob], [op1], 100)
#print(mc.states[0,0].shape)
result[:,0] = mc.expect[0]


"""
#start time evolution
for t_ind, t_now in enumerate(times):
    result[t_ind,0] = np.trace(rho @ m_op)
    #print(t_ind, result[t_ind], np.trace(rho).real)
    rho = evolve.rk4_rho(rho, H0, L,Ld,Lp)
"""

gibbs_m = np.trace(calc_rho_vdv(w,v,T) @ m_op)
print("gibbs m ", gibbs_m)
result[:,1] = np.ones(len(times))*gibbs_m

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{m}$'
sf = np.array([[0,2]])
plotfig(data, 'mqtmc_50', values, sf=sf)
if verbose: print('run finished in:\t',time.time()-start)
