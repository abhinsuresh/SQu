import sys, numpy as np, time
import scipy.linalg as la
from scipy.io import savemat
import qutip as qt
from scipy.integrate import ode

from spin_core import Spin
from evolve_step import Lindblad
from master_equation import *
sys.path.append('../plot_manager/')
from plot2d import plotfig

#parameters
N       = 4
S       = 1/2
eta     = 1
gamma   = 0.01 * eta
Bz      = 1 * eta
omega0  = 2 * eta
temp    = 400
T       = 1
Lambda  = 50*T
verbose = True

start = time.time()
spin = Spin(S=S, L = N, kronD=1, sc = 1)
Se = spin.op

H0   = -1*spin.heisenberg(J=1*eta,Jz=1*eta) + -1*spin.field_op(Bz,'sz')

#L, Ld, Lp = get_Lexact(H0, Se['sx3'], gamma, BathSpectral, omega0, T, Lambda)
w,v = la.eigh(H0.toarray())
m_op = spin.field_op(1,'sz')/N

#jump operator
Xs = Se['sx3']

#function
omega = np.arange(-10*Lambda,10*Lambda,0.01)
Sw = OmicSpectral(omega, omega0, Lambda)
Jw = BathSpectral(omega, omega0, T, Lambda)

#full diag
def e_basis(mat, v):
    return v.conj().T @ mat @ v
def s_basis(mat, v):
    return v @ mat @ v.conj().T

Xe = e_basis(Xs, v)
Ediff = (w[0] - w).reshape(-1,1)
for ind in range(1,len(w)):
    Ediff = np.append(Ediff,(w[ind]-w).reshape(-1,1),axis=1)
#limiting case
Ediff = Ediff + 1e-14
L1a = np.sqrt(2*np.pi*gamma*BathSpectral(Ediff,omega0,T,Lambda))
L1b = Xe
L1e = L1a * L1b
L = s_basis(L1e, v)
Ld = L.T.conj()
Lp = Ld @ L
#----------------------------

w,v = la.eigh(H0.toarray())
psi0 = v[:,0].reshape((-1,1))

tf  = 2000
dt  = 0.1
times = np.arange(0, tf+dt, dt)
result = np.zeros((len(times),2))

print("initial m: ", (psi0.conj().T @ m_op @ psi0)[0,0])

Heff = H0 - 1j*0.5*Lp



"""
r.set_initial_value(psi0,0).set_f_params(Heff)
print(np.random.rand(1)[0])
for t in range(1000):
    psit = r.integrate(r.t + 0.1)
    if t%100==0: print("norm psit", np.linalg.norm(psit), r.t)
"""

def f(t,y,heff):
    return -1j* (heff @ y)

r = ode(f).set_integrator('zvode',method='adams')

Ntraj = 100
for n_ind in range(Ntraj):
    print(n_ind)
    r.set_initial_value(psi0,0).set_f_params(Heff)
    
    draw = True
    for t_ind,t_now in enumerate(times):

        if draw: 
            r1 = np.random.rand(1)[0]
        psi = r.integrate(r.t + dt)
        dp = np.linalg.norm(psi)**2
        draw = False

        if dp <= r1:
            #print("jump")
            psi = L @ psi
            psi = psi/np.linalg.norm(psi)
            r.set_initial_value(psi, r.t)
            draw = True

        psi_t = psi/np.linalg.norm(psi)
        result[t_ind,0] = result[t_ind,0] + (psi_t.conj().T @ m_op @ psi_t)[0,0]

result = result/Ntraj

gibbs_m = np.trace(calc_rho_vdv(w,v,T) @ m_op)
print("gibbs m ", gibbs_m)
result[:,1] = np.ones(len(times))*gibbs_m

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{m}$'
sf = np.array([[0,2]])
plotfig(data, 'mode', values, sf=sf)


if verbose: print('run finished in:\t',time.time()-start)
