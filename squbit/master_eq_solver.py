import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy import sparse


HBAR = 0.6582119569
HBAR = 1
KB = 8.617333*1e-5   #eV K-1

class Henk_lindblad:
    """Class implemented to call Lindblad evolution
    """
    def __init__(self,
                 w,
                 v,
                 dt = 0.1,
                 gamma_sc=0,
                 gamma_sf=0,
                 gamma_dp=0,
                 mu = 0,
                 temp = 0
                ):
        self.w = w
        self.v = v
        self.dt = dt
        self.mu = mu
        self.temp = temp + 1e-15
        self.gamma_sc = gamma_sc
        self.gamma_sf = gamma_sf
        self.gamma_dp = gamma_dp
        self.generate()
        return
    
    def fbe(self, de):
        beta = 1/(KB*self.temp)
        return 1/( np.exp((de-self.mu)*beta) - 1 )  

    def generate(self):
        L = len(self.w)
        self.outer = np.zeros((L, L, L, L), dtype=np.complex128)
        self.inner = np.zeros((L, L), dtype=np.complex128)
        for n in range(L):
            for m in range(L):
                self.outer[n,m,:,:] = np.outer(self.v[:,n], self.v[:,m].conj())
                self.inner[n,m] = self.v[:,n].conj() @ self.v[:,m]
        print('all generated')

    #def rho_nm(self, rho, n, m):
    #    return self.v[:,n].conj() @ self.v[:,m]

    def lindblad(self, rho):
        """
        """
        gamma = self.gamma_sc
        L = len(self.w)
        rho_new = np.zeros(rho.shape, dtype=np.complex128)
        for m in range(L):
            rhod = np.zeros(rho.shape, dtype=np.complex128)
            for i in range(L):
                rho_mi = self.inner[m,i]
                rho_im = self.inner[i,m]
                rhod = rhod + self.outer[m,i,:,:]*rho_mi + \
                              self.outer[i,m,:,:]*rho_im
            for n in range(L):
                de = self.w[m] - self.w[n] 
                rho_mm = self.inner[m,m]
                rho_nn = self.inner[n,n]
                pi_mn = rho_mm * (1 - rho_nn)    
                
                if m != n:
                    if de > 0:
                        rho_new = rho_new + gamma * pi_mn * (self.fbe(de)+1) * \
                            (self.outer[n,n,:,:]*rho_mm - 0.5*rhod)
                    elif de < 0:
                        rho_new = rho_new + gamma * pi_mn *  self.fbe(de)    * \
                            (self.outer[n,n,:,:]*rho_mm - 0.5*rhod)
                elif m == n:
                    rho_new = rho_new + self.gamma_dp * \
                            (self.outer[n,n,:,:]*rho_mm - 0.5*rhod)
        return rho_new

    def evolve_rho(self, rho, Ht):
        d_rho = 1j*(rho @ Ht - Ht @ rho) + self.lindblad(rho)
        return d_rho
    
    def rk4_open(self, rho, H_static, H_dynamic, t=0):
        #implementation
        gamma = self.gamma_sc
        #spin scattering
        if H_dynamic == 0:
            Ht0 = Ht1 = Ht2 = H_static
        else:
            Ht0 = H_static + H_dynamic(t)
            Ht1 = H_static + H_dynamic(t + self.dt/2)
            Ht2 = H_static + H_dynamic(t + self.dt)
        rho1 = self.evolve_rho(rho,            Ht0)*self.dt
        rho2 = self.evolve_rho(rho + rho1*0.5, Ht1)*self.dt
        rho3 = self.evolve_rho(rho + rho2*0.5, Ht1)*self.dt
        rho4 = self.evolve_rho(rho + rho3,     Ht2)*self.dt

        rho = rho + (rho1 + 2*rho2 + 2*rho3 + rho4)*(1/6)
        return rho
