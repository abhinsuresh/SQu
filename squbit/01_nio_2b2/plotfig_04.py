import sys, numpy as np, h5py, os
from quspin.basis import spinful_fermion_basis_general
sys.path.append('../')
from spinful_basis import Basis
from mult_orb_ham import Hamiltonian
from density_core import entanglement_entropy, partial_psi_trace
from numba_wrapper import updn_list
sys.path.append('../../plot_manager')
from plot2d import plotfig_4p
fol = sys.argv[1]
saved = True
trange = np.arange(0,100,0.1)

if not saved:
    print("Calculcating data from the saved wavefunction")
    sign = [1,-1,-1,1,1,-1,-1,1]
    stag    = np.zeros((len(trange),4))
    ent     = np.zeros((len(trange),4))
    results = np.zeros((len(trange),8))
    Ns = 8
    sys = Basis(L=Ns, N=Ns, model = 1)
    ham = Hamiltonian(sys, periodic=0, Lx=2, Ly=2, orb=2)
    op = ham.op_spin()
    b = spinful_fermion_basis_general(8, Nf = updn_list(8)) 
    #print('basis size:\t', b.Ns)

    def expect(psi, site='0'):
        return np.real(psi.conj() @ op['sz'+site] @ psi)
    nm = "05"
    for t_ind in range(len(trange)):
        f = h5py.File("psi_"+fol+"/psi_"+nm+".h5","r")
        psi = f[str(t_ind)+"r"][:] + 1j*f[str(t_ind)+"i"][:]
        for site in range(Ns):
            stag[t_ind, 0] += expect(psi, str(site)) * sign[site]
        ent[t_ind, 0] = b.ent_entropy(psi, sub_sys_A=([0,1,4,5],[0,1,4,5]), 
                                   enforce_pure=True,alpha=1.0,density=False)["Sent_A"]
    nm = "06"
    for t_ind in range(len(trange)):
        f = h5py.File("psi_"+fol+"/psi_"+nm+".h5","r")
        psi = f[str(t_ind)+"r"][:] + 1j*f[str(t_ind)+"i"][:]
        for site in range(Ns):
            stag[t_ind, 1] += expect(psi, str(site)) * sign[site]
        ent[t_ind, 1] = b.ent_entropy(psi, sub_sys_A=([0,1,4,5],[0,1,4,5]), 
                                   enforce_pure=True,alpha=1.0,density=False)["Sent_A"]
    nm = "01"
    for t_ind in range(len(trange)):
        f = h5py.File("psi_"+fol+"/psi_"+nm+".h5","r")
        psi = f[str(t_ind)+"r"][:] + 1j*f[str(t_ind)+"i"][:]
        for site in range(Ns):
            stag[t_ind, 2] += expect(psi, str(site)) * sign[site]
        ent[t_ind, 2] = b.ent_entropy(psi, sub_sys_A=([0,1,4,5],[0,1,4,5]), 
                                   enforce_pure=True,alpha=1.0,density=False)["Sent_A"]
    nm = "02"
    for t_ind in range(len(trange)):
        f = h5py.File("psi_"+fol+"/psi_"+nm+".h5","r")
        psi = f[str(t_ind)+"r"][:] + 1j*f[str(t_ind)+"i"][:]
        for site in range(Ns):
            stag[t_ind, 3] += expect(psi, str(site)) * sign[site]
        ent[t_ind, 3] = b.ent_entropy(psi, sub_sys_A=([0,1,4,5],[0,1,4,5]), 
                                   enforce_pure=True,alpha=1.0,density=False)["Sent_A"]
    results[:,:4]  = stag[:,:]/4
    results[:,4:8] = ent[:,:]
        
    ft = h5py.File("results/04.h5","w")
    ft.create_dataset("results",data=results)
    ft.close()
else:
    print("!!! reading data from saved file: results/04.h5 !!!")
    ft = h5py.File("results/04.h5","r")
    results = ft["results"][:]
    ft.close()


t0 = 50; s = 10
zmax = 0.5; hbarw = 1.54; w = hbarw/0.6852119
A = (zmax * np.cos(w * trange) * np.exp(-(trange-t0)**2 / (2*s*s))).reshape(len(trange),1)
A = A*0.1 + 0.80
results = np.hstack((results, A))
 
data = dict();values=dict(); Gs4p=dict()
sf = np.array([[0,2],[2,4],[4,6],[6,8]])
Alpha = [1, 1]*4 +[1]
Cl = ["k","C3"]*4 + ["C0"]
Gs4p['t']=0.95; Gs4p['b']=0.1; Gs4p['l']=0.11; Gs4p['r']=0.97
Gs4p['w']=0.3; Gs4p['h']=0.2
data['x_plot'] = trange; data['y_plot'] = results
values['xlabel1'] = values['xlabel2'] = values['ylabel2'] = values['ylabel4'] = ""
values['xlabel3'] = r'$\mathrm{Time~(fs)}$'
values['xlabel4'] = r'$\mathrm{Time~(fs)}$';
#values['ylabel1'] = r'$\mathrm{Staggered}$'+"\n"+r'$\mathrm{Magnetization/N}$'
values['ylabel1'] = r'$\textrm{N\'eel~vector}~\rm N_z/N$'
values['ylabel3'] = r'$\mathrm{\mathcal{S}_{\rm half}}$'
plotfig_4p(data, "4", values, sf=sf, Gs=Gs4p, Alpha=Alpha, Cl=Cl, 
           minor=1, fs=8, lw=0.4, ylim=[0,1], xlim=[0,100])
