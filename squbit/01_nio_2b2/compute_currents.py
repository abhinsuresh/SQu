import sys, numpy as np, h5py, os
from quspin.basis import spinful_fermion_basis_general
sys.path.append('../')
from spinful_basis import Basis
from mult_orb_ham import Hamiltonian
from density_core import entanglement_entropy, partial_psi_trace
from numba_wrapper import updn_list
sys.path.append('../../plot_manager')
from plot2d import plotfig_4p
trange = np.arange(0,100,0.1)
fol = sys.argv[1]
nm = sys.argv[2]
print("!!! on file:"+fol+nm+" !!!")

print("Calculcating data from the saved wavefunction")
Ns = 8
sys = Basis(L=Ns, N=Ns, model = 1)
ham = Hamiltonian(sys, periodic=0, Lx=2, Ly=2, orb=2)
op = ham.op_spin()
b = spinful_fermion_basis_general(8, Nf = updn_list(8)) 
#print('basis size:\t', b.Ns)
Ix1 =   -1j*ham.spin_curr(0,1)['ch'] \
        -1j*ham.spin_curr(4,5)['ch'] 
Ix2 =   -1j*ham.spin_curr(2,3)['ch'] \
        -1j*ham.spin_curr(6,7)['ch']
Iy1 =   -1j*ham.spin_curr(0,2)['ch'] \
        -1j*ham.spin_curr(4,6)['ch'] 
Iy2 =   -1j*ham.spin_curr(1,3)['ch'] \
        -1j*ham.spin_curr(5,7)['ch']

N1 = ham.number_op(0) + ham.number_op(4)
N2 = ham.number_op(1) + ham.number_op(5)
N3 = ham.number_op(2) + ham.number_op(6)
N4 = ham.number_op(3) + ham.number_op(7)

cx      = np.zeros((len(trange),2))
cy      = np.zeros((len(trange),2))
ni      = np.zeros((len(trange),4))
dcx     = np.zeros((len(trange),2))
dcy     = np.zeros((len(trange),2))
dni     = np.zeros((len(trange),4))
results = np.zeros((len(trange),8))

f = h5py.File("psi_"+fol+"/psi_"+nm+".h5","r")
for t_ind in range(len(trange)):
    psi = f[str(t_ind)+"r"][:] + 1j*f[str(t_ind)+"i"][:]
    cx[t_ind,0] += psi.conj() @ (Ix1 + Ix1.conj().T) @ psi
    cx[t_ind,1] += psi.conj() @ (Ix2 + Ix2.conj().T) @ psi
    cy[t_ind,0] += psi.conj() @ (Iy1 + Iy1.conj().T) @ psi
    cy[t_ind,1] += psi.conj() @ (Iy2 + Iy2.conj().T) @ psi
    ni[t_ind,0]  += psi.conj() @ (N1) @ psi
    ni[t_ind,1]  += psi.conj() @ (N2) @ psi
    ni[t_ind,2]  += psi.conj() @ (N3) @ psi
    ni[t_ind,3]  += psi.conj() @ (N4) @ psi
f.close()

for t_ind in range(len(trange)-1):
    dcx[t_ind, :] = (cx[t_ind+1, :] - cx[t_ind, :])/2
    dcy[t_ind, :] = (cy[t_ind+1, :] - cy[t_ind, :])/2
    dni[t_ind, :] = (ni[t_ind+1, :] - ni[t_ind, :])/2
#filling last value with same as 2nd last value
dcx[-1,:] = dcx[-2,:]; dcy[-1,:] = dcy[-2,:]
dni[-1,:] = dni[-2,:]


ft = h5py.File("results/i_"+fol+nm+".h5","w")
ft.create_dataset("cx",data=cx)
ft.create_dataset("cy",data=cy)
ft.create_dataset("ni",data=ni)
ft.create_dataset("dcx",data=dcx)
ft.create_dataset("dcy",data=dcy)
ft.create_dataset("dni",data=dni)
ft.close()
