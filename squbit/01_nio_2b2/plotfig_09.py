import sys, numpy as np, h5py, os
sys.path.append('../')
from spinful_basis import Basis
from mult_orb_ham import Hamiltonian
from field_wrapper import E_Jefimenko, get_fft
sys.path.append('../../plot_manager')
from plot2d import plotfig_4p
#fol = sys.argv[1]
saved = False
trange = np.arange(0,100,0.1)
if not saved:
    print("!!! reading data from saved file: results/06_01.h5 !!!")
    results = np.zeros((500,4)) 
    
    nm = "01"
    ft = h5py.File("results/07_"+nm+".h5","r")
    E = ft["E"][:]
    fftx = ft["fftx"][:]
    ffty = ft["ffty"][:]
    ft.close()
    print(fftx.shape, ffty.shape)
    results[:,0] = ffty
    
    nm = "02"
    ft = h5py.File("results/07_"+nm+".h5","r")
    E = ft["E"][:]
    fftx = ft["fftx"][:]
    ffty = ft["ffty"][:]
    ft.close()
    print(fftx.shape, ffty.shape)
    results[:,1] = ffty
    
    nm = "03"
    ft = h5py.File("results/07_"+nm+".h5","r")
    E = ft["E"][:]
    fftx = ft["fftx"][:]
    ffty = ft["ffty"][:]
    ft.close()
    print(fftx.shape, ffty.shape)
    results[:,2] = ffty
    
    nm = "04"
    ft = h5py.File("results/07_"+nm+".h5","r")
    E = ft["E"][:]
    fftx = ft["fftx"][:]
    ffty = ft["ffty"][:]
    ft.close()
    print(fftx.shape, ffty.shape)
    results[:,3] = ffty

data = dict();values=dict(); Gs4p=dict()
ps = 0.3747
data['x_plot'] = fftx*1000; data['y_plot'] = results

sf = np.array([[0,1],[1,2],[2,3],[3,4]])
Alpha = [1, 1]*4 +[1]
Cl = ["k"]*4
Ls = ['-',':']*2 + ['-']*4
Mk = ['','o']*2 + ['']*4
Gs4p['t']=0.95; Gs4p['b']=0.1; Gs4p['l']=0.2; Gs4p['r']=0.95
Gs4p['w']=0.3; Gs4p['h']=0.3
values['xlabel1'] = values['xlabel2'] = values['ylabel2'] = values['ylabel4'] = ""
values['xlabel3'] = r'$\mathrm{Frequency\ (THz)}$'
values['xlabel4'] = r'$\mathrm{Frequency\ (THz)}$';
values['ylabel1'] = r'$\mathrm{FFT\ Power}$' +'\n'+ r'$\mathrm{|E_x|^2\ (au)}$'
values['ylabel3'] = r'$\mathrm{FFT\ Power}$' +'\n'+ r'$\mathrm{|E_x|^2\ (au)}$'
plotfig_4p(data, "9", values, sf=sf, Gs=Gs4p, Cl=Cl, 
           minor=1, fs=8, lw=0.4, xlim=[0,300])
