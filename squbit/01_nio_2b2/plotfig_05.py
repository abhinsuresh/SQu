import sys, numpy as np, h5py, os
from quspin.basis import spinful_fermion_basis_general
sys.path.append('../')
from spinful_basis import Basis
from mult_orb_ham import Hamiltonian
from density_core import entanglement_entropy, partial_psi_trace
from numba_wrapper import updn_list
sys.path.append('../../plot_manager')
from plot2d import plotfig_4p
fol = sys.argv[1]
saved = True
trange = np.arange(0,100,0.1)

if not saved:
    print("Calculcating data from the saved wavefunction")
    sign = [1,-1,-1,1,1,-1,-1,1]
    Ns = 8
    sys = Basis(L=Ns, N=Ns, model = 1)
    ham = Hamiltonian(sys, periodic=0, Lx=2, Ly=2, orb=2)
    op = ham.op_spin()
    b = spinful_fermion_basis_general(8, Nf = updn_list(8)) 
    #print('basis size:\t', b.Ns)
    Iszx = -1j*ham.spin_curr(0,1)['sz'] -1j*ham.spin_curr(2,3)['sz'] \
           -1j*ham.spin_curr(4,5)['sz'] -1j*ham.spin_curr(6,7)['sz']
    Iszy = -1j*ham.spin_curr(0,2)['sz'] -1j*ham.spin_curr(4,6)['sz'] \
           -1j*ham.spin_curr(1,3)['sz'] -1j*ham.spin_curr(5,7)['sz']
    Ix =   -1j*ham.spin_curr(0,1)['ch'] -1j*ham.spin_curr(2,3)['ch'] \
           -1j*ham.spin_curr(4,5)['ch'] -1j*ham.spin_curr(6,7)['ch']
    Iy =   -1j*ham.spin_curr(0,2)['ch'] -1j*ham.spin_curr(4,6)['ch'] \
           -1j*ham.spin_curr(1,3)['ch'] -1j*ham.spin_curr(5,7)['ch']

    cx      = np.zeros((len(trange),4))
    cy      = np.zeros((len(trange),4))
    cszx    = np.zeros((len(trange),4))
    cszy    = np.zeros((len(trange),4))
    results = np.zeros((len(trange),16))

    def expect(psi, site='0'):
        return np.real(psi.conj() @ op['sz'+site] @ psi)
    
    nm = "01"
    for t_ind in range(len(trange)):
        f = h5py.File("psi_"+fol+"/psi_"+nm+".h5","r")
        psi = f[str(t_ind)+"r"][:] + 1j*f[str(t_ind)+"i"][:]
        cx[t_ind,0] += psi.conj() @ (Ix + Ix.conj().T) @ psi
        cy[t_ind,0] += psi.conj() @ (Iy + Iy.conj().T) @ psi
        cszx[t_ind,0] += psi.conj() @ (Iszx + Iszx.conj().T) @ psi
        cszy[t_ind,0] += psi.conj() @ (Iszy + Iszy.conj().T) @ psi

    nm = "02"
    for t_ind in range(len(trange)):
        f = h5py.File("psi_"+fol+"/psi_"+nm+".h5","r")
        psi = f[str(t_ind)+"r"][:] + 1j*f[str(t_ind)+"i"][:]        
        cx[t_ind,1] += psi.conj() @ (Ix + Ix.conj().T) @ psi
        cy[t_ind,1] += psi.conj() @ (Iy + Iy.conj().T) @ psi
        cszx[t_ind,1] += psi.conj() @ (Iszx + Iszx.conj().T) @ psi
        cszy[t_ind,1] += psi.conj() @ (Iszy + Iszy.conj().T) @ psi

    nm = "03"
    for t_ind in range(len(trange)):
        f = h5py.File("psi_"+fol+"/psi_"+nm+".h5","r")
        psi = f[str(t_ind)+"r"][:] + 1j*f[str(t_ind)+"i"][:]
        cx[t_ind,2] += psi.conj() @ (Ix + Ix.conj().T) @ psi
        cy[t_ind,2] += psi.conj() @ (Iy + Iy.conj().T) @ psi
        cszx[t_ind,2] += psi.conj() @ (Iszx + Iszx.conj().T) @ psi
        cszy[t_ind,2] += psi.conj() @ (Iszy + Iszy.conj().T) @ psi

    nm = "04"
    for t_ind in range(len(trange)):
        f = h5py.File("psi_"+fol+"/psi_"+nm+".h5","r")
        psi = f[str(t_ind)+"r"][:] + 1j*f[str(t_ind)+"i"][:]
        cx[t_ind,3] += psi.conj() @ (Ix + Ix.conj().T) @ psi
        cy[t_ind,3] += psi.conj() @ (Iy + Iy.conj().T) @ psi
        cszx[t_ind,3] += psi.conj() @ (Iszx + Iszx.conj().T) @ psi
        cszy[t_ind,3] += psi.conj() @ (Iszy + Iszy.conj().T) @ psi

    results[:,:4]  = cx[:,:]
    results[:,4:8] = cszx[:,:]
    results[:,8:12]  = cy[:,:]
    results[:,12:16] = cszy[:,:]
        
    ft = h5py.File("results/05.h5","w")
    ft.create_dataset("results",data=results)
    ft.close()
else:
    print("!!! reading data from saved file: results/05.h5 !!!")
    ft = h5py.File("results/05.h5","r")
    results = ft["results"][:]
    ft.close()


t0 = 50; s = 10
zmax = 0.5; hbarw = 1.54; w = hbarw/0.6852119
A = (zmax * np.cos(w * trange) * np.exp(-(trange-t0)**2 / (2*s*s))).reshape(len(trange),1)
A = A*0.1 + 0.80
results = np.hstack((results, A))
 
data = dict();values=dict(); Gs4p=dict()
sf = np.array([[0,2],[2,4],[4,6],[6,8]])
Alpha = [1, 1]*4 +[1]
Cl = ["k","C3"]*4 + ["C0"]
Ls = ['-',':']*2 + ['-']*4
Mk = ['','o']*2 + ['']*4
Gs4p['t']=0.95; Gs4p['b']=0.1; Gs4p['l']=0.12; Gs4p['r']=0.97
Gs4p['w']=0.3; Gs4p['h']=0.3
data['x_plot'] = trange; data['y_plot'] = results
values['xlabel1'] = values['xlabel2'] = values['ylabel2'] = values['ylabel4'] = ""
values['xlabel3'] = r'$\mathrm{Time~(fs)}$'
values['xlabel4'] = r'$\mathrm{Time~(fs)}$';
values['ylabel1'] = r'$I_x~(\frac{e\gamma}{h})$'
values['ylabel3'] = r'$I^{S_z}_x~(\frac{e\gamma}{h})$'
plotfig_4p(data, "5", values, sf=sf, Gs=Gs4p, Alpha=Alpha, Cl=Cl, Ls=Ls,
           minor=1, fs=8, lw=0.4, xlim=[0,100])
