import sys, numpy as np, h5py, os
sys.path.append('../../plot_manager')
from plot2d import plotfig_spec
saved = False
trange = np.arange(0,100,0.1)

if not saved:
    print("reading data from txt files")

    pre = [float(filename[2:][:-4]) for filename in os.listdir('spectral_function/.') 
           if filename.startswith("V_")] 
    k1 = np.zeros(100)
    w1 = np.zeros(len(pre))
    z1 = np.zeros((len(pre),100))
    s_ind = np.argsort(pre)
    print("w len, k len:", len(pre), np.loadtxt("spectral_function/V_"+str(pre[s_ind[0]])+".txt").shape)
    for ind in range(len(pre)):
        z1[ind,:] = np.loadtxt("spectral_function/V_"+str(pre[s_ind[ind]])+".txt")
        w1[ind] = pre[s_ind[ind]]
        #print(pre[s_ind[ind]])
    k1 = np.linspace(-np.pi,np.pi,100)


    pre = [float(filename[4:][:-4]) for filename in os.listdir('spectral_function/.') 
           if filename.startswith("SO1_")] 
    k2 = np.zeros(100)
    w2 = np.zeros(len(pre))
    z2 = np.zeros((len(pre),100))
    s_ind = np.argsort(pre)
    print("w len, k len", len(pre), np.loadtxt("spectral_function/SO1_"+str(pre[s_ind[0]])+".txt").shape)
    for ind in range(len(pre)):
        z2[ind,:] = np.loadtxt("spectral_function/SO1_"+str(pre[s_ind[ind]])+".txt")
        if np.sum(z2[ind,:]) > 50:
            z2[ind,:] = 1 - z2[ind,:]
        w2[ind] = pre[s_ind[ind]]
        #print(pre[s_ind[ind]])
    print(w1[-5:])
    print(w2[-5:])
    k2 = np.linspace(-np.pi,np.pi,100)

    data = dict()
    data['x1_plot'] = k1; data['y1_plot'] = w1; data['z1_plot'] = z1
    data['x2_plot'] = k2; data['y2_plot'] = w2; data['z2_plot'] = z2
    #ft = h5py.File("results/09.h5","w")
    #ft.create_dataset("results",data=results)
    #ft.close()
else:
    print("!!! plotting data from saved file: results/09.h5 !!!")
    #ft = h5py.File("results/09.h5","r")
    #results = ft["results"][:]
    #ft.close()

values=dict(); Gs2p=dict()
#sf = np.array([[0,2],[2,4],[4,6],[6,8]])
#Alpha = [1, 1]*4 +[1]
#Cl = ["k","C3"]*4 + ["C0"]
Gs2p['t']=0.97; Gs2p['b']=0.19; Gs2p['l']=0.09; Gs2p['r']=0.96
Gs2p['w']=0.3; Gs2p['h']=0.4
values['xlabel1'] = values['xlabel2'] = r"$\mathrm{k}$"
values['ylabel1'] = r'$\mathrm{\omega}$'
values['ylabel2'] = ""
plotfig_spec(data, "3", values, Gs=Gs2p, minor=1, fs=8, lw=0.4)
