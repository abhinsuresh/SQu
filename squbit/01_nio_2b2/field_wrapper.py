import numpy as np
#unit convention
#-------------------------------------------------------------------
kl  = 4.154 * 1e-10      #[m]
c   = 2.99792458*1e8     #[m/s]
t_r = kl/(c*1e-15 * 0.1) #[None] number of time steps taken for t-retardation in 0.1 fs

def ef_t1(rn, r):
    dr = r - rn
    l_dr = np.linalg.norm(dr)
    sb = int(t_r*l_dr)
    return dr/(l_dr**3), sb

def ef_t2(rn, r):
    dr = r - rn
    l_dr = np.linalg.norm(dr)
    sb = int(t_r*l_dr)
    return dr/(l_dr**2), sb

def ef_t3(r1, r2, r, Nint):
    dt = 1/Nint
    val = [0.0, 0.0, 0.0]
    dl = (r2 - r1)*dt
    for tstep in range(1, Nint):
        l = r1 + tstep*dl
        rl = r - l
        val = val + dl/np.linalg.norm(rl)
    sb = int(t_r*np.linalg.norm(r - (r2-r1)/2))
    return val, sb

def E_Jefimenko(pos_atoms, bonds, curr, dcurr, rho, drho, R, ec1, ec2, ec3, mu0, Nint, trange):
    E = np.zeros((len(trange),3))
    for t_ind, t_now in enumerate(trange):
        for ind, pos in enumerate(pos_atoms):
            e1, sb1 = ef_t1(pos, R)
            #if (t_ind==0): print(e1)
            e2, sb2 = ef_t2(pos, R)
            if (t_ind - sb1) < 0: v1 = 0
            else: v1 = rho[t_ind, ind]
            if (t_ind - sb2) < 0: v2 = 0
            else: v2 = drho[t_ind, ind]
            
            E[t_ind, :] += ec1 * e1 *v1
            E[t_ind, :] += ec2 * e2 *v2
    
        for b_ind, bond in enumerate(bonds):
            e3, sb3 = ef_t3(pos_atoms[bond[0]], pos_atoms[bond[1]], R, Nint)
            if (t_ind - sb3) < 0: v3 = 0
            else: v3 = dcurr[t_ind, b_ind]

            E[t_ind, :] += ec3 * e3 * v3
    return E

def get_fft(t_span, fun, nm='filename'):
    """
    tspan: time step in fs 
    fun: function to get fft
    """
    #print('max frequency')
    #print('min frequency')

    dt = t_span[1] - t_span[0]
    N = t_span.size
    yif = np.fft.fft(fun)
    yf = (2/N)*np.abs(yif[0:N//2])**2
    xf = np.fft.fftfreq(N, dt)[:N//2]

    #normalize max value to 1
    m_val = np.max(yf)
    yf = yf/m_val

    return xf, yf
