import sys, numpy as np, h5py, os
sys.path.append('../')
from spinful_basis import Basis
from mult_orb_ham import Hamiltonian
from field_wrapper import E_Jefimenko, get_fft
sys.path.append('../../plot_manager')
from plot2d import plotfig_4p
#fol = sys.argv[1]
trange = np.arange(0,100,0.1)
fol = sys.argv[1]
nm = sys.argv[2]

print("!!! reading curents from saved file: results/i_"+fol+nm+".h5 !!!")

ft  = h5py.File("results/i_"+fol+nm+".h5","r")
cx  = ft["cx"][:]           # e/fs
cy  = ft["cy"][:]           
ni  = ft["ni"][:]           # e
dcx = ft["dcx"][:]          # e/fs2
dcy = ft["dcy"][:]
dni = ft["dni"][:]          # e/fs
ft.close()

ki  = 1.602176634 * 1e-4         #[A] or [C/s]
kc  = 1.602176634 * 1e-19        #[C]
kl  = 4.154 * 1e-10              #[m]
mu0  = 1.256637062 * 1e-6         #[H/m]
ep0 = 8.8541878128 * 1e-12      #[F/m]
c   = 2.99792458 * 1e8           #[m/s]
Nint = 10

# all in SI units for derivative 1/dt is not taken into consideration now
curr    = ki * np.hstack((cx, cy))
dcurr   = ki * np.hstack((dcx, dcy))        #/dt not taken into account
rho       = kc * ni
drho      = kc * dni            

#input currents and its derivatives are in SI, the distance and time units are in
#base units of corresponding angstrom which is taken into consideration with ec
#parameters
ec1 = 1/(4*np.pi*ep0 * kl * kl)
ec2 = 1/(4*np.pi*ep0 * kl * c * 1e-15)
ec3 = 1/(4*np.pi*ep0 * c * c * 1e-15)

print(np.max(ni[:,0]), np.min(ni[:,0]))

bonds = [[0, 1], [2, 3], [0, 2], [1, 3]]
pos_atoms = np.array([[-0.5,-0.5,0],[0.5,-0.5,0],[-0.5,0.5,0],[0.5,0.5,0]])
R = np.array([0,0,10000])
E = E_Jefimenko(pos_atoms, bonds, curr, dcurr, rho, drho, 
                R, ec1, ec2, ec3, mu0, Nint, trange)
fftx, ffty = get_fft(trange, E[:,0])
ft = h5py.File("results/f_"+fol+nm+".h5","w")
ft.create_dataset("E",data=E[:,0])
ft.create_dataset("fftx",data=fftx)
ft.create_dataset("ffty",data=ffty)
ft.close()
#Ns = 8
#sys = Basis(L=Ns, N=Ns, model = 1)
#ham = Hamiltonian(sys, periodic=0, Lx=2, Ly=2, orb=2)
#x_list, y_list = ham.index.nn_int()
#bonds = [x_list[0]] + [x_list[1]] + [y_list[0]] + [y_list[2]]

 
data = dict();values=dict(); Gs4p=dict()
sf = np.array([[0,1],[1,2],[2,3],[3,3]])
Alpha = [1, 1]*4 +[1]
Cl = ["k","C3"]*4 + ["C0"]
Ls = ['-',':']*2 + ['-']*4
Mk = ['','o']*2 + ['']*4
Gs4p['t']=0.95; Gs4p['b']=0.1; Gs4p['l']=0.12; Gs4p['r']=0.97
Gs4p['w']=0.3; Gs4p['h']=0.3
data['x_plot'] = trange; data['y_plot'] = E
values['xlabel1'] = values['xlabel2'] = values['ylabel2'] = values['ylabel4'] = ""
values['xlabel3'] = r'$\mathrm{Time~(fs)}$'
values['xlabel4'] = r'$\mathrm{Time~(fs)}$';
values['ylabel1'] = r'$\mathrm{E_x}$'
values['ylabel2'] = r'$\mathrm{E_y}$'
plotfig_4p(data, "7", values, sf=sf, Gs=Gs4p,
           minor=1, fs=8, lw=0.4, xlim=[0,100])
