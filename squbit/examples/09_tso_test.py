import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import identity as iden
import scipy.integrate as inte
import h5py

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc
from cupy import linalg as cula

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from numba_wrapper import calc_rho


float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951

def vec(t):
    omega = 1.5/HBAR
    s = 5
    t0 = 25
    E0 = 0
    Et = E0*np.cos(omega*t) * np.exp(-(t - t0)*(t - t0)/(2*s*s))
    return Et


#parameters
Lx  = 2
Ly  = 2
t   = 1.0
U   = 8.0
J   = 0.1
Jz  = 0.2
tso = 0.5
err = 1e-3
dt = 0.1
inp = [Lx, Ly, t, J, Jz, err, dt, 2.1/HBAR,5,25,0.12]
inp = np.asarray(inp)
#derived parameters
_time = np.arange(0,10,dt)
L = Lx*Ly
start = time.time()
sys = Basis(L=L, N=L, model = 1)
spin = np.zeros((L ,3))
curr = np.zeros(4)
#pulse = np.zeros(len(_time))

ham = Hamiltonian(sys, periodic=1, Lx=Lx, Ly=Ly)
spcurr1 = ham.spin_curr(0,1)
spcurr2 = ham.spin_curr(0,2)

H_j, op, repj = ham.heisenberg(J=J, Jz=Jz, return_op=True)
HU = ham.hubbard_U(U=U)
defect = -err * op['sz0']
H_hop = ham.hubbard_sparse(t=t, alpha = 0)
H_so = ham.spin_orbit(alpha = tso)

#H = H_j + H_hop + HU + defect + H_so
H_fixed = H_j + HU + defect + H_so

H0 = H_hop + H_fixed


linalg.init()
a_gpu = gpuarray.to_gpu(H0.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V', 'T')
w = w_gpu.get()
v = vr_gpu.get().transpose()
print('gpu gone mad: ', w[:5])
print('gpu gone mad: ', np.conjugate(v[:,0]) @ H0 @ v[:,0])
print('\n')


w,v = sla.eigsh(H0, k = 5, which = 'SA')
print('Energies: ',w[:5])
print('Eig vec test: '+ str(v[:,0].conj()@ H0 @v[:,0]))
print('\n')
#print('sz0: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz0']@v[:,0])))

w,v = la.eigh(H0.toarray())
print('Energies: ',w[:5])
print('Eig vec test: '+ str(v[:,0].conj()@ H0 @v[:,0]))
print('\n')
#print('sz0: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz0']@v[:,0])))

a_gpu    = gpuarray.to_gpu(H0.toarray()) 
ainv_gpu = linalg.inv(a_gpu, overwrite=True)
mat_inv  = ainv_gpu.get()

if not np.allclose(H0.toarray() @ mat_inv, np.eye(len(w))):
    print('gpu inversion failed')
else:
    print('pass gpu inversion')


with open('results/spin_09.txt','w') as f:
    print('#output printed to file', file=f)
with open('results/curr_09.txt','w') as f:
    print('#output printed to file', file=f)
    
Et = vec(_time)
At = 0
psi = v[:,0]
for t_ind, ti in enumerate(_time):
    t_start = time.time()

    #observable
    for site in range(L):
        for ax_ind, axis in enumerate(['x', 'y', 'z']):
            spin[site,ax_ind] = \
            np.real(psi.conj() @ op['s'+axis+str(site)] @ psi)
    
    curr = [2/HBAR*(psi.conj() @ spcurr1['ch'] @ psi).imag
            ,2/HBAR*(psi.conj() @ spcurr1['sz'] @ psi).imag
            ,2/HBAR*(psi.conj() @ spcurr2['ch'] @ psi).imag
            ,2/HBAR*(psi.conj() @ spcurr2['sz'] @ psi).imag]
    
    #evolution
    At      = -1*Et[t_ind]*dt + At
    alpha   = 2*np.pi * At / (HBAR)
    H_hop   = ham.hubbard_sparse(t=t, alpha = 0.0)
    Ht      = H_hop + H_fixed
    
    mat      = (iden(sys.Ns) + 1j*dt*Ht/2)
    a_gpu    = gpuarray.to_gpu(mat.toarray()) 
    ainv_gpu = linalg.inv(a_gpu, overwrite=True)
    mat_inv  = ainv_gpu.get()
    Ut       = mat_inv @ (iden(sys.Ns) - 1j*dt*Ht/2)
    psi      = Ut @ psi
     
    #write to txt file
    with open('results/out_09.txt','a') as fadd:
        print('t step', t_ind, file=fadd)
        print('time', time.time()-t_start, file=fadd)
    with open('results/spin_09.txt','a') as f:
        out_str = '{:3.2e} '.format(ti)
        for sp in spin:
            out_str += '{0: 1.3e} {1: 1.3e} {2: 1.3e} '.format(*sp)
        out_str +='\n'
        f.write(out_str)
    
    with open('results/curr_09.txt','a') as f:
        out_str = '{:3.2e} '.format(ti)
        out_str += '{0: 1.3e} {1: 1.3e} {2: 1.3e} {3: 1.3e} '.format(*curr)
        out_str += '{0: 1.3e}'.format(At)
        out_str +='\n'
        f.write(out_str)
        
end = time.time()
with open('results/out_09.txt','a') as fadd:
    print('time taken: ', "%1.2f"%(end - start),' s', file = fadd)
