import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from numba_wrapper import calc_rho

sys.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951

def vec(t):
    omega = 0.1/HBAR
    s = 5
    t0 = 25
    E0 = 0.12
    Et = E0*np.cos(omega*t) * np.exp(-(t - t0)*(t - t0)/(2*s*s))
    return Et


#parameters
Lx  = 2
Ly  = 2
t   = -1
J   = 0.1
Jz  = 0.1
err = 1.0
dt = 0.1
inp = [Lx, Ly, t, J, Jz, err, dt, 0.1/HBAR,5,25,0.12]
inp = np.asarray(inp)
#derived parameters
L   = Lx*Ly
if Ly>=2: dim = '2d'
_time = np.arange(0,50,dt)
spin = np.zeros((L ,3, len(_time)))
curr = np.zeros((4, len(_time)))
pulse = np.zeros(len(_time))

start = time.time()
sys = Basis(L=L, N=L, model = 1)
print(sys.Ns)

ham = Hamiltonian(sys, periodic=1, Lx=Lx, Ly=Ly, dim=dim, dtype=np.complex128)

spcurr1 = ham.spin_curr(0,1)
spcurr2 = ham.spin_curr(2,3)
#print(spcurr['spz'].toarray().real)

H_j, op, repj = ham.heisenberg(J=J, Jz=Jz, return_op=True)
defect = -err * op['sz0']
HU = ham.hubbard_U(U=8)

H_hop = ham.hubbard_sparse(t=t, alpha = 0)

H_static = H_j + defect + HU
H = H_static + H_hop



linalg.init()
a_gpu = gpuarray.to_gpu(H.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
w = w_gpu.get()
v = vr_gpu.get().transpose()
print(w[:5])
print(v[:,0].conj()@H.toarray()@v[:,0])

print('sz0: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz0']@v[:,0])))

Et = vec(_time)
At = 0
psi = v[:,0]
for t_ind, ti in enumerate(_time):

    #observable
    for site in range(L):
        for ax_ind, axis in enumerate(['x', 'y', 'z']):
            spin[site,ax_ind,t_ind] = \
            np.real((psi.conj() @ op['s'+axis+str(site)]) @ psi.transpose())
    pulse[t_ind] = At
    curr[:,t_ind] = [2/HBAR*(psi.conj() @ spcurr1['ch'] @ psi).imag
    ,2/HBAR*(psi.conj() @ spcurr1['sz'] @ psi).imag
    ,2/HBAR*(psi.conj() @ spcurr2['ch'] @ psi).imag
    ,2/HBAR*(psi.conj() @ spcurr2['sz'] @ psi).imag]
    
    #if t_ind%10==0: print(spin0[:,t_ind])
    #evolution
    At = -1*Et[t_ind]*dt + At
    alpha = 2*np.pi * At / (HBAR)
    H_hop = ham.hubbard_sparse(t=t, alpha = alpha)
    Ht = H_static + H_hop
    
    a_gpu = gpuarray.to_gpu(Ht.toarray()) 
    vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
    W = w_gpu.get()
    V = vr_gpu.get().transpose()
    #W, V = la.eigh(Ht.toarray())
    
    cn = [V[:,i].conj() @ psi.reshape((sys.Ns,1)) for i in range(sys.Ns)]
    #Ut = la.inv(np.eye(sys.Ns) + 1j*dt*Ht/(2*HBAR)) @ \
    #           (np.eye(sys.Ns) - 1j*dt*Ht/(2*HBAR))
    Ut = [np.exp(-1j*W[i]*dt/HBAR) for i in range(sys.Ns)]

    psi = np.sum([cn[i] * (Ut[i] * V[:,i]) for i in range(sys.Ns)], axis=0)


with h5py.File('results/31.hdf5', 'w') as f:
    dset = f.create_dataset("spin", 
             data = spin, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("curr", 
             data = curr, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("pulse", 
             data = pulse, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("input", 
             data = inp, 
             dtype=np.float64, compression="gzip")
f.close()

end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
