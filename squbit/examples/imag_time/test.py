#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix


import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian, Spin
from numba_wrapper import calc_rho_vdv
from evolve_step import rk4_rho_imag, rk4_rho

sys.path.append('../../../plot_manager/')
from plot2d import plotfig
#parameters
Lx  = 4
Ly  = 1
N   = 1
S   = 1
J   = -0.5
Jz  = -0.5
Bz  = 0.5
Bx  = 0.5
temp = 300
dt = 0.1
#derived parameters
L   = Lx*Ly
start = time.time()

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
Se = spin.op
#print(np.sqrt(2)*spin.Sy.toarray())

HJ   = spin.heisenberg(J=J,Jz=Jz)
Hb  = csr_matrix(HJ.shape)
for i in range(Lx):
    Hb = Hb - Bz*((1)**i)*spin.op['sz'+str(i)]
H0 = HJ + Hb

Ht = H0 - Bx*spin.op['sx0']

w,v = la.eigh(H0.toarray())

print('hshape\t', H0.shape)

rho = calc_rho_vdv(w, v, 0)
print(np.real(np.trace(rho @ Se['sz0'])))
print(np.real(np.trace(rho @ Se['sz1'])))
print(np.real(np.trace(rho @ Se['sz2'])))
print(np.real(np.trace(rho @ Se['sz3'])))

_time = np.arange(0,100,dt)
sx = np.zeros(len(_time))
sy = np.zeros(len(_time))
sz = np.zeros(len(_time))


for t_ind, ti in enumerate(_time):
    sx[t_ind] = np.real(np.trace(rho @ Se['sx1']))
    sy[t_ind] = np.real(np.trace(rho @ Se['sy1']))
    sz[t_ind] = np.real(np.trace(rho @ Se['sz1']))    
    rho = rk4_rho(Ht, [], rho, dt)


sx1 = np.zeros(len(_time))
sy1 = np.zeros(len(_time))
sz1 = np.zeros(len(_time))

psi = v[:,0]
Ut = la.expm(-1j*Ht.toarray()*dt)


for t_ind, ti in enumerate(_time):
    sx1[t_ind] = psi.conj() @ Se['sx1'] @ psi
    sy1[t_ind] = psi.conj() @ Se['sy1'] @ psi
    sz1[t_ind] = psi.conj() @ Se['sz1'] @ psi
    psi  = Ut @ psi



data = dict()
values = dict()
sf = np.array([[0,6]])

L = len(sx)
plot_x = _time.reshape(L,1);
plot_y = sz.reshape(L,1)
plot_y = np.concatenate((plot_y, sz1.reshape(L,1)), axis=1)
plot_y = np.concatenate((plot_y, sx.reshape(L,1)), axis=1)
plot_y = np.concatenate((plot_y, sx1.reshape(L,1)), axis=1)
plot_y = np.concatenate((plot_y, sy.reshape(L,1)), axis=1)
plot_y = np.concatenate((plot_y, sy1.reshape(L,1)), axis=1)

values['xlabel1'] = '' 
values['ylabel1'] = r'$\mathrm{s}$'
data['x_plot'] = plot_x; data['y_plot'] = plot_y

Gs = dict();
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.1; Gs['r']=0.9
Gs['w']=0.5; Gs['h']=0.4
Ls = ['-','--']*3
Cl = ['k', 'C0']*3

plotfig(data, '1', values, Gs=Gs, Ls=Ls, Cl=Cl, sf=sf, fs=8)




