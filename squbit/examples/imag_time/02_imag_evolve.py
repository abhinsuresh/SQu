#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix


import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from numba_wrapper import calc_rho_vdv, calc_rho0
from evolve_step import rk4_rho_imag, rho_imag

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
Lx  = 4
Ly  = 1
N   = 1
S   = 1
J   = -0.01
Jz  = -0.01
Bz  = 0.01
Bx  = 0.0
temp = 300
#derived parameters
L   = Lx*Ly
start = time.time()

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
Se = spin.op
#print(np.sqrt(2)*spin.Sy.toarray())

HJ   = spin.heisenberg(J=J,Jz=Jz)
Hb  = csr_matrix(HJ.shape)
print(Hb.shape)
for i in range(Lx):
    Hb = Hb - Bz*((1)**i)*spin.op['sz'+str(i)]
H0 = HJ + Hb

#H = H0 - Bx*spin.op['sx0']

w,v = la.eigh(H0.toarray())

print('hshape\t', H0.shape)
rho = calc_rho_vdv(w, v, temp)
print(np.real(np.trace(rho @ Se['sz0'])))
print(np.real(np.trace(rho @ Se['sz1'])))
print(np.real(np.trace(rho @ Se['sz2'])))
print(np.real(np.trace(rho @ Se['sz3'])))

rho0 = calc_rho0(w, v)
print('rho0 exp:\t', np.real(np.trace(rho0 @ Se['sz0'])))

KB = 8.617333*1e-5
temp = temp+1e-15
beta = 1*HBAR/(KB*temp)
print('\n hbar beta code\t', beta)
t_range = np.linspace(0, beta, 500)
dt =  t_range[1] - t_range[0]
print('time step\t', t_range[1] - t_range[0])

print('initial trace\t', np.trace(rho0))

#rhot = la.expm(-beta * H0.toarray())
rho_new = rho0

norm = np.zeros(len(t_range))
sz = np.zeros(len(t_range))

for i, j in enumerate(t_range[:-1]):
    rho_new = rho_imag(H0, rho_new, dt)
    #rho_new = rho_new/np.trace(rho_new)
    sz[i] = np.trace(rho_new @ Se['sz0'])
    norm[i] = np.abs(np.sum(np.sum(rho_new - rho)))

print('final trace\t', np.trace(rho_new))
rho_new = rho_new/ np.trace(rho_new)

print(np.trace(rho_new @ Se['sz0']))
print(np.trace(rho_new @ Se['sz1']))
print(np.trace(rho_new @ Se['sz2']))
print(np.trace(rho_new @ Se['sz3']))


"""
data = dict();values = dict();Gs = dict();

L = len(norm)
plot_x = t_range.reshape(L,1);
plot_y = norm.reshape(L,1)

values['xlabel1'] = r'$beta steps$' 
values['ylabel1'] = r'$\mathrm{norm}$'
data['x_plot'] = plot_x; data['y_plot'] = plot_y

sf = np.array([[0,1]])
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.1; Gs['r']=0.9
Gs['w']=0.5; Gs['h']=0.4
Ls = ['-','--']*3
Cl = ['k', 'C0']*3
Pf = []

plotfig(data, '2', values, Gs=Gs, Ls=Ls, Cl=Cl, sf=sf, fs=8)
"""






