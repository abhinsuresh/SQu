#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import sys
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix

sys.path.append('../../')
from hamiltonian_core import Spin
from numba_wrapper import calc_rho_vdv
from evolve_step import evolve, rk4_rho
from master_eq_solver import Henk_lindblad
from density_core import entanglement_entropy

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

HBAR = 0.658211951

#parameters
Lx  = 4
Ly  = 1
N   = 1
S   = 1
J   = -0.01
Jz  = -0.01
Bz  = 0.01
Bx  = 0.05
temp = 0
dt = 0.1
tf = 60
#derived parameters
L   = Lx*Ly

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
Se = spin.op
#print(np.sqrt(2)*spin.Sy.toarray())

HJ   = spin.heisenberg(J=J,Jz=Jz)
Hb  = csr_matrix(HJ.shape)
print(Hb.shape)
for i in range(Lx):
    Hb = Hb - Bz*((1)**i)*spin.op['sz'+str(i)]
H0 = HJ + Hb

Ht = H0 - Bx*spin.op['sx0']

w,v = la.eigh(H0.toarray())

oss = Henk_lindblad(w,v,dt, gamma_dp=0.05)


psi = v[:,0]
rho = calc_rho_vdv(w, v, temp)
rho_open = rho

print(np.real(np.trace(rho @ Se['sz0'])))
print(np.real(np.trace(rho @ Se['sz1'])))
print(np.real(np.trace(rho @ Se['sz2'])))
print(np.real(np.trace(rho @ Se['sz3'])))

_time = np.arange(0, tf, dt)
spin = np.zeros((6,len(_time)))


#Ut = evolve(Ht, 0, psi, dt, method='CN', time_dep=0)

for t_ind, time in enumerate(_time):

    spin[0,t_ind] = np.trace(Se['sx1'] @ rho)
    spin[1,t_ind] = np.trace(Se['sy1'] @ rho)
    spin[2,t_ind] = np.trace(Se['sz1'] @ rho)
    
    spin[3,t_ind] = np.trace(Se['sx1'] @ rho_open)
    spin[4,t_ind] = np.trace(Se['sy1'] @ rho_open)
    spin[5,t_ind] = np.trace(Se['sz1'] @ rho_open)

    
    
    rho = rk4_rho(Ht, 0, rho, dt)
    
    rho_open = oss.rk4_open(rho_open,Ht,0)
    
    #print(time)
    #psi = Ut @ psi


data = dict();values = dict();Gs = dict();

L = len(_time)
plot_x = _time.reshape(L,1);
plot_y = spin.transpose()

values['xlabel1'] = r'$\mathrm{Time\ (fs)}$' 
values['ylabel1'] = r'$\mathrm{S_1^{\alpha}}$'
data['x_plot'] = plot_x; data['y_plot'] = plot_y

sf = np.array([[0,6]])
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.3; Gs['r']=0.7
Gs['w']=0.5; Gs['h']=0.4
Ls = ['-']*3 + ['']*3
Cl = ['C0', 'C2', 'C3']*2
Mk = ['']*3 + [r'$\mathrm{O}$']*3
Ps = [1]*3 + [10]*3

plotfig(data, '4', values, Gs=Gs, Ls=Ls, Cl=Cl, Mk=Mk, Ps=Ps, sf=sf, fs=8, lw=0.4)






