import numpy as np
import h5py
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix


import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from numba_wrapper import calc_rho_vdv, calc_rho0
from evolve_step import rk4_rho_imag, rho_imag

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
KB = 8.617333*1e-5

#parameters
Lx  = 4
Ly  = 1
N   = 1
S   = 1
J   = -0.01
Jz  = -0.01
Bz  = 0.01
Bx  = 0.0
#derived parameters
L   = Lx*Ly
temp = np.arange(0,300,5)
rho_gibbs   = np.zeros(len(temp))
rho_ev      = np.zeros(len(temp))
f_dt      = np.zeros(len(temp))

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
Se = spin.op
HJ   = spin.heisenberg(J=J,Jz=Jz)
Hb  = csr_matrix(HJ.shape)
for i in range(Lx):
    Hb = Hb - Bz*((1)**i)*spin.op['sz'+str(i)]
H0 = HJ + Hb
#H = H0 - Bx*spin.op['sx0']
w,v = la.eigh(H0.toarray())
rho0 = calc_rho0(w, v)

for t_ind, temp in enumerate(temp):
    rho = calc_rho_vdv(w, v, temp)
    rho_gibbs[t_ind] =  np.real(np.trace(rho @ Se['sz0'])) + \
                        np.real(np.trace(rho @ Se['sz1'])) + \
                        np.real(np.trace(rho @ Se['sz2'])) + \
                        np.real(np.trace(rho @ Se['sz3']))


    temp = temp+1e-15
    beta = HBAR/(KB*temp)
    t_range = np.linspace(0, beta, 500)
    dt =  t_range[1] - t_range[0]
    rho_new = rho0
    for i, j in enumerate(t_range[:-1]):
        rho_new = rho_imag(H0, rho_new, dt)
    #outputs
    f_dt[t_ind] = dt
    rho_new = rho_new/ np.trace(rho_new)
    rho_ev[t_ind] =    np.trace(rho_new @ Se['sz0']) + \
                    np.trace(rho_new @ Se['sz1']) + \
                    np.trace(rho_new @ Se['sz2']) + \
                    np.trace(rho_new @ Se['sz3'])


with h5py.File('results/03.hdf5', 'w') as f:
    f.create_dataset("gibbs",
        data = rho_gibbs,
        dtype=np.float64, compression="gzip")
    f.create_dataset("evol",
        data = rho_ev,
        dtype=np.float64, compression="gzip")
    f.create_dataset("dt",
        data = f_dt,
        dtype=np.float64, compression="gzip")
f.close()
