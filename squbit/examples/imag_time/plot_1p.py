import numpy as np
import h5py
import sys
sys.path.append('../../../plot_manager/')
from plot2d import plotfig as plot_1p

tf = 60
dt = 0.1
times = np.arange(0, tf , dt)
lneg = h5py.File('results/05.hdf5')['lneg'][:]


#gibbs = f['gibbs'][:]
#evol  = f['evol'][:]
#f.close()
plot_y = lneg.transpose()
plot_x = times

data = dict();values = dict()
data['x_plot'] = plot_x; data['y_plot'] = plot_y
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{Log Negativity}$'
sf = np.array([[0,2]])
Cl = ['C1', 'k']
Ls = ['-', '']
Mk = ['', r'$\mathrm{O}$']
Lg = [r'$\mathrm{Gibbs}$', r'$\mathrm{Evolution}$']
Gs = dict();
Ps = [1, 20]
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.3; Gs['r']=0.7
Gs['w']=0.4; Gs['h']=0.4
plot_1p(data, 'fig_lneg', values, sf=sf, Cl=Cl, Ls=Ls, Mk=Mk,
        Gs=Gs, Lg=Lg, Ps=Ps, legend=0, fs=7, lw=0.4)
