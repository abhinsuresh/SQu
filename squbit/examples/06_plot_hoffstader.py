import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from numba_wrapper import calc_rho

sys.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

Lx  = 3
Ly  = 3
t   = -1
L   = Lx*Ly
fig_name='hoff'
_alpha = np.linspace(0,1,100)

start = time.time()
#sys = Basis(L=L, Nup=2, Ndn=2, model =2)
sys = Basis(L=L, Nup=1,Ndn=0, model = 1, use_iterator=True)
print(sys.Ns)
energy = np.zeros((len(_alpha), sys.Ns))

ham = Hamiltonian(sys, periodic=0, Lx=Lx, Ly=Ly)

H = ham.hubbard_sparse(t=t, alpha = 0.0)  

#H, rep = ham.hubbard_sparse(t=t, alpha = 0)
#w, v = la.eigh(H.toarray())
#print(H.toarray().real)
#print(np.round(w,2))
#print(np.round(H.toarray(),1))

for a_ind, alpha in enumerate(_alpha):
    H = ham.hubbard_sparse(t=t, alpha = alpha, Bfield=True)  
    w, v = la.eigh(H.toarray())
    #print(alpha, np.sum(H), w[0]) 
    energy[a_ind,:] = w
print(energy.shape) 
data = dict();values = dict()
data['alpha'] = _alpha; data['energy'] = energy/2
values['x_plot'] = 'alpha'; values['y_plot'] = 'energy'
values['xlabel'] = r'$\mathrm{\alpha}$'
values['ylabel'] = r'$\mathrm{E/|t|}$'
sf = np.array([[0,energy.shape[1]]])
Cl = ['k']*energy.shape[1]
Ls = ['-']*energy.shape[1]
Mk = ['']*energy.shape[1]
Gs = dict();
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.3; Gs['r']=0.7
Gs['w']=0.4; Gs['h']=0.4
#plt.plot(J_xy,spin_exp)
#plt.savefig("results/11.pdf")
plotfig(data, fig_name, values, sf=sf, Cl=Cl, Ls=Ls, Mk=Mk, 
        Gs=Gs, 
        fs=8, lw=0.2)

end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')

