import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from evolve import evolve
import sys
sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
E0  = 0
V   = 1
D   = 1
_time = np.arange(0,200,0.1)

def H_d(ti, L=2):
    #return csr_matrix((L, L), dtype=np.int8) #zero matrix
    omega = 1
    t_f = np.exp(1j*omega*ti)
    return csr_matrix([[0, V*t_f],[V*t_f.conj(), 0]], dtype=np.complex128)



Omega = np.sqrt( (2*D/4)**2 + 1  )

H_static = csr_matrix([[E0-D, 0],[0, E0+D]])
print('Ham: ',H_static)

w, v = la.eigh(H_static.toarray())
print('E: ',w)

#psi = v[:,0].reshape(2,1)
psi = np.asarray([[1],[0]])

res = evolve(_time, H_static, H_d, psi, method='CN')
data = dict()
data['x_plot'] = _time; data['y_plot'] = res
plotfig(data, '1')




