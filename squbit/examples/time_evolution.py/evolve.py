import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla


import sys




def evolve(_time,H_static, H_dynamic, psi, op=np.eye(2), method='diag',
            time_indep=False):
    """a function to evolve the wave function in time
    op can be later made into dict
    """
    observable = np.zeros(len(_time))
    if method == 'gpu':
        import pycuda.gpuarray as gpuarray
        import pycuda.autoinit
        from skcuda import linalg
        from skcuda import misc
    
    dt = _time[1] - _time[0]
    if time_indep:
        if method=='expm': Ut = la.expm(-1j*H_static.toarray()*dt)
        elif method=='eig_vec':
            w,v = la.eigh(H_static.toarray())
            Ut = np.asarray([np.exp(-1j*w[i]*dt) for i in range(2)])
        elif method=='CN':
            Ut = la.inv(np.eye(2) + 1j*dt*H_static/2) @ \
                       (np.eye(2) - 1j*dt*H_static/2)
    
    for t_ind, ti in enumerate(_time):

        #observable       
        observable[t_ind] = np.abs(psi[1])**2

        #Ht = H_static + H_dynamic(ti)
        if method=='expm': 
            if not time_indep:
                Ht = H_static + H_dynamic(ti)
                Ut = la.expm(-1j*Ht.toarray()*dt)
            psi = Ut @ psi

        elif method=='CN':
            if not time_indep:
                Ht = H_static + H_dynamic(ti)
                Ut = la.inv(np.eye(2) + 1j*dt*Ht/2) @ \
                           (np.eye(2) - 1j*dt*Ht/2)
            psi = Ut @ psi
            
        if method=='eig_vec':
            if not time_indep:
                Ht = H_static + H_dynamic(ti)
                w, v = la.eigh(Ht.toarray())
                Ut = np.asarray([np.exp(-1j*w[i]*dt) for i in range(2)])
            cn = [v[:,i].conj() @ psi for i in range(2)]
            psi = np.sum([cn[i] * (Ut[i] * v[:,i]) for i in range(2)], axis=0)

    return observable

"""        
        a_gpu = gpuarray.to_gpu(Ht.toarray()) 
        vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
        W = w_gpu.get()
        V = vr_gpu.get().transpose()
        
        for site in range(L):
            for ax_ind, axis in enumerate(['x', 'y', 'z']):
                spin[site,ax_ind,t_ind] = \
                np.real((psi.conj() @ op['s'+axis+str(site)]) @ psi.transpose())
        pulse[t_ind] = At
        
        #if t_ind%10==0: print(spin0[:,t_ind])
        #evolution
        At = -1*Et[t_ind]*dt + At
        alpha = 2*np.pi * At / (HBAR)
        H_hop = ham.hubbard_sparse(t=t, alpha = alpha)
        Ht = H_j + H_hop + defect
"""
