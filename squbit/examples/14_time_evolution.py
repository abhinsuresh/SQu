import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import matplotlib.pyplot as plt

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from numba_wrapper import calc_rho

sys.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951

def vec(t):
    omega = 2.01/HBAR
    s = 5
    t0 = 25
    E0 = 0.12
    Et = E0*np.cos(omega*t) * np.exp(-(t - t0)*(t - t0)/(2*s*s))
    return Et


#parameters
Lx  = 2
Ly  = 2
t   = -1
J   = 0.1
Jz  = 0.1
err = 1
dt = 0.001

#derived parameters
L   = Lx*Ly
if Ly>=2: dim = '2d'
fig_name='time_nodef'
#_alpha = np.linspace(0,1,100)
_time = np.arange(0,50,dt)
spin0 = np.zeros(len(_time))
pulse = np.zeros(len(_time))


start = time.time()
sys = Basis(L=L, N=L, model = 1)
print(sys.Ns)
#energy = np.zeros((len(_alpha), sys.Ns))

ham = Hamiltonian(sys, periodic=1, Lx=Lx, Ly=Ly, dim=dim, dtype=np.complex128)

H_j, op, repj = ham.heisenberg(J=J, Jz=Jz, return_op=True)
defect = -err * op['sz0']
H_hop, rep = ham.hubbard_sparse(t=t, alpha = 0)
H = H_j + H_hop + defect
w, v = la.eigh(H.toarray())
print(w[:5])

print('sz0: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz0']@v[:,0])))
print('sz1: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz1']@v[:,0])))
print('sz2: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz2']@v[:,0])))
print('sz3: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz3']@v[:,0])))

Et = vec(_time)
At = 0
psi = v[:,0]
for t_ind, ti in enumerate(_time):

    #observable
    spin0[t_ind] = np.real((psi.conj() @ op['sz0']) @ psi.transpose())
    #pulse[t_ind] = At
    
    if t_ind%10 == 0:  print(spin0[t_ind])

    #evolution
    At = -1*Et[t_ind]*dt + At
    alpha = 2*np.pi * At / (HBAR)
    
    H_hop, rep = ham.hubbard_sparse(t=t, alpha = alpha)
    Ht = H_j + H_hop + defect
    
    W, V = la.eigh(Ht.toarray())

    cn = [V[:,i].conj() @ psi.reshape((sys.Ns,1)) for i in range(sys.Ns)]
    #Ut = la.inv(np.eye(sys.Ns) + 1j*dt*Ht/(2*HBAR)) @ \
    #           (np.eye(sys.Ns) - 1j*dt*Ht/(2*HBAR))
    Ut = [np.exp(-1j*W[i]*dt/HBAR) for i in range(sys.Ns)]

    psi = np.sum([cn[i] * (Ut[i] * V[:,i]) for i in range(sys.Ns)], axis=0)
    
    
#w, v = la.eigh(H.toarray())
#print(np.round(w[:10],2))





data = dict();values = dict()
data['time'] = _time; data['spin'] = spin0
values['x_plot'] = 'time'; values['y_plot'] = 'spin'
values['xlabel'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel'] = r'$\mathrm{\langle \hat{s}^z_i \rangle}$'
#plt.plot(_time,pulse)
#plt.savefig("results/pulse.pdf")
plotfig(data, values, fig_name, fs=8, lw=0.4)

end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
