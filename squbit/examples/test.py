import numpy as np
import scipy.linalg as la

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian

#input parameters
L   = 4
L1  = L//2


sys = Basis(L=L, Nup=L//2, Ndn=L//2, model =1)
print(sys)
ham = Hamiltonian(sys, periodic=1)
H, rep = ham.hubbard_sparse(t=-1)

w, v = la.eigh(H.todense())

#print('Half entropy', '%0.3f'%sys.ent_entropy(v[:,0], L1, norm=False), '\n')
