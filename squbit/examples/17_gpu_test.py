import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from numba_wrapper import calc_rho

sys.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951


#parameters
Lx  = 4
Ly  = 2
t   = -1
J   = 0.1
Jz  = 0.1
err = 1.0
dt = 0.01

#derived parameters
L   = Lx*Ly
if Ly>=2: dim = '2d'

start = time.time()
sys = Basis(L=L, N=L, model = 1)
print(sys.Ns)

ham = Hamiltonian(sys, periodic=1, Lx=Lx, Ly=Ly, dim=dim)

H_j, op, repj = ham.heisenberg(J=J, Jz=Jz, return_op=True)
#defect1 = -err * op['sz0']
#defect2 =  err * op['sz4']
H_hop, rep = ham.hubbard_sparse(t=t, alpha = 0)
H = H_j + H_hop \
    -err * op['sz0'] + err*op['sz1'] \
    -err * op['sz2'] + err*op['sz3'] \
    +err * op['sz4'] - err*op['sz5'] \
    +err * op['sz6'] - err*op['sz7'] \


linalg.init()
a_gpu = gpuarray.to_gpu(H.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
print(vr_gpu[0,:].shape)

E = w_gpu.get()
v = vr_gpu.get().transpose()
print(E[:5])
print(v[:,0].conj()@H.toarray()@v[:,0])
#w, v = la.eigh(H.toarray())

print('sz0: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz0']@v[:,0])))
print('sz1: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz1']@v[:,0])))
print('sz2: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz2']@v[:,0])))
print('sz3: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz3']@v[:,0])))
print('sz4: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz4']@v[:,0])))
print('sz5: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz5']@v[:,0])))
print('sz6: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz6']@v[:,0])))
print('sz7: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz7']@v[:,0])))

end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
