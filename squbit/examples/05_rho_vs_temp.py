import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from index_core import Index
from numba_wrapper import calc_rho

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
dim='1d'
#parameters
name = '05'
Lx  = 4
Ly  = 2
t   = -1.0
U   = 8.0
mu  = 0
J   = 0.1
Jz  = 6.67
tso = 0.0
err = 1e-3
_temp = np.linspace(0,300,20)
#derived parameters
L   = Lx*Ly
if Ly>=2: dim = '2d'
spin = np.zeros((Lx*Ly,3,len(_temp)))
with open('results/out_'+name+'.txt','w') as f:
    print('#time check line of '+name, file=f)

start = time.time()
sys = Basis(L=L, N=L, model = 1)
#print("System size:", sys.Ns)

ham = Hamiltonian(sys, periodic=1, Lx=Lx, Ly=Ly, dim=dim)

H_j, op, repj = ham.heisenberg(J=J, Jz=Jz, return_op=True)
defect = -err * op['sz0']
H_hop = ham.hubbard_sparse(t=t, alpha = 0)
H_U = ham.hubbard_U(U=U)
H_so = ham.spin_orbit(alpha = tso)
#H_mu = ham.onsite_mu(mu=mu)

H = H_j + H_hop + H_U + defect + H_so

if not np.allclose(H.toarray(), np.asmatrix(H.toarray()).H):
    raise ValueError('bleh not hermitian')

linalg.init()
a_gpu = gpuarray.to_gpu(H.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
w = w_gpu.get()
v = vr_gpu.get().transpose()
with open('results/out_'+name+'.txt','a') as f:
    print('time for sys.gen+ham+diagonalizing= ',time.time()-start, file=f)
ss = 0
#for site in range(L):
#    print('sz'+str(site)+': ','{0: 1.3e}'.\
#    format(np.real(v[:,ss].conj()@op['sz'+str(site)]@v[:,ss])))

#rho = calc_rho(w, v, 0)
#print('spin at site 0', np.trace(rho @ op['sz0']))
with open('results/spin_'+name+'.txt','w') as f:
    print('#output printed from next line of '+name, file=f)

for t_ind, temp in enumerate(_temp):
    st = time.time()
    rho = calc_rho(w, v, temp)
    with open('results/out_'+name+'.txt','a') as f:
        print('time for calculating rho=',time.time()-st, file=f)

    with open('results/spin_'+name+'.txt','a') as f:
        out_str = '{:1.3e} '.format(temp)
        for i in range(Lx*Ly):
            out_str += '{0: 1.3e} {1: 1.3e} {2: 1.3e} '.format(
                            np.real(np.trace(rho @ op['sx'+str(i)])),
                            np.real(np.trace(rho @ op['sy'+str(i)])),
                            np.real(np.trace(rho @ op['sz'+str(i)])),)
        out_str +='\n'
        f.write(out_str)


end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
