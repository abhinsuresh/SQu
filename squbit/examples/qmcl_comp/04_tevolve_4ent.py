#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py
from scipy.sparse import csr_matrix

#import pycuda.gpuarray as gpuarray
#import pycuda.autoinit
#from skcuda import linalg
#from skcuda import misc

import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from numba_wrapper import calc_rho
from evolve_step import evolve
from density_core import partial_trace
from density_core import entanglement_entropy
from density_core import logarithmic_negativity

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
Lx  = 4
Ly  = 1
N   = 1
S   = 1
J   = 0.01
Jz  = 0.01
Bz  = -0.06
Bx  = 0.01
Jsd = 0.01
t   = 1.0
#derived parameters
L   = Lx*Ly
start = time.time()
#electronic system
sys = Basis(L=L, N=N, model = 1)
#print(sys)
#electronic hamiltonian
ham = Hamiltonian(sys, periodic=0, Lx=Lx, Ly=Ly, dim=dim)
H_t = ham.hubbard_sparse(t=t, alpha = 0)
op_espin = ham.op_spin()

#spin system + eDOF
spin = Spin(S=S, L = L, kronD=sys.Ns, sc = 1)
sa = Spin(S=S, L = L, kronD=1, sc =1)
SA = sa.op
#spin spin and electronic spin
Se = spin.op
se   = ham.kron_iden(op_espin, spin.Ns)

#heisenberg hamitonian
HJ   = spin.heisenberg(J=J,Jz=Jz)
#stagered magnetic field
Hb  = csr_matrix(HJ.shape)
for i in range(L):
    Hb = Hb + Bz*((-1)**i)*spin.op['sz'+str(i)]
#initial hamitlonian
H0 = HJ + Hb
#t=0 pulse
Hp = -Bx*spin.op['sx0']

#hopping hamiltonian
H_t  = ham.kron_single(H_t,    spin.Ns)
#jsd hamitlonian
H_sd = spin.sd_ham(se, Jsd=Jsd)
#t=0 hamiltonian
Ht = H0 + H_t + H_sd + Hp

#creating the initial wavefunction
HI   = spin.heisenberg(J=0,Jz=Jz)
Hi = partial_trace(HI.toarray(), sub_sys='A', m=sys.Ns, n=spin.Ns)
Hi = Hi/(sys.Ns)
w,v = la.eigh(Hi)
print(w[:5])
s = 0
Se_i = partial_trace(Se['sz0'].toarray(), sub_sys='A', m=sys.Ns, n=spin.Ns)
Se_i = Se_i/(sys.Ns)
print(v[:,s].conj() @ Se_i @ v[:,s])
#putting one up electron at site 1
psi = np.zeros(sys.Ns)
psi[sys.Ns-1] = 1
psi = np.kron(psi, v[:,s])
print('spin z at site 1: ',psi @ se['sz0'] @ psi)

#time evolution
dt = 0.1
_time = np.arange(0,100,dt)
sx = np.zeros(len(_time))
sy = np.zeros(len(_time))
sz = np.zeros(len(_time))
sent = np.zeros(len(_time))
Ut = evolve(Ht, [], psi, dt, method='CN', time_dep=0)
for t_ind, t in enumerate(_time):
    sx[t_ind] = np.real(np.conjugate(psi) @ Se['sx1'] @ psi)
    sy[t_ind] = np.real(np.conjugate(psi) @ Se['sy1'] @ psi)
    sz[t_ind] = np.real(np.conjugate(psi) @ Se['sz1'] @ psi)

    rho = np.outer(np.conjugate(psi), psi)
    rho_s = partial_trace(rho, 'A', sys.Ns, spin.Ns)
    #print(np.trace(rho_s @ SA['sz0']))
    #print(np.trace(rho_s @ SA['sz1']))
    #sent[t_ind] = entanglement_entropy(rho_s)
    sent[t_ind] = logarithmic_negativity(rho_s, m=9)
    #print(sent[t_ind])
    
    psi = Ut @ psi  
    
data = dict();values = dict()
data['x_plot'] = _time; data['y_plot'] = sent;
values['x_plot'] = 'x_plot'; values['y_plot'] = 'y_plot'
values['xlabel'] = r'$\mathrm{Time\ (fs)}$'
#values['ylabel'] = r'$\mathrm{Entanglement\ Entropy}$'
values['ylabel'] = r'$\mathrm{Logrithmic\ Negativity}$'
#plt.plot(J_xy,spin_exp)
#plt.savefig("results/11.pdf")
plotfig(data, 'lneg', values, fs=8, lw=0.3)


end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')

