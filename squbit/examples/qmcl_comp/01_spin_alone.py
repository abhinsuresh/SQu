#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix


import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from numba_wrapper import calc_rho

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
Lx  = 4
Ly  = 1
N   = 1
S   = 2.5
J   = -0.01
Jz  = -0.01
Bz  = 0.1
Bx  = 0.1
#derived parameters
L   = Lx*Ly
start = time.time()

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
Se = spin.op
#print(np.sqrt(2)*spin.Sy.toarray())

HJ   = spin.heisenberg(J=J,Jz=Jz)
Hb  = csr_matrix(HJ.shape)
print(Hb.shape)
for i in range(Lx):
    Hb = Hb - Bz*((1)**i)*spin.op['sz'+str(i)]
H0 = HJ + Hb

#H = H0 - Bx*spin.op['sx0']

w,v = la.eigh(H0.toarray())
print(w[:5])
s = 0
print(v[:,s].conj() @ Se['sz0'] @ v[:,s])
print(v[:,s].conj() @ Se['sz1'] @ v[:,s])
print(v[:,s].conj() @ Se['sz2'] @ v[:,s])
print(v[:,s].conj() @ Se['sz3'] @ v[:,s])

