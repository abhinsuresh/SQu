#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py
from scipy.sparse import csr_matrix

#import pycuda.gpuarray as gpuarray
#import pycuda.autoinit
#from skcuda import linalg
#from skcuda import misc

import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from numba_wrapper import calc_rho
from evolve_step import evolve
from density_core import partial_trace

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
name = '03.6'
Lx  = 4
Ly  = 1
N   = 1
S   = 1
J   = 0.01
Jz  = 0.01
Bz  = -0.06
Bx  = 0.01
Jsd = -0.01
t   = 1.0
dt  = 0.1
tf  = 120
#derived parameters
L   = Lx*Ly
start = time.time()
#electronic system
sys = Basis(L=L, N=N, model = 1)
#electronic hamiltonian
ham = Hamiltonian(sys, periodic=0, Lx=Lx, Ly=Ly, dim=dim)
H_t = ham.hubbard_sparse(t=t, alpha = 0)
op_espin = ham.op_spin()

#spin system + eDOF
spin = Spin(S=S, L = L, kronD=sys.Ns, sc = 1)
#spin spin and electronic spin
Se = spin.op
se   = ham.kron_iden(op_espin, spin.Ns)

#heisenberg hamitonian
HJ   = spin.heisenberg(J=J,Jz=Jz)
#stagered magnetic field
Hb  = csr_matrix(HJ.shape)
for i in range(L):
    Hb = Hb + Bz*((-1)**i)*spin.op['sz'+str(i)]
#initial hamitlonian
H0 = HJ + Hb
#t=0 pulse
Hp = -Bx*spin.op['sx0']

H = H0 + Hp

#print(H0.shape)
#w,v = la.eigh(H.toarray())
#print(w[:5])

#hopping hamiltonian
H_t  = ham.kron_single(H_t,    spin.Ns)
#jsd hamitlonian
H_sd = spin.sd_ham(se, Jsd=Jsd)
#t=0 hamiltonian
Ht = H0 + H_t + H_sd + Hp
#Ht = H0 + Hp

#creating the initial wavefunction
HI   = spin.heisenberg(J=0,Jz=Jz)
Hi = partial_trace(HI.toarray(), sub_sys='A', m=sys.Ns, n=spin.Ns)
Hi = Hi/(sys.Ns)
w,v = la.eigh(Hi)
print(w[:5])
s = 0
Se_i = partial_trace(Se['sz0'].toarray(), sub_sys='A', m=sys.Ns, n=spin.Ns)
Se_i = Se_i/(sys.Ns)
print(v[:,s].conj() @ Se_i @ v[:,s])
#putting one up electron at site 1
psi = np.zeros(sys.Ns)
psi[sys.Ns-1] = 1
psi = np.kron(psi, v[:,s])
#psi = v[:,s]
#print('spin z at site 1: ',psi @ se['sz0'] @ psi)

#time evolution
_time = np.arange(0,tf,dt)
sx = np.zeros(len(_time))
sy = np.zeros(len(_time))
sz = np.zeros(len(_time))
Ut = evolve(Ht, [], psi, dt, method='CN', time_dep=0)
with open('results/spin_'+name+'.txt','w') as f:
    print('#output printed to file', file=f)

for t_ind, t in enumerate(_time):
    sx[t_ind] = np.real(np.conjugate(psi) @ Se['sx1'] @ psi)
    sy[t_ind] = np.real(np.conjugate(psi) @ Se['sy1'] @ psi)
    sz[t_ind] = np.real(np.conjugate(psi) @ Se['sz1'] @ psi)
    with open('results/spin_'+name+'.txt','a') as f:
        out_str = '{:3.2e} '.format(t)
        out_str += '{0: 1.3e} {1: 1.3e} {2: 1.3e} '.\
        format(sx[t_ind],sy[t_ind], sz[t_ind])
        out_str +='\n'
        f.write(out_str)
    
    psi = Ut @ psi
    
data = dict();values = dict()
L = len(sz); plot_y = sy.reshape(L,1)
#plot_y = np.concatenate((plot_y, sy.reshape(L,1)), axis=1)
#plot_y = np.concatenate((plot_y, sz.reshape(L,1)), axis=1)
data['x_plot'] = _time; data['y_plot'] = plot_y;
values['x_plot'] = 'x_plot'; values['y_plot'] = 'y_plot'
values['xlabel'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel'] = r'$\mathrm{\left< \hat{S}^z_2\right >}$'
Cl = ['b', 'C3', 'k']
#plt.plot(J_xy,spin_exp)
#plt.savefig("results/11.pdf")
plotfig(data, 'fmsz', values, Cl=Cl, fs=8, lw=0.3, ylim=((-0.02,0.02)))

end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')

