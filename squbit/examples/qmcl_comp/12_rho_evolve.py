#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py
from scipy.sparse import csr_matrix
from scipy.sparse import kron

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from numba_wrapper import calc_rho_vdv
from evolve_step import rk4_rho
from density_core import partial_trace
from density_core import logarithmic_negativity

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
nm = '12_rho_ev_200'
Lx  = 4
Ly  = 1
N   = 1
S   = 1
J   = 0.01
Jz  = 0.01
Bz  = -0.06
Bx  = 0.01
Jsd = 0.01
t   = 1.0
temp = 100
dt = 0.1
_time = np.arange(0,120,dt)
#derived parameters
L   = Lx*Ly
start = time.time()
#electronic system
sys = Basis(L=L, N=1, model = 1, use_iterator=1)
#electronic hamiltonian
ham = Hamiltonian(sys, periodic=0, Lx=Lx, Ly=Ly, dim=dim)
H_t = ham.hubbard_sparse(t=t, alpha = 0)
op_espin = ham.op_spin()

#spin system + eDOF
spin = Spin(S=S, L = L, kronD=sys.Ns, sc = 1)
#spin spin and electronic spin
Se = spin.op
se   = ham.kron_iden(op_espin, spin.Ns)

#heisenberg hamitonian
HJ   = spin.heisenberg(J=J,Jz=Jz)
#stagered magnetic field
Hb  = csr_matrix(HJ.shape)
for i in range(L):
    Hb = Hb + Bz*((-1)**i)*spin.op['sz'+str(i)]
#t=0 pulse
Hp = -Bx*spin.op['sx0']
#hopping hamiltonian
H_t  = ham.kron_single(H_t,    spin.Ns)
#jsd hamitlonian
H_sd = spin.sd_ham(se, Jsd=Jsd)
#t=0 hamiltonian
H0 = HJ + Hb + H_t + H_sd
Ht = H0 + Hp

linalg.init()
a_gpu = gpuarray.to_gpu(H0.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
w = w_gpu.get()
v = vr_gpu.get().transpose()
rho = calc_rho_vdv(w,v, temp = temp)

inp = np.array([Lx, Ly, N, S, J, Jz, Bz, Bx, Jsd, t])
sx = np.zeros(len(_time))
sy = np.zeros(len(_time))
sz = np.zeros(len(_time))
sent = np.zeros(len(_time))


for t_ind, ti in enumerate(_time):
    sx[t_ind] = np.real(np.trace(rho @ Se['sx1']))
    sy[t_ind] = np.real(np.trace(rho @ Se['sy1']))
    sz[t_ind] = np.real(np.trace(rho @ Se['sz1']))
    rho_s = partial_trace(rho, sub_sys='A', m=sys.Ns, n=spin.Ns)
    sent[t_ind] = logarithmic_negativity(rho_s, m=3**(L//2))
    
    rho = rk4_rho(Ht, [], rho, dt)
    


with h5py.File('results/'+nm+'.hdf5', 'w') as f:
    dset = f.create_dataset("sx", 
             data = sx, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("sy", 
             data = sy, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("sz", 
             data = sz, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("lneg", 
             data = sent, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("inp", 
             data = inp, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("time", 
             data = _time, 
             dtype=np.float64, compression="gzip")
f.close()


end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
