#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py
from scipy.sparse import csr_matrix

#import pycuda.gpuarray as gpuarray
#import pycuda.autoinit
#from skcuda import linalg
#from skcuda import misc

import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from numba_wrapper import calc_rho
from evolve_step import evolve
from density_core import partial_trace

sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
Lx  = 4
Ly  = 1
N   = 1
S   = 1
J   = 0.01
Jz  = 0.01
Bz  = -0.06
Bx  = 0.01
Jsd = 0.00
t   = 1.0
#derived parameters
L   = Lx*Ly
start = time.time()
sys = Basis(L=L, N=N, model = 1)
#print("System size:", sys.Ns)
#print("System:", sys)
ham = Hamiltonian(sys, periodic=0, Lx=Lx, Ly=Ly, dim=dim)
op_espin = ham.op_spin()
H_t = ham.hubbard_sparse(t=t, alpha = 0)

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=sys.Ns, sc = 1)
Se = spin.op
#heisenberg
HJ   = spin.heisenberg(J=J,Jz=Jz)
#stagered magnetic field
Hb  = csr_matrix(HJ.shape)
for i in range(L):
    Hb = Hb + Bz*((-1)**i)*spin.op['sz'+str(i)]
#initial hamitlonian
H0 = HJ + Hb
#t=0 pulse
Hp = -Bx*spin.op['sx0']
#hopping hamiltonian
H_t  = ham.kron_single(H_t,    spin.Ns)
#jsd hamitlonian
se   = ham.kron_iden(op_espin, spin.Ns)
H_sd = spin.sd_ham(se, Jsd=Jsd)
#t=0 hamiltonian
Ht = H0 + H_t + H_sd + Hp



#w1,v1 = la.eigh(Ht.toarray())
#print(w1[:5])
HI   = spin.heisenberg(J=0,Jz=Jz)
"""
print('HI shape', HI.shape)
w,v = la.eigh(HI.toarray())
print(w[:5])
s = 0
print(v[:,s].conj() @ Se['sz0'] @ v[:,s])
print(v[:,s].conj() @ Se['sz1'] @ v[:,s])
print(v[:,s].conj() @ Se['sz2'] @ v[:,s])
print(v[:,s].conj() @ Se['sz3'] @ v[:,s])
"""

Hi = partial_trace(HI.toarray(), sub_sys='A', m=sys.Ns, n=spin.Ns)
Hi = Hi/8
#print('Hi shape', Hi.shape)
w,v = la.eigh(Hi)
print('eigen value:', w[:5])
s = 0
Se_i = partial_trace(Se['sz0'].toarray(), sub_sys='A', m=sys.Ns, n=spin.Ns)
Se_i = Se_i/8
print(v[:,s].conj() @ Se_i @ v[:,s])
#print(v[:,s].conj() @ Se['sz1'] @ v[:,s])
#print(v[:,s].conj() @ Se['sz2'] @ v[:,s])
#print(v[:,s].conj() @ Se['sz3'] @ v[:,s])

psi = np.zeros(sys.Ns)
psi[7] = 1
print('spin z at site 1: ',psi @ op_espin['sz0'] @ psi)
print('spin z at site 4: ',psi @ op_espin['sz3'] @ psi)

psi = np.kron(psi, v[:,s])

print('spin z at site 1 for full: ',psi @ se['sz0'] @ psi)
print('spin z at site 4 for full: ',psi @ se['sz3'] @ psi)

print('\nfinal wave function properties')
print(psi.conj() @ Se['sz0'] @ psi)
print(psi.conj() @ Se['sz1'] @ psi)
print(psi.conj() @ Se['sz2'] @ psi)
print(psi.conj() @ Se['sz3'] @ psi)
print('spin z at site 1 for full: ',psi @ se['sz0'] @ psi)


end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')

