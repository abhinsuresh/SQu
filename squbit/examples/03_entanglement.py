#Only one measure of entanglement implemented in code, half system
#entropy which is availble in model 2 of system

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import matplotlib.pyplot as plt

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian

#input parameters
L   = 4
L1  = L//2
J   = 0.1
Jz  = 0.1

start = time.time()
#system of L sites at half filling
sys = Basis(L=L, N=L, model =2)
print('System basis generated')
#hamiltonian class periodic in model 2
ham = Hamiltonian(sys, periodic=1)
#getting spin operators as a dict for measurements
op = ham.op_spin()
print('Observable operators created')

#defining heisenberg hamiltonian
Ht = ham.hubbard_sparse(t=-1)
Hj, rep = ham.heisenberg(J=J, Jz=Jz)
H = Ht + Hj
print('Hamiltonian created')
#print(rep)
print('Finding eigen values')
w, v = la.eigh(H.toarray())

psi = np.zeros(len(w))
psi[20] = 1
#psi = v[:,0]

#w, v = sla.eigs(H, k = 3, which = 'SR')
#print(w.shape, v.shape)
#print('Eig vals: ', np.real(w[:7]),'\n')
print(sys)
print('Half entropy', '%0.3f'%sys.ent_entropy(psi, L1), '\n')


#ent = np.zeros(L+1)
#for l1 in range(L+1):
#    ent[l1] = sys.ent_entropy(v[:,0], l1)

#plt.plot(np.arange(L+1), ent)
#plt.savefig('1.png')
    

#printing sz of first four sites
for i in range(3):
    print('sz: ','%0.2f'%(psi.conj() @ op['sz0'] @ psi).real,
          '%0.2f'%(psi.conj() @ op['sz1'] @ psi).real,
          '%0.2f'%(psi.conj() @ op['sz2'] @ psi).real,
          '%0.2f'%(psi.conj() @ op['sz3'] @ psi).real)

end = time.time()

print('time taken: ', '%0.3f'%(end - start),' s')
