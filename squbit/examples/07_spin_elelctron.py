#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from numba_wrapper import calc_rho

sys.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
Lx  = 4
Ly  = 1
N   = 1
S   = 1
t   = -1.0
J   = 0.01
Jz  = 0.01
err = 0.06
Hp  = 0.01
#derived parameters
L   = Lx*Ly
start = time.time()
sys = Basis(L=L, N=N, model = 1)
print("System size:", sys.Ns)
ham = Hamiltonian(sys, periodic=0, Lx=Lx, Ly=Ly, dim=dim)
op_espin = ham.op_spin()
#defect = -err * op['sz0']
H_t = ham.hubbard_sparse(t=t, alpha = 0)

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=sys.Ns)
Se = spin.op
print(spin.Sx.toarray() )
print(spin.Sx.toarray() @ spin.Sx.toarray())
#print(Se.keys())
#created spin-spin ham in full space
HJ   = spin.heisenberg(J=J,Jz=Jz)
#created e-hop ham in full space
H_t  = ham.kron_single(H_t,    spin.Ns)
se   = ham.kron_iden(op_espin, spin.Ns)
#created e-spin ham
H_sd = spin.sd_ham(se, J=0.01)

print(H_t.shape)
print(H_sd.shape)
print(sys.Ns, spin.Ns)

H0 = HJ + H_t

w,v = la.eigh(H0.toarray())
print(w[:5])
s = 1
print(v[:,s].conj() @ Se['sz0'] @ v[:,s])
print(v[:,s].conj() @ Se['sz1'] @ v[:,s])
print(v[:,s].conj() @ Se['sz2'] @ v[:,s])
print(v[:,s].conj() @ Se['sz3'] @ v[:,s])

#if not np.allclose(H.toarray(), np.asmatrix(H.toarray()).H):
#    raise ValueError('bleh not hermitian')

#linalg.init()
#a_gpu = gpuarray.to_gpu(H.toarray()) 
#vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
#w = w_gpu.get()
#v = vr_gpu.get().transpose()
#w,v = la.eigh(H.toarray())

#num_op = ham.onsite_mu()

#test = v[:,0]
#print(test.conjugate() @ num_op @ test)

#print(w[:20])
#print(w[0]*4)
#print(v[:,0].conj()@H.toarray()@v[:,0])
"""
ss = 0
for site in range(L):
    print('sz'+str(site)+': ','{0: 1.3e}'.\
    format(np.real(v[:,ss].conj()@op['sz'+str(site)]@v[:,ss])))

end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
"""




"""
spin0 = np.zeros(L)
spin1 = np.zeros(L)
for site in range(L):
    spin0[site] = np.real(v[:,0].conj()@op['sz'+str(site)]@v[:,0])
    spin1[site] = np.real(v[:,1].conj()@op['sz'+str(site)]@v[:,1])

with h5py.File('results/'+name+'.hdf5', 'w') as f:
    dset = f.create_dataset("spin0", 
             data = spin0, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("spin1", 
             data = spin1, 
             dtype=np.float64, compression="gzip")
f.close()
"""
