import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py

#import pycuda.gpuarray as gpuarray
#import pycuda.autoinit
#from skcuda import linalg
#from skcuda import misc

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from numba_wrapper import calc_rho

sys.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim ='1d'
#parameters
name = '34.6'
Lx  = 4
Ly  = 2
t   = 0.0
U   = 0.0
J   = 0.1
_Jz  = J*np.linspace(1,500,20)
err = 0.0
periodic = 1
inp = [Lx, Ly, t, U, J, _Jz[0],_Jz[-1], err]
inp = np.asarray(inp)
#derived parameters
L   = Lx*Ly
if Ly>=2: dim = '2d'

#System creation
start = time.time()
sys = Basis(L=L, N=L, model = 1)
print(sys.Ns)

#Hamiltonian
ham = Hamiltonian(sys, periodic=periodic, Lx=Lx, Ly=Ly, dim=dim, dtype=np.complex128)
op = ham.op_spin()
H_hop = ham.hubbard_sparse(t=t, alpha = 0)
H_U = ham.hubbard_U(U=U)
defect = -err * op['sz0']
H_fixed = H_hop + H_U + defect

#Measurements
spinz = np.zeros((L,len(_Jz)))

#linalg.init()
for Jz_ind, Jz in enumerate(_Jz):
    #Measurements
    
    H_j, repj = ham.heisenberg(J=J, Jz=Jz)
    H = H_fixed + H_j

    w, v = sla.eigsh(H, k=2, which='SA')
    if (np.abs(w[0]-w[1]) < 1e-10):
        raise ValueError("degeneracy encountered on step: ",Jz_ind)

    #a_gpu = gpuarray.to_gpu(H.toarray()) 
    #vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
    #w = w_gpu.get()
    #v = vr_gpu.get().transpose()
    for site in range(L):
        spinz[site,Jz_ind] = np.real(v[:,0].conj()@\
                             op['sz'+str(site)]@v[:,0])

with h5py.File('results/'+name+'.hdf5', 'w') as f:
    dset = f.create_dataset("spinz", 
             data = spinz, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("Jz", 
             data = _Jz, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("input", 
             data = inp, 
             dtype=np.float64, compression="gzip")
f.close()
end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')

