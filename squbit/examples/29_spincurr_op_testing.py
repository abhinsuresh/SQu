import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from index_core import Index

HBAR = 0.658211951
float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})


start = time.time()
Lx = 2; Ly = 2
L = Lx*Ly
sys = Basis(L, N=2, model=1)
#ham = Hamiltonian(sys, periodic=1)
ham = Hamiltonian(sys, periodic=0, Lx=Lx, Ly=Ly, dim='2d')
#index = Index(sys, periodic=1, model =2)

print(sys.Ns)

H = ham.hubbard_sparse(t=-1)
Hu = ham.hubbard_U(U=5)
Hmu = ham.onsite_mu(mu=1)
ham.H_tso()
#print(Hmu.toarray())
#w,v = la.eigh(H.toarray())
#print(H.toarray().real)

#spcurr = ham.spin_curr(0,1)
#print(spcurr['sz'].toarray().real)
#print(w[:5])
#curr = 2/HBAR*(v[:,0].conj() @ spcurr['ch'] @ v[:,0]).imag
#e0 = v[:,0].conj() @ H @ v[:,0]
#print(curr)


end = time.time()

print('time taken: ', end - start)
