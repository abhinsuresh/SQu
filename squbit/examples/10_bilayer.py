import numpy as np
import time

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from index_core import Index

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
dim = '1d'

start = time.time()
Lx = 4
Ly = 2
Lz = 1
L = Lx * Ly * Lz
N = 1
print('N=',N)
model = 1
periodic = 1
use_iterator = True

#tests needed
# light hitting along x and y and later also at an angle

sys = Basis(L=L, N=N, model=model, use_iterator=use_iterator, double_occu=True)

ham = Hamiltonian(sys, periodic=1, Lx=Lx, Ly=Ly, Lz=Lz)
#hopping in all 3D
H_hop = ham.hubbard_sparse(t=-1, alpha = 0)
#spin spin interaction only in 1st layer
H_j, op, repj = ham.heisenberg(J=0.1, Jz=0.1, return_op=True)
#hubbard u only in one layers as of now
H_U = ham.hubbard_U(U=8.0, layers=1)
#spin orbit coupling only in second layer(layer 1)
H_so = ham.spin_orbit(alpha = 0.1, layer = 0, new_ind=True)


print('time taken: ', time.time() - start)
