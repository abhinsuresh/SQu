#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from evolve_step import psi_qj
from numba_wrapper import calc_rho

sys.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
L   = 1
N   = 1
S   = 1/2
Omega = 1.0
Delta = 0.0
Gamma = Omega/6
Ntraj = 1
method = 'rk4' #'qt'
#derived parameters

#spin system creation with eDOF
spin = Spin(S=S, L = L, sc=2, kronD=N)

Ham = (-(Omega/2)*spin.Sx - Delta*(spin.Sp @ spin.Sm)).toarray()
w,v = la.eigh(Ham)
dt = 0.1
_time = np.arange(0,40,dt)
pe = np.zeros(len(_time))
#print(w)

if method == 'qt':
    psi_0 = np.zeros((2,1), dtype=np.complex128)
    psi_0[1,0] = 1

    Heff = Ham - 1j*0.5*Gamma*spin.Sp @ spin.Sm

    for traj in np.arange(Ntraj):   
        psi = psi_0
        for t_ind, t in enumerate(_time):
            #drawing a random number
            r1 = np.random.rand(1)
            psi_new, dp = psi_qj(Heff,psi,dt)
            if r1 > dp:
                psi_p = psi_new/(np.sqrt(1-dp)) 
            else:
                psi_p = (spin.Sm @ psi )/(np.sqrt(dp/dt))
            psi = psi_p
            rho = psi @ np.conjugate(psi.transpose())
            #adding all values to single time index
            pe[t_ind] = pe[t_ind] + np.real(rho[0,0])

    pe = pe/Ntraj        
        
    with h5py.File('results/pe_'+str(Ntraj)+'.hdf5', 'w') as f:
        dset = f.create_dataset("pe", 
                 data = pe, 
                 dtype=np.float64, compression="gzip")
    f.close()

elif method == 'rk4':
    rho = np.zeros((2,2),dtype=np.complex128)
    rho[1,1] = 1

    for t_ind, t in enumerate(_time):
        pe[t_ind] = np.real(rho[0,0])
        #pe[t_ind] = np.real(np.trace(Ham @ rho))
        rho = rk4_rho(Ham, [], rho, dt, spin.Sp, spin.Sm, Gamma, 1)
        #print(np.trace(rho))

        
    with h5py.File('results/pe_exact.hdf5', 'w') as f:
        dset = f.create_dataset("pe", 
                 data = pe, 
                 dtype=np.float64, compression="gzip")
    f.close()

data = dict();values = dict()
data['x_plot'] = _time; data['y_plot'] = pe
values['xlabel'] = r'$\mathrm{t}$'
values['ylabel'] = r'$\mathrm{P_{e}}$'
#plt.plot(J_xy,spin_exp)
#plt.savefig("results/11.pdf")
plotfig(data, fig_name=method, values=values, fs=8, lw=0.2)


