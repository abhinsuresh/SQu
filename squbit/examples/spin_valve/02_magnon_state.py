#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix

import sys
sys.path.append('../../')
from hamiltonian_core import Spin
from density_core import entanglement_entropy
sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
L  = 4
S   = 1/2
J   = -0.1
Jz  = -0.1005
start = time.time()

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
Se = spin.op
H0   = spin.heisenberg(J=J,Jz=Jz)
w,v = la.eigh(H0.toarray())

#print(constructing states)
up = np.array([1,0])
dn = np.array([0,1])
v0 = np.kron(np.kron(np.kron(dn,dn),dn),dn)
v1 = np.kron(np.kron(np.kron(up,dn),dn),dn)
v2 = np.kron(np.kron(np.kron(dn,up),dn),dn)
v3 = np.kron(np.kron(np.kron(dn,dn),up),dn)
v4 = np.kron(np.kron(np.kron(dn,dn),dn),up)
v5 = np.kron(np.kron(np.kron(up,up),dn),dn)
v6 = np.kron(np.kron(np.kron(up,up),up),dn)
v7 = np.kron(np.kron(np.kron(up,dn),up),dn)

gs = (v0 + v1 + v2 + v3 + v4 + v5 + v6 + v7)/np.sqrt(8)

rho = gs.reshape(len(gs),1) @ gs.conj().reshape(1, len(gs))
print(rho.shape, np.trace(rho))

print('ent:\t', entanglement_entropy(rho))

print('check gs norm:\t', gs.conj() @ gs)

s0 = Se['sp0'] @ v0
#s0 = v0

print(s0.conj() @ Se['sz0'] @ s0)
print(s0.conj() @ Se['sz1'] @ s0)
print(s0.conj() @ Se['sz2'] @ s0)
print(s0.conj() @ Se['sz3'] @ s0)

#1 magnon state
w1 = np.kron(np.kron(np.kron(dn,up),up),up)
w2 = np.kron(np.kron(np.kron(up,dn),up),up)
w3 = np.kron(np.kron(np.kron(up,up),dn),up)
w4 = np.kron(np.kron(np.kron(up,up),up),dn)
def magnon(q):
    q1 = (np.exp(1j*q*0)*w1 + np.exp(1j*q*1)*w2 +
          np.exp(1j*q*2)*w3 + np.exp(1j*q*3)*w4)/np.sqrt(4)
    return q1

q1 = magnon(0.2*np.pi)
s0 = q1

print(s0.conj() @ Se['sz0'] @ s0)
print(np.abs(psi.conj() @ q1))
print(psi.conj() @ q1)


#print(s0.conj() @ Se['sz0'] @ s0)
#print(s0.conj() @ Se['sz1'] @ s0)
#print(s0.conj() @ Se['sz2'] @ s0)
#print(s0.conj() @ Se['sz3'] @ s0)


