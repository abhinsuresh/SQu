import numpy as np
import sys
import h5py

from SQu.plot_manager.plot2d import plotfig

f = h5py.File(sys.argv[1], 'r')['curr'][:]
cs = h5py.File(sys.argv[1], 'r')['cspin'][:]

ts = 3000
tf = 3200
data = f[ts:tf, :]
#data = cs[ts:tf, :]

plot_x = data[:,0:1]
plot_y = data[:,2:]


data = dict();values = dict()
data['x_plot'] = plot_x; data['y_plot'] = plot_y
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{I_R}$'
sf = np.array([[0,3]])
Cl = ['C0', 'C2', 'C3']
Gs = dict();
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.3; Gs['r']=0.7
Gs['w']=0.4; Gs['h']=0.4
plotfig(data, 'ch', values, sf=sf, Cl=Cl,
        Gs=Gs, fs=7, lw=0.4)

#Ps = [1, 1]
#Ls = ['-', '']
#Mk = ['', r'$\mathrm{O}$']
#Lg = [r'$\mathrm{Gibbs}$', r'$\mathrm{Evolution}$']
