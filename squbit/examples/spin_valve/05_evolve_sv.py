#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy import sparse
from scipy.sparse import eye, kron
import h5py

import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian, Spin
from numba_wrapper import calc_rho, create_tb_spinm 
from evolve_step import rk4_rho
from density_core import partial_trace, logarithmic_negativity
from density_core import entanglement_entropy
from density_core import partial_trace_A

sys.path.append('../../../llg/')
from constants import SIG_X, SIG_Y, SIG_Z

sys.path.append('../../../plot_manager/')
from plot2d import plotfig
data = dict();values = dict()
values['xlabel1'] = r'$\mathrm{Sites}$'
values['ylabel1'] = r'$\mathrm{\Psi}$'
sf = np.array([[0,1]])
Cl = ['C0', 'C2', 'C3']
Gs = dict();
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.3; Gs['r']=0.7
Gs['w']=0.4; Gs['h']=0.4

#float_formatter = "{:.1f}".format
#np.set_printoptions(formatter={'float_kind':float_formatter})

#parameters
Lx  = 110
t   = -1
kx = 0.1
dkx = 0.2
x0 = 40
J   = -0.1
Jz  = J*1.005
Jsd = -0.5
tf = 70
dt = 0.1  #\hbar/gamma
p0 = 60
a0 = 68



start = time.time()
sys_e = Basis(L=Lx, Nup=1, Ndn=0,  model = 1, use_iterator=True)
ham = Hamiltonian(sys_e, periodic=0, Lx=Lx, Ly=1)
H_hop = ham.hubbard_sparse(t=t, alpha = 0)
Ht = kron(H_hop,eye(2))
Ie = eye(Ht.shape[0])
#constructing spinless gaussain psi
cl = np.arange(Lx)
data['x_plot'] = cl
psi_orb = np.exp(1j*kx*cl - (dkx**2 * (cl - x0)**2)/4)
psi_orb = psi_orb

#unpolarized rho_e
rho_e = kron(np.outer(psi_orb,psi_orb.conj()),eye(2)).tocsr()
rho_e = rho_e/np.trace(rho_e.toarray())

#spin hamiltonian
spin = Spin(S=1/2, L = 4)
Se = spin.op
Hs   = spin.heisenberg(J=J,Jz=Jz)
Is = eye(Hs.shape[0]).tocsr()

w,v = la.eigh(Hs.toarray())
#spin p and a density matrix
rho_p = np.outer(v[:,0], v[:,0].conj())
rho_a = np.outer(v[:,1], v[:,1].conj())

#total density matrix
rho = kron(kron(rho_e,rho_p),rho_a).tocsr()
print('rho type:\t', type(rho))
#total hamiltonian
#-------------------------------------------------------------------
#total orbital hopping hamiltonian
H_E = kron(Ht,kron(Is,Is)).tocsr()
print('e ham shape:\t', H_E.shape)
print('e_ham nnz:\t', H_E.count_nonzero())

#constructing electron spin matrixes as a dict
#verified the commutation realtions
se = create_tb_spinm(Lx, p0, a0, SIG_X, SIG_Y, SIG_Z, ex=Hs.shape[0]**2)
print('e-spin matrix shape:\t', se['sx0'].shape)


#Heisenberg hamiltonian
HJ1 = kron(Ie,kron(Hs,Is))
HJ2 = kron(Ie,kron(Is,Hs))
print('HJ1/2 shape:\t', HJ1.shape, HJ2.shape)

#making jsd hamiltonian
#expading matrixes to larger space
HJSD = sparse.csr_matrix(H_E.shape, dtype=np.complex)
#print(HJSD.count_nonzero())
for i in range(4):
    HJSD = HJSD + Jsd * (se['sx0'] @ kron(kron(Ie,Se['sx0']),Is) + 
                         se['sy0'] @ kron(kron(Ie,Se['sy0']),Is) + 
                         se['sz0'] @ kron(kron(Ie,Se['sz0']),Is))

for i in range(4,8):
    HJSD = HJSD + Jsd * (se['sx0'] @ kron(kron(Ie,Is),Se['sx0']) + 
                         se['sy0'] @ kron(kron(Ie,Is),Se['sy0']) + 
                         se['sz0'] @ kron(kron(Ie,Is),Se['sz0']))

print('HJSD shape:\t', HJSD.shape)

H_tot = H_E + HJ1 + HJ2 + HJSD


#evolve rho_e
_time = np.arange(0, tf+dt, dt)
sz1 = np.zeros(len(_time)); sz2 = np.zeros(len(_time))
sz3 = np.zeros(len(_time)); sz4 = np.zeros(len(_time))
sz5 = np.zeros(len(_time)); sz6 = np.zeros(len(_time))
sz7 = np.zeros(len(_time)); sz8 = np.zeros(len(_time))
ent = np.zeros(len(_time))
lneg = np.zeros(len(_time))


#making non sparse
rho = rho.toarray()
H_tot = H_tot.toarray()


for t_ind, ti in enumerate(_time):
    rho_s = partial_trace_A(rho, m=sys_e.Ns*2,n=spin.Ns**2)
    sz1[t_ind]  = np.trace(rho_s @ kron(Se['sz0'], Is))
    sz2[t_ind]  = np.trace(rho_s @ kron(Se['sz1'], Is))
    sz3[t_ind]  = np.trace(rho_s @ kron(Se['sz2'], Is))
    sz4[t_ind]  = np.trace(rho_s @ kron(Se['sz3'], Is))
    sz5[t_ind]  = np.trace(rho_s @ kron(Is, Se['sz0']))
    sz6[t_ind]  = np.trace(rho_s @ kron(Is, Se['sz1']))
    sz7[t_ind]  = np.trace(rho_s @ kron(Is, Se['sz2']))
    sz8[t_ind]  = np.trace(rho_s @ kron(Is, Se['sz3']))
    ent[t_ind]  = entanglement_entropy(rho_s)
    lneg[t_ind] = logarithmic_negativity(rho_s, 'B', 16)
    print('curr time:\t', ti)
    #print('sparsity', rho.count_nonzero(), H_tot.count_nonzero())
    st = time.time()
    rho = rk4_rho(H_tot, 0, rho, dt)
    print('time for rk4:\t', time.time()-st)


with h5py.File('results/05.hdf5', 'w') as f:
    f.create_dataset("sz1",
         data = sz1,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz2",
         data = sz2,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz3",
         data = sz3,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz4",
         data = sz4,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz5",
         data = sz5,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz6",
         data = sz6,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz7",
         data = sz7,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz8",
         data = sz8,
         dtype=np.float64, compression="gzip")
    f.create_dataset("ent",
         data = ent,
         dtype=np.float64, compression="gzip")
    f.create_dataset("lneg",
         data = lneg,
         dtype=np.float64, compression="gzip")
f.close()

end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
