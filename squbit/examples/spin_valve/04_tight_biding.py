#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy import sparse
from scipy.sparse import eye, kron

import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian, Spin
from numba_wrapper import calc_rho, create_tb_spinm, calc_rho_vdv 
from evolve_step import rk4_rho
from density_core import partial_trace, logarithmic_negativity
from density_core import entanglement_entropy
from density_core import partial_trace_A

sys.path.append('../../../llg/')
from constants import SIG_X, SIG_Y, SIG_Z

sys.path.append('../../../plot_manager/')
from plot2d import plotfig
data = dict();values = dict()
values['xlabel1'] = r'$\mathrm{Sites}$'
values['ylabel1'] = r'$\mathrm{\Psi}$'
sf = np.array([[0,1]])
Cl = ['C0', 'C2', 'C3']
Gs = dict();
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.3; Gs['r']=0.7
Gs['w']=0.4; Gs['h']=0.4

#float_formatter = "{:.1f}".format
#np.set_printoptions(formatter={'float_kind':float_formatter})

#parameters
verbose = 0
Lx  = 120
t   = -1
kx = 0.1
dkx = 0.2
x0 = 40
J   = -0.1
Jz  = J*1.005
Jsd = -0.5
tf = 100
dt = 0.1  #\hbar/gamma
p0 = 60
a0 = 68



start = time.time()
sys_e = Basis(L=Lx, Nup=1, Ndn=0,  model = 1, use_iterator=True)
ham = Hamiltonian(sys_e, periodic=0, Lx=Lx, Ly=1)
H_hop = ham.hubbard_sparse(t=t, alpha = 0)
Ht = kron(H_hop,eye(2))
Ie = eye(Ht.shape[0])
#constructing spinless gaussain psi
cl = np.arange(Lx)
data['x_plot'] = cl
psi_orb = np.exp(1j*kx*cl - (dkx**2 * (cl - x0)**2)/4)
psi_orb = psi_orb

#unpolarized rho_e
rho_e = kron(np.outer(psi_orb,psi_orb.conj()),eye(2)).tocsr()
rho_e = rho_e/np.trace(rho_e.toarray())

if verbose: print('total energy:\t',np.trace((rho_e @ Ht).toarray()))

#spin hamiltonian
spin = Spin(S=1/2, L = 4, sc=2)
Se = spin.op
Hs   = spin.heisenberg(J=J,Jz=Jz)
Hs = Hs - 0.1*Se['sz0']
Is = eye(Hs.shape[0]).tocsr()
#w,v = la.eigh((kron(Is,Hs)+kron(Hs,Is)).toarray())
w,v = la.eigh(Hs.toarray())
if verbose: print(w[:5])
rhoT = calc_rho_vdv(w,v,300)
print('sz0:', np.trace(rhoT @ Se['sz0']))
print('sz1:', np.trace(rhoT @ Se['sz1']))


w,v = la.eigh(Hs.toarray())
#spin p and a density matrix
rho_p = np.outer(v[:,0], v[:,0].conj())
rho_a = np.outer(v[:,1], v[:,1].conj())

#total density matrix
rho = kron(kron(rho_e,rho_p),rho_a).tocsr()
#print('rho type:\t', type(rho))
#total hamiltonian
#-------------------------------------------------------------------
#total orbital hopping hamiltonian
H_E = kron(Ht,kron(Is,Is)).tocsr()
#if verbose: print('e ham shape:\t', H_E.shape)
#if verbose: print('e_ham nnz:\t', H_E.count_nonzero())

#constructing electron spin matrixes as a dict
#verified the commutation realtions
se = create_tb_spinm(Lx, p0, a0, SIG_X, SIG_Y, SIG_Z, ex=Hs.shape[0]**2)
if verbose: print('e-spin matrix shape:\t', se['sx0'].shape)


#Heisenberg hamiltonian
HJ1 = kron(Ie,kron(Hs,Is))
HJ2 = kron(Ie,kron(Is,Hs))
if verbose: print('HJ1/2 shape:\t', HJ1.shape, HJ2.shape)

#making jsd hamiltonian
#expading matrixes to larger space
HJSD = sparse.csr_matrix(H_E.shape, dtype=np.complex)
#if verbose: print(HJSD.count_nonzero())
for i in range(4):
    HJSD = HJSD + Jsd * (se['sx0'] @ kron(kron(Ie,Se['sx0']),Is) + 
                         se['sy0'] @ kron(kron(Ie,Se['sy0']),Is) + 
                         se['sz0'] @ kron(kron(Ie,Se['sz0']),Is))

for i in range(4,8):
    HJSD = HJSD + Jsd * (se['sx0'] @ kron(kron(Ie,Is),Se['sx0']) + 
                         se['sy0'] @ kron(kron(Ie,Is),Se['sy0']) + 
                         se['sz0'] @ kron(kron(Ie,Is),Se['sz0']))

if 1: print('HJSD shape:\t', HJSD.shape)

H_tot = H_E + HJ1 + HJ2 + HJSD

#converting to nonsparse
#rho = rho.toarray()
#H_tot = H_tot.toarray()

st = time.time()
#measurements
rho_s = partial_trace_A(rho.toarray(), m=sys_e.Ns*2,n=spin.Ns**2)
if verbose: print('spin-zp exp:\t',np.trace(rho_s @ kron(Se['sz0'], Is)))
if verbose: print('spin-za exp:\t',np.trace(rho_s @ kron(Is, Se['sz0'])))
if verbose: print('entropy:\t', entanglement_entropy(rho_s))
if verbose: print('neagtivity:\t', logarithmic_negativity(rho_s, 'B', 16))

rho = rk4_rho(H_tot, 0, rho, dt)
if verbose: print('rk4 step time:\t', time.time()-st)

"""
#evolve rho_e
_time = np.arange(0, tf+dt, dt)
n = np.zeros((Lx,len(_time)))
for t_ind, ti in enumerate(_time):
    n =  np.array([rho_e[2*i, 2*i] + rho_e[2*i+1, 2*i+1] \
            for i in range(Lx)])
    
    plot_y = n.reshape(len(n),1)
    data['y_plot'] = plot_y
    #if t_ind%10 == 0:
    #    plotfig(data, 's01/fig_'+str(t_ind//10), values, ylim=(0,0.013), sf=sf, Gs=Gs, 
    #            Cl=Cl, fs=8, lw=0.2, ft='.png')

    #rho = rk4_rho(Ht, 0, rho, dt)

"""
end = time.time()
if verbose: print('time taken: ', "%1.2f"%(end - start),' s')









"""
i = '7'
if verbose: print('comm:\t', (se['sx'+i]@se['sy'+i] - se['sy'+i]@se['sx'+i]) - 1j*se['sz'+i])
if verbose: print('comm:\t', (se['sz'+i]@se['sx'+i] - se['sx'+i]@se['sz'+i]) - 1j*se['sy'+i])
if verbose: print('comm:\t', (se['sy'+i]@se['sz'+i] - se['sz'+i]@se['sy'+i]) - 1j*se['sx'+i])
"""
