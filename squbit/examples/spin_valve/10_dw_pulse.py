#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy import sparse
from scipy.sparse import eye, kron
import h5py

import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian, Spin
from numba_wrapper import calc_rho, create_tb_spinm 
from evolve_step import rk4_rho
from density_core import partial_trace, logarithmic_negativity
from density_core import entanglement_entropy
from density_core import partial_trace_A

sys.path.append('../../../llg/')
from constants import SIG_X, SIG_Y, SIG_Z
from spins import Spins
from kwant_systems import make_ribbon

sys.path.append('../../../plot_manager/')
from plot2d import plotfig
data = dict();values = dict()
values['xlabel1'] = r'$\mathrm{Sites}$'
values['ylabel1'] = r'$\mathrm{\Psi}$'
sf = np.array([[0,1]])
Cl = ['C0', 'C2', 'C3']
Gs = dict();
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.3; Gs['r']=0.7
Gs['w']=0.4; Gs['h']=0.4

#float_formatter = "{:.1f}".format
#np.set_printoptions(formatter={'float_kind':float_formatter})

#parameters
Lx  = 4
t   = -1
kx = 0.1
dkx = 0.2
x0 = 40
J   = -0.1
Jz  = J*1.005
Jsd = -0.5
tf = 70
dt = 0.1  #\hbar/gamma
p0 = 60
a0 = 68



start = time.time()
sys_e = Basis(L=Lx, Nup=1, Ndn=0,  model = 1, use_iterator=True)
ham = Hamiltonian(sys_e, periodic=0, Lx=Lx, Ly=1)
#spinless hoppin ham
H_hop = ham.hubbard_sparse(t=t, alpha = 0)
#spinfull hopping ham
Ht = kron(H_hop,eye(2))

#constructing spinless gaussain psi
cl = np.arange(Lx)
data['x_plot'] = cl
psi_orb = np.exp(1j*kx*cl - (dkx**2 * (cl - x0)**2)/4)
psi_orb = psi_orb

#unpolarized rho_e
rho_e = kron(np.outer(psi_orb,psi_orb.conj()),eye(2)).tocsr()
rho_e = rho_e/np.trace(rho_e.toarray())

#spin system
ribbon = make_ribbon(Lx,1)
cspins = Spins(ribbon, jexc=J, g_lambda=0, dt = dt,
              jsd_to_llg=-Jsd)

#setting up spin system
for i in np.arange(Lx):
    cspins.s[i] = [0,0,(-1)**i]
#print(cspins.s)
print(Ht.toarray())


"""
#creating jsd hamiltonian as a function which accepts cspins.s
H_jsd  = csr_matrix(H_t.shape)
H_jsd = jsd_fun(...,cspins.s)
#....
#......


#total hamiltonian
# H_tot = Ht + H_jsd


_time = np.arange(0, tf+dt, dt)


#making non sparse
#rho = rho.toarray()
#H_tot = H_tot.toarray()


#evolve rho_e
for t_ind, ti in enumerate(_time):

    #save measurables 
    #electron spin matrix rho_s
    rho_s = partial_trace_A(rho, m=sys_e.Ns,n=2)
    

    #set up cspins and evlove
    H_jsd = jsd_fun(...,cspins.s)
    H_tot = Ht + H_jsd
    rho = rk4_rho(H_tot, 0, rho, dt)

    #measure electron density
    #to get espins

    #cspins.llg(espins, time)


with h5py.File('results/05.hdf5', 'w') as f:
    f.create_dataset("sz1",
         data = sz1,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz2",
         data = sz2,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz3",
         data = sz3,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz4",
         data = sz4,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz5",
         data = sz5,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz6",
         data = sz6,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz7",
         data = sz7,
         dtype=np.float64, compression="gzip")
    f.create_dataset("sz8",
         data = sz8,
         dtype=np.float64, compression="gzip")
    f.create_dataset("ent",
         data = ent,
         dtype=np.float64, compression="gzip")
    f.create_dataset("lneg",
         data = lneg,
         dtype=np.float64, compression="gzip")
f.close()
"""
end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
