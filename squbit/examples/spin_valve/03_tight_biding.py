#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import eye, kron

import sys
sys.path.append('../../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from hamiltonian_core import Spin
from numba_wrapper import calc_rho
from evolve_step import rk4_rho

sys.path.append('../../../plot_manager/')
from plot2d import plotfig
data = dict();values = dict()
values['xlabel1'] = r'$\mathrm{Sites}$'
values['ylabel1'] = r'$\mathrm{\Psi}$'
sf = np.array([[0,1]])
Cl = ['C0', 'C2', 'C3']
Gs = dict();
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.3; Gs['r']=0.7
Gs['w']=0.4; Gs['h']=0.4

#float_formatter = "{:.1f}".format
#np.set_printoptions(formatter={'float_kind':float_formatter})

#parameters
Lx  = 110
t   = -1
kx = 0.1
dkx = 0.2
x0 = 40
J   = -0.1
Jz  = J*1.005
tf = 70
dt = 0.1  #\hbar/gamma


start = time.time()
sys = Basis(L=Lx, Nup=1, Ndn=0,  model = 1, use_iterator=True)
print(sys.Ns)
ham = Hamiltonian(sys, periodic=0, Lx=Lx, Ly=1)
H_hop = ham.hubbard_sparse(t=t, alpha = 0)
Ht = kron(H_hop,eye(2))
#constructing spinless gaussain psi
cl = np.arange(Lx)
data['x_plot'] = cl
psi_orb = np.exp(1j*kx*cl - (dkx**2 * (cl - x0)**2)/4)
psi_orb = psi_orb/(np.dot(psi_orb,psi_orb.conj()))

#unpolarized rho_e
rho_e = kron(np.outer(psi_orb,psi_orb.conj()),eye(2)).tocsr()
print(rho_e.shape, 2*(Lx))

#spin hamiltonian
spin = Spin(S=1/2, L = 4)
Se = spin.op
H0   = spin.heisenberg(J=J,Jz=Jz)

w,v = la.eigh(H0.toarray())
#spin p and a density matrix
rho_p = np.outer(v[:,0], v[:,0].conj())
rho_a = np.outer(v[:,1], v[:,1].conj())

#total density matrix
rho = kron(kron(rho_e,rho_p),rho_a)


#evolve rho_e
_time = np.arange(0, tf+dt, dt)
n = np.zeros((Lx,len(_time)))
rho_e = rho_e.toarray()
Ht = Ht.toarray()
for t_ind, ti in enumerate(_time):
    n =  np.array([rho_e[2*i, 2*i] + rho_e[2*i+1, 2*i+1] \
            for i in range(Lx)])
    
    plot_y = n.reshape(len(n),1)
    data['y_plot'] = plot_y
    if t_ind%10 == 0:
        plotfig(data, 's01/fig_'+str(t_ind//10), values, ylim=(0,0.002), sf=sf, Gs=Gs, 
                Cl=Cl, fs=8, lw=0.2, ft='.png')

    rho_e = rk4_rho(Ht, 0, rho_e, dt)



end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')










plotfig(data, 'fig_psi', values, sf=sf, Cl=Cl,
        Gs=Gs, fs=7, lw=0.4)
