#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix

import sys
sys.path.append('../../')
from hamiltonian_core import Spin
from density_core import entanglement_entropy
sys.path.append('../../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
L  = 4
S   = 1/2
J   = -0.1
Jz  = -0.1005
start = time.time()

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
Se = spin.op
H0   = spin.heisenberg(J=J,Jz=Jz)
print(H0)
w,v = la.eigh(H0.toarray())

#print(constructing states)
up = np.array([1,0])
dn = np.array([0,1])
v1 = np.kron(np.kron(np.kron(up,up),up),up)
#v0 = up

s0 = Se['sm0'] @ v1
#s0 = v1

print(s0.conj() @ Se['sz0'] @ s0)
print(s0.conj() @ Se['sz1'] @ s0)
print(s0.conj() @ Se['sz2'] @ s0)
print(s0.conj() @ Se['sz3'] @ s0)

