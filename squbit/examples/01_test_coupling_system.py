import numpy as np
import time

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from index_core import Index

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
dim = '1d'

start = time.time()
Lx = 4
Ly = 2
L = Lx * Ly
N = 1
print('N=',N)
model = 1
periodic = 1
use_iterator = False

if Ly>=2: dim = '2d'
sys = Basis(L=L, N=N, model=model, use_iterator=use_iterator, double_occu=True)
print(sys.Ns)
ham = Hamiltonian(sys, periodic=periodic, Lx=Lx, Ly=Ly)
gg = ham.hubbard_sparse(t=-1)

print(ham.index.kin_hop()[0])
print('gg')
print(ham.index.kin_hop()[1])


if False:
    a,b = ham.index.kin_hop()
    a,b = ham.index.nn_int()
    print(a)
    print(b)
end = time.time()

print('time taken: ', end - start)
