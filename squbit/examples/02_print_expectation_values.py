#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py

#import pycuda.gpuarray as gpuarray
#import pycuda.autoinit
#from skcuda import linalg
#from skcuda import misc

import sys as sysf
sysf.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from numba_wrapper import calc_rho
from evolve_step import construct_V_matrix as vmat

sysf.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
Lx  = 2
Ly  = 2
t   = -1.0
U   = 8.0
mu  = 0
J   = 0.1
Jz  = 6.67
err = 1e-3
tso = -1.0
#derived parameters
L   = Lx*Ly

start = time.time()
sys = Basis(L=L, N=1, model = 1, use_iterator=True)
#sys = Basis(L=L, N=(L-1,L,L+1), model = 1)
print("System size:", sys.Ns)

ham = Hamiltonian(sys, periodic=1, Lx=Lx, Ly=Ly)

#H_j, op, repj = ham.heisenberg(J=J, Jz=Jz, return_op=True)
#defect = -err * op['sz0']

H_hop = ham.hubbard_sparse(t=t, alpha = 4)

print(np.round(H_hop[:4,:4].toarray(),2))

#H_U = ham.hubbard_U(U=U)
#H_so = ham.spin_orbit(alpha = tso)
#H_mu = ham.onsite_mu(mu=mu)

#H = H_j + H_hop + H_U + defect + H_so

#if not np.allclose(H.toarray(), np.asmatrix(H.toarray()).H):
#    raise ValueError('bleh not hermitian')

#print('sparse H:', sysf.getsizeof(H))
#print('full H:', sysf.getsizeof(H.toarray()))


"""
#linalg.init()
#a_gpu = gpuarray.to_gpu(H.toarray()) 
#vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
#w = w_gpu.get()
#v = vr_gpu.get().transpose()
w, v = sla.eigsh(H, k = 3, which='SA')
#print('time taken for diag: ',time.time()-start)

#vmat = vmat(H, w, v)
#print('time taken for vmat: ',time.time()-start)
#w,v = la.eigh(H.toarray())

#num_op = ham.onsite_mu()

#test = v[:,0]
#print(test.conjugate() @ num_op @ test)

#print(w[:20])
#print(w[0]*4)
#print(v[:,0].conj()@H.toarray()@v[:,0])
print(w[:5])
print(np.real(v[:,0].conj()@ H @v[:,0]))
ss = 0
for site in range(L):
    print('sz'+str(site)+': ','{0: 1.3e}'.\
    format(np.real(v[:,ss].conj()@op['sz'+str(site)]@v[:,ss])))

end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')





spin0 = np.zeros(L)
spin1 = np.zeros(L)
for site in range(L):
    spin0[site] = np.real(v[:,0].conj()@op['sz'+str(site)]@v[:,0])
    spin1[site] = np.real(v[:,1].conj()@op['sz'+str(site)]@v[:,1])

with h5py.File('results/'+name+'.hdf5', 'w') as f:
    dset = f.create_dataset("spin0", 
             data = spin0, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("spin1", 
             data = spin1, 
             dtype=np.float64, compression="gzip")
f.close()
"""
