import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
import h5py

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from numba_wrapper import calc_rho

sys.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951

def vec(t):
    omega = 2.01/HBAR
    s = 5
    t0 = 25
    E0 = 0.12
    Et = E0*np.cos(omega*t) * np.exp(-(t - t0)*(t - t0)/(2*s*s))
    return Et


#parameters
Lx  = 4
Ly  = 2
t   = -1
J   = 0.1
Jz  = 0.1
err = 1
dt = 0.01
inp = [[Lx, Ly, t, J, Jz, err, dt],[2.01/HBAR,5,25,0.12]]

#derived parameters
L   = Lx*Ly
if Ly>=2: dim = '2d'
_time = np.arange(0,50,dt)
spin0 = spin1 = spin2 = spin3 = spin4 \
      = spin5 = spin6 = spin7 = np.zeros((3, len(_time)))
pulse = np.zeros(len(_time))

start = time.time()
sys = Basis(L=L, N=L, model = 1)
print(sys.Ns)

ham = Hamiltonian(sys, periodic=1, Lx=Lx, Ly=Ly, dim=dim, dtype=np.complex128)

H_j, op, repj = ham.heisenberg(J=J, Jz=Jz, return_op=True)
defect = -err * op['sz0']
H_hop, rep = ham.hubbard_sparse(t=t, alpha = 0)
H = H_j + H_hop + defect

linalg.init()
a_gpu = gpuarray.to_gpu(H.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
w = w_gpu.get()
v = vr_gpu.get().transpose()
#w,v = la.eigh(H.toarray())
print(w[:5])
print(v[:,0].conj()@H.toarray()@v[:,0])
#w, v = la.eigh(H.toarray())

print('sz0: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz0']@v[:,0])))
print('sz1: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz1']@v[:,0])))
print('sz2: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz2']@v[:,0])))
print('sz3: ','{0: 1.3e}'.format(np.real(v[:,0].conj()@op['sz3']@v[:,0])))

Et = vec(_time)
At = 0
psi = v[:,0]
for t_ind, ti in enumerate(_time):

    #observable
    spin0[:,t_ind] = (np.real((psi.conj() @ op['sx0']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sy0']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sz0']) @ psi.transpose()))
    spin1[:,t_ind] = (np.real((psi.conj() @ op['sx1']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sy1']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sz1']) @ psi.transpose()))
    spin2[:,t_ind] = (np.real((psi.conj() @ op['sx2']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sy2']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sz2']) @ psi.transpose()))
    spin3[:,t_ind] = (np.real((psi.conj() @ op['sx3']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sy3']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sz3']) @ psi.transpose()))
    spin4[:,t_ind] = (np.real((psi.conj() @ op['sx4']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sy4']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sz4']) @ psi.transpose()))
    spin5[:,t_ind] = (np.real((psi.conj() @ op['sx5']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sy5']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sz5']) @ psi.transpose()))
    spin6[:,t_ind] = (np.real((psi.conj() @ op['sx6']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sy6']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sz6']) @ psi.transpose()))
    spin7[:,t_ind] = (np.real((psi.conj() @ op['sx7']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sy7']) @ psi.transpose()),
                      np.real((psi.conj() @ op['sz7']) @ psi.transpose()))
    pulse[t_ind] = At
    
    if t_ind%10==0: print(spin0[:,t_ind])
    #evolution
    At = -1*Et[t_ind]*dt + At
    alpha = 2*np.pi * At / (HBAR)
    H_hop, rep = ham.hubbard_sparse(t=t, alpha = alpha)
    Ht = H_j + H_hop + defect
    
    a_gpu = gpuarray.to_gpu(Ht.toarray()) 
    vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
    W = w_gpu.get()
    V = vr_gpu.get().transpose()
    #W, V = la.eigh(Ht.toarray())
    
    cn = [V[:,i].conj() @ psi.reshape((sys.Ns,1)) for i in range(sys.Ns)]
    #Ut = la.inv(np.eye(sys.Ns) + 1j*dt*Ht/(2*HBAR)) @ \
    #           (np.eye(sys.Ns) - 1j*dt*Ht/(2*HBAR))
    Ut = [np.exp(-1j*W[i]*dt/HBAR) for i in range(sys.Ns)]

    psi = np.sum([cn[i] * (Ut[i] * V[:,i]) for i in range(sys.Ns)], axis=0)

    
#w, v = la.eigh(H.toarray())
#print(np.round(w[:10],2))



with h5py.File('result_1.hdf5', 'w') as f:
    dset = f.create_dataset("spin0", 
             data = spin0, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("spin1", 
             data = spin1, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("spin2", 
             data = spin2, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("spin3", 
             data = spin3, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("spin4", 
             data = spin4, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("spin5", 
             data = spin5, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("spin6", 
             data = spin6, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("spin7", 
             data = spin7, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("pulse", 
             data = pulse, 
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("input", 
             data = inp, 
             dtype=np.float64, compression="gzip")
f.close()


data = dict();values = dict()
data['time'] = _time; data['spin'] = spin0[2,:]
values['x_plot'] = 'time'; values['y_plot'] = 'spin'
values['xlabel'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel'] = r'$\mathrm{\langle \hat{s}^z_i \rangle}$'
plotfig(data, values, 'r1', fs=8, lw=0.4)

end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
