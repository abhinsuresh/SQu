import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
from scipy.sparse import eye
from scipy.sparse import kron

from index_core import Index


class Hamiltonian:
    
    def __init__(self,
                 sys,
                 periodic = 1,
                 dim = '1d',
                 Lx = 1,
                 Ly = 1,
                 Lz = 1,
                 spin_names =  ['sx', 'sy', 'sz'],
                 spin_unit = 1/2,
                 dtype = np.complex128,
                 double_occ = True,
                 check_herm=0,
                ):
        self.sys = sys
        self.spin_unit = 1/2
        self.model = sys.model
        self.periodic = periodic
        if Ly > 1:  self.dim = '2d'
        else :      self.dim = '1d'
        if Lz > 1:  self.dim = '3d'
        self.Lx = Lx
        self.Ly = Ly
        self.Lz = Lz
        self.index = Index(self.sys, periodic=periodic,
                            Lx = self.Lx, Ly = self.Ly, Lz = self.Lz)
        self.spin_names = spin_names
        self.dtype = dtype
        self.check_herm = check_herm
        self.generate()
        return

    def __type__(self):
        return "<type 'squbit.Hamiltonian'>"

    def generate(self):
        """generate flip spin and up list based on model
        """
        if self.model == 1:
            self.flip_sp = self.sys.bitL//2
            self.uplist = np.arange(0, self.sys.bitL//2)
        elif self.model == 2:
            self.flip_sp = 1
            self.uplist = np.arange(0, self.sys.bitL, 2)

    def c_operators(self):
        """ create c_dagger operators
        """
        nums = ['{0:1.0f}'.format(i) for i in np.arange(self.sys.L)]
        result = dict()
        #upspin
        for site_ind, site in enumerate(self.uplist):
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            op = spin_u*(op + op.getH())
            result[self.spin_names[0]+str(site_ind)] = op.tocsr()
        
            
        return result
        
    def op_spin(self):
        """ create spin operators
        """
        nums = ['{0:1.0f}'.format(i) for i in np.arange(self.sys.L)]
        result = dict()
        spin_u = self.spin_unit
        #spin_x
        for site_ind, site in enumerate(self.uplist):
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            op = spin_u*(op + op.getH())
            result[self.spin_names[0]+str(site_ind)] = op.tocsr()
        
        #spin_y
        for site_ind, site in enumerate(self.uplist):
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            op = (-1j)*spin_u*(op - op.getH())
            result[self.spin_names[1]+str(site_ind)] = op.tocsr()
        
        #spin_z
        for site_ind, site in enumerate(self.uplist):
            #up_spin
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #dn_spin
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site+self.flip_sp)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            
            op = spin_u*(op1 - op2)
            result[self.spin_names[2]+str(site_ind)] = op.tocsr()
            
        return result

    def hubbard_U(self, U=0, conserve_PH=False, layers=2):
        """ create hubbard u operators 
        """
        #Extra option include to add hubbard u only in 1 layer
        if layers == 1:
            Nl = len(self.uplist)//2
        else:
            Nl = len(self.uplist)
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        if not conserve_PH:
            """H = +U*(nup * ndn)
            """
            for site_ind, site in enumerate(self.uplist[:Nl]):
                row = []; col = []; data = []
                for state_ind, state in enumerate(self.sys.basisN):
                    coup = [site, site + self.flip_sp]
                    out_ind, ds = self.sys.op_diag_quad(state, coup)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append(U*(-1)**(ds))
                op = coo_matrix((data, (row,col)), 
                         shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
                H = H + op
        else:
            """H = +U*(nup - 1/2)(ndn - 1/2)
            """
            for site_ind, site in enumerate(self.uplist):
                #nup
                row = []; col = []; data = []
                for state_ind, state in enumerate(self.sys.basisN):
                    out_ind, ds = self.sys.op_diag(state, site)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append((-1)**(ds))
                op_up = coo_matrix((data, (row,col)), 
                         shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
                #ndn
                row = []; col = []; data = []
                for state_ind, state in enumerate(self.sys.basisN):
                    out_ind, ds = self.sys.op_diag(state, site + self.flip_sp)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append((-1)**(ds))
                op_dn = coo_matrix((data, (row,col)), 
                         shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
                H = H + U*(op_up-0.5*eye(self.sys.Ns)).dot(op_dn-0.5*eye(self.sys.Ns))
        return H.tocsr()
                        
    def onsite_mu(self, mu=1, mup=None, mdn=None):
        """ create hubbard u operators hop_val should be list
            H = mup*(Nup) + mdn*(Ndn)
        """
        if mup is None and mdn is None:
            mup = mu; mdn = mu
        if np.array(mup).shape==():
            mup = [mup]*len(self.uplist)
            mdn = [mdn]*len(self.uplist)
        elif len(mup) != len(self.uplist):
            raise ValueError('Length of hop_val should be same as lattice sites')
            
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        for site_ind, site in enumerate(self.uplist):
            #nup
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(mup[site_ind]*(-1)**(ds))
            op_up = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op_up
            #ndn
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site+self.flip_sp)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(mdn[site_ind]*(-1)**(ds))
            op_dn = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op_dn
        return H.tocsr()

    def number_op(self, site):
        """ create hubbard u operators hop_val should be list
            H = Nup_i + Ndn_i
        """ 
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        #nup
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            out_ind, ds = self.sys.op_diag(state, site)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op_up = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        H = H + op_up
        #ndn
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            out_ind, ds = self.sys.op_diag(state, site+self.flip_sp)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op_dn = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        H = H + op_dn
        return H.tocsr()

    def hubbard_sparse(self, t=-1, alpha=0, hx=1, hy=1, return_rep=False,
                       Bfield=False):
        """lacks the ability to handle index[a,b] where a>=b
        light implemented only in first layer using manual
        multiplicity factor 
        """
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        ham_rep = "H = "
        if self.dim == '1d':
            hop_list, em_list = self.index.kin_hop()
        elif self.dim =='2d':
            hopx_list, hopy_list = self.index.kin_hop()
            hop_list = hopx_list + hopy_list
            print(hop_list)
            #mulplicative factor for adding light in each column
            mult_y = np.ones(len(hopy_list))
            mult_x = np.ones(len(hopx_list))
            if Bfield:
                if not self.periodic:
                    mult_y = [[i+1]*(self.Ly-1) for i in range(self.Lx)]
                    mult_y = self.index.flatten_list(mult_y)
                    mult_y = mult_y * 2
                    mult_x = [1+i//(self.Lx-1) for i in range((self.Lx-1)*self.Ly)]
                    mult_x = mult_x * 2
                if self.periodic:
                    mult_y = [[i+1, i+1]*(self.Ly-1) for i in range(self.Lx)]
                    mult_y = self.index.flatten_list(mult_y)
                    mult_y = mult_y * 2
                    mult_x = [1+i//self.Lx for i in range(self.Lx*self.Ly)]
                    mult_x = mult_x * 2
                
        elif self.dim == '3d':
            hopx_list, hopy_list, hopz_list = self.index.kin_hop()
            hop_list = hopx_list + hopy_list + hopz_list
            #print(hopx_list)
            #print(hopy_list)
            #print(hopz_list)
            """
            mult_y = np.ones(len(hopy_list))
            mult_x = np.ones(len(hopx_list))
            if not self.periodic:
                mult_y = [[i+1]*(self.Ly-1) for i in range(self.Lx)]
                mult_y = self.index.flatten_list(mult_y)
                mult_y = mult_y * 2
                mult_x = [1+i//(self.Lx-1) for i in range((self.Lx-1)*self.Ly)]
                mult_x = mult_x * 2
            if self.periodic:
                mult_y = [[i+1, i+1]*(self.Ly-1) for i in range(self.Lx)]
                mult_y = self.index.flatten_list(mult_y)
                mult_y = mult_y * 2
                mult_x = [1+i//self.Lx for i in range(self.Lx*self.Ly)]
                mult_x = mult_x * 2
            """
            # overwrtiten by manual implementaion
            mult_y = [1,1,2,2,1,2,1,2,0,0,0,0,0,0,0,0]
            mult_x = [1,1,2,2,1,2,1,2,0,0,0,0,0,0,0,0]

        count_y = 0; count_x = 0
        print(hop_list)
        for hop_ind in hop_list:
            row = []
            col = []
            data = []
            h_f = 1
            #adding light factor
            if self.dim=='2d' or self.dim=='3d':
                if hop_ind in hopy_list:
                    h_f = np.exp(2*np.pi*1j*alpha*mult_y[count_y])
                    h_f = h_f * hy
                    #print(hop_ind,mult_y[count_y])
                    count_y += 1
                elif hop_ind in hopx_list:
                    #h_f = np.exp(2*np.pi*1j*alpha_x*mult_y[count_y])
                    count_x += 1
                    h_f = h_f * hx
            #constructing the hamiltonian
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_nondiag(state, hop_ind)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(t * h_f * ( (-1)**(ds) ))
            op = coo_matrix((data, (row,col)), shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype) 
            H = H + op
        
        H = H + H.getH()
            
        #hamiltonian_representation
        for hop_ind in hop_list:
            if self.model==1 and  return_rep:
                if hop_ind[1] < self.sys.bitL//2:
                    ham_rep = ham_rep + 'Cup'+str(hop_ind[0])+"'." + \
                              'Cup'+str(hop_ind[1])+ " + "
                else:
                    ham_rep = ham_rep + 'Cdn'+str(hop_ind[0]-self.sys.bitL//2)+"'." + \
                              'Cdn'+str(hop_ind[1]-self.sys.bitL//2)+ " + "
            elif self.model==2 and return_rep:
                if hop_ind[1]%2==0:
                    ham_rep = ham_rep + 'Cup'+str(hop_ind[0]//2)+"'." + \
                              'Cup'+str(hop_ind[1]//2)+ " + "
                else:
                    ham_rep = ham_rep + 'Cdn'+str(hop_ind[0]//2)+"'." + \
                              'Cdn'+str(hop_ind[1]//2)+ " + "
        if not return_rep:
            return H.tocsr()
        else:
            return H.tocsr(), ham_rep + 'h.c.'

    def heisenberg(self, J=0.1, Jz=0.1, return_op=False, return_rep=False):
        """Should only be defined in basis with Nup and Ndn not individually
        conserved, raises error 
        """
        ham_rep = ""
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        if self.dim == '1d' or self.dim == '2d' or self.dim=='3d':
            op = self.op_spin()
            coupx_list,coupy_list = self.index.nn_int()
            coup_list = coupx_list + coupy_list
            for site in coup_list:
                for sigma in self.spin_names:
                    if sigma == self.spin_names[2]:
                        H = H + Jz*op[sigma + str(site[0])] @ \
                                   op[sigma + str(site[1])]
                        #hamiltonian_representation
                        ham_rep = ham_rep + 'Jz.'+sigma+str(site[0]) \
                                  +'.'+sigma+str(site[1]) + ' + '
                    else:
                        H = H + J*op[sigma + str(site[0])] @ \
                                  op[sigma + str(site[1])]
                        #hamiltonian_representation
                        ham_rep = ham_rep + 'J.'+sigma+str(site[0]) \
                                  +'.'+sigma+str(site[1]) + ' + '
        if not return_op:    
            return H.tocsr()
        else:
            return H.tocsr(), op
            
        
    def spin_curr(self, site_i, site_j, compute_all=False):
        """ create spin operators, only fectching the operators for
        speficfic bond currents and only z components as of now
        """
        #x_list,y_list = self.index.nn_int()
        #c_list = x_list + y_list
        #print(y_list)
        result = dict()        
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            coup = [site_i, site_j]
            out_ind, ds = self.sys.op_nondiag(state, coup)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op1 = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            coup = [site_i+self.flip_sp, site_j+self.flip_sp]
            out_ind, ds = self.sys.op_nondiag(state, coup)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op2 = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        op = op1 + op2 
        result['ch'] = self.spin_unit*op.tocsr()
        op = op1 - op2
        result['sz'] = self.spin_unit*op.tocsr()

        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            coup = [site_i, site_j+self.flip_sp]
            out_ind, ds = self.sys.op_nondiag(state, coup)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op3 = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            coup = [site_j, site_i+self.flip_sp]
            out_ind, ds = self.sys.op_nondiag(state, coup)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op4 = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        op4 = op4.conj().T
        op = op3 + op4 
        result['sx'] = self.spin_unit*op.tocsr()
        op = -1j*op3 + 1j*op4
        result['sy'] = self.spin_unit*op.tocsr()
            
        return result
        
    def spin_orbit(self, alpha = 0, layer = 0, new_ind=False):
        """ create spin-orbit coupling
        works in the model 1. not model 2, since reordering required
        """
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        x_list,y_list = self.index.nn_int(layer, new_ind)
        c_list = x_list + y_list
        # y_hop 
        for site_ind, site in enumerate(y_list):
            #1st term
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site[0], site[1] + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(alpha*(-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #2nd term
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site[1], site[0] + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(alpha*(-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + 1j*(op1 + op2.getH())

        # x_hop
        for site_ind, site in enumerate(x_list):
            #1st term
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site[0], site[1] + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(alpha*(-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #2nd term
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site[1], site[0] + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(alpha*(-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #H = H + (op1 - op2.getH())
            H = H + (op2.getH() - op1)

        H = H + H.getH()

        return H.tocsr()
        

    def kron_single(self, op, size, verbose=False):
        """Kron indentity to the last
        """
        if verbose:
            print('taking kron(operator, eye(size)) of single operator op')
        result = kron(op, eye(size))
        return result

    def kron_iden(self, op, size, verbose=False):
        """Kron eDOF, spinDOF)
        """
        if verbose:
            print('taking kron(operator, eye(size)) of list of operator dict(op)')
        result = dict()
        names = list(op.keys())
        for key in names:
            result[key] = kron(op[key], eye(size))
        return result


class Spin:
    """A class to add external quantum spins to the system
        can also create heisenberg hamiltonian
    """

    def __init__(self,
                 S = 1/2,   #spin value
                 L = 1,     #number of sites
                 sc = 1,    #spin scale (usually used for spin 1/2)
                 kronD = 1,
                 dtype = np.complex128,
                 names = ['sx', 'sy', 'sz', 'sp', 'sm']):
        self.S = S
        self.L = L
        self.SC = sc
        self.dim = int(2*self.S + 1)
        self.Ns = self.dim**(self.L)
        self.kronD = kronD
        self.dtype = dtype
        self.names = names
        self.extended_op = self.extend_to_nsites()
        self.op = self.kron_iden(kronD) 
        return

    def basic_op(self):
        """creates sigma matrix
        """
        row_x = []; col_x = []; data_x = []
        row_y = []; col_y = []; data_y = []
        row_z = range(self.dim)
        col_z = range(self.dim)
        data_z = -1*np.arange(-self.S,self.S+1,1)
        self.Sz = coo_matrix((data_z, (row_z,col_z)), 
            shape=((self.dim,self.dim)), dtype=self.dtype)*self.SC
        for i in range(self.dim-1):
            row_x.append(i); row_x.append(i+1)
            col_x.append(i+1); col_x.append(i)
            data_x.append(0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
            data_x.append(0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
            data_y.append(-1j*0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
            data_y.append(1j*0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
        self.Sx = coo_matrix((data_x, (row_x,col_x)), 
            shape=((self.dim,self.dim)), dtype=self.dtype)*self.SC
        self.Sy = coo_matrix((data_y, (row_x,col_x)), 
            shape=((self.dim,self.dim)), dtype=self.dtype)*self.SC

        self.Sp = (self.Sx + 1j*self.Sy)
        self.Sm = (self.Sx - 1j*self.Sy)
        return

    def extend_to_nsites(self):
        """extend spin matrix to n sites
        """
        result = dict()    
        sites = self.L
        I4 = eye(self.dim)
        self.basic_op() 
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sx)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sx)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sx'+str(ind)] = op.tocsr()

        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sy)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sy)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sy'+str(ind)] = op.tocsr()
            
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sz)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sz)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sz'+str(ind)] = op.tocsr()

        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sp)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sp)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sp'+str(ind)] = op.tocsr()

        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sm)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sm)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sm'+str(ind)] = op.tocsr()
        return result

    def kron_iden(self,size):
        """Kron (electronic identity, spinDOF) 
        """
        result = dict()
        names = list(self.extended_op.keys())
        for key in names:
            result[key] = kron(eye(size),self.extended_op[key])
        return result
    
    def heisenberg(self, J=0.1, Jz=0.1, periodic=0):
        #access the operators to create number operators
        OP = self.op
        NM = self.names[:3] # only getting x,y and z 
        L = OP[NM[0]+'0'].shape[0]
        H = coo_matrix((L,L),dtype=self.dtype)
        for i in range(self.L-1):
            for nm in NM[:2]:
                H = H + J*OP[nm+str(i)] @ OP[nm+str(i+1)]
            H = H + Jz*OP[NM[2]+str(i)] @ OP[NM[2]+str(i+1)]
        if periodic:
            H = H +  J*OP['sx0'] @ OP['sx'+str(self.L-1)]
            H = H +  J*OP['sy0'] @ OP['sy'+str(self.L-1)]
            H = H + Jz*OP['sz0'] @ OP['sz'+str(self.L-1)]
        return H
    
    def heisenberg_2d(self, J=0.1, Jz=0.1, Lx=2, Ly=2, hop_list=None):
        #access the operators to create number operators
        OP = self.op
        NM = self.names[:3] # only getting x,y and z 
        L = OP[NM[0]+'0'].shape[0]
        H = coo_matrix((L,L),dtype=self.dtype)
        if hop_list == None:
            hop_list = [[i,i+1] for i in range(Lx-1)] + \
                       [[i,i+1] for i in range(Lx,2*Lx-1)] + \
                       [[i,i+Lx] for i in range(Lx)]
        for [i,j] in hop_list:
            for nm in NM[:2]:
                H = H + J*OP[nm+str(i)] @ OP[nm+str(j)]
            H = H + Jz*OP[NM[2]+str(i)] @ OP[NM[2]+str(j)]
        return H
    
    def sd_ham(self, se, Jsd=0.1, periodic=0):
        """Make sure to have extended the system
        """
        #access the operators to create number operators
        L = se['sx0'].shape[0]
        H = coo_matrix((L,L),dtype=self.dtype)
        for i in range(self.L):
            for nm in self.names[:3]:
                H = H - Jsd*self.op[nm+str(i)] @ se[nm+str(i)]
        return H
 
    def check_hermitian(self, a, rtol=1e-05, atol=1e-08):
        return np.allclose(a, a.conj().T, rtol=rtol, atol=atol)
    
    def check_equal(self, a, b, rtol=1e-05, atol=1e-06):
        return np.allclose(a, b, rtol=rtol, atol=atol)
    
    def check_comm(self, a, b, rtol=1e-05, atol=1e-05):
        return np.allclose(a@b, b@a, rtol=rtol, atol=atol)



