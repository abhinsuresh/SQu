import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import identity as eye
import sys
from numba import jit

from numba_wrapper import vmat

HBAR = 0.6582119569
HBAR = 1

def evolve(H_static, H_dynamic, psi, dt, method='diag', time_dep=1):
    """a function to evolve the wave function in time
    if time independet, we return Ut and should be called outside
    the loop
    """
    size = H_static.shape[0]
    if method == 'gpu' or method == 'CN_gpu':
        import pycuda.gpuarray as gpuarray
        import pycuda.autoinit
        from skcuda import linalg
        from skcuda import misc
        linalg.init()
    
    if time_dep == 0:
        if method=='expm': 
            Ut = la.expm(-1j*H_static.toarray()*dt/HBAR)
            print('expm')
            return Ut
        elif method=='eig_vec':
            w,v = la.eigh(H_static.toarray())
            Ut = np.asarray([np.exp(-1j*w[i]*dt) for i in range(size)])
            return Ut
        elif method=='CN':
            Ut = sla.inv((eye(size) + 1j*dt*H_static/(2*HBAR)).tocsc()) @ \
                       (eye(size) - 1j*dt*H_static/(2*HBAR))
            return Ut
        elif method=='CN_spilu':
            B = sla.spilu(eye(size) + 1j*dt*H_static/(2*HBAR))
            return B
        elif method=='CN_gpu':
            mat      = (eye(size) + 1j*dt*H_static/(2*HBAR))
            a_gpu    = gpuarray.to_gpu(mat.toarray()) 
            ainv_gpu = linalg.inv(a_gpu, overwrite=True)
            mat_inv  = ainv_gpu.get()
            Ut       = mat_inv @ (eye(size) - 1j*dt*H_static/(2*HBAR))
            return Ut
    else: 
        if method=='expm': 
            Ht = H_static + H_dynamic
            Ut = la.expm(-1j*Ht.toarray()*dt/HBAR)
            psi_new = Ut @ psi
            return psi

        elif method=='CN_sparse':
            Ht = H_static + H_dynamic
            Ut = sla.inv(eye(size) + 1j*dt*Ht/(2*HBAR)) @ \
                        (eye(size) - 1j*dt*Ht/(2*HBAR))
            psi = Ut @ psi
            return psi
        
        elif method=='CN':
            Ht = H_static + H_dynamic
            Ut =  la.inv(np.eye(size) + 1j*dt*Ht/(2*HBAR)) @ \
                        (np.eye(size) - 1j*dt*Ht/(2*HBAR))
            psi = Ut @ psi
            return psi
            
        if method=='eig_vec':
            Ht = H_static + H_dynamic
            w, v = la.eigh(Ht.toarray())
            Ut = np.asarray([np.exp(-1j*w[i]*dt) for i in range(2)])
            cn = [v[:,i].conj() @ psi for i in range(2)]
            psi = np.sum([cn[i] * (Ut[i] * v[:,i]) for i in range(2)], axis=0)
            return psi

        elif method=='CN_gpu':
            Ht = H_static + H_dynamic
            mat      = (eye(size) + 1j*dt*Ht/(2*HBAR))
            a_gpu    = gpuarray.to_gpu(mat.toarray()) 
            ainv_gpu = linalg.inv(a_gpu, overwrite=True)
            mat_inv  = ainv_gpu.get()
            Ut       = mat_inv @ (eye(size) - 1j*dt*Ht/(2*HBAR))
            psi = Ut @ psi
            return psi

#@jit(nopython=True)
def evolve_rho(rho, Ht):
    rho_new = 1j*(rho @ Ht - Ht @ rho)/HBAR
    return rho_new

#@jit(nopython=True)
def rk4_rho(H_static, H_dynamic, rho, dt): 
    """a function to evolve the wave function in time
    H_dynamic should be a function of time if not []
    
    """
    Ht0 = Ht1 = Ht2 = H_static
    rho1 = evolve_rho(rho,            Ht0)*dt
    rho2 = evolve_rho(rho + rho1*0.5, Ht1)*dt
    rho3 = evolve_rho(rho + rho2*0.5, Ht1)*dt
    rho4 = evolve_rho(rho + rho3,     Ht2)*dt

    rho = rho + (rho1 + 2*rho2 + 2*rho3 + rho4)*(1/6)
    return rho

def psi_qj(Heff, psi, dt):
    """Evolve using quantum trajectories
    """
    I2 = np.eye(len(Heff))
    psi_new = (I2 - 1j*Heff*dt) @ psi
    dp = 1 - la.norm(psi_new)
    
    return psi_new, dp
    

def construct_V_matrix(H0, w=None, v=None, GM=0.1, mode='T=0'):
    """Constructing the V matrix for linblad super operator
    """ 
    size = H0.shape[0]
    gamma = np.zeros((size,size))
    if w.any() == None:
        linalg.init()
        a_gpu = gpuarray.to_gpu(H0.toarray()) 
        vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
        w = w_gpu.get()
        v = vr_gpu.get().transpose()

    if mode == 'T=0':
        x_range = np.arange(0,1)    
        y_range = np.arange(size)
        gamma[0,:] = GM
        
        V = vmat(v, gamma, x_range, y_range, size)
        return V 


def rk4_rho_imag(H, rho, dt, method='rk4'):
    """a function to evolve the wave function in time
    H_dynamic should be a function of time if not []
    
    """
    #HBAR = 1 
    rho1 = -1*(H @ (rho           )/HBAR)*dt
    rho2 = -1*(H @ (rho + rho1*0.5)/HBAR)*dt
    rho3 = -1*(H @ (rho + rho2*0.5)/HBAR)*dt
    rho4 = -1*(H @ (rho + rho3    )/HBAR)*dt
    
    rho_new = rho + (rho1 + 2*rho2 + 2*rho3 + rho4)*(1/6)
    return rho_new

def rho_imag(H, rho, dt, method='rk4'):
    """a function to evolve the wave function in time
    H_dynamic should be a function of time if not []
    
    """
    
    rho_new = rho - (H @ rho)*dt/HBAR
    return rho_new
