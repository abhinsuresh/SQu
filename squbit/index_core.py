import numpy as np


class Index:

    def __init__(self,
                 sys,
                 periodic= 1,
                 Lx = 1,
                 Ly = 1,
                 Lz = 1,
                 orb = 1,
                 dim = '1d'
                ):
        self.sys = sys
        self.model = sys.model
        self.periodic = periodic
        self.y_periodic = False
        self.Lx = Lx
        self.Ly = Ly
        self.Lz = Lz
        self.orb = orb
        self.dim = dim
        if self.Ly > 1:
            self.dim = '2d'
        if self.Lz > 1:
            self.dim = '3d'
        self.generate()
        return

    def __type__(self):
        return "<type 'squbit.Index'>"
    
    def generate(self):
        """method to save the hoping indices once called
        and return without calculating again, initialize with None
        """
        self.out = dict()
        self.out['kin_hop'] = None
        self.out['nn_int'] = None

    def kin_hop(self):
        """kinectic hopping
        1d and 2d periodic and non-periodic in model 1 and 2 tested
        """
        if not self.out['kin_hop'] is None: return self.out['kin_hop']

        if self.model == 1 and self.periodic and self.dim =='1d' and self.orb==1:
            up_list = [[i%(self.sys.bitL//2), (i+1)%(self.sys.bitL//2)] 
                        for i in range(self.sys.bitL//2 - 1)] \
                        + [[0, self.sys.bitL//2 -1]]
            dn_list = [[i%(self.sys.bitL//2) + self.sys.bitL//2, 
                       (i+1)%(self.sys.bitL//2) + self.sys.bitL//2] 
                        for i in range(self.sys.bitL//2 -1 )] \
                        + [[self.sys.bitL//2, self.sys.bitL - 1]]
            hop_list = up_list + dn_list
            self.out['kin_hop'] = hop_list, []
            return hop_list, []

        elif self.model == 1 and not self.periodic and self.dim=='1d' and self.orb==1:
            up_list = [[i%(self.sys.bitL//2), (i+1)%(self.sys.bitL//2)] 
                        for i in range(self.sys.bitL//2 - 1)] 
            dn_list = [[i%(self.sys.bitL//2) + self.sys.bitL//2, 
                       (i+1)%(self.sys.bitL//2) + self.sys.bitL//2] 
                        for i in range(self.sys.bitL//2 -1 )] 
            hop_list = up_list + dn_list
            self.out['kin_hop'] = hop_list, []
            return hop_list, []
        
        elif self.model == 2 and self.periodic and self.dim =='1d' and self.orb==1:
            up_list = [[2*(i), 2*(i+1)] 
                    for i in range(self.sys.bitL//2 - 1)] + [[0, self.sys.bitL-2]]
            dn_list = [[2*(i)+1, 2*(i+1)+1] 
                    for i in range(self.sys.bitL//2 - 1)] + [[1, self.sys.bitL-1]]
            hop_list = up_list + dn_list
            self.out['kin_hop'] = hop_list, []
            return hop_list, []
        elif self.model == 2 and not self.periodic and self.dim =='1d' and self.orb==1:
            up_list = [[2*(i), 2*(i+1)] 
                    for i in range(self.sys.bitL//2 - 1)]
            dn_list = [[2*(i)+1, 2*(i+1)+1] 
                    for i in range(self.sys.bitL//2 - 1)]
            hop_list = up_list + dn_list
            self.out['kin_hop'] = hop_list, []
            return hop_list, []

        # 2d system hopping in model 2
        if self.dim =='2d' and self.model==2 and self.orb == 1:
            N_2d = self.Lx * self.Ly
            s = np.arange(N_2d)
            x = s%self.Lx
            y = s//self.Ly
            hopx = []
            hopy = []
            for i in range(self.Ly):
                hopx.append(range(2*self.Lx*i, 2*self.Lx*i + self.Lx*2, 2))
                
            for i in range(self.Lx):
                hopy.append(range(2*i, 2*self.Lx*(self.Ly-1) + 2*i + 1, 2*self.Lx))
            upx_list = []
            dnx_list = []
            for ele in hopx:
                upx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0], ele[-1]]])

                dnx_list.append( [[ele[i]+1, ele[i+1]+1] for i in range(len(ele)-1)]  )
                if self.periodic: dnx_list.append([[ele[0]+1, ele[-1]+1]])

            upx_list = self.flatten_list(upx_list)
            dnx_list = self.flatten_list(dnx_list)
            
            upy_list = []
            dny_list = []
            for ele in hopy:
                upy_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0], ele[-1]]])

                dny_list.append( [[ele[i]+1, ele[i+1]+1] for i in range(len(ele)-1)]  )
                if self.periodic: dny_list.append([[ele[0]+1, ele[-1]+1]])

            upy_list = self.flatten_list(upy_list)
            dny_list = self.flatten_list(dny_list)
            self.out['kin_hop'] = upx_list + dnx_list, upy_list + dny_list
            return upx_list + dnx_list, upy_list + dny_list
            
        # 2d system hopping in model 1
        if self.model==1 and self.orb ==1:
            N_2d = self.Lx * self.Ly
            s = np.arange(N_2d)
            x = s%self.Lx
            y = s//self.Ly
            hopx = []
            hopy = []
            for i in range(self.Ly):
                hopx.append(range(self.Lx*i, self.Lx*i + self.Lx, 1))
            for i in range(self.Lx):
                hopy.append(range(i, self.Lx*(self.Ly-1) + i + 1,  self.Lx))
            upx_list = []
            dnx_list = []
            for ele in hopx:
                upx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0], ele[-1]]])

                dnx_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: dnx_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])

            upx_list = self.flatten_list(upx_list)
            dnx_list = self.flatten_list(dnx_list)
            
            upy_list = []
            dny_list = []
            for ele in hopy:
                upy_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0], ele[-1]]])

                dny_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: dny_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])

            upy_list = self.flatten_list(upy_list)
            dny_list = self.flatten_list(dny_list)
            self.out['kin_hop'] = upx_list + dnx_list, upy_list + dny_list
            return upx_list + dnx_list, upy_list + dny_list
        
        # 2d system hopping in model 1 
        if self.model==1 and self.orb == 2:
            N_2d = self.Lx * self.Ly
            s = np.arange(N_2d)
            x = s%self.Lx
            y = s//self.Ly
            hopx = []
            hopy = []
            for i in range(self.Ly):
                hopx.append(range(self.Lx*i, self.Lx*i + self.Lx, 1))
            for i in range(self.Lx):
                hopy.append(range(i, self.Lx*(self.Ly-1) + i + 1,  self.Lx))
            upx_list = []
            dnx_list = []
            for ele in hopx:
                upx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0], ele[-1]]])
                upx_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])

                dnx_list.append( [[ele[i]+N_2d*2, ele[i+1]+N_2d*2] for i in range(len(ele)-1)]  )
                if self.periodic: dnx_list.append([[ele[0]+N_2d*2, ele[-1]+N_2d*2]])
                dnx_list.append( [[ele[i]+N_2d*3, ele[i+1]+N_2d*3] for i in range(len(ele)-1)]  )
                if self.periodic: dnx_list.append([[ele[0]+N_2d*3, ele[-1]+N_2d*3]])

            upx_list = self.flatten_list(upx_list)
            dnx_list = self.flatten_list(dnx_list)
            upy_list = []
            dny_list = []
            for ele in hopy:
                upy_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0], ele[-1]]])
                upy_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])

                dny_list.append( [[ele[i]+N_2d*2, ele[i+1]+N_2d*2] for i in range(len(ele)-1)]  )
                if self.periodic: dny_list.append([[ele[0]+N_2d*2, ele[-1]+N_2d*2]])
                dny_list.append( [[ele[i]+N_2d*3, ele[i+1]+N_2d*3] for i in range(len(ele)-1)]  )
                if self.periodic: dny_list.append([[ele[0]+N_2d*3, ele[-1]+N_2d*3]])

            upy_list = self.flatten_list(upy_list)
            dny_list = self.flatten_list(dny_list)
            self.out['kin_hop'] = upx_list + dnx_list, upy_list + dny_list
            return upx_list + dnx_list, upy_list + dny_list
        
        # 2d system hopping in model 2
        if self.dim =='2d' and self.model==2 and self.orb == 2:
            N_2d = self.Lx * self.Ly 
            s = np.arange(N_2d)
            x = s%self.Lx
            y = s//self.Ly
            hopx = []
            hopy = []
            hopx.append(range(0,9,8))
            hopx.append(range(2,11,8))
            hopy.append(range(0,3,2))
            hopy.append(range(8,11,2))
            upx_list = []
            dnx_list = []
            for ele in hopx:
                upx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0], ele[-1]]])
                upx_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])

                dnx_list.append( [[ele[i]+1, ele[i+1]+1] for i in range(len(ele)-1)]  )
                if self.periodic: dnx_list.append([[ele[0]+1, ele[-1]+1]])
                dnx_list.append( [[ele[i]+N_2d+1, ele[i+1]+N_2d+1] for i in range(len(ele)-1)]  )
                if self.periodic: dnx_list.append([[ele[0]+N_2d+1, ele[-1]+N_2d+1]])

            upx_list = self.flatten_list(upx_list)
            dnx_list = self.flatten_list(dnx_list)
            upy_list = []
            dny_list = []
            for ele in hopy:
                upy_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0], ele[-1]]])
                upy_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])

                dny_list.append( [[ele[i]+1, ele[i+1]+1] for i in range(len(ele)-1)]  )
                if self.periodic: dny_list.append([[ele[0]+1, ele[-1]+1]])
                dny_list.append( [[ele[i]+N_2d+1, ele[i+1]+N_2d+1] for i in range(len(ele)-1)]  )
                if self.periodic: dny_list.append([[ele[0]+N_2d+1, ele[-1]+N_2d+1]])

            upy_list = self.flatten_list(upy_list)
            dny_list = self.flatten_list(dny_list)
            self.out['kin_hop'] = upx_list + dnx_list, upy_list + dny_list
            return upx_list + dnx_list, upy_list + dny_list

        # 3d system hopping in model 1
        if self.dim =='3d' and self.model==1:
            #N_3d = self.Lx * self.Ly * self.Lz
            N_2d = self.Lx * self.Ly
            s = np.arange(N_2d)
            x = s%self.Lx
            y = s//self.Ly
            hopx = []
            hopy = []
            for i in range(self.Ly):
                hopx.append(range(self.Lx*i, self.Lx*i + self.Lx, 1))
                
            for i in range(self.Lx):
                hopy.append(range(i, self.Lx*(self.Ly-1) + i + 1,  self.Lx))
            upx_list = []
            dnx_list = []
            upx_list2 = []
            dnx_list2 = []
            for ele in hopx:
                upx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0], ele[-1]]])

                dnx_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: dnx_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])
            for ele in hopx:
                upx_list2.append( [[ele[i]+2*N_2d, ele[i+1]+2*N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list2.append([[ele[0]+2*N_2d, ele[-1]+2*N_2d]])

                dnx_list2.append( [[ele[i]+N_2d+2*N_2d, ele[i+1]+N_2d+2*N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: dnx_list2.append([[ele[0]+N_2d+2*N_2d, ele[-1]+N_2d+2*N_2d]])

            upx_list = self.flatten_list(upx_list)
            dnx_list = self.flatten_list(dnx_list)
            upx_list2 = self.flatten_list(upx_list2)
            dnx_list2 = self.flatten_list(dnx_list2)
            upy_list = []
            dny_list = []
            upy_list2 = []
            dny_list2 = []
            for ele in hopy:
                upy_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0], ele[-1]]])

                dny_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: dny_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])
            for ele in hopy:
                upy_list2.append( [[ele[i]+2*N_2d, ele[i+1]+2*N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0]+2*N_2d, ele[-1]+2*N_2d]])

                dny_list2.append( [[ele[i]+N_2d+2*N_2d, ele[i+1]+N_2d+2*N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: dny_list.append([[ele[0]+N_2d+2*N_2d, ele[-1]+N_2d+2*N_2d]])

            upy_list = self.flatten_list(upy_list)
            dny_list = self.flatten_list(dny_list)
            upy_list2 = self.flatten_list(upy_list2)
            dny_list2 = self.flatten_list(dny_list2)

            upz_list = []
            dnz_list = []
            for s in range(N_2d):
                upz_list.append([s, s + N_2d])
                dnz_list.append([s+2*N_2d, s+2*N_2d + N_2d])
                

            self.out['kin_hop'] = upx_list + upx_list2 + dnx_list + dnx_list2, \
                                  upy_list + upy_list2 + dny_list + dny_list2, \
                                  upz_list + dnz_list
            return upx_list + upx_list2 + dnx_list + dnx_list2, \
                   upy_list + upy_list2 + dny_list + dny_list2, \
                   upz_list + dnz_list
        
    def nn_int(self, layer = 0, new_ind=False):
        """1d and 2d, periodic and non-periodic, independent of model
        used only for spin spin interaction where we use spin_op
        which handles model differences
        """
        if not self.out['nn_int'] is None\
           and not new_ind: return self.out['nn_int']

        if self.periodic and self.dim=='1d' and self.orb==1:
            coup_list = [[i%(self.sys.bitL//2), (i+1)%(self.sys.bitL//2)]
                        for i in range(self.sys.bitL//2 - 1)] \
                        + [[0, self.sys.bitL//2 -1]]
            self.out['nn_int'] = coup_list, []
            return coup_list, []
        if not self.periodic and self.dim =='1d' and self.orb==1:
            coup_list = [[i%(self.sys.bitL//2), (i+1)%(self.sys.bitL//2)]
                        for i in range(self.sys.bitL//2 - 1)] 
            self.out['nn_int'] = coup_list, []
            return coup_list, []

        if (self.dim =='2d' or self.dim == '3d') and self.orb==1:
            # 3d indexing based on 2 layers
            if layer == 0: Nl = 0
            else: Nl = self.Lx*self.Ly
            N_2d = self.Lx * self.Ly
            hopx = []
            hopy = []
            for i in range(self.Ly):
                hopx.append(range(self.Lx*i, self.Lx*i + self.Lx, 1))
            for i in range(self.Lx):
                hopy.append(range(i, self.Lx*(self.Ly-1) + i + 1, self.Lx))
            upx_list = []
            for ele in hopx:
                upx_list.append( [[ele[i]+Nl, ele[i+1]+Nl] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0]+Nl, ele[-1]+Nl]])
            upx_list = self.flatten_list(upx_list)
            upy_list = []
            for ele in hopy:
                upy_list.append( [[ele[i]+Nl, ele[i+1]+Nl] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0]+Nl, ele[-1]+Nl]])
            upy_list = self.flatten_list(upy_list)
            hop_list = upx_list + upy_list
            self.out['nn_int'] = upx_list, upy_list
            return upx_list, upy_list
        
        if (self.dim == '1d' or self.dim =='2d' or self.dim == '3d') and self.orb==2:
            # 3d indexing based on 2 layers
            N_2d = self.Lx * self.Ly
            hopx = []
            hopy = []
            for i in range(self.Ly):
                hopx.append(range(self.Lx*i, self.Lx*i + self.Lx, 1))
            for i in range(self.Lx):
                hopy.append(range(i, self.Lx*(self.Ly-1) + i + 1, self.Lx))
            upx_list = []
            for ele in hopx:
                upx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0], ele[-1]]])
            for ele in hopx:
                upx_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: upx_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])
            upx_list = self.flatten_list(upx_list)
            upy_list = []
            for ele in hopy:
                upy_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0], ele[-1]]])
                upy_list.append( [[ele[i]+N_2d, ele[i+1]+N_2d] for i in range(len(ele)-1)]  )
                if self.periodic: upy_list.append([[ele[0]+N_2d, ele[-1]+N_2d]])
            upy_list = self.flatten_list(upy_list)
            hop_list = upx_list + upy_list
            self.out['nn_int'] = upx_list, upy_list
            return upx_list, upy_list

    def flatten_list(self, _2d_list):
        """flatten a 2d list
        """
        flat_list = []
        for element in _2d_list:
            if type(element) is list:
                for item in element:
                    flat_list.append(item)
            else:
                flat_list.append(element)
        return flat_list
