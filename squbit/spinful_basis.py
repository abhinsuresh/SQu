import numpy as np
import h5py
import scipy.linalg as la
from scipy.sparse import coo_matrix
from scipy.sparse import csr_matrix

from numba_wrapper import get_states as n_get_states
from numba_wrapper import get_mult_states as nm_get_states
from numba_wrapper import get_states_occu as n_get_states_occu
from numba_wrapper import combiner

class Basis:
    
    def __init__(self,
                        L = 1,  # length of lattice
                        Ns = 4, # number of states in hibert space
                        dtype = 'int8', # datatype of basis state o fintegers
                        N = None, # number of electrons
                        Nup = None, # number of up electrons
                        Ndn = None, # number of dn electrons
                        model = 1,
                        double_occu = True, # only with numba
                        use_numba = True,
                        use_iterator = False,
                        generate_basis = True
                        ):
        self.L = L
        self.N = N
        self.Nup = Nup
        self.Ndn = Ndn
        self.Ns = Ns
        self.model = model
        self.basisN = []
        self.bitL = 2 * self.L
        self.dtype = dtype
        self.double_occu = double_occu        
        self.use_numba = use_numba
        self.use_iterator = use_iterator
        if generate_basis:
            self.generate()
        return
    
    def __str__(self):
        """Printing the basis representation in two models
        """
        rep = ""
        if self.model==1:
            rep = rep + 'index\t' + 'fock state\t' + 'int_rep\n'
            for i in np.arange(len(self.basisN)):
                rep = rep + str(i) + '\t' +  \
                            '|' + self.int_to_bit(self.basisN[i])[:self.bitL//2] + '>' + \
                            '|' + self.int_to_bit(self.basisN[i])[self.bitL//2:] + '>' + \
                            '\t' + str(self.basisN[i]) + '\n'
        if self.model==2:
            rep = rep + 'index\t' + 'fock state\t' + 'int_rep\n'
            for st_ind in np.arange(len(self.basisN)):
                mod = '|'
                for l_ind in range(self.bitL):
                    mod = mod + self.int_to_bit(self.basisN[st_ind])[l_ind]
                    if (l_ind+1)%2==0:
                        mod = mod + '>|'
                rep = rep + str(st_ind) + '\t' +  mod[:-2] + '>' + \
                            '\t' + str(self.basisN[st_ind]) + '\n'
        
        return rep
    
    def __type__(self):
        return "<type 'squbit.spinfull_basis'>"
    
    def generate(self):
        """ generation of basis using different methods
            the method by saving as hdf5 need to be optimised
            since currently it is being used as a list at the end
            since search index needs all elements to be loaded into
            the array
            Combiner is faster for large size systems than numba 
            versions
            model 2 needs a different version to handle the system
        """
        if self.N is not None:
            if self.use_numba and not self.use_iterator:
                with h5py.File('basis.hdf5', 'w') as f:
                    if self.double_occu:
                        if np.array(self.N).shape == ():
                            dset = f.create_dataset("data", 
                                     data = n_get_states(self.L, self.N), 
                                     dtype='i8', compression="gzip")
                        else:
                            dset = f.create_dataset("data", 
                                     data = nm_get_states(self.L, self.N), 
                                     dtype='i8', compression="gzip")
                    else:
                        dset = f.create_dataset("data", 
                                 data = n_get_states_occu(self.L, self.N), 
                                 dtype='i8', compression="gzip")
                f.close()
                f = h5py.File('basis.hdf5', 'r')
                self.basisN = f['data'][:].tolist()
            elif self.use_iterator:
                print('Using iterator')
                if self.model == 1:
                    bit_list = list(combiner(zeros=self.bitL-self.N, ones=self.N) )
                    self.basisN = self.bit_to_int(bit_list)
                elif self.model == 2:
                    print('Numba and combiner for model 2 not implemented')
             
            self.Ns = len(self.basisN)
        elif self.Ndn == 0 and self.use_iterator:
            print('Using iterator')
            if self.model == 1:
                up_list = list(combiner(zeros=self.bitL//2-self.Nup, ones=self.Nup) )
                dn_list = ['0'*(self.bitL//2)] 
                bit_list = []
                for ele_i in up_list:
                    for ele_j in dn_list:
                        bit_list.append(ele_i + ele_j)
                self.basisN = self.bit_to_int(bit_list)
                self.Ns = len(self.basisN)
            
        else:
            self.get_states()

    def __add__(self, sys_2):
        """function to define adding two systems, not fully implemented
        """
        sys_new = Basis(L=self.L, model=self.model, generate_basis=False)
        sys_new.basisN = self.basisN + sys_2.basisN
        sys_new.Ns = len(sys_new.basisN)
        return sys_new
        
        

    def get_states(self):
        """Primal way of Constructing the basis
        """
        for st_int in np.arange(4**self.L):            
            n, nup, ndn = self.count_n(st_int)           
            if self.Nup is None and self.Ndn is None:
                if self.N is None:
                    self.basisN.append(st_int)
                if n == self.N:
                    self.basisN.append(st_int)
            elif self.Nup is not None and self.Ndn is not None:
                if nup == self.Nup and ndn == self.Ndn:
                    self.basisN.append(st_int)
            else:
                raise('define both Nup and Ndn, or none')
        self.Ns = len(self.basisN)
 
    def count_n(self, int_type):
        """Counting the bits, not the fastest method at all
        """
        bit_rep = self.int_to_bit(int_type)
        bit_len = len(bit_rep)
        n = bit_rep.count('1')
        if self.model == 1:
            nup = bit_rep[:bit_len//2].count('1')
            ndn = bit_rep[bit_len//2:].count('1')   
        elif self.model == 2:
            nup = bit_rep[::2].count('1')
            ndn = bit_rep[1::2].count('1')
        return n, nup, ndn

    def bitLenCount(self, int_type):
        """Another fun to count bits, not used anymore
        """
        length = 0
        count = 0
        while (int_type):
            count += (int_type & 1)
            length += 1
            int_type >>= 1
        return count
    
    def int_to_bit(self, int_type):
        """Converting int to bit
        """
        result = str(bin(int_type))[2:]
        if len(result) == self.bitL:
            return result
        else:
            return '0' * (self.bitL - len(result)) + result
    
    def bit_to_int(self, str_type):
        """Converting bit to int
        """
        if type(str_type) is str:
            return int(str_type,2)
        else:
            r_list = []
            for str_tp in str_type:
                r_list.append(self.bit_to_int(str_tp))
            return r_list
    
    def bit_flip(self, state, ind1, ind2):        
        """ works on state as bit_rep, flipping bits at ind 1 and 2
        """
        result = state[: ind1]       + state[ind2] + \
                 state[ind1+1: ind2] + \
                 state[ind1]         + state[ind2+1:]
        return result

    def bit_add(self, state, site):    
        """works on state as bit_rep, replace by '1'
        """    
        return state[: site] + '1' +  state[site+1:]
    
    def bit_rm(self, state, site):    
        """works on state as bit_rep, replace by '0'
        """    
        return state[: site] + '0' +  state[site+1:]

    def op_nondiag(self, state, index):
        """returning C_ind[0]' * C_ind[1] operator action on basis
        can return a state outside of basis 
        for hopping index[i,j] in [0, bitL]
        """
        if (index[0] >= index[1]):
            raise ValueError('index[0] should be < index[1]')
        bit_rep = self.int_to_bit(state)
        if bit_rep[index[0]] == '0' and bit_rep[index[1]] == '1':
            array = [i for i in range(self.bitL) if bit_rep[i] =='1']
            #print(i, '\t', a, array)
            b = min(x for x in array if x > index[0])
            ds = array.index(index[1]) + array.index(b)
            #print('hop', index[0], index[1])
            #print('in state:  ',bit_rep, state)
            #print('out state: ',self.bit_flip(bit_rep, index[0], index[1]))
            out_ind = self.bit_to_int(self.bit_flip(
                           bit_rep, index[0], index[1]))
            #if self.double_occu:
            #    return self.basisN.index(out_ind), ds
            #else:
            if out_ind in self.basisN:
                return self.basisN.index(out_ind), ds
            else: return -1, 0
                
        else:
            return -1, 0
        
        
    def op_diag(self, state, index, op = '+-'):
        """returning C_ind' * C_ind operator action on basis
        """
        if op == '+-': 
            if (index > self.bitL):
                raise ValueError('Index should be <= chain length')
            bit_rep = self.int_to_bit(state)
            if bit_rep[index] == '1':
                return self.basisN.index(state), 0
            else:
                return -1, 0
    
    def op_diag_quad(self, state, index):
        """returning n[ind_0] * n[ind_1] operator action on basis
        """
        if (index[0] > self.bitL or index[1] > self.bitL):
            raise ValueError('Index should be <= chain length')
        bit_rep = self.int_to_bit(state)
        if bit_rep[index[0]] == '1' and bit_rep[index[1]]=='1':
            return self.basisN.index(state), 0
        else:
            return -1, 0

    def op_one_dagger(self, state, site):
        """returning C_ind[site]' works only in added basis 
        """
        bit_rep = self.int_to_bit(state)
        if bit_rep[site] == '0':
            array = [i for i in range(site) if bit_rep[i] =='1']
            print(bit_rep)
            print(site, '\t', array)
            ds = len(array)
            #print('in state:  ',bit_rep, state)
            #print('out state: ',self.bit_add(bit_rep, site), ds)
            out_ind = self.bit_to_int(self.bit_add(bit_rep, site))
            return self.basisN.index(out_ind), ds
        else:
            return -1, 0

    def op_one(self, state, site):
        """returning C_ind[site] works only in added basis 
        """
        bit_rep = self.int_to_bit(state)
        if bit_rep[site] == '1':
            array = [i for i in range(site) if bit_rep[i] =='1']
            print(bit_rep)
            print(site, '\t', array)
            ds = len(array)
            out_ind = self.bit_to_int(self.bit_rm(bit_rep, site))
            return self.basisN.index(out_ind), ds
        else:
            return -1, 0

    def ent_entropy(self, state, L1, norm=True):
        """calculate entanglement entropy by splitting one
        dimensional chain into L1 and L-L1 sites
        current implementation only applicable to linear
        chain considering it as spinless, or has to modify
        implementation in the state
        """
        nf = 1
        if norm: nf = 2*L1

        row = []
        col = []
        data = []
        D1 = 4**L1
        D2 = 4**(self.L - L1)
        #H = coo_matrix((D2, D1), dtype=np.float64)
        for st_ind, factor in enumerate(state):
            #print(factor)
            if factor != 0:
                row.append(self.basisN[st_ind] // D1) #quotient
                col.append(self.basisN[st_ind] %  D1) #reminder
                data.append(factor)
        S = coo_matrix((data, (row,col)), shape=((D2, D1)), dtype=np.complex64)
        singular = la.svd(S.toarray(), compute_uv=False, overwrite_a=False)
        entropy = 0
        count = 0
        for s in singular:
            if np.abs(s) != 0:
                count = count + 1
                entropy = entropy - \
                          np.conj(s)*s * np.log(np.conj(s)*s)
            #else:
            #    print(s)
        #print('count',count)
        #print('num', len(singular))
        return entropy/nf
        
            
            
        
    
        
        
