import numpy as np, time, h5py, sys, os, datetime
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import identity as iden

from quspin.basis import spinful_fermion_basis_general

sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from density_core import entanglement_entropy, partial_psi_trace
from numba_wrapper import updn_list
nm = os.path.basename(__file__)[:2]
print("Evaluation file name:\t", os.path.basename(__file__), 'res:\t', nm)
print("Evaluation time:\t", datetime.datetime.now())
#parameters
Lx      = 4
Ly      = 1
t       = 1.0
U       = 8.0
periodic = 1
L   = Lx*Ly
#Measurements
spinz = np.zeros(L)
spinx = np.zeros(L)
#System creation
start = time.time()
sys = Basis(L=L, N=L, model = 1)
print('system size\t', sys.Ns)

#Hamiltonian
ham = Hamiltonian(sys, periodic=periodic, Lx=Lx, Ly=Ly)
H_kin, rep = ham.hubbard_sparse(t=t, return_rep=True)
H_U = ham.hubbard_U(U=U)
Htot    = H_U + H_kin
H_fixed = H_U

#w, v = sla.eigsh(H, k=2, which='SA')
w,v = np.linalg.eigh(Htot.toarray()) 
print('Energy values:\t', w[:5])
#----------------------------------------------
# time evolution part
#----------------------------------------------
psi = v[:,0]
dt = 0.1
t0 = 50
s = 10
zmax = 0.5
w = 1.54/0.6582119
trange = np.arange(0,100,dt)
A = zmax * np.cos(w * trange) * np.exp(-(trange-t0)**2 / (2*s*s))
spinz = np.zeros((len(trange), L))
spinx = np.zeros((len(trange), L))
for t_ind, t_now in enumerate(trange):
    #observable
    #for site in range(L):
    #    spinz[t_ind, site] = np.real(psi.conj()@op['sz'+str(site)]@psi)
    #    spinx[t_ind, site] = np.real(psi.conj()@op['sz'+str(site)]@psi)
    
    ft = h5py.File("psi_"+nm+"/psi.h5","a")
    ft.create_dataset(str(t_ind)+"r",data=psi.real)
    ft.create_dataset(str(t_ind)+"i",data=psi.imag)
    ft.close()    

    #evolution
    hx = np.exp(1j * A[t_ind])
    Ht   = ham.hubbard_sparse(t=t, hx=hx) + H_fixed
    mat      = (iden(sys.Ns) + 1j*dt*Ht/2).toarray()
    mat_inv = np.linalg.inv(mat)
    Ut       = mat_inv @ (iden(sys.Ns) - 1j*dt*Ht/2)
    psi      = Ut @ psi
    print(t_ind)
 
#f = h5py.File("results/02t.h5","w")
#f.create_dataset("spinz", data=spinz)
#f.create_dataset("spinx", data=spinx)
#f.close()
end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
