import sys, h5py, numpy as np

sys.path.append('../../plot_manager')
from plot2d import plotfig_2p

data = dict();values=dict()

#file1
res = h5py.File(sys.argv[1],'r')
Jz = res['Jz'][:]
ent = res['entlist'][:]
sz = res['spinz'][:]
mz2 = res['msq'][:]
print(sz[:,39])
result = np.zeros((len(Jz),6))
result[:,0] = mz2/8
result[:,3] = ent


#file2
res = h5py.File(sys.argv[2],'r')
Jz = res['Jz'][:]
ent = res['entlist'][:]
sz = res['spinz'][:]
mz2 = res['msq'][:]
result[:,1] = mz2/8
result[:,4] = ent

#file3
res = h5py.File(sys.argv[3],'r')
Jz = res['Jz'][:]
ent = res['entlist'][:]
sz = res['spinz'][:]
mz2 = res['msq'][:]
result[:,2] = mz2/8
result[:,5] = ent

data['x_plot'] = Jz/0.1; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{J_z/J}$'; 
values['xlabel2'] = r'$\mathrm{J_z/J}$'; 
values['ylabel1'] = r'$\mathrm{\left<  m_z^2 \right>}$'
values['ylabel2'] = r'$\mathrm{Entanglement~Entropy}$'
Cl = ['k','C2','C3']*2
Ls = ['-','--',':']*2
Gs = dict()
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.12; Gs['r']=0.98
Gs['w']=0.3; Gs['h']=0.4

sf = np.array([[0,3],[3,6]])
plotfig_2p(data, '3', values, sf=sf, Cl=Cl, Ls=Ls, Gs=Gs,
            lw=0.4, minor=1)
