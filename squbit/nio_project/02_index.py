import numpy as np, time, h5py, sys, os, datetime
import scipy.linalg as la
import scipy.sparse.linalg as sla

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

sys.path.append('../')
from spinful_basis import Basis
from mult_orb_ham import Hamiltonian

nm = os.path.basename(__file__)[:2]
print("Evaluation file name:\t", os.path.basename(__file__), 'res:\t', nm)
print("Evaluation time:\t", datetime.datetime.now())

#parameters
Lx      = 2
Ly      = 2
orb     = 2
periodic = 1
L   = Lx*Ly*orb
#System creation
start = time.time()
sys = Basis(L=L, N=L, model = 2)
print(sys.Ns)
#Hamiltonian
ham = Hamiltonian(sys, periodic=periodic, Lx=Lx, Ly=Ly, orb=orb)
hopx_list, hopy_list = ham.index.nn_int()
print(hopx_list)
print(hopy_list)
print(ham.flip_sp, ham.flip_orb)
print(ham.uplist, ham.aorb_l)
end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
