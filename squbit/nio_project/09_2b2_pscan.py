import numpy as np, time, h5py, sys, os, datetime
import scipy.linalg as la
import scipy.sparse.linalg as sla
from quspin.basis import spinful_fermion_basis_general

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

sys.path.append('../')
from spinful_basis import Basis
from mult_orb_ham import Hamiltonian
from numba_wrapper import updn_list

nm = os.path.basename(__file__)[:2]
print("Evaluation file name:\t", os.path.basename(__file__), 'res:\t', nm)
print("Evaluation time:\t", datetime.datetime.now())

#parameters
Lx      = 2
Ly      = 2
t       = 1.0
U       = 8.0
J       = 0.1
_Jz      = J*np.linspace(1,300,40)
JH      = 0*1.0
Uprime  = 0*(U - 2*JH)
gJH     = 0.0
mu      = (3*U - 5*JH)/2
err     = 1e-3
orb     = 2
periodic = 1
inp = np.asarray([t, U, J, JH, Uprime, gJH, mu, err])
L   = Lx*Ly*orb
#System creation
start = time.time()
sys = Basis(L=L, N=L, model = 1)
print('system size\t', sys.Ns)

#Hamiltonian
ham = Hamiltonian(sys, periodic=periodic, Lx=Lx, Ly=Ly, orb=orb)
H_gJH = ham.hubbard_gammaJH(gJH=gJH)
H_kin = ham.hubbard_sparse(t=t, alpha = 0)
H_U = ham.hubbard_U(U=U)
H_j, op = ham.heisenberg(J=J, Jz=J, return_op=True)
H_JH = ham.hubbard_JH(JH=JH)
H_Uprime = ham.hubbard_Uprime(Uprime=Uprime)
H_mu = ham.onsite_mu(mu=mu)

defect = -err *(op['sz0']) 
#defect = -err *(op['sz0'] + op['sz1'] + op['sz2'] + op['sz3'] + 
#                op['sz4'] + op['sz5'] + op['sz6'] + op['sz7'])

H0 = H_kin + H_U + H_Uprime - H_JH - H_mu + H_gJH  + defect

#Measurements
basis = spinful_fermion_basis_general(8, Nf = updn_list(8)) 
Usum    = ham.hubbard_U(U=1)
spinz   = np.zeros((Lx*Ly*orb,len(_Jz)))
mz2     = np.zeros(len(_Jz))
Ulist   = np.zeros(len(_Jz))
entlist = np.zeros(len(_Jz))
Msq = ham.mz_sq()


linalg.init()
for Jz_ind, Jz in enumerate(_Jz):
    H_j = ham.heisenberg(J=J, Jz=Jz)
    H = H0 + H_j
    a_gpu = gpuarray.to_gpu(H.toarray()) 
    vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
    w = w_gpu.get()
    v = vr_gpu.get().transpose()
    #print('Energy values:\t', w[:5])
    for site in range(Lx*Ly*2):
        spinz[site, Jz_ind] = np.real(v[:,0].conj() @ op['sz'+str(site)] @ v[:,0])
    
    mz2[Jz_ind] = np.real(v[:,0].conj() @ Msq @ v[:,0])

    Ulist[Jz_ind] = np.real(v[:,0].conj() @ Usum @ v[:,0])
    entlist[Jz_ind] = basis.ent_entropy(v[:,0], sub_sys_A=([0,2,4,6],[0,2,4,6]),
                                        enforce_pure=True, alpha=1.0, 
                                        density=False)["Sent_A"]

with h5py.File('results/'+nm+'.hdf5', 'w') as f:
    dset = f.create_dataset("spinz",
             data = spinz,
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("msq",
             data = mz2,
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("Ulist",
             data = Ulist,
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("entlist",
             data = entlist,
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("Jz",
             data = _Jz,
             dtype=np.float64, compression="gzip")
    dset = f.create_dataset("input",
             data = inp,
             dtype=np.float64, compression="gzip")
f.close()


end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
