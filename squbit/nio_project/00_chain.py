import numpy as np, time, h5py, sys, os, datetime
import scipy.linalg as la
import scipy.sparse.linalg as sla

from quspin.basis import spinful_fermion_basis_general
import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from density_core import entanglement_entropy, partial_psi_trace
from numba_wrapper import updn_list
nm = os.path.basename(__file__)[:2]
print("Evaluation file name:\t", os.path.basename(__file__), 'res:\t', nm)
print("Evaluation time:\t", datetime.datetime.now())
#parameters
Lx      = 8
t       = 1.0
U       = 8.0
J       = 1
Jz      = 5
Bz      = 1e-3
orb     = 1
periodic = 0
L   = Lx*orb
#System creation
start = time.time()
sys = Basis(L=L, N=L, model = 1)
print('system size\t', sys.Ns)

#Hamiltonian
ham = Hamiltonian(sys, periodic=periodic, Lx=Lx)
H_kin = ham.hubbard_sparse(t=t, alpha = 0)
H_U = ham.hubbard_U(U=U)
op = ham.op_spin()
H_j = ham.heisenberg(J=J, Jz=Jz)
Hb = -Bz *(op['sz0'] )

H = H_U + H_kin + H_j + Hb

linalg.init()
a_gpu = gpuarray.to_gpu(H.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
w = w_gpu.get()
v = vr_gpu.get().transpose()
b = spinful_fermion_basis_general(Lx, Nf = updn_list(Lx)) 
#print('basis size:\t', b.Ns)
ent = b.ent_entropy(v[:,0], sub_sys_A=([0,1,2,3],[0,1,2,3]), enforce_pure=True, 
                    alpha=1.0, density=False)["Sent_A"]
spinz = np.zeros(Lx)
for site in range(Lx*orb):
    spinz[site] = np.real(v[:,0].conj()@\
                         op['sz'+str(site)]@v[:,0])
    #spinx[site] = np.real(v[:,0].conj()@\
    #                     op['sx'+str(site)]@v[:,0])
print('Energy values:\t', w[:5])
print('spinz values:\t', spinz)
print('entropy:\t', ent)
"""

#print('mz2 0:\t', np.real(v[:,0].conj() @ Msq  @ v[:,0]))
"""
end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
