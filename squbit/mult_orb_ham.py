import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
from scipy.sparse import eye
from scipy.sparse import kron

from index_core import Index


class Hamiltonian:
    
    def __init__(self,
                 sys,
                 periodic = 1,
                 dim = '1d',
                 Lx = 1,
                 Ly = 1,
                 Lz = 1,
                 spin_names =  ['sx', 'sy', 'sz'],
                 spin_unit = 1/2,
                 dtype = np.complex128,
                 double_occ = True,
                 check_herm=0,
                 orb = 1,
                ):
        self.sys = sys
        self.spin_unit = 1/2
        self.spin_op = None
        self.model = sys.model
        self.periodic = periodic
        self.orb = orb
        if Ly > 1:  self.dim = '2d'
        else :      self.dim = '1d'
        if Lz > 1:  self.dim = '3d'
        self.Lx = Lx
        self.Ly = Ly
        self.Lz = Lz
        self.index = Index(self.sys, periodic=periodic,
                            Lx = self.Lx, Ly = self.Ly, Lz = self.Lz, orb= orb)
        self.spin_names = spin_names
        self.dtype = dtype
        self.check_herm = check_herm
        self.generate()
        return

    def __type__(self):
        return "<type 'squbit.Hamiltonian'>"

    def generate(self):
        """generate flip spin and up list based on model
        """
        if self.model == 1:
            self.flip_sp = self.sys.bitL//2
            self.uplist = np.arange(0, self.sys.bitL//2)
            self.flip_orb = self.Lx * self.Ly
            self.aorb_l = np.arange(self.Lx*self.Ly)
        elif self.model == 2:
            self.flip_sp = 1
            self.uplist = [0, 2, 8, 10, 4, 6, 12, 14]
            self.flip_orb = self.Lx * self.Ly
            #-------------
            #HARD CODED
            #-------------
            self.aorb_l = np.array([0,2,8,10])

    def c_operators(self):
        """ create c_dagger operators
        """
        nums = ['{0:1.0f}'.format(i) for i in np.arange(self.sys.L)]
        result = dict()
        #upspin
        for site_ind, site in enumerate(self.uplist):
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            op = spin_u*(op + op.getH())
            result[self.spin_names[0]+str(site_ind)] = op.tocsr()
        
            
        return result
        
    def op_spin(self):
        """ create spin operators
        """
        nums = ['{0:1.0f}'.format(i) for i in np.arange(self.sys.L)]
        result = dict()
        spin_u = self.spin_unit
        #spin_x
        for site_ind, site in enumerate(self.uplist):
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            op = spin_u*(op + op.getH())
            result[self.spin_names[0]+str(site_ind)] = op.tocsr()
        
        #spin_y
        for site_ind, site in enumerate(self.uplist):
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            op = (-1j)*spin_u*(op - op.getH())
            result[self.spin_names[1]+str(site_ind)] = op.tocsr()
        
        #spin_z
        for site_ind, site in enumerate(self.uplist):
            #up_spin
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #dn_spin
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site+self.flip_sp)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            
            op = spin_u*(op1 - op2)
            result[self.spin_names[2]+str(site_ind)] = op.tocsr()

        self.spin_op = result
        return result

    def hubbard_U(self, U=0, conserve_PH=False):
        """ create hubbard u operators 
        """
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        if not conserve_PH:
            """H = +U*(nup * ndn)
            """
            #print(self.uplist)
            for site_ind, site in enumerate(self.uplist):
                row = []; col = []; data = []
                for state_ind, state in enumerate(self.sys.basisN):
                    coup = [site, site + self.flip_sp]
                    out_ind, ds = self.sys.op_diag_quad(state, coup)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append(U*(-1)**(ds))
                op = coo_matrix((data, (row,col)), 
                         shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
                H = H + op
        else:
            """H = +U*(nup - 1/2)(ndn - 1/2)
            """
            for site_ind, site in enumerate(self.uplist):
                #nup
                row = []; col = []; data = []
                for state_ind, state in enumerate(self.sys.basisN):
                    out_ind, ds = self.sys.op_diag(state, site)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append((-1)**(ds))
                op_up = coo_matrix((data, (row,col)), 
                         shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
                #ndn
                row = []; col = []; data = []
                for state_ind, state in enumerate(self.sys.basisN):
                    out_ind, ds = self.sys.op_diag(state, site + self.flip_sp)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append((-1)**(ds))
                op_dn = coo_matrix((data, (row,col)), 
                         shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
                H = H + U*(op_up-0.5*eye(self.sys.Ns)).dot(op_dn-0.5*eye(self.sys.Ns))
        return H.tocsr()

    def hubbard_JH(self, JH=0):
        """ create hubbard JH, made for 2 orbital per site
        going over half the uplist corresponding to first set of orbitals
        n_alpha_sigma * n_beta_sigma
        """
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        #for up up spins over two orbitals
        for site_ind, site in enumerate(self.aorb_l):
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_orb]
                #if state_ind==0: print(coup)
                out_ind, ds = self.sys.op_diag_quad(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(JH*(-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op
        #for dn dn spins over two orbitals
        for site_ind, site in enumerate(self.aorb_l):
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site + self.flip_sp, site + self.flip_sp + self.flip_orb]
                out_ind, ds = self.sys.op_diag_quad(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(JH*(-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op
        return H.tocsr()

    def hubbard_Uprime(self, Uprime=0):
        """ create hubbard Uprime, made for 2 orbital per site
        going over half the uplist corresponding to first set of orbitals
        n_alpha_sigma * n_beta_sigma'
        """
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        #for up up spins over two orbitals
        for site in self.aorb_l:
            #print(site, site + self.flip_orb)
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_orb]
                out_ind, ds = self.sys.op_diag_quad(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(Uprime*(-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op
        #for dn dn spins over two orbitals
        for site in self.aorb_l:
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site + self.flip_sp, site + self.flip_sp + self.flip_orb]
                out_ind, ds = self.sys.op_diag_quad(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(Uprime*(-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op
        #for up dn sins over two orbitals
        for site in self.aorb_l:
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_sp + self.flip_orb]
                out_ind, ds = self.sys.op_diag_quad(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(Uprime*(-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op
        #for dn up sins over two orbitals
        for site in self.aorb_l:
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site + self.flip_sp, site + self.flip_orb]
                out_ind, ds = self.sys.op_diag_quad(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(Uprime*(-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op
        return H.tocsr()

    def hubbard_gammaJH(self, gJH=0):
        """ create hubbard gamma JH, made for 2 orbital per site
        going over half the uplist corresponding to first set of 
        orbitals(alpha < beta) and (beta, alpha)
        c_{i alpha up}' c_{i alpha dn} * c_{i beta dn}' c_{i beta up}
        """
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)

        #mixed up dn
        #------------------------------------------
        op1 = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        op2 = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        for site in self.aorb_l:
            #alpha up beta dn 
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_orb + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #alpha dn beta up
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                if self.model == 1:
                    coup_flip = [site + self.flip_orb, site + self.flip_sp]
                    out_ind, ds = self.sys.op_nondiag(state, coup_flip)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append((-1)**(ds))
                elif self.model == 2:
                    coup = [site + self.flip_sp, site + self.flip_orb]
                    out_ind, ds = self.sys.op_nondiag(state, coup)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append((-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #with extra minus added to account for commutating
            #extra conjugate for flip part
            if self.model == 1: H = H + (-1) * gJH * op1 * op2.getH()
            if self.model == 2: H = H + (-1) * gJH * op1 * op2
        #------------------------------------------
        # conjugate term inverting alpha and beta
        #------------------------------------------
        #mixed up dn
        op1 = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        op2 = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        for site in self.aorb_l:
            #beta up alpha dn 
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                if self.model == 1:
                    coup = [site + self.flip_orb, site + self.flip_sp]
                    out_ind, ds = self.sys.op_nondiag(state, coup)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append((-1)**(ds))
                elif self.model == 2:
                    coup_flip = [site + self.flip_sp, site + self.flip_orb,]
                    out_ind, ds = self.sys.op_nondiag(state, coup_flip)
                    if out_ind != -1:
                        row.append(state_ind)
                        col.append(out_ind)
                        data.append((-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #beta dn alpha up
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup_flip = [site, site + self.flip_sp + self.flip_orb]
                out_ind, ds = self.sys.op_nondiag(state, coup_flip)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #with extra minus added to account for commutating
            #extra conjugate for flip part
            if self.model == 1: H = H + (-1) * gJH * op1 * op2.getH()
            if self.model == 2: H = H + (-1) * gJH * op1.getH() * op2.getH()
        #------------------------------------------
        # Second term in the Hamiltonian
        #------------------------------------------
        #for up dn for same orbital
        op1 = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        op2 = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        for site in self.aorb_l:
            #alpha up dn
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #beta dn up
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup_flip = [site + self.flip_orb, site + self.flip_orb + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup_flip)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #with extra minus added to account for commutating
            #extra conjugate for flip part
            H = H + (-1) * gJH * op1 * op2.getH()
        #------------------------------------------
        # conjugate term inverting alpha and beta
        #------------------------------------------
        #for up dn for same orbital
        op1 = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        op2 = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        for site in self.aorb_l:
            #beta up dn
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site + self.flip_orb, site + self.flip_orb + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #alpha dn up
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup_flip = [site, site + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup_flip)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #with extra minus added to account for commutating
            #extra conjugate for flip part
            H = H + (-1) * gJH * op1 * op2.getH()

        return H.tocsr()
                        
    def onsite_mu(self, mu=1, mup=None, mdn=None):
        """ create hubbard u operators hop_val should be list
            H = mup*(Nup) + mdn*(Ndn)
        """
        if mup is None and mdn is None:
            mup = mu; mdn = mu
        if np.array(mup).shape==():
            mup = [mup]*len(self.uplist)
            mdn = [mdn]*len(self.uplist)
        elif len(mup) != len(self.uplist):
            raise ValueError('Length of hop_val should be same as lattice sites')
            
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        for site_ind, site in enumerate(self.uplist):
            #nup
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(mup[site_ind]*(-1)**(ds))
            op_up = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op_up
            #ndn
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site+self.flip_sp)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(mdn[site_ind]*(-1)**(ds))
            op_dn = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op_dn
        return H.tocsr()

    def number_op(self, site):
        """ create hubbard u operators hop_val should be list
            H = Nup_i + Ndn_i
        """ 
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        #nup
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            out_ind, ds = self.sys.op_diag(state, site)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op_up = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        H = H + op_up
        #ndn
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            out_ind, ds = self.sys.op_diag(state, site+self.flip_sp)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op_dn = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        H = H + op_dn
        return H.tocsr()

    def hubbard_sparse(self, t=-1, alpha=0, hx=1, hy=1, return_rep=False,
                       Bfield=False, get_conj=True):
        """lacks the ability to handle index[a,b] where a>=b
        light implemented only in first layer using manual
        multiplicity factor 
        """
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        ham_rep = "H = "
        #if self.dim == '1d':
        #    hop_list, em_list = self.index.kin_hop()
        if self.dim =='2d' or self.dim=='1d':
            hopx_list, hopy_list = self.index.kin_hop()
            hop_list = hopx_list + hopy_list
            #print(hop_list)
            #mulplicative factor for adding light in each column
            mult_y = np.ones(len(hopy_list))
            mult_x = np.ones(len(hopx_list))
            if Bfield:
                if not self.periodic:
                    mult_y = [[i+1]*(self.Ly-1) for i in range(self.Lx)]
                    mult_y = self.index.flatten_list(mult_y)
                    mult_y = mult_y * 2
                    mult_x = [1+i//(self.Lx-1) for i in range((self.Lx-1)*self.Ly)]
                    mult_x = mult_x * 2
                if self.periodic:
                    mult_y = [[i+1, i+1]*(self.Ly-1) for i in range(self.Lx)]
                    mult_y = self.index.flatten_list(mult_y)
                    mult_y = mult_y * 2
                    mult_x = [1+i//self.Lx for i in range(self.Lx*self.Ly)]
                    mult_x = mult_x * 2
        count_y = 0; count_x = 0
        #print(hop_list)
        for hop_ind in hop_list:
            row = []
            col = []
            data = []
            h_f = 1
            #adding light factor
            if self.dim=='2d' or self.dim=='3d':
                if hop_ind in hopy_list:
                    h_f = np.exp(2*np.pi*1j*alpha*mult_y[count_y])
                    h_f = h_f * hy
                    #if h_f != 1:    print("Something fishy")
                    #print(hop_ind,mult_y[count_y])
                    count_y += 1
                elif hop_ind in hopx_list:
                    #h_f = np.exp(2*np.pi*1j*alpha_x*mult_y[count_y])
                    h_f = hx
                    #count_x += 1
            #constructing the hamiltonian
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_nondiag(state, hop_ind)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append(t * h_f * ( (-1)**(ds) ))
            op = coo_matrix((data, (row,col)), shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype) 
            H = H + op
        
        if get_conj: H = H + H.getH()
            
        return H.tocsr()

    def heisenberg(self, J=0.1, Jz=0.1, return_op=False, return_rep=False):
        """Should only be defined in basis with Nup and Ndn not individually
        conserved, raises error 
        """
        ham_rep = ""
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        if self.dim == '1d' or self.dim == '2d' or self.dim=='3d':
            if self.spin_op is None:
                op = self.op_spin()
            else:
                op = self.spin_op

            coupx_list,coupy_list = self.index.nn_int()
            coup_list = coupx_list + coupy_list
            #print(coup_list)
            for site in coup_list:
                for sigma in self.spin_names:
                    if sigma == self.spin_names[2]:
                        H = H + Jz*op[sigma + str(site[0])] @ \
                                   op[sigma + str(site[1])]
                        #hamiltonian_representation
                        ham_rep = ham_rep + 'Jz.'+sigma+str(site[0]) \
                                  +'.'+sigma+str(site[1]) + ' + '
                    else:
                        H = H + J*op[sigma + str(site[0])] @ \
                                  op[sigma + str(site[1])]
                        #hamiltonian_representation
                        ham_rep = ham_rep + 'J.'+sigma+str(site[0]) \
                                  +'.'+sigma+str(site[1]) + ' + '
        if return_op:    
            return H.tocsr(), op
        else:
            return H.tocsr()
            
        
    def spin_curr(self, site_i, site_j, compute_all=False):
        """ create spin operators, only fectching the operators for
        speficfic bond currents and only z components as of now
        """
        #x_list,y_list = self.index.nn_int()
        #c_list = x_list + y_list
        #print(y_list)
        result = dict()        
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            coup = [site_i, site_j]
            out_ind, ds = self.sys.op_nondiag(state, coup)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op1 = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            coup = [site_i+self.flip_sp, site_j+self.flip_sp]
            out_ind, ds = self.sys.op_nondiag(state, coup)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op2 = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        op = op1 + op2 
        result['ch'] = op.tocsr()
        op = op1 - op2
        result['sz'] = op.tocsr()

        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            coup = [site_i, site_j+self.flip_sp]
            out_ind, ds = self.sys.op_nondiag(state, coup)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op3 = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        row = []; col = []; data = []
        for state_ind, state in enumerate(self.sys.basisN):
            coup = [site_j, site_i+self.flip_sp]
            out_ind, ds = self.sys.op_nondiag(state, coup)
            if out_ind != -1:
                row.append(state_ind)
                col.append(out_ind)
                data.append((-1)**(ds))
        op4 = coo_matrix((data, (row,col)), 
                 shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
        op4 = op4.conj().T
        op = op3 + op4 
        result['sx'] = self.spin_unit*op.tocsr()
        op = -1j*op3 + 1j*op4
        result['sy'] = self.spin_unit*op.tocsr()
            
        return result
        
    def spin_orbit(self, tso = 0):
        """ create spin-orbit coupling
        works in the model 1. not model 2, since reordering required
        """
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        x_list,y_list = self.index.nn_int()
        c_list = x_list + y_list
        #print(c_list)
        # y_hop 
        for site in y_list:
            #c_iup' c_jdn
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site[0], site[1] + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #c_idn' c_jup
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup_flip = [site[1], site[0] + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup_flip)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #dagger to flip it
            H = H + 0.5*1j*(op1 + op2.getH())*tso

        # x_hop
        for site in x_list:
            #c_iup' c_jdn
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site[0], site[1] + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op1 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            #c_idn' c_jup
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup_flip = [site[1], site[0] + self.flip_sp]
                out_ind, ds = self.sys.op_nondiag(state, coup_flip)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op2 = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + 0.5*(op2.getH() - op1)*tso

        H = H + H.getH()

        return H.tocsr()
    
    def check_hermitian(self, a, rtol=1e-05, atol=1e-08):
        return np.allclose(a, a.conj().T, rtol=rtol, atol=atol)
    
    def check_equal(self, a, b, rtol=1e-05, atol=1e-06):
        return np.allclose(a, b, rtol=rtol, atol=atol)
    
    def mz_sq(self):
        """ create hubbard u operators 
        Op = (nup + ndn - 2nup*ndn)
        """
        result = dict()
        H = coo_matrix((self.sys.Ns, self.sys.Ns), dtype=self.dtype)
        for site in self.uplist:
            #nup
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op_up = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op_up
            #ndn
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                out_ind, ds = self.sys.op_diag(state, site+self.flip_sp)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op_dn = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H + op_dn
            #nup*ndn
            row = []; col = []; data = []
            for state_ind, state in enumerate(self.sys.basisN):
                coup = [site, site + self.flip_sp]
                out_ind, ds = self.sys.op_diag_quad(state, coup)
                if out_ind != -1:
                    row.append(state_ind)
                    col.append(out_ind)
                    data.append((-1)**(ds))
            op = coo_matrix((data, (row,col)), 
                     shape=((self.sys.Ns,self.sys.Ns)), dtype=self.dtype)
            H = H - 2*op
        return H.tocsr()
