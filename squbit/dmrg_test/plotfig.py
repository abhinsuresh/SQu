import sys, numpy as np, h5py, os

sys.path.append('../../plot_manager')
from plot2d import plotfig

data = dict();values=dict()
nm="07"
Ns = 8
trange = np.arange(0,100,0.1)

spinx = h5py.File("results/"+nm+".hdf5","r")["spinx"][:]
spiny = h5py.File("results/"+nm+".hdf5","r")["spiny"][:]
spinz = h5py.File("results/"+nm+".hdf5","r")["spinz"][:]
print(spinz.shape)

result = np.zeros((len(trange),3))
result[:,0] = spinx[:,0]
result[:,1] = spiny[:,0]
result[:,2] = spinz[:,0]

data['x_plot'] = trange; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time}$';
values['ylabel1'] = r'$\mathrm{S^\alpha_1}$'
Cl = ['C0','C2','C3']
Gs = dict()

sf = np.array([[0,3]])
plotfig(data, nm, values, Cl=Cl, lw=0.4, sf=sf, minor=1, fs=6)
