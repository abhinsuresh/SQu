#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import numpy as np, time
import scipy.linalg as la

import sys
sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Spin
from density_core import partial_psi_trace, entanglement_entropy

sys.path.append('../../plot_manager/')
from plot2d import plotfig

float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
Lx  = 4
Ly  = 2
N   = 1
S   = 1/2
J   = 0.1
Jz  = 0.11
Bz  = 0.001
#derived parameters
L   = Lx*Ly
start = time.time()

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
Se = spin.op
hop_list = [[i, i+2] for i in range(0,Ly*(Lx-1),Ly)] + \
           [[i, i+2] for i in range(1,Ly*(Lx-1),Ly)] + \
           [[i, i+1] for i in range(0,Ly*Lx,Ly)]
#print(hop_list)

H0   = spin.heisenberg_2d(J=J,Jz=Jz, Lx=Lx, Ly=Ly, hop_list=hop_list) + Bz*Se['sz0']

w,v = la.eigh(H0.toarray())
print("final energy: ", w[0])
s = 0
print(v[:,s].conj() @ Se['sz0'] @ v[:,s])
print(v[:,s].conj() @ Se['sz1'] @ v[:,s])
print(v[:,s].conj() @ Se['sz2'] @ v[:,s])
print(v[:,s].conj() @ Se['sz3'] @ v[:,s])
rho1 = partial_psi_trace(v[:,0],np.sqrt(len(v[:,0])))
#print(entanglement_entropy(rho1, True))
print(entanglement_entropy(rho1,full=True ))

