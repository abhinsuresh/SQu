import numpy as np, time, sys, h5py
import scipy.linalg as la

sys.path.append('../')
from hamiltonian_core import Spin
from evolve_step import evolve 
sys.path.append('../../plot_manager/')
from plot2d import plotfig

HBAR = 0.658211951
dim = '1d'
nm = '07'
#parameters
L  = 8
S   = 1/2
J   = 0.1
Jz  = 0.2
Bz  = 0.01
Bx  = 0.2
dt = 0.1
omega = 0.05/0.658211
#derived parameters
start = time.time()

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
Se = spin.op

H0   = spin.heisenberg(J=J,Jz=Jz) + Bz*Se['sz0']

w,v = la.eigh(H0.toarray())
print("Final energy: ", w[:2])
s = 0
print(v[:,s].conj() @ Se['sz0'] @ v[:,s])
print(v[:,s].conj() @ Se['sz1'] @ v[:,s])
print(v[:,s].conj() @ Se['sz2'] @ v[:,s])
print(v[:,s].conj() @ Se['sz3'] @ v[:,s])

psi = v[:,0].reshape(len(v[:,0]),1)
print(psi.shape)
spinz = np.zeros((1000,8))
spinx = np.zeros((1000,8))
spiny = np.zeros((1000,8))
#Ht = H0 + Bx*Se['sx0']
#Ut = evolve(Ht, [], psi, dt, method='CN', time_dep=0)
for ind, t_now in enumerate(np.arange(0,100,0.1)):
    #print([(psi.conj().T @Se['sz'+str(i)]@psi).item().real for i in range(L)])
    spinz[ind,:] = [(psi.conj().T @Se['sz'+str(i)]@psi).item().real for i in range(L)]
    spiny[ind,:] = [(psi.conj().T @Se['sy'+str(i)]@psi).item().real for i in range(L)]
    spinx[ind,:] = [(psi.conj().T @Se['sx'+str(i)]@psi).item().real for i in range(L)]
    Ht = H0 + np.sin(omega * t_now) * Bx*Se['sx0']
    #Ht = H0 + Bx*Se['sx0']
    psi = evolve(Ht, 0, psi, dt, method='CN', time_dep=1)
    #psi = Ut @ psi
print((psi.conj().T @Se['sx'+str(0)]@psi).item().real)
print((psi.conj().T @Se['sz'+str(0)]@psi).item().real)
print((psi.conj().T @Se['sy'+str(0)]@psi).item().real)
with h5py.File('results/'+nm+'.hdf5', 'w') as f:
    f.create_dataset("spinz", data = spinz)
    f.create_dataset("spinx", data = spinx)
    f.create_dataset("spiny", data = spiny)
f.close()
