import numpy as np, time, h5py, sys, os, datetime
import scipy.linalg as la
import scipy.sparse.linalg as sla

from quspin.basis import spinful_fermion_basis_general
import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

sys.path.append('../')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian
from density_core import entanglement_entropy, partial_psi_trace
from numba_wrapper import updn_list
nm = os.path.basename(__file__)[:2]
#print("Evaluation file name:\t", os.path.basename(__file__), 'res:\t', nm)
#print("Evaluation time:\t", datetime.datetime.now())
#parameters
Lx      = 4
Ly      = 2
t       = 1.0
U       = 8.0
J       = 1
Jz      = 2
Bz      = 1

JH      = 0.0
Uprime  = 0.0
gJH     = 0.0
orb     = 1
periodic = 0
L   = Lx*Ly*orb
#System creation
start = time.time()
sys = Basis(L=L, N=L, model = 1)
print('system size\t', sys.Ns)

#Hamiltonian
ham = Hamiltonian(sys, periodic=periodic, Lx=Lx, Ly=Ly)
H_kin = ham.hubbard_sparse(t=t, alpha = 0)
H_U = ham.hubbard_U(U=U)
op = ham.op_spin()
H_j = ham.heisenberg(J=J, Jz=Jz, return_op=False)
Hb = Bz *(op['sz0'] )
#H_gJH = ham.hubbard_gammaJH(gJH=gJH)
#H_JH = ham.hubbard_JH(JH=JH)
#H_Uprime = ham.hubbard_Uprime(Uprime=Uprime)
#H_mu = ham.onsite_mu(mu=mu)
#H_tso = ham.spin_orbit(tso=tso)

H = H_U + H_kin + H_j + Hb

linalg.init()
a_gpu = gpuarray.to_gpu(H.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
w = w_gpu.get()
v = vr_gpu.get().transpose()
b = spinful_fermion_basis_general(8, Nf = updn_list(8)) 
#density if ture divides the entropy by number of sites
spinz = np.zeros(L)
for site in range(L):
    spinz[site] = np.real(v[:,0].conj()@\
                         op['sz'+str(site)]@v[:,0])
    #spinx[site] = np.real(v[:,0].conj()@\
    #                     op['sx'+str(site)]@v[:,0])
print('Energy values:\t', w[0])
print('spinz values:\t', spinz[0])
print('spinz values:\t', spinz[4])
print('spinz values:\t', spinz[1])
print('spinz values:\t', spinz[5])
ent = b.ent_entropy(v[:,0], sub_sys_A=([0,1,4,5],[0,1,4,5]), enforce_pure=True, alpha=1.0, density=False)["Sent_A"]
print('entropy:\t', ent)
"""
ent = b.ent_entropy(v[:,0], sub_sys_A=([0,1],[0,1]), enforce_pure=True, alpha=1.0, density=False)["Sent_A"]
print('entropy:\t', ent)
ent = b.ent_entropy(v[:,0], sub_sys_A=([0,4],[0,4]), enforce_pure=True, alpha=1.0, density=False)["Sent_A"]
print('entropy:\t', ent)
ent = b.ent_entropy(v[:,0], sub_sys_A=([0,1,4],[0,1,4]), enforce_pure=True, alpha=1.0, density=False)["Sent_A"]
print('entropy:\t', ent)
"""
end = time.time()
print('time taken: ', "%1.2f"%(end - start),' s')
