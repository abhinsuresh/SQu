import sys

def unique(list1): 
    unique_list = []
    for x in list1:
        if x not in unique_list:
            unique_list.append(x)
    return unique_list

#--------------------------------------------------------------------
def get_cite_list(tex_file):
    data = open(tex_file,"r").read()
    words_list = data.split("\n")
    cite_key_list = []
    for ind, word in enumerate(words_list):
        while r"\cite{" in word:
            start = word.find(r"\cite{")
            end = word[start:].find("}")
            citation = word[start+6: start+end]
            if len(citation.split(",")) > 1:
                for cite_key in citation.split(","):
                    cite_key_list.append(cite_key.strip())
            else: cite_key_list.append(citation)
            word = word[start+end+1:]
    key_list = unique(cite_key_list)
    return key_list

#--------------------------------------------------------------------
def generate_dict(key, bib_file="qttg.bib"):
    bib_file = open(bib_file,"r").read()
    #key = "Gueckstock2021"
    start = bib_file.find(key)
    if start == -1: 
        print("!!! Key not found in .bib file !!!")
        return None
    end = bib_file[start:].find("@")
    cite = bib_file[start: start + end]
    info=dict()
    info['author'] = []
    for cite_ele in cite.split("\n"):
        line = cite_ele.strip()
        if line.lower().startswith("author"):
            author_list = line.split("=")[1].strip()[1:-2]
            for author in author_list.split(" and "):
                info["author"].append(author.strip())
        if line.lower().startswith("title"):
            info["title"] = line.split("=")[1].strip()[1:-2]
        if line.lower().startswith("journal"):
            info["journal"] = line.split("=")[1].strip()[1:-2]
        if line.lower().startswith("pages") or line.startswith("article-number"):
            info["pages"] = line.split("=")[1].strip()[1:-2]
        if line.lower().startswith("volume"):
            info["volume"] = line.split("=")[1].strip()[1:-2]
        if line.lower().startswith("year"):
            info["year"] = line.split("=")[1].strip()[1:-2]
        if line.lower().startswith("doi") or line.lower().startswith("url"):
            doi = line.split("=")[1].strip()[1:-2]
            if doi.startswith("https:"):
                info["doi"] = line.split("=")[1].strip()[1:-2]
            else:
                info["doi"] = "https://doi.org/"+line.split("=")[1].strip()[1:-2]
    return info

#--------------------------------------------------------------------
def generate_bibitem(info, href=True):
    author_type = 1
    if "doi" not in info.keys(): href= False
    
    out = r"\bibitem{"+key+"}\n"
    a_len = len(info["author"])
    a_end = -1
    if a_len > 10: a_end = 9
    
    if len(info["author"][0].split(",")) > 1: author_type = 2

    if author_type == 1:
        #author format "first-name middle-name last-name"
        for author in info["author"][:a_end]:
            for name in author.split(" ")[:-1]:
                out += name[0] + ".~"
            out += author.split(" ")[-1] + ", "

        #no comma if only two authors
        if a_len == 2:
            out = out[:-2] + " "

        #adding last author after and
        out = out + "and "
        author = info["author"][a_end]
        for name in author.split(" ")[:-1]:
            out += name[0] + ".~"
        out += author.split(" ")[-1] + ", "
        if a_end ==9:
            out = out[:-2] + " "
            out = out + "\emph{et al}, "
    

    if author_type == 2:
        #author format "last-name, first-name middle-name"
        for author in info["author"][:a_end]:
            for name in author.split(",")[1:]:
                for sub_name in name.strip().split(" "):
                    out += sub_name[0] + ".~"
            out += author.split(",")[0] + ", "

        #no comma if only two authors
        if a_len == 2:
            out = out[:-2] + " "

        #adding last author after and
        out = out + "and "
        author = info["author"][a_end]
        for name in author.split(",")[1:]:
            for sub_name in name.strip().split(" "):
                out += sub_name[0] + ".~"
        out += author.split(",")[0] + ", "
        if a_end ==9:
            out = out[:-2] + " "
            out = out + "\emph{et al}, "
    
    #adding title 
    out += info["title"] + ", "
    if not href: out += info["journal"] + " "
    if href: out +=r"\href{"+ info["doi"] +"}{"+ info["journal"] +" "
    if "volume" in info.keys(): out += r"{\bf " + info["volume"] + r"}" + ", "
    if "pages" in info.keys(): out += info["pages"].split(" ")[0] + ", "
    out += r"(" + info["year"] + r")"
    if href: out += "}."
    else: out +="."
    
    return out

key_list = get_cite_list("NiO.tex")
key = key_list[25]
info = generate_dict(key)
#print(info["author"])
out = generate_bibitem(info)
print(out)

out = ""
for key_ind, key in enumerate(key_list):
    #print(key_ind, key)
    info = generate_dict(key)
    if info is not None: out += generate_bibitem(info) + "\n\n"

fout = open("out.bbl", "w")
fout.write(out)
fout.close()
