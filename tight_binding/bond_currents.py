import numpy as np
import h5py
import scipy.linalg as la
from mn3sn_core import Lattice
from hamiltonian_core import Op
from evolve_step import rk4_rho
from numba_wrapper import vector, unit_vector, flatten_list 
from numba_wrapper import fermi_fun, l_int_t1, l_int_t2 
from numba_wrapper import find_mid
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

tf = 3
dt = 0.1
temp = 300
r = np.array([0,0,100])
nm = 'test'
def vec(t):
    omega_l = 1.5/0.6582119569 
    s = 25
    t_0 = 100
    E0 = 0
    Et = E0*np.cos(omega_l*t) * np.exp(-(t - t_0)*(t - t_0)/(2*s*s))
    return Et
theta_l = 0 * np.pi/180
lp_vec = np.array([np.cos(theta_l), np.sin(theta_l), 0])
pulse =  vec(0) * lp_vec

#create the system
#-----------------
lat = Lattice(Lx=10,Ly=10,layers=2,ad_pt=0)
ham = Op(lat, t0=-1, tj=-0.5, t1=-0.5, lambda_z=0.5)
H = ham.create_H(ham.cspins.s)
#print(lat.bonds)
r = np.array([0,0,100])
#print(l_int_t1(lat, lat.bonds[2], r, N=200))
#print(l_int_t1(lat, lat.bonds[2], r))
#print(lat.bond_uv(5,1))

#find the ground state
#---------------------
w,v = la.eigh(H)
rho_eq = np.zeros(H.shape, dtype=np.complex128)
mu = (w[199] + w[200])/2
print('mu:', mu)
for i in range(len(w)):
    rho_eq = rho_eq + np.outer(v[:,i], v[:,i].conj()) *\
                 fermi_fun(w[i], mu, temp)
rho = rho_eq
print('trace rho:', np.trace(rho))
print(rho.shape)
print(lat.bonds.shape)

#time evolution initialization
#-----------------------------
times = np.arange(0, tf + dt, dt)
curr = np.zeros((4, len(lat.bonds), len(times)))
mag = np.zeros((3, len(times)))
lp_pulse = np.zeros((3, times.shape[0]))

for t_ind, t_now in enumerate(times):
    #01_printing out output
    print(t_now)
    #finding the magnetic field
    for ind, bond in enumerate(lat.bonds):
        It = ham.spin_curr_rho(bond, rho, H, 'i')
        #curr[0, ind, t_ind] = It
        mag[:, t_ind] = mag[:, t_ind] + It*l_int_t1(lat, bond, r) 
        #curr[1, ind, t_ind] = ham.spin_curr_rho(bond,rho,H, 'x')
        #curr[2, ind, t_ind] = ham.spin_curr_rho(bond,rho,H, 'y')
        #curr[3, ind, t_ind] = ham.spin_curr_rho(bond,rho,H, 'z')
    print('measured')
    #------------------------------------------------------
    #04_create_ham
    pulse = lp_vec * vec(t_now)
    lp_pulse[:,t_ind] = pulse
    H = ham.create_H(ham.cspins.s, lp_vec=pulse) 
    #------------------------------------------------------
    #05_evolve psi
    rho = rk4_rho(H, rho, dt)

with h5py.File('results/bc'+nm+'.hdf5', 'w') as f:
    f.create_dataset("curr", data = curr,
         dtype=np.float64, compression="gzip")
    f.create_dataset("mag", data = mag,
         dtype=np.float64, compression="gzip")
f.close()
print(time.time()-start, '\t seconds')


