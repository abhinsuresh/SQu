import numpy as np
import kwant
import matplotlib
import matplotlib.pyplot as plt
import warnings
from constants import HBAR, SIG_X, SIG_Y, SIG_Z, UNIT

warnings.filterwarnings("ignore")
matplotlib.use('Agg')
tri = np.array(((1, 0), (0.5, 0.5 * np.sqrt(3))))
tri_3d = np.array(((1, 0, 0), (0.5, 0.5 * np.sqrt(3), 0)))

def make_kagome(Lx,Ly):
    sys = kwant.Builder()

    #lat = kwant.lattice.kagome()
    syst = kwant.Builder()

    lat = kwant.lattice.Polyatomic(tri_3d, ((0, 0, 0),) \
                    + tuple(0.5*1*tri_3d), name='', norbs=1)
    a, b, c = lat.sublattices 

    for i in range(Lx-1):
        for j in range (Ly-1):
            syst[a(i+1, j+1)] = 0.0
            syst[b(i, j+1)] = 0.0
            syst[c(i+1, j)] = 0.0
            syst[c(i+1, j+1)] = 0.0
            syst[b(i+1, j+1)] = 0.0
    syst[lat.neighbors(1)]=-1
    sysf = syst.finalized()  
    return sysf


def make_kagome_bilayer(Lx,Ly):
    sys = kwant.Builder()

    lat_1 = kwant.lattice.kagome()

    a1, b1, c1 = lat_1.sublattices 

    for i in range(Lx-1):
        for j in range (Ly-1):
            sys[a1(i+1, j+1)] = 0.0
            sys[b1(i, j+1)] = 0.0
            sys[c1(i+1, j)] = 0.0
            sys[c1(i+1, j+1)] = 0.0
            sys[b1(i+1, j+1)] = 0.0
    sys[lat_1.neighbors()]=-1

    #defining second layer with atoms shifted by (not exact) amt
    lat_2 = kwant.lattice.Polyatomic(tri, 
            np.array(((0, 0),) + tuple(0.5 * 1 * tri)) + 
                    (0.5*1, 0.6*np.sqrt(3)*1/4 ),
                     name='', norbs=1)
    a2, b2, c2 = lat_2.sublattices
    for i in range(Lx-1):
        for j in range (Ly-1):
            sys[a2(i+1, j+1)] = 0.0
            sys[b2(i, j+1)] = 0.0
            sys[c2(i+1, j)] = 0.0
            sys[c2(i+1, j+1)] = 0.0
            sys[b2(i+1, j+1)] = 0.0
    sys[lat_2.neighbors()]=-1

    #interlayer hoppings
    sys[kwant.builder.HoppingKind((1, 0), a1, b2)] = -1
    sys[kwant.builder.HoppingKind((1, 0), c1, b2)] = -1

    sys[kwant.builder.HoppingKind((0, 0), c1, a2)] = -1
    sys[kwant.builder.HoppingKind((0, 0), b1, a2)] = -1

    sys[kwant.builder.HoppingKind((0, 1), b1, c2)] = -1

    sys[kwant.builder.HoppingKind((0, 1), a1, c2)] = -1
    
    sysf = sys.finalized()  
    return sysf


def make_kagome_bilayer_3d(Lx,Ly):
    sys = kwant.Builder()

    #lat_1 = kwant.lattice.kagome()
    lat_1 = kwant.lattice.Polyatomic(tri_3d, ((0, 0, 0),) \
                    + tuple(0.5*1*tri_3d), name='', norbs=1)

    a1, b1, c1 = lat_1.sublattices 

    for i in range(Lx-1):
        for j in range (Ly-1):
            sys[a1(i+1, j+1)] = 0.0
            sys[b1(i, j+1)] = 0.0
            sys[c1(i+1, j)] = 0.0
            sys[c1(i+1, j+1)] = 0.0
            sys[b1(i+1, j+1)] = 0.0
    sys[lat_1.neighbors()]=-1

    #defining second layer with atoms shifted by (not exact) amt
    lat_2 = kwant.lattice.Polyatomic(tri_3d, 
            np.array(((0, 0, 0),) + tuple(0.5*1*tri_3d)) + 
                    (0.5*1, 0.6*np.sqrt(3)*1/4, 0.7689*1),
                     name='', norbs=1)
    a2, b2, c2 = lat_2.sublattices
    for i in range(Lx-1):
        for j in range (Ly-1):
            sys[a2(i+1, j+1)] = 0.0
            sys[b2(i, j+1)] = 0.0
            sys[c2(i+1, j)] = 0.0
            sys[c2(i+1, j+1)] = 0.0
            sys[b2(i+1, j+1)] = 0.0
    sys[lat_2.neighbors()]=-1

    #interlayer hoppings
    sys[kwant.builder.HoppingKind((1, 0), a1, b2)] = -1
    sys[kwant.builder.HoppingKind((1, 0), c1, b2)] = -1

    sys[kwant.builder.HoppingKind((0, 0), c1, a2)] = -1
    sys[kwant.builder.HoppingKind((0, 0), b1, a2)] = -1

    sys[kwant.builder.HoppingKind((0, 1), b1, c2)] = -1

    sys[kwant.builder.HoppingKind((0, 1), a1, c2)] = -1
    
    sysf = sys.finalized()  
    return sysf

