import numpy as np
from mn3sn_core import Lattice
from hamiltonian_core import Op
from numba_wrapper import vector, unit_vector, flatten_list 
from numba_wrapper import l_int_t1, l_int_t2 
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})


lat = Lattice(Lx=2,Ly=2,layers=1,ad_pt=0)

#print(sys.hopy1_list)
lat.plotfig('gg', lw=0.2, mew=0.2, mks=3)
print(lat.bonds)
#print(lat.pos_atoms[lat.bonds[0,0]])
#print(lat.pos_atoms[lat.bonds[0,1]])

r = np.array([0,0,100])
print(l_int_t1(lat, lat.bonds[2], r, N=200))
print(l_int_t1(lat, lat.bonds[2], r))
#print(lat.bond_len)
#print(lat.pos_atoms)  #ndarray
#print(lat.bond_v(5,1))
#print(lat.bond_v(5,1)/2)
#print(lat.bond_uv(5,1))

ham = Op(lat, t0=1, tj=0, t1=0, lambda_z=0)

H = ham.create_H(ham.cspins.s)
#print(H.real)
#print(H.imag)

It = 1



