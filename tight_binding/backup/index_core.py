import numpy as np

#functions
def intra_Sn(Lx, Ly, bi, return_sd_hop=0):
    """Find the intra layer hopping list for triangular plane lattice
    and extend for bilayer
    """
    hopx = []
    hopy = []
    hopx1 = []
    for i in range(Ly):
        hopx.append(range(Lx*i, Lx*i + Lx, 1))
    for i in range(Lx):
        hopy.append(range(i, Lx*(Ly-1) + i + 1, Lx))

    #special hopping for jsd
    for i in range(Lx-1):
        hopx1.append(range(i, i + Lx*Ly - Lx + 1, Lx))

    hopx_list = []
    hopx1_list = []
    hopy1_list = []
    hopy2_list = []
    for ele in hopx:
        hopx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
    hopx_list = flatten_list(hopx_list)

    for ele in hopy:
        hopy1_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
    hopy1_list = flatten_list(hopy1_list)

    for ele in hopy[1:]:
        hopy2_list.append( [[ele[i], ele[i]+Lx-1] for i in range(len(ele)-1)]  )
    hopy2_list = flatten_list(hopy2_list)


    #special hoppping for jsd
    for ele in hopx1:
        hopx1_list.append( [[ele[i], ele[i]+1] for i in range(len(ele))]  )
    hopx1_list = flatten_list(hopx1_list)
    sd_hop = hopy2_list + hopx1_list + hopy1_list
    if bi == 2:
        sd_bi = np.array(sd_hop)
        sd_bi = np.array([sd_bi[i]+Lx*Ly for i in range(len(sd_hop))])
        sd_hop = np.concatenate((sd_hop, sd_bi), axis=0)
    
    #print(hopx1_list)


    #finding corresponding jsd terms for classical spins
    ccoup = []
    ccoup.append(hopy2_list)
    shop = []
    for i in range(Lx-1):
        shop.append(hopx_list[i : i +(Lx-1)*Ly:Lx-1])
    shop = flatten_list(shop)
    ccoup.append(shop)
    ccoup.append(hopy1_list)
    ccoup = flatten_list(ccoup)

    #making np.array and exteding for bilayer    
    hopx_list = np.array(hopx_list, dtype=np.int8)
    hopy1_list = np.array(hopy1_list, dtype=np.int8)
    hopy2_list = np.array(hopy2_list, dtype=np.int8)
    if bi == 2:
        hopx_list  = np.append(hopx_list,  hopx_list  + Lx*Ly, axis=0)
        hopy1_list = np.append(hopy1_list, hopy1_list + Lx*Ly, axis=0)
        hopy2_list = np.append(hopy2_list, hopy2_list + Lx*Ly, axis=0)

    hop_list = np.append(hopx_list, hopy1_list, axis=0)
    hop_list = np.append(hop_list, hopy2_list, axis=0)

    if return_sd_hop == 0:
        return hopx_list, hopy1_list, hopy2_list, hop_list, ccoup
    elif return_sd_hop==1:
        return hopx_list, hopy1_list, hopy2_list, hop_list, ccoup, np.array(sd_hop)

def link_MnSn(Lx, Ly, ribbon, bi):
    """Find the classical spin Mn connecting the intra layer
    Sn hoppings adding bilayer part doen outside
    """
    st = (Lx-1)*(Ly-1)
    cup_x = []
    for i in range(Ly):
        cup_x.append(np.arange(st + i, (st+i) + Ly*(Lx-1), Ly))
    cup_x = np.array(cup_x, dtype=np.int8)
        
    #print(hopy1_list)
    st = (Lx-1)*(Ly-1) + Ly*(Lx-1)
    cup_y1 = []
    for i in range(Lx):
        cup_y1.append(np.arange(st + i*(Ly-1), st + (i+1)*(Ly-1)))
    cup_y1 = np.array(cup_y1, dtype=np.int8)

    #print(hopy2_list)
    st = 0
    cup_y2 = []
    for i in range(Lx-1):
        cup_y2.append(np.arange(st + i*(Ly-1), st + (i+1)*(Ly-1)))
    cup_y2 = np.array(cup_y2, dtype=np.int8)
    
    x_cup  = np.array([j for sub in cup_x for j in sub] )
    y1_cup = np.array([j for sub in cup_y1 for j in sub] ) 
    y2_cup = np.array([j for sub in cup_y1 for j in sub] ) 

    #extending to bilayer
    if bi == 2:
        x_cup  = np.append(x_cup,  x_cup  + len(ribbon.sites)//2, axis=0)
        y1_cup = np.append(y1_cup, y1_cup + len(ribbon.sites)//2, axis=0)
        y2_cup = np.append(y2_cup, y2_cup + len(ribbon.sites)//2, axis=0)

    return x_cup, y1_cup, y2_cup

def inter_Sn(Lx, Ly):
    """Find the inter layer hopping elements of bilayer Sn, only
    exist in the presence of bilayer
    """
    hop_inter = []
    ind = 0 
    for i in range(Ly):
        for j in range(Lx):
            hop_inter.append([ind + 1*Lx*Ly, ind])
            if j != Lx-1:
                hop_inter.append([ind + 1*Lx*Ly, ind+1])
            if i != Ly-1:
                hop_inter.append([ind + 1*Lx*Ly, ind+Lx])
            ind = ind + 1
    hop_inter = np.array(hop_inter)
    return hop_inter

def hop_Pt(Lx, Ly):
    """Find the hopping in Pt layer, assuming its the
    third layer
    """
    hopx = []
    hopy = []
    for i in (np.arange(Ly)):
        hopx.append(range(Lx*i + 2*Lx*Ly, 2*Lx*Ly + Lx*i + Lx, 1))
    for i in (2*Lx*Ly + np.arange(Lx)):
        hopy.append(range(i, Lx*(Ly-1) + i + 1, Lx))

    hopx_list = []
    hopy_list = []
    for ele in hopx:
        hopx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
    hopx_list = flatten_list(hopx_list)
    for ele in hopy:
        hopy_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
    hopy_list = flatten_list(hopy_list)
    
    return hopx_list, hopy_list


def inter_Pt(Lx, Ly):
    """Find the inter layer hopping 2nd layer Sn and Pt
    """
    hop = []
    for i in range(Lx*Ly, 2*Lx*Ly):
        hop = hop + [[i, i + Lx*Ly]]
    return hop

def pos_Sn(Lx, Ly, ribbon, ccoup, bi):
    """Find the position of Sn atoms for both layers position of 
    second relative to first is not real values, but they don't
    appear in equations as i know it
    also calcualates the dir_vec the direction of Mn and 
    bond hopping directions
    """
    pos_Sn1 = []
    for j in range(Ly):
        for i in range(Lx):
            pos_Sn1.append([0.75 + 1*i + 0.5*j, 0.4330 + 0.8660*j, 0])
    pos_Sn1 = np.array(pos_Sn1)

    pos_Sn2 = []
    #gave c/a = 0.7689
    for j in range(Ly):
        for i in range(Lx):
            pos_Sn2.append([1.25 + 1*i + 0.5*j, 1.6*0.4330 + 0.8660*j, 0.7689])
    pos_Sn2 = np.array(pos_Sn2)
    
    # finding the direction of Mn_spin in between Sn
    dir_vec = []
    sign = 1
    for i in range(len(ribbon.sites)//bi):
        if i >= ((Lx-1)*(Ly-1) + Ly*(Lx-1)):
            sign = -1
        dir_vec.append(sign*unit_vector(pos_Sn1[ccoup[i][1]], 
                        pos_Sn1[ccoup[i][0]]))
    sign = 1
    if bi == 2:
        for i in range(len(ribbon.sites)//bi, len(ribbon.sites)):
            j = i - len(ribbon.sites)//bi
            if j >= ((Lx-1)*(Ly-1) + Ly*(Lx-1)):
                sign = -1
            dir_vec.append(sign*unit_vector(pos_Sn2[ccoup[j][1]], 
                            pos_Sn2[ccoup[j][0]]))
    dir_vec = np.array(dir_vec)

    return pos_Sn1, pos_Sn2, dir_vec

def dir_cspin(Lx, Ly, bi):
    """get the orientation of the cspins
    """
    cspin_dir = []

    for i in range((Lx-1)*(Ly-1)):
        cspin_dir.append([-1, 0., 0.])
    for i in range((Lx-1)*(Ly)):
        cspin_dir.append([0.500011, -0.86601905, 0.])
    for i in range((Lx)*(Ly-1)):
        cspin_dir.append([0.500011, 0.86601905, 0.])
    if bi==2:
        for i in range((Lx-1)*(Ly-1)):
            cspin_dir.append([-1, 0., 0.])
        for i in range((Lx-1)*(Ly)):
            cspin_dir.append([0.500011, -0.86601905, 0.])
        for i in range((Lx)*(Ly-1)):
            cspin_dir.append([0.500011, 0.86601905, 0.])

    return np.array(cspin_dir)


def dir_hop(hopx_list, hopy1_list, hopy2_list, pos_Sn1, pos_Sn2, bi):        
    """find the vector along the direction of kin hop
    """
    dir_x = []
    if bi==2: hopx_list = np.split(hopx_list,2)[0]
    for ind, hop in enumerate(hopx_list):
        dir_x.append(unit_vector(pos_Sn1[hop[1]], pos_Sn1[hop[0]]))
        if bi==2:
            dir_x.append(unit_vector(pos_Sn2[hop[1]], pos_Sn2[hop[0]]))
    dir_y1 = []
    if bi==2: hopy1_list = np.split(hopy1_list,2)[0]
    for ind, hop in enumerate(hopy1_list):
        dir_y1.append(unit_vector(pos_Sn1[hop[1]], pos_Sn1[hop[0]]))
        if bi==2:
            dir_y1.append(unit_vector(pos_Sn2[hop[1]], pos_Sn2[hop[0]]))
    dir_y2 = []
    if bi==2: hopy2_list = np.split(hopy2_list,2)[0]
    for ind, hop in enumerate(hopy2_list):
        dir_y2.append(unit_vector(pos_Sn1[hop[1]], pos_Sn1[hop[0]]))
        if bi==2:
            dir_y2.append(unit_vector(pos_Sn2[hop[1]], pos_Sn2[hop[0]]))
    return dir_x, dir_y1, dir_y2

def dir_hop_inter(hop_inter, pos_Sn1, pos_Sn2, L):        
    """find the vector along the direction of kin hop
    """
    dir_z = []
    for ind, hop in enumerate(hop_inter):
        dir_z.append(vector(pos_Sn2[hop[0]-L], pos_Sn1[hop[1]]))
    return dir_z

def flatten_list(_2d_list):
    """flatten a 2d list
    """
    flat_list = []
    for element in _2d_list:
        if type(element) is list:
            for item in element:
                flat_list.append(item)
        else:
            flat_list.append(element)
    return flat_list

def unit_vector(x2, x1):
    """Find the unit vector along x2 - x1
    """
    x1 = np.array(x1)
    x2 = np.array(x2)

    x = x2 - x1
    x_norm = np.linalg.norm(x)
    return x/x_norm

def vector(x2, x1):
    """Find the unit vector along x2 - x1
    """
    x1 = np.array(x1)
    x2 = np.array(x2)

    x = x2 - x1
    return x

def r_z(vec, th):
    """roatate the vector with z axis
    """
    vec
    mat = np.array([[ np.cos(th), np.sin(th)],\
                    [-np.sin(th), np.cos(th)]])
    return mat @ vec
