#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
from rcparams import rc_update
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MaxNLocator

x = 0.1*np.arange(100).reshape(100,1)
y = np.sin(x)
val = dict();val['x_plot']='x_plot';val['y_plot']='y_plot'
dat = dict();dat['x_plot']=x;dat['y_plot']=y
for i in range(10): 
    val['ylabel'+str(i+1)]=''
    val['xlabel'+str(i+1)]=''
Cl = ['k', 'C0', 'C2', 'C3', 'C4', 'C5', 'C6']
Ls = ['-', '-', '-', '-', '-', '-', '-']
Mk = ['', '', '', '', '', '', '']*10
Ps = [1]*100; Pf = [-1]*100
Tx = ['']
sf = np.array([[0,1],[1,2],[2,3],[3,4],[5,6],[7,8],[9,10],[11,12]])
Gs = dict();
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.2; Gs['r']=0.9
Gs['w']=0.4; Gs['h']=0.4
Lg = ['','']

#------------------------------------------------------------
#------------------------------------------------------------

def plotfig(data, fig_name, pos_Mn, pos_Sn1, pos_Sn2, hopx_list, x_cup,
            hopy1_list, y1_cup, hopy2_list, y2_cup, hop_inter, 
            dir_vec, ribbon, bi,
            values=val, 
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim = None, Cl=Cl, Ls=Ls, Mk=Mk, Ps=Ps, Pf=Pf, 
            Tx=Tx, Gs=Gs, Lg=Lg, sf=sf, **kwargs):		

    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    values = {**val, **values}; data = {**dat, **data}
    #------------------------------------------------------------
    f = data
    plot_x = f[values['x_plot']]; plot_y = f[values['y_plot']]
    #print(plot_y.shape)
    pc = 0
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 1, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0])
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel1'], labelpad=1)
    ax1.set_ylabel(values['ylabel1'], labelpad=3)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax1.set_xticks([]); ax1.set_yticks([])

    #------------------------------------------------------------
    #plotting hopings and printing spin coupling number of Sn
    plot_Sn = 1
    plot_Mn = 1
    plot_spin = 1
    ax1.axis('off')
    if plot_Sn:
        #printing atom and numbers
        ax1.plot(pos_Sn1[:,0], pos_Sn1[:,1], marker='o', ls='', color='darkgray')
        if bi==2: ax1.plot(pos_Sn2[:,0], pos_Sn2[:,1], marker='o', ls='', color='gray')
        for i, loc in enumerate(pos_Sn1):
            ax1.text(loc[0]+0.02, loc[1]-0.08, str(i), color="k", fontsize=3)
        if bi==2:
            for i, loc in enumerate(pos_Sn2):
                ax1.text(loc[0]+0.02, loc[1]-0.08, str(i+len(pos_Sn1)), color="k", fontsize=3)
        #------------------------------------------------------------
        #x_cup = [j for sub in cup_x for j in sub]
        if bi==2: hopx_list = np.split(hopx_list,2)[0] 
        for ind, hop in enumerate(hopx_list):
            ax1.plot([pos_Sn1[hop[0],0], pos_Sn1[hop[1],0]], 
                     [pos_Sn1[hop[0],1], pos_Sn1[hop[1],1]], '--', color='silver')
            if bi==2:
                ax1.plot([pos_Sn2[hop[0],0], pos_Sn2[hop[1],0]], 
                [pos_Sn2[hop[0],1], pos_Sn2[hop[1],1]], '--', color='silver')
            if pc: 
                ax1.text(0.5*(pos_Sn1[hop[0],0]+ pos_Sn1[hop[1],0]), 
                0.5*(pos_Sn1[hop[0],1]+ pos_Sn1[hop[1],1])+0.05, str(x_cup[ind]), color='k', fontsize=3)
        #y1_cup = [j for sub in cup_y1 for j in sub]
        if bi==2: hopy1_list = np.split(hopy1_list,2)[0] 
        for ind, hop in enumerate(hopy1_list):
            ax1.plot([pos_Sn1[hop[0],0], pos_Sn1[hop[1],0]], 
                     [pos_Sn1[hop[0],1], pos_Sn1[hop[1],1]], '--', color='silver')
            if bi==2: 
                ax1.plot([pos_Sn2[hop[0],0], pos_Sn2[hop[1],0]], 
                     [pos_Sn2[hop[0],1], pos_Sn2[hop[1],1]], '--', color='silver')
            if pc: 
                ax1.text(0.5*(pos_Sn1[hop[0],0]+ pos_Sn1[hop[1],0]), 
                0.5*(pos_Sn1[hop[0],1]+ pos_Sn1[hop[1],1])+0.05, str(y1_cup[ind]), color='k', fontsize=3)
        #------------------------------------------------------------
        #y2_cup = [j for sub in cup_y2 for j in sub] 
        if bi==2: hopy2_list = np.split(hopy2_list,2)[0] 
        for ind, hop in enumerate(hopy2_list):
            ax1.plot([pos_Sn1[hop[0],0], pos_Sn1[hop[1],0]], 
                     [pos_Sn1[hop[0],1], pos_Sn1[hop[1],1]], '--', color='silver')
            if bi==2:
                ax1.plot([pos_Sn2[hop[0],0], pos_Sn2[hop[1],0]], 
                     [pos_Sn2[hop[0],1], pos_Sn2[hop[1],1]], '--', color='silver')
            if pc:
                ax1.text(0.5*(pos_Sn1[hop[0],0]+ pos_Sn1[hop[1],0]), 
                0.5*(pos_Sn1[hop[0],1]+ pos_Sn1[hop[1],1])+0.05, str(y2_cup[ind]), color='k', fontsize=3)
        #------------------------------------------------------------
        if bi==2:
            for ind, hop in enumerate(hop_inter):
                ax1.plot([pos_Sn2[hop[0]-len(pos_Sn1),0], pos_Sn1[hop[1],0]], 
                     [pos_Sn2[hop[0]-len(pos_Sn1),1], pos_Sn1[hop[1],1]], '--', color='brown')
    #------------------------------------------------------------
    if plot_Mn:
        #printing atom and numbers
        ax1.plot(pos_Mn[:,0], pos_Mn[:,1], marker='o', ls='', color='mediumorchid')
        for i, loc in enumerate(pos_Mn):
            ax1.text(loc[0]+0.02, loc[1]-0.08, str(i), color="purple", fontsize=3)
        #plotting hopings and printing spin coupling number of Mn
        cup_mn = []
        for site in ribbon.sites:
            ind = ribbon.id_by_site[site]
            #print(ind)
            #print(len(ribbon.graph.out_neighbors(ind)))
            for neigh_ind in ribbon.graph.out_neighbors(ind):
                cup_mn.append([ind, neigh_ind])
        cup_mn = np.array(cup_mn)
        for ind, hop in enumerate(cup_mn):
            ax1.plot([pos_Mn[hop[0],0], pos_Mn[hop[1],0]], 
                     [pos_Mn[hop[0],1], pos_Mn[hop[1],1]], ':', color='plum')

    #------------------------------------------------------------
    if plot_spin:
        x = pos_Mn[:,0]
        y = pos_Mn[:,1]
        ax1.quiver(pos_Mn[:,0], pos_Mn[:,1], 
           dir_vec[:,0]/5, dir_vec[:,1]/5, units = 'dots', 
           scale_units='x', width = 0.5, scale = 1, pivot='mid',
           angles='xy', color='r', edgecolor='k', zorder=100,
            linewidth=0.1) 


    #------------------------------------------------------------
    #------------------------------------------------------------
    if xlim is not None:
        ax1.set_xlim(xlim) 
    if ylim is not None:
        ax1.set_ylim(ylim)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + fig_name + '.pdf')

#------------------------------------------------------------
#------------------------------------------------------------
def plot_anim(data, fig_name, pos_Mn, pos_Sn1, pos_Sn2, hopx_list, x_cup,
            hopy1_list, y1_cup, hopy2_list, y2_cup, hop_inter, 
            dir_vec, ribbon, bi,
            values=val, 
            text=False, legend=False, xlim=None, ax_twin=False,
            ylim = None, Cl=Cl, Ls=Ls, Mk=Mk, Ps=Ps, Pf=Pf, 
            Tx=Tx, Gs=Gs, Lg=Lg, sf=sf, **kwargs):		

    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    values = {**val, **values}; data = {**dat, **data}
    #------------------------------------------------------------
    f = data
    plot_x = f[values['x_plot']]; plot_y = f[values['y_plot']]
    #print(plot_y.shape)
    pc = 0
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 1, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0])
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel1'], labelpad=1)
    ax1.set_ylabel(values['ylabel1'], labelpad=3)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax1.set_xticks([]); ax1.set_yticks([])

    #------------------------------------------------------------
    #plotting hopings and printing spin coupling number of Sn
    plot_Sn = 1
    plot_Mn = 1
    plot_spin = 1
    ax1.axis('off')
    if plot_Sn:
        #printing atom and numbers
        ax1.plot(pos_Sn1[:,0], pos_Sn1[:,1], marker='o', ls='', color='darkgray', alpha=0.3)
        if bi==2: ax1.plot(pos_Sn2[:,0], pos_Sn2[:,1], marker='o', ls='', color='gray', alpha=0.3)
        #------------------------------------------------------------
        #x_cup = [j for sub in cup_x for j in sub]
        if bi==2: hopx_list = np.split(hopx_list,2)[0] 
        for ind, hop in enumerate(hopx_list):
            ax1.plot([pos_Sn1[hop[0],0], pos_Sn1[hop[1],0]], 
                     [pos_Sn1[hop[0],1], pos_Sn1[hop[1],1]], '--', color='silver', alpha=0.3)
            if bi==2:
                ax1.plot([pos_Sn2[hop[0],0], pos_Sn2[hop[1],0]], 
                [pos_Sn2[hop[0],1], pos_Sn2[hop[1],1]], '--', color='silver', alpha=0.3)
        #y1_cup = [j for sub in cup_y1 for j in sub]
        if bi==2: hopy1_list = np.split(hopy1_list,2)[0] 
        for ind, hop in enumerate(hopy1_list):
            ax1.plot([pos_Sn1[hop[0],0], pos_Sn1[hop[1],0]], 
                     [pos_Sn1[hop[0],1], pos_Sn1[hop[1],1]], '--', color='silver', alpha=0.3)
            if bi==2: 
                ax1.plot([pos_Sn2[hop[0],0], pos_Sn2[hop[1],0]], 
                     [pos_Sn2[hop[0],1], pos_Sn2[hop[1],1]], '--', color='silver', alpha=0.3)
        #------------------------------------------------------------
        #y2_cup = [j for sub in cup_y2 for j in sub] 
        if bi==2: hopy2_list = np.split(hopy2_list,2)[0] 
        for ind, hop in enumerate(hopy2_list):
            ax1.plot([pos_Sn1[hop[0],0], pos_Sn1[hop[1],0]], 
                     [pos_Sn1[hop[0],1], pos_Sn1[hop[1],1]], '--', color='silver', alpha=0.3)
            if bi==2:
                ax1.plot([pos_Sn2[hop[0],0], pos_Sn2[hop[1],0]], 
                     [pos_Sn2[hop[0],1], pos_Sn2[hop[1],1]], '--', color='silver', alpha=0.3)
        #------------------------------------------------------------
        if bi==2:
            for ind, hop in enumerate(hop_inter):
                ax1.plot([pos_Sn2[hop[0]-len(pos_Sn1),0], pos_Sn1[hop[1],0]], 
                     [pos_Sn2[hop[0]-len(pos_Sn1),1], pos_Sn1[hop[1],1]], '--', color='brown', alpha=0.3)
    #------------------------------------------------------------
    if plot_Mn:
        #printing atom and numbers
        ax1.plot(pos_Mn[:,0], pos_Mn[:,1], marker='o', ls='', color='mediumorchid')
        #for i, loc in enumerate(pos_Mn):
        #    ax1.text(loc[0]+0.02, loc[1]-0.08, str(i), color="purple", fontsize=3)
        #plotting hopings and printing spin coupling number of Mn
        cup_mn = []
        for site in ribbon.sites:
            ind = ribbon.id_by_site[site]
            #print(ind)
            #print(len(ribbon.graph.out_neighbors(ind)))
            for neigh_ind in ribbon.graph.out_neighbors(ind):
                cup_mn.append([ind, neigh_ind])
        cup_mn = np.array(cup_mn)
        for ind, hop in enumerate(cup_mn):
            ax1.plot([pos_Mn[hop[0],0], pos_Mn[hop[1],0]], 
                     [pos_Mn[hop[0],1], pos_Mn[hop[1],1]], ':', color='plum')

    #------------------------------------------------------------
    if plot_spin:
        x = pos_Mn[:,0]
        y = pos_Mn[:,1]
        ax1.quiver(pos_Mn[:,0], pos_Mn[:,1], 
           dir_vec[:,0]/5, dir_vec[:,1]/5, units = 'dots', 
           scale_units='x', width = 10, scale = 1, pivot='mid',
           angles='xy', color='r', zorder=100, linewidth=0.1,
            edgecolor='k') 


    #------------------------------------------------------------
    #------------------------------------------------------------
    if xlim is not None:
        ax1.set_xlim(xlim) 
    if ylim is not None:
        ax1.set_ylim(ylim)
    #------------------------------------------------------------
    plt.savefig('save/fig_' + fig_name + '.png')

