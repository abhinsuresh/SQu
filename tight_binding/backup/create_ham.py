import numpy as np
import sys
from index_core import *
sys.path.append('../llg/')
from constants import SIG_X, SIG_Y, SIG_Z



def create_H(t0, tj, t1, lambda_z, Lx, Ly, alpha,
             hopx_list, hopy1_list, hopy2_list, 
             x_cup, y1_cup, y2_cup, spins, hop_inter, 
             hopx_pt, hopy_pt, hop_inter_pt, bi, ad_pt, 
             dir_x, dir_y1, dir_y2, dir_z, lp_vec):
    
    if ad_pt==1: H = np.zeros((3*2*Lx*Ly,3*2*Lx*Ly), dtype=np.complex128)
    else: H = np.zeros((bi*2*Lx*Ly,bi*2*Lx*Ly), dtype=np.complex128)
    #-------------------------------------------------------------------
    #adding spin coupling
    for sc, [x,y] in enumerate(hopx_list):
        ind = x_cup[sc]
        H[2*x:2*(x+1), 2*y:2*(y+1)] +=  \
            + tj * ( SIG_X*spins[ind,0] + SIG_Y*spins[ind,1] 
                   + SIG_Z*spins[ind,2])
    for sc, [x,y] in enumerate(hopy1_list):
        ind = y1_cup[sc]
        H[2*x:2*(x+1), 2*y:2*(y+1)] +=  \
            + tj * ( SIG_X*spins[ind,0] + SIG_Y*spins[ind,1] 
                   + SIG_Z*spins[ind,2])
    for sc, [x,y] in enumerate(hopy2_list):
        ind = y2_cup[sc]
        H[2*x:2*(x+1), 2*y:2*(y+1)] +=  \
            + tj * ( SIG_X*spins[ind,0] + SIG_Y*spins[ind,1] 
                   + SIG_Z*spins[ind,2])
    #-------------------------------------------------------------------
    #adding kin_hop intra
    #only for adding magnetic field along z, Bz
    if alpha != 0:
        mult_y = flatten_list([[i+1]*(Ly-1) for i in range(Lx)])*2
    else: mult_y = np.ones(len(hopy1_list))


    #adding light only onto first layer if light
    h_fx = np.ones(len(dir_x), dtype=np.complex128)
    h_fy1 = np.ones(len(dir_y1), dtype=np.complex128)
    h_fy2 = np.ones(len(dir_y2), dtype=np.complex128)
    #adding light only onto first layer if light
    if bi == 2:
        h_fx = np.ones(len(dir_x), dtype=np.complex128)
        h_fy1 = np.ones(len(dir_y1), dtype=np.complex128)
        h_fy2 = np.ones(len(dir_y2), dtype=np.complex128)

        h_fx[:len(dir_x)//2]   = np.array([np.exp(-1j*np.dot(dir_x[i],  lp_vec)) for i in range(len(dir_x)//2)])
        h_fy1[:len(dir_y1)//2] = np.array([np.exp(-1j*np.dot(dir_y1[i], lp_vec)) for i in range(len(dir_y1)//2)])
        h_fy2[:len(dir_y2)//2] = np.array([np.exp(-1j*np.dot(dir_y2[i], lp_vec)) for i in range(len(dir_y2)//2)])
    
    for ind, [x,y] in enumerate(hopx_list):
        H[2*x:2*(x+1), 2*y:2*(y+1)] += t0 * np.eye(2) * h_fx[ind]
    for ind, [x,y] in enumerate(hopy1_list):
        h_f = np.exp(2*np.pi*1j*alpha*mult_y[ind])
        H[2*x:2*(x+1), 2*y:2*(y+1)] += t0 * np.eye(2) * h_fy1[ind]
    for ind, [x,y] in enumerate(hopy2_list):
        H[2*x:2*(x+1), 2*y:2*(y+1)] += t0 * np.eye(2) * h_fy2[ind]
    #-------------------------------------------------------------------
    #adding kin_hop inter without light is z hopping
    #h_fz = [np.exp(-1j*np.dot(dir_z[i], lp_vec)) for i in range(len(dir_z))]
    h_fz = np.ones(len(dir_z), dtype=np.complex128)
    if bi == 2:
        for ind, [x,y] in enumerate(hop_inter):
            x_p = x + 0*Lx*Ly
            H[2*x:2*(x+1), 2*y:2*(y+1)] += t1 * np.eye(2) * h_fz[ind]

    #-------------------------------------------------------------------
    #adding spin orbit part hop_(x and y1) +ve hop_y2 -ve
    for [x,y] in np.append(hopx_list, hopy1_list, axis=0):
        H[2*x:2*(x+1), 2*y:2*(y+1)] += -1*1j*lambda_z * SIG_Z
    for [x,y] in hopy2_list:
        H[2*x:2*(x+1), 2*y:2*(y+1)] += 1*1j*lambda_z * SIG_Z

    #-------------------------------------------------------------------
    #adding hopping within in the Pt part
    if ad_pt==1:
        for [x,y] in hopx_pt:
            H[2*x:2*(x+1), 2*y:2*(y+1)] += t0 * np.eye(2)
        for [x,y] in hopy_pt:
            H[2*x:2*(x+1), 2*y:2*(y+1)] += t0 * np.eye(2)

        #adding hopping between Pt and Sn
        for [x,y] in hop_inter_pt:
            H[2*x:2*(x+1), 2*y:2*(y+1)] += t0 * np.eye(2)

    #-------------------------------------------------------------------
    H = H + np.conjugate(H.transpose())

    return H

def spin_op(i, spin = 'x', shape=(0,0)):
    """find the spin operators for corresponding site
    """
    S = np.zeros(shape, dtype=np.complex128)
    if spin == 'x':
        S[2*i:2*(i+1), 2*i:2*(i+1)] = SIG_X
    elif spin == 'y':
        S[2*i:2*(i+1), 2*i:2*(i+1)] = SIG_Y
    elif spin == 'z':
        S[2*i:2*(i+1), 2*i:2*(i+1)] = SIG_Z
    elif spin == 'i':
        S[2*i:2*(i+1), 2*i:2*(i+1)] = np.eye(2)

    return S

def spin_curr_psi(l, k, psi, H, spin='x'):
    """find the bond spin current operator from psi
    """
    #1st term
    val = psi[2*l:2*(l+1)].reshape(2,1)
    rholk = val @ psi[2*k:2*(k+1)].reshape(1,2).conj()
    Hkl = H[2*k:2*(k+1), 2*l:2*(l+1)]

    #2nd term
    val = psi[2*k:2*(k+1)].reshape(2,1)
    rhokl = val @ psi[2*l:2*(l+1)].reshape(1,2).conj() 
    Hlk = H[2*l:2*(l+1), 2*k:2*(k+1)]
    
    if spin=='x':
        curr1 = np.trace(rholk @ (SIG_X @ Hkl + Hkl @ SIG_X)) 
        curr2 = np.trace(rhokl @ (SIG_X @ Hlk + Hlk @ SIG_X)) 
        return 1j*(curr1 - curr2)/4
    if spin=='y':
        curr1 = np.trace(rholk @ (SIG_Y @ Hkl + Hkl @ SIG_Y)) 
        curr2 = np.trace(rhokl @ (SIG_Y @ Hlk + Hlk @ SIG_Y)) 
        return 1j*(curr1 - curr2)/4
    if spin=='z':
        curr1 = np.trace(rholk @ (SIG_Z @ Hkl + Hkl @ SIG_Z)) 
        curr2 = np.trace(rhokl @ (SIG_Z @ Hlk + Hlk @ SIG_Z)) 
        return 1j*(curr1 - curr2)/4
    if spin=='i':
        curr1 = np.trace(rholk @ Hkl) 
        curr2 = np.trace(rhokl @ Hlk) 
        return 1j*(curr1 - curr2)/2
    
def spin_curr_rho(l, k, rho, H, spin='x'):
    """find the bond spin current operator from psi
    """
    #1st term
    rholk = rho[2*l:2*(l+1), 2*k:2*(k+1)]
    Hkl = H[2*k:2*(k+1), 2*l:2*(l+1)]

    #2nd term
    rhokl = rho[2*k:2*(k+1), 2*l:2*(l+1)]
    Hlk = H[2*l:2*(l+1), 2*k:2*(k+1)]
    
    if spin=='x':
        curr1 = np.trace(rholk @ (SIG_X @ Hkl + Hkl @ SIG_X)) 
        curr2 = np.trace(rhokl @ (SIG_X @ Hlk + Hlk @ SIG_X)) 
        return 1j*(curr1 - curr2)/4
    if spin=='y':
        curr1 = np.trace(rholk @ (SIG_Y @ Hkl + Hkl @ SIG_Y)) 
        curr2 = np.trace(rhokl @ (SIG_Y @ Hlk + Hlk @ SIG_Y)) 
        return 1j*(curr1 - curr2)/4
    if spin=='z':
        curr1 = np.trace(rholk @ (SIG_Z @ Hkl + Hkl @ SIG_Z)) 
        curr2 = np.trace(rhokl @ (SIG_Z @ Hlk + Hlk @ SIG_Z)) 
        return 1j*(curr1 - curr2)/4
    if spin=='i':
        curr1 = np.trace(rholk @ Hkl) 
        curr2 = np.trace(rhokl @ Hlk) 
        return 1j*(curr1 - curr2)/2

def espin_density(psi, length, sd_hop):
    """find the espins for Mn3Sn
    """
    espins = np.zeros((length,3))
    
    for i in range(length):
        espins[i,0] = np.conj(psi[2*sd_hop[i,0]:2*(sd_hop[i,0]+1)]) @ SIG_X @ \
                              psi[2*sd_hop[i,0]:2*(sd_hop[i,0]+1)] + \
                      np.conj(psi[2*sd_hop[i,1]:2*(sd_hop[i,1]+1)]) @ SIG_X @ \
                              psi[2*sd_hop[i,1]:2*(sd_hop[i,1]+1)] 
        espins[i,1] = np.conj(psi[2*sd_hop[i,0]:2*(sd_hop[i,0]+1)]) @ SIG_Y @ \
                              psi[2*sd_hop[i,0]:2*(sd_hop[i,0]+1)] + \
                      np.conj(psi[2*sd_hop[i,1]:2*(sd_hop[i,1]+1)]) @ SIG_Y @ \
                              psi[2*sd_hop[i,1]:2*(sd_hop[i,1]+1)] 
        espins[i,2] = np.conj(psi[2*sd_hop[i,0]:2*(sd_hop[i,0]+1)]) @ SIG_Z @ \
                              psi[2*sd_hop[i,0]:2*(sd_hop[i,0]+1)] + \
                      np.conj(psi[2*sd_hop[i,1]:2*(sd_hop[i,1]+1)]) @ SIG_Z @ \
                              psi[2*sd_hop[i,1]:2*(sd_hop[i,1]+1)] 
    return espins


def rho_alpha_nn(rho, length, sd_hop):
    """find the espins for Mn3Sn
    """
    espins = np.zeros((length,3))
    
    for i in range(length):
        espins[i,0] =np.trace(rho[2*sd_hop[i,0]:2*(sd_hop[i,0]+1), 2*sd_hop[i,0]:2*(sd_hop[i,0]+1)] @ SIG_X) 
        espins[i,1] =np.trace(rho[2*sd_hop[i,0]:2*(sd_hop[i,0]+1), 2*sd_hop[i,0]:2*(sd_hop[i,0]+1)] @ SIG_Y) 
        espins[i,2] =np.trace(rho[2*sd_hop[i,0]:2*(sd_hop[i,0]+1), 2*sd_hop[i,0]:2*(sd_hop[i,0]+1)] @ SIG_Z) 
    return espins

def create_1d_ham(t0=1, Lx=1, Ly=1, hop=[], cspins=[0,0,1], jsd=0.1):
    """create 1d tight binding hamiltonian to do pumping
    verifications with one spin at the center
    """
    H = np.zeros((2*Lx*Ly,2*Lx*Ly), dtype=np.complex128)
    
    for ind, [x,y] in enumerate(hop):
        H[2*x:2*(x+1), 2*y:2*(y+1)] += t0 * np.eye(2) 
    H = H + np.conjugate(H.transpose())
    x = 1   
    H[2*x:2*(x+1), 2*x:2*(x+1)] += jsd*(SIG_X*cspins[0] + 
                                       SIG_Y*cspins[1] + 
                                       SIG_Z*cspins[2])
    return H
    
"""
def spin_curr_op(i, j, spin = 'x', H):
    find the spin current operators for corresponding site 
    spin curr = 2/hbar Imag(this) only for psi
    
    S = np.zeros(H.shape, dtype=np.complex128)
    if spin == 'x':
        S[2*i:2*(i+1), 2*j:2*(j+1)] = SIG_X
    elif spin == 'y':
        S[2*i:2*(i+1), 2*j:2*(j+1)] = SIG_Y
    elif spin == 'z':
        S[2*i:2*(i+1), 2*j:2*(j+1)] = SIG_Z
    elif spin == 'ch':
        S[2*i:2*(i+1), 2*j:2*(j+1)] = np.eye(2)

    return S
"""
