import numpy as np, sys, h5py, time
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from field_wrapper import *
sys.path.append('../../plot_manager/')
from plot2d import plotfig_6p, plotfig_2p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
start = time.time()
case = ''
#print('file prop:', sys.argv[1], case)


#postion in which we are measuring
r = np.array([0,0,1000])
N = 10
#create the system
#-----------------
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1)
ham = Op(lat, tso_pt=0.0)
Ht = ham.create_H()
#-------------------------------------------------------------------
curr    = h5py.File(sys.argv[1])['curr'][:,:,:-1]   
rho     = h5py.File(sys.argv[1])['rho_diag'][:,:-1]
pulse   = h5py.File(sys.argv[1])['pulse'][:]

E, B, S = extract_light(r, N, curr, rho, pulse, lat.pos_atoms,
                        lat.bonds, sys.argv[1], case) 
                        #p=[19,27],b=[53,65])

#print(E.shape, B.shape)
