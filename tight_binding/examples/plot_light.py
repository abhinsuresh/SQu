import numpy as np, sys, h5py, time
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from field_wrapper import *
sys.path.append('../../plot_manager/')
from plot2d import plotfig_6p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#unit convention
#-------------------------------------------------------------------
#if code multiplied by 2pi
ki = 1/(2*np.pi)
ki = ki/0.6582119569            #[efs-1]
ki = ki*1.602176634 * 1e-4      #[A]
kc = 1.602176634 * 1e-19        #[C]

kl  = 600*1e-10                  #[m] from Angstrom cgs=600 A
mu0 = 1.256637062*1e-6          #[H/m]
c   = 2.99792458*1e8            #[m/s]
ep0 = 8.8541878128*1e-12        #[F/m]


#postion in which we are measuring
r = np.array([0,0,1000])
N = 10
#create the system
#-----------------
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1)
ham = Op(lat, tso_pt=0.0)
Ht = ham.create_H()
#-------------------------------------------------------------------
start = time.time()
nm = '19Et'
print('file prop:', 'results/19.hdf5', nm)
curr    = h5py.File('results/19.hdf5')['curr'][:,:,:-1]   * ki
rho     = h5py.File('results/19.hdf5')['rho_diag'][:,:-1] * kc
pulse   = h5py.File('results/19.hdf5')['pulse'][:]
print('pulse shape:', pulse.shape)
#extracting the current and charge density values
bl,tl   = curr.shape[1:]
rr,tr   = rho.shape
tf      = (tl - 1)//10
dt      = 0.1
times   = np.arange(0, tf + dt, dt)
dcurr   = np.zeros((bl,tl-1))
drho    = np.zeros((rr,tr-1))

#finding the derivatives
for b_ind in range(bl):
    for t_ind in range(tl-1):
        dcurr[b_ind, t_ind] = (curr[0, b_ind,t_ind+1] - curr[0, b_ind,t_ind])/dt
for r_ind in range(rr):
    for t_ind in range(tr-1):
        drho[r_ind, t_ind] = (rho[r_ind,t_ind+1] - rho[r_ind,t_ind])/dt

#setting the derivatives
t_span = times[:-1]
I = curr[:,:,:-1]
dI = dcurr
n = rho[:,:-1]
dn = drho
E = np.zeros((3,len(t_span)))
B = np.zeros((3,len(t_span)))
plot_x  = t_span
plot_y  = np.zeros((len(t_span),6))

#maybe plot all these quantities to see them
print('shape of quantities:\t',t_span.shape, I.shape, dI.shape, n.shape, dn.shape)

start = time.time()
#finding electric field
#constants for each term
ec1 = (kc)/(4*np.pi*ep0 * kl)
ec2 = (kc)/(4*np.pi*ep0 * kl * c * 1e-16)
ec3 = (1)/(4*np.pi*ep0 * c*c * 1e-16)

for t_ind in range(len(t_span)):
    #summing over all sites
    for ind, site in enumerate(range(len(lat.pos_atoms))):
        E[:,t_ind] = E[:,t_ind] + ec1 * ef_t1(lat.pos_atoms[site], r)*n[ind,t_ind]
        E[:,t_ind] = E[:,t_ind] + ec2 * ef_t2(lat.pos_atoms[site], r)*dn[ind,t_ind]
    #summing over all bonds
    for ind, bond in enumerate(lat.bonds):
        E[:,t_ind] = E[:,t_ind] + ec3 * ef_nt3(lat.pos_atoms[bond[0]],
                                        lat.pos_atoms[bond[1]], r, N)* dI[ind,t_ind]
print("time taken for E:\t", time.time()-start)

#finding magnetic field
#B = t_nsum(len(t_span), lat.pos_atoms, lat.bonds, r, I[0,:,:], dI)
#constants for each term
bc1 = mu0/(4*np.pi*kl)
bc2 = mu0/(4*np.pi*1e-16*c)

for t_ind in range(len(t_span)):
    #summing over all bonds
    for ind, bond in enumerate(lat.bonds):
        #B[:,t_ind] = B[:,t_ind] + bf_t1(lat, bond, r, 10)* I[0,ind,t_ind]
        #B[:,t_ind] = B[:,t_ind] + bf_t2(lat, bond, r, 10)*dI[ind,t_ind]
        B[:,t_ind] = B[:,t_ind] + bc1 * bf_nt1(lat.pos_atoms[bond[0]], 
                                        lat.pos_atoms[bond[1]], 
                                        r, N)* I[0,ind,t_ind] \
                                + bc2 * bf_nt2(lat.pos_atoms[bond[0]], 
                                        lat.pos_atoms[bond[1]], 
                                        r, N)* dI[ind,t_ind]
print("time taken for E + B:\t", time.time()-start)

plot_y[:,0] = E[0,:]; plot_y[:,1] = E[1,:]
plot_y[:,2] = E[2,:]
plot_y[:,3] = B[0,:]; plot_y[:,4] = B[1,:]
plot_y[:,5] = B[2,:]


data = dict();values = dict()
data['x_plot'] = plot_x; data['y_plot'] = plot_y
values['xlabel1'] = values['xlabel2'] = values['xlabel3'] = ''
values['xlabel4'] = values['xlabel5'] = values['xlabel6'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{E_x\ (N/C)}$'; values['ylabel2'] = r'$\mathrm{E_y\ (N/C)}$'
values['ylabel3'] = r'$\mathrm{E_z\ (N/C)}$'
values['ylabel4'] = r'$\mathrm{B_x\ (T)}$'; values['ylabel5'] = r'$\mathrm{B_y\ (T)}$'
values['ylabel6'] = r'$\mathrm{B_z\ (T)}$'
sf = np.array([[0,1],[1,2],[2,3],[3,4],[4,5],[5,6]])
Cl = ['C0', 'C2', 'C3']*2
Gs = dict();
Gs['t']=0.92; Gs['b']=0.13; Gs['l']=0.1; Gs['r']=0.99
Gs['w']=0.5; Gs['h']=0.33
Pi = [4000]*6
Pf = [6500]*6
Lpx = [1,1,1, 1,1,1]; Lpy = [1,1,1, 1,1,1]
plotfig_6p(data, nm, values, sf=sf, Cl=Cl,
        Gs=Gs, Pi=Pi, Pf=Pf, Lpx=Lpx, Lpy=Lpy, fs=10, lw=0.4)
