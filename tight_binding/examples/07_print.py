import numpy as np
import scipy.linalg as la
import sys
import h5py
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from numba_wrapper import fermi_fun, calc_rho_vdv
from evolve_step import rk4_rho
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#create the system
#-----------------
tf = 1
dt = 0.1
temp = 300

lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1)
spin_eq = h5py.File('scl_eq.hdf5','r')['cspins'][:]
ham = Op(lat, spin_dir=spin_eq)#, make_square=0,
        #tj=0, t1=0, lambda_z=0, t_pt=0)
H = ham.create_H()
print('Hermiticity:', ham.check_hermitian(H))

#finding initial state
w,v = la.eigh(H)
mu = 0.5*(w[len(H)//2] + w[len(H)//2])
print('mu', mu)
rho_eq = np.zeros(H.shape, dtype=np.complex128)
for i in range(len(w)):
    rho_eq = rho_eq + np.outer(v[:,i], v[:,i].conj())*fermi_fun(w[i], 0, temp)

e_exp = np.diag(fermi_fun(w, 0, temp))
rho_v = v @ e_exp @ v.conj().T

rho1 = calc_rho_vdv(w, v, temp)
#print(fermi_fun(w,0,temp))
rho = rho_v
#rho1 = np.outer(v[:,0], v[:,0].conj()) * 0.33535
#rho2 = np.outer(v[:,1], v[:,1].conj()) 


print('trace rho:\t', np.trace(rho_v), len(H))

print('comp:\t', ham.check_equal(rho_eq, rho_v))

print('commutation of rho and H:\t', ham.check_comm(rho, H))
print('commutation of rho and H:\t', ham.check_comm(rho_eq, H))

