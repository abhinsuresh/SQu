"""A code to verify the implementaion of magnetic field in the 
system, by plotting hofstader butterfly results for a square lattice
"""
import numpy as np
import scipy.linalg as la
import sys
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#create the system
#-----------------
Lx = 4
Ly = 4
lat = Lattice(Lx=Lx,Ly=Ly,layers=1,ad_pt=0)
ham = Op(lat, t0=1.0, tj=0.0, t1=0.0, lambda_z=0.0, make_square=0)
H = ham.create_H()

#-------------------------------------------------------------------
_alpha = np.linspace(0,1,100)
energy = np.zeros((len(_alpha), 2*Lx*Ly))
for a_ind, alpha in enumerate(_alpha):
    #print(alpha)
    H = ham.create_H(alpha = alpha)
    w, v = la.eigh(H)
    energy[a_ind,:] = w

data = dict();values = dict()
data['x_plot'] = _alpha; data['y_plot'] = energy/2
values['xlabel1'] = r'$\mathrm{\alpha}$'
values['ylabel1'] = r'$\mathrm{E/|t|}$'
sf = np.array([[0,energy.shape[1]]])
Cl = ['k']*energy.shape[1]
#plt.plot(J_xy,spin_exp)
plot_1p(data, 'hg', values, sf=sf, Cl=Cl, fs=8, lw=0.2)

