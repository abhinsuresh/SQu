import numpy as np
import scipy.linalg as la
import sys
import h5py
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from numba_wrapper import fermi_fun, calc_rho_vdv
from evolve_step import rk4_rho
sys.path.append('../../plot_manager/')
from plot2d import plotfig_4p, plotfig
float_formatter = "{: .2f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#create the system
#-----------------
tf = 300
dt = 0.1
temp = 0
mu = 0

lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1)
ham = Op(lat, g_lambda=0.1)
H = ham.create_H()
#finding initial state
w,v = np.linalg.eigh(H)
e_exp = np.diag(fermi_fun(w, mu, temp))
rho_eq = v @ e_exp @ v.conj().T
print('trace rho:', np.trace(rho_eq))

rho = rho_eq
#default espins
espins = np.zeros((len(ham.cspins.s),3))
times = np.arange(0, tf + dt, dt)
save_spins = np.zeros((len(espins),3,len(times)))
curr = np.zeros((len(times),3))
for t_ind, t_now in enumerate(times):
    #measurements   
    curr[t_ind,0] = ham.spin_curr_rho([0,1], rho, H, 'x')
    curr[t_ind,1] = ham.spin_curr_rho([0,1], rho, H, 'y')
    curr[t_ind,2] = ham.spin_curr_rho([0,1], rho, H, 'z')
    save_spins[:,:,t_ind] = ham.cspins.s
    if t_ind % 10 == 0:
        print(t_ind, ':\t', '%0.5e'%np.trace(rho))

    #time update
    #update quantum
    H = ham.create_H(spins=ham.cspins.s)
    rho = rk4_rho(rho, H, 0.1)
    espins = ham.rho_alpha_nn(rho)
    #update classical
    ham.cspins.llg(espins, t_now)

result = np.zeros((len(times),12))
result[:,:3] = save_spins[0,:,:].T
result[:,3:6] = save_spins[4,:,:].T
result[:,6:9] = save_spins[10,:,:].T
result[:,9:12] = (save_spins[0,:,:].T + save_spins[4,:,:].T + save_spins[10,:,:].T)/2

#-------------------------------------------------------------------
data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = '';values['xlabel2'] = ''
values['xlabel3'] = r'$\mathrm{Time\ (fs)}$';values['xlabel4'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{{\bf M}_{A}^{\alpha}}$'
values['ylabel2'] = r'$\mathrm{{\bf M}_{B}^{\alpha}}$'
values['ylabel3'] = r'$\mathrm{{\bf M}_{C}^{\alpha}}$'
values['ylabel4'] = r'$\mathrm{{\bf M}^{\alpha}}$'
sf = np.array([[0,3],[3,6],[6,9],[9,12]])
Cl = ['C0', 'C2', 'C3']*4
plotfig_4p(data, 'llg_1', values, sf=sf, Cl=Cl, fs=8, lw=0.4)
#plt.plot(J_xy,spin_exp)
data1 = dict();values1 = dict()
data1['x_plot'] = times; data1['y_plot'] = curr
values1['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values1['ylabel1'] = r'$\mathrm{{\bf I}_{1\rightarrow 2}^{S^\alpha}}$'
plotfig(data1, 'cur_1', values1, sf=sf, Cl=Cl, fs=8, lw=0.4)

lat.plotfig(ham, 'sys_1', lw=0.2, mks=2.5)
with h5py.File('05_cspin3b3.hdf5', 'w') as f:
    f.create_dataset("cspins",
    data = ham.cspins.s, dtype=np.float64, compression="gzip")
f.close()

