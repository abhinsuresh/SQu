"""A code to verify the implementaion of magnetic field in the 
system, by plotting hofstader butterfly results for a square lattice
"""
import numpy as np
import scipy.linalg as la
import sys
import os
import h5py
import time, datetime
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from numba_wrapper import *
from constants import HBAR
from evolve_step import rk4_rho, Henk_Lindblad 
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
start = time.time()


print("Evaluation file name:\t", os.path.basename(__file__))
print("Evaluation time:\t", datetime.datetime.now())
#input parameters
#-----------------
dt = 0.1
temp = 300
tf = 700
dt = 0.1
m_step = 1
nm = '22'
#light vector potential function
def vec(t, return_par=False):
    omega_l = 0.5/HBAR
    s = 25
    t_0 = 500
    E0 = 0.12
    Et = E0*np.cos(omega_l*t) * np.exp(-(t - t_0)*(t - t_0)/(2*s*s))
    if return_par:
        return np.array([s, t_0, E0, omega_l]), Et
    else: return Et

theta_l = 0 * np.pi/180
lp_vec = np.array([np.cos(theta_l), np.sin(theta_l), 0])
inp, pulse =  vec(0, True)
pulse = pulse * lp_vec

#create the system
#-----------------
Lx = 8
Ly = 8
lat = Lattice(Lx=Lx,Ly=Ly,layers=2,ad_pt=1)
ham = Op(lat, tso_pt=0.0)
Ht = ham.create_H()
#-------------------------------------------------------------------
#finding initial state
w,v = la.eigh(Ht)
#transforming to energy basis
def e_basis(mat):
    return v.conj().T @ mat @ v
#transforming to site basis
def s_basis(mat):
    return v @ mat @ v.conj().T
e_exp = np.diag(fermi_fun(w, 0, temp))
rho_e = e_exp


rho = s_basis(rho_e)
bb = [0, 1]
print('equilibrium currents', bb, ham.spin_curr_rho(bb, rho, Ht, 'all'), np.trace(rho))

#setting the evolution and transformation
evolve = Henk_Lindblad(w, temp=temp)

#time evolution
t_range = np.arange(0, tf + dt, dt)
lp_pulse = np.zeros((len(t_range),3))
espins = np.zeros((len(ham.cspins.s),3))
curr = np.zeros((4,len(lat.bonds), len(t_range)))
rho_save =  np.zeros((len(w), len(t_range)))
save_spins = np.zeros((len(espins),3,len(t_range)))
print(curr.shape, w.shape)

t_c = 0
for t_ind, t_now in enumerate(t_range):    
    #measurements
    if t_ind % m_step == 0:
        rho_s = s_basis(rho_e)
        rho_save[:,t_c] = np.diag(rho_s)
        save_spins[:,:,t_c] = ham.cspins.s
        for c_ind in range(len(lat.bonds)):
            curr[:, c_ind, t_c] = \
            ham.spin_curr_rho(lat.bonds[c_ind], rho_s, Ht, 'all')
        t_c = t_c +  1

    #print
    if t_ind%1000 == 0:
        print(t_ind, ':\t', '%0.5e'%np.trace(rho_e), time.time()-start)

    #time update the hamiltonian with pulse and spins
    pulse = lp_vec * vec(t_now)
    lp_pulse[t_ind,:] = pulse
    Ht = ham.create_H(lp_vec=pulse, spins=ham.cspins.s)

    #convert to energy basis and evolve
    Ht_e = e_basis(Ht)
    rho_e = evolve.rk4_open(rho_e, Ht_e)

    #espins = ham.rho_alpha_nn(s_basis(rho_e))
    #update classical
    #ham.cspins.llg(espins, t_now)

#-------------------------------------------------------------------
with h5py.File('results/'+nm+'.hdf5', 'w') as f:
    f.create_dataset("curr",data = curr, dtype=np.float64, compression="gzip")
    f.create_dataset("rho_diag",data = rho_save, dtype=np.float64, compression="gzip")
    f.create_dataset("cspins",data = save_spins, dtype=np.float64, compression="gzip")
f.close()
print(time.time()-start)
