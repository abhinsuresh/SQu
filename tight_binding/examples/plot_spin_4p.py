import numpy as np
import h5py
import sys
sys.path.append('../../plot_manager/')
from plot2d import plotfig_4p

#scl = np.loadtxt('results/scl11_light.txt')
#scl = scl[1700:,:]
scl = h5py.File(sys.argv[1])['cspins'][:]
time = np.arange(0,1000+1,1)
print(scl.shape, time.shape)
plot_y = np.zeros((len(time),12))
ma = 16
mb = 20
mc = 26

d = 0
s1x = scl[ma,0,:] - scl[ma,0,0]*d
s1y = scl[ma,1,:] - scl[ma,1,0]*d
s1z = scl[ma,2,:] - scl[ma,2,0]*d

s2x = scl[mb,0,:] - scl[mb,0,0]*d
s2y = scl[mb,1,:] - scl[mb,1,0]*d
s2z = scl[mb,2,:] - scl[mb,2,0]*d

s3x = scl[mc,0,:] - scl[mc,0,0]*d
s3y = scl[mc,1,:] - scl[mc,1,0]*d
s3z = scl[mc,2,:] - scl[mc,2,0]*d

#sx = np.sum(scl[:,0,:],axis=0)
#sy = np.sum(scl[:,1,:],axis=0)
#sz = np.sum(scl[:,2,:],axis=0)
sx = s1x + s2x + s3x
sy = s1y + s2y + s3y
sz = s1z + s2z + s3z

plot_y[:,0] = s1x; plot_y[:,1] = s1y; plot_y[:,2] = s1z
plot_y[:,3] = s2x; plot_y[:,4] = s2y; plot_y[:,5] = s2z
plot_y[:,6] = s3x; plot_y[:,7] = s3y; plot_y[:,8] = s3z
plot_y[:,9] = sx; plot_y[:,10] = sy; plot_y[:,11] = sz
plot_x = time


#st=1000
#sf=3000

data = dict();values = dict()
data['x_plot'] = plot_x; data['y_plot'] = plot_y
values['xlabel1'] = ''; values['xlabel2'] = ''
values['xlabel3'] = r'$\mathrm{Time\ (fs)}$'; values['xlabel4'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{\Delta M_{A}^{\alpha}}$'
values['ylabel2'] = r'$\mathrm{\Delta M_{B}^{\alpha}}$'
values['ylabel3'] = r'$\mathrm{\Delta M_{C}^{\alpha}}$'
values['ylabel4'] = r'$\mathrm{M^{\alpha}}$'
sf = np.array([[0,3],[3,6],[6,9],[9,12]])
Cl = ['C0', 'C2', 'C3']*4
Pf = [300]*4
Gs = dict();
Gs['t']=0.94; Gs['b']=0.1; Gs['l']=0.16; Gs['r']=0.98
Gs['w']=0.5; Gs['h']=0.4
Pi = [400]*12
Pf = [650]*12
plotfig_4p(data, sys.argv[2], values, sf=sf, Cl=Cl,
        Gs=Gs, Pi=Pi, Pf=Pf, fs=8, lw=0.4)
