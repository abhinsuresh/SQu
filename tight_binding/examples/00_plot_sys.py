import numpy as np
import scipy.linalg as la
import h5py, sys, time
sys.path.append('../')
from mn3sn_core import Lattice
from constants import *
from hamiltonian_core import Op
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#create the system
#-----------------
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1)
#spins_eq = h5py.File('scl_eq.hdf5','r')['cspins'][:]
#ham = Op(lat, spin_dir=spins_eq)
spin_eq = h5py.File('cspin3b3_0524.hdf5','r')['cspins'][:]
ham = Op(lat, spin_dir=spin_eq, tso_pt=0.0)
lat.plotfig(ham, 'sys_eq', lw=0.2, mew=0.5, mks=2.5, a_w=0.5)
#ham = Op(lat, tso_pt=1.0)
H = ham.create_H()


print('t0:',ham.t0,'tj:',ham.tj,'t1:',ham.t1,'t_pt:',ham.t_pt,'l_z:',ham.lambda_z,'tso_pt:',ham.tso_pt)
cspin = ham.cspins
print('jexc:',cspin.jexc,'dmi:',cspin.dmi, 'ani:',cspin.ani,'jsd:',cspin.jsd_to_llg,"dmi':",cspin.dmi_prime, 'g_lambda:',cspin.g_lambda)

#start = time.time()
#w,v = la.eigh(H)

espins = np.zeros((len(ham.cspins.s),3))
ham.cspins.llg(espins, 1000)

print(H.shape)
#print(np.imag(H[16:,16:]))
#print(np.imag(H[8:16,:8]))

#print(lat.sd_hop)


#print(time.time() - start)
#lat.plotfig(ham, 'sys_2b2', lw=0.2, mew=0.5, mks=2.5, a_w=0.5)
#print(lat.bond_len)
#print(len(lat.pos_atoms))


#curr = h5py.File('results/06_light.hdf5','r')['curr'][:]
#print(curr.shape)
#print(lat.bonds)
#print(lat.pos_atoms)

#lat.plotfig(ham, 'curr10_z', curr=0.3*curr, lw=0.2, mew=0.2, mks=1)

