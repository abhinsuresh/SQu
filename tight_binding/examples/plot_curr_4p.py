import numpy as np
import h5py
import sys
sys.path.append('../../plot_manager/')
from plot2d import plotfig_4p

nm = sys.argv[2]
curr = h5py.File(sys.argv[1])['curr'][:]

pulse = h5py.File(sys.argv[1])['pulse'][:]
print(pulse.shape)

tf = curr.shape[2] - 1
print(tf)
dt = 1
times = np.arange(0, tf + dt, dt)
print(sys.argv[1], nm)

plot_x = times
plot_y = np.zeros((len(times),4))
sx = np.zeros(len(times))
sy = np.zeros(len(times))
sz = np.zeros(len(times))
ch = np.zeros(len(times))

#53-65
#56-72
for i in range(len(times)):
    for j in range(53,65):
        ch[i] = ch[i] + curr[0, j, i]
        sx[i] = sx[i] + curr[1, j, i]
        sy[i] = sy[i] + curr[2, j, i]
        sz[i] = sz[i] + curr[3, j, i]

#plot_y[:,3] = pulse[::10,0] 
plot_y[:,3] = ch
plot_y[:,0] = sx
plot_y[:,1] = sy
plot_y[:,2] = sz

ss = 'Mn_3Sn\rightarrow Pt'

data = dict();values = dict()
data['x_plot'] = plot_x; data['y_plot'] = plot_y
values['xlabel1'] = ''; values['xlabel2'] = ''
values['xlabel3'] = r'$\mathrm{Light\ Pulse}$'; values['xlabel4'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{I_{Mn_3Sn\rightarrow Pt}^{S_{x}}\ (e\gamma/h)}$'
values['ylabel2'] = r'$\mathrm{I_{Mn_3Sn\rightarrow Pt}^{S_{y}}\ (e\gamma/h)}$'
values['ylabel3'] = r'$\mathrm{I_{Mn_3Sn\rightarrow Pt}^{S_{z}}\ (e\gamma/h)}$'
values['ylabel4'] = r'$\mathrm{I_{Mn_3Sn\rightarrow Pt}\ (e\gamma/h)}$'
sf = np.array([[0,1],[1,2],[2,3],[3,4]])
Cl = ['C0', 'C2', 'C3', 'k']
Ls = ['-']
Mk = ['']
Gs = dict();
Gs['t']=0.99; Gs['b']=0.13; Gs['l']=0.16; Gs['r']=0.99
Gs['w']=0.5; Gs['h']=0.28
Pi = [400]*4
Pf = [650]*4
Lpx = [1,1,1,1]; Lpy = [8,1,1,1]
plotfig_4p(data, nm, values, sf=sf, Cl=Cl,
        Gs=Gs, Pi=Pi, Pf=Pf, Lpx=Lpx, Lpy=Lpy, fs=10, lw=0.4)
