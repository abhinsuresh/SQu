import numpy as np
import h5py
import sys
sys.path.append('../../plot_manager/')
from plot2d import plotfig_8p

nm = '3.1'
curr1 = h5py.File(sys.argv[1])['curr'][:]
curr2 = h5py.File(sys.argv[2])['curr'][:]
curr3 = h5py.File(sys.argv[3])['curr'][:]
pulse = h5py.File(sys.argv[1])['pulse'][:]

if 1:
    curr1 = np.abs(curr1)
    curr2 = np.abs(curr2)
    curr3 = np.abs(curr3)

print(pulse.shape)

tf = curr1.shape[2] - 1
print(tf); dt = 1
times = np.arange(0, tf + dt, dt)
print(sys.argv[1], nm)

plot_x = times
plot_y = np.zeros((len(times),8))
sx = np.zeros((len(times),4))
sy = np.zeros((len(times),4))
sz = np.zeros((len(times),4))
ch = np.zeros((len(times),4))

for i in range(len(times)):
    for j in range(65,74):
        ch[i,0] = ch[i,0] + curr1[0, j, i]
        sx[i,0] = sx[i,0] + curr1[1, j, i]
        sy[i,0] = sy[i,0] + curr1[2, j, i]
        sz[i,0] = sz[i,0] + curr1[3, j, i]
for i in range(len(times)):
    for j in range(65,74):
        ch[i,1] = ch[i,1] + curr2[0, j, i]
        sx[i,1] = sx[i,1] + curr2[1, j, i]
        sy[i,1] = sy[i,1] + curr2[2, j, i]
        sz[i,1] = sz[i,1] + curr2[3, j, i]
for i in range(len(times)):
    for j in range(53,65):
        ch[i,2] = ch[i,2] + curr2[0, j, i]
        sx[i,2] = sx[i,2] + curr2[1, j, i]
        sy[i,2] = sy[i,2] + curr2[2, j, i]
        sz[i,2] = sz[i,2] + curr2[3, j, i]
for i in range(len(times)):
    for j in range(53,65):
        ch[i,3] = ch[i,3] + curr3[0, j, i]
        sx[i,3] = sx[i,3] + curr3[1, j, i]
        sy[i,3] = sy[i,3] + curr3[2, j, i]
        sz[i,3] = sz[i,3] + curr3[3, j, i]

#plot_y[:,3] = pulse[::10,0] 
plot_y[:,0] = ch[:,0]
plot_y[:,1] = ch[:,1]
plot_y[:,2] = sz[:,0]
plot_y[:,3] = sz[:,1]

plot_y[:,4] = ch[:,2]
plot_y[:,5] = ch[:,3]
plot_y[:,6] = sz[:,2]
plot_y[:,7] = sz[:,3]

ss = 'Mn_3Sn\rightarrow Pt'

data = dict();values = dict()
data['x_plot'] = plot_x; data['y_plot'] = plot_y
values['xlabel1'] = values['xlabel2'] = values['ylabel2'] = \
values['ylabel4'] = values['xlabel5'] = values['xlabel6'] = \
values['ylabel6'] = values['ylabel8'] = ''
values['xlabel3'] = values['xlabel4'] = values['xlabel7'] = \
values['xlabel8'] = r'$\mathrm{Time\ (fs)}$'

values['ylabel1'] = r'$\mathrm{I_{Mn_3Sn\rightarrow Pt}\ (e\gamma/h)}$'
values['ylabel3'] = r'$\mathrm{I_{Mn_3Sn\rightarrow Pt}^{S_z}\ (e\gamma/h)}$'
values['ylabel5'] = r'$\mathrm{I_{Pt}\ (e\gamma/h)}$'
values['ylabel7'] = r'$\mathrm{I_{Pt}^{S_z}\ (e\gamma/h)}$'
sf = np.array([[0,1],[1,2],[2,3],[3,4],[4,5],[5,6],[6,7],[7,8]])
Cl = ['k', 'k', 'C3', 'C3']*2
Ls = ['-']
Mk = ['']
Gs = dict();
Gs['t']=0.95; Gs['b']=0.11; Gs['l']=0.07; Gs['r']=0.99
Gs['w']=0.4; Gs['h']=0.28
Pi = [400]*8
Pf = [650]*8
Lpx = [1,1,1,1, 1,1,1,1]; Lpy = [1,1,1,1, 1,1,1,1]
Tx = [r'$\mathrm{LLG\ off\ and\ SOC\ off}$', r'$\mathrm{LLG\ on\ and\ SOC\ off}$',
       r'$\mathrm{LLG\ on\ and\ SOC\ off}$', r'$\mathrm{LLG\ on\ and\ SOC\ on}$']
plotfig_8p(data, nm, values, sf=sf, Cl=Cl, Gs=Gs, Pi=Pi, Pf=Pf, 
           Lpx=Lpx, Lpy=Lpy, Tx=Tx, text=1, fs=8, lw=0.4)
