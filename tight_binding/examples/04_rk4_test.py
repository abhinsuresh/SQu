"""A code to test the rk4 evolution, also serves as the verification
of ground state commutation with the Hamiltonian and plot the 
currents as the function of time to see if they remain zero
"""
import numpy as np
import scipy.linalg as la
import sys
import h5py
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from numba_wrapper import fermi_fun, calc_rho_vdv
from evolve_step import rk4_rho
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#create the system
#-----------------
tf = 1
dt = 0.1
temp = 0
mu = 0.28614068*0

lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1)
#spin_eq = h5py.File('scl_eq.hdf5','r')['cspins'][:]
ham = Op(lat)#, spin_dir=spin_eq), make_square=1, tj=-0.5, t1=-1.0, lambda_z=0.5)
H = ham.create_H()
#finding initial state
w,v = np.linalg.eigh(H)
rho_b = np.outer(v[:,0], v[:,0].conj())
#print(rho_b.real, np.trace(rho_b))
e_exp = np.diag(fermi_fun(w, mu, temp))
rho_v = v @ e_exp @ v.conj().T

rho_eq = np.zeros(H.shape, dtype=np.complex128)
for i in range(len(w)):
    rho_eq = rho_eq + np.outer(v[:,i], v[:,i].conj())*fermi_fun(w[i], mu, temp)
print('comp:\t', ham.check_equal(rho_eq, rho_v))

rho = rho_v
#default espins
espins = np.zeros((len(ham.cspins.s),3))
times = np.arange(0, tf + dt, dt)
for t_ind, t_now in enumerate(times):
    #measurements   
    curr = ham.spin_curr_rho([0,1], rho, H, 'z')
    curri = ham.spin_curr_rho([0,1], rho, H, 'i')
    currz = ham.spin_curr_rho([0,1], rho, H, 'z_old')
    print(t_ind, ':\t', '%0.5e'%np.trace(rho), '%0.3e'%curr, '%0.3e'%currz, '%0.3e'%curri)

    #time update
    H = ham.create_H()
    rho = rk4_rho(rho, H, 0.1)



"""

#-------------------------------------------------------------------
data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = save_spins[0,:,:].T
values['xlabel1'] = r'$\mathrm{\alpha}$'
values['ylabel1'] = r'$\mathrm{E/|t|}$'
sf = np.array([[0,3]])
Cl = ['k']
#plt.plot(J_xy,spin_exp)
plot_1p(data, 'llg', values, sf=sf, fs=8, lw=0.2)
"""
