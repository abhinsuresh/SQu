"""A code to run LLG alone in Mn3Sn to verfiy the LLG ground state
of the system with default parameters taken from base paper 
multiplied by some factor to increase the speed of the evolution.
Also save the final configuration of LLG as scl_eq file, if needed
to load as initial value for running with electronic dof, and plot
the configuration and dynamics as function of time
"""
import numpy as np
import scipy.linalg as la
import h5py
import sys
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#create the system
#-----------------
tf = 1000
lat = Lattice(Lx=5,Ly=5,layers=2,ad_pt=1, domain=1)
#spin_eq = h5py.File('cspin3b3_300k.hdf5','r')['cspins'][:]
#ham = Op(lat, spin_dir=spin_eq, tso_pt=0.0)
ham = Op(lat)
print('t0:',ham.t0,'tj:',ham.tj,'t1:',ham.t1,'t_pt:',ham.t_pt,'l_z:',ham.lambda_z,'tso_pt:',ham.tso_pt)
cspin = ham.cspins
print('jexc:',cspin.jexc,'dmi:',cspin.dmi, 'ani:',cspin.ani,'jsd:',cspin.jsd_to_llg,"dmi':",cspin.dmi_prime)
#ham = Op(lat, spin_dir=spin_eq, tso_pt=0.0, jexc=-0.0028,
#         ani=-0.000187, dmi=-0.00063, g_lambda=0.01)
#ham = Op(lat, spin_dir=spin_eq, tso_pt=0.0, jexc=-0.0028,
#         ani=-0.000187, dmi=-0.00063, g_lambda=0.01)
#print(ham.cspins.g_lambda)
#ham = Op(lat, spin_dir=spins_eq)
#H = ham.create_H()
#print(lat.ccoup)
#print(np.array(lat.dir_z))

times = np.arange(0, tf + ham.dt, ham.dt)

espins = np.zeros((len(ham.cspins.s),3))
#ham.cspins.llg(espins, 0)
#lat.plotfig(ham, 'sys_t0', lw=0.1, mew=0.2, mks=0.7, a_w=0.2)
lat.plotfig(ham, 'sys_t0', lw=0.2, mks=2.5)
#-------------------------------------------------------------------
with open('scl.txt', "w") as fref:
    fref.write('#reference data printed\n')      
fref.close()
save_spins = np.zeros((len(espins),3,len(times)))

#with open('scl.txt', "a") as f:
for t_ind, t_now in enumerate(times):
    #out_str = '{:5.2e} '.format(t_now)
    #for s in ham.cspins.s:
    #    out_str += '{0: 1.3e} {1: 1.3e} {2: 1.3e} '.format(*s)
    #out_str += '\n'
    #f.write(out_str)
    save_spins[:,:,t_ind] = ham.cspins.s
    ham.cspins.llg(espins, t_now)
#f.close()
#lat.plotfig(ham, 'sys', lw=0.2, mew=0.2, mks=3)
lat.plotfig(ham, 'sys_tf', lw=0.2, mks=2.5)

#with h5py.File('scl_eq_8.hdf5', 'w') as f:
#    f.create_dataset("cspins",
#    data = ham.cspins.s, dtype=np.float64, compression="gzip")
#f.close()
#-------------------------------------------------------------------
data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = save_spins[0,:,:].T
"""
print(save_spins[[0,4,10],:,-1])
print(np.sum(save_spins[[0,4,10],0,-1]))
print(np.sum(save_spins[[0,4,10],1,-1]))
print(np.sum(save_spins[[0,4,10],2,-1]))
print(save_spins[[0,4,10],:,0])
print(np.sum(save_spins[[0,4,10],0,0]))
print(np.sum(save_spins[[0,4,10],1,0]))
print(np.sum(save_spins[[0,4,10],2,0]))
"""
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{{\bf M}_1}$'
sf = np.array([[0,3]])
Cl = ['C0', 'C2', 'C3']
#plt.plot(J_xy,spin_exp)
plot_1p(data, 'llg_ev', values, sf=sf, Cl=Cl, fs=8, lw=0.2)
