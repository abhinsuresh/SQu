"""A code to measure the results of current as a result of shinning
light on the system without LLG, then later to add LLG may be by 
subtracting the equilibrium density matrix
"""
import numpy as np
import scipy.linalg as la
import sys
import h5py
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from numba_wrapper import fermi_fun, calc_rho_vdv
from constants import HBAR
from evolve_step import rk4_rho
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#input parameters
#-----------------
tf = 1
dt = 0.1
temp = 0
tf = 500
dt = 0.1
m_step = 10
nm = '06_light'
theta_l = 0 # deg

#light vector potential function
def vec(t, return_par=False):
    omega_l = 1.01/HBAR
    s = 25
    t_0 = 200
    E0 = 0.12
    Et = E0*np.cos(omega_l*t) * np.exp(-(t - t_0)*(t - t_0)/(2*s*s))
    if return_par:
        return np.array([s, t_0, E0, omega_l]), Et
    else: return Et

theta_l = 0 * np.pi/180
lp_vec = np.array([np.cos(theta_l), np.sin(theta_l), 0])
inp, pulse =  vec(0, True)
pulse = pulse * lp_vec

#create the system
lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1)
spin_eq = h5py.File('scl_eq.hdf5','r')['cspins'][:]
ham = Op(lat, spin_dir=spin_eq)#, make_square=1, tj=-0.5, t1=-1.0, lambda_z=0.5)
H = ham.create_H()
print('Hermiticity:', ham.check_hermitian(H))

#finding initial state
w,v = la.eigh(H)
e_exp = np.diag(fermi_fun(w, 0, temp))
rho_v = v @ e_exp @ v.conj().T

rho = rho_v

#time evolution
t_range = np.arange(0, tf + dt, dt)
lp_pulse = np.zeros((len(t_range),3))
curr = np.zeros((4,len(lat.bonds), len(t_range)//m_step+1))
print(curr.shape)

t_c = 0
for t_ind, t_now in enumerate(t_range):
    #measurements
    
    #curr = ham.spin_curr_rho([0,1], rho, H, 'z')
    if t_ind % m_step == 0:
        for c_ind in range(len(lat.bonds)):
            print(c_ind, t_c)
            curr[:, c_ind, t_c] = ham.spin_curr_rho(lat.bonds[c_ind], rho, H, 'all')
        t_c = t_c +  1

    if t_ind%1000 == 0:
        print(t_ind, ':\t', '%0.5e'%np.trace(rho))

    #time update
    pulse = lp_vec * vec(t_now)
    lp_pulse[t_ind,:] = pulse
    H = ham.create_H(lp_vec=pulse)
    rho = rk4_rho(rho, H, dt)




#-------------------------------------------------------------------
with h5py.File('results/06_light_tso.hdf5', 'w') as f:
    f.create_dataset("curr",data = curr, dtype=np.float64, compression="gzip")
    f.create_dataset("inp", data = inp, dtype=np.float64, compression="gzip")
f.close()

data = dict();values = dict()
data['x_plot'] = t_range; data['y_plot'] = lp_pulse[:,0:1]
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{Light}$'
sf = np.array([[0,1]])
Cl = ['k']
plot_1p(data, 'light', values, sf=sf, fs=8, lw=0.2)
