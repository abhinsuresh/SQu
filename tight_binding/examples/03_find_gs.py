"""A code to check the grand canonical equilibrium density matrix
propeties of Mn3Sn electronic system, check if it commutes with
Hamiltonian and compare it with two methods of finding the same
"""
import numpy as np
import scipy.linalg as la
import sys
import h5py
sys.path.append('../')
from mn3sn_core import Lattice
from hamiltonian_core import Op
from numba_wrapper import fermi_fun, calc_rho_vdv
from evolve_step import rk4_rho
sys.path.append('../../plot_manager/')
from plot2d import plotfig as plot_1p
float_formatter = "{: .2e}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

#create the system
#-----------------
tf = 1
dt = 0.1
temp = 300

lat = Lattice(Lx=3,Ly=3,layers=2,ad_pt=1)
#spin_eq = h5py.File('scl_eq.hdf5','r')['cspins'][:]
spin_eq = h5py.File('cspin3b3.hdf5','r')['cspins'][:]
ham = Op(lat, spin_dir=spin_eq, tso_pt=0.0)
#ham = Op(lat)#, spin_dir=spin_eq), make_square=0,
        #tj=0, t1=0, lambda_z=0, t_pt=0)
H = ham.create_H()
print('Hermiticity:', ham.check_hermitian(H))

#finding initial state
w,v = np.linalg.eigh(H)
mu = 0.5*(w[len(H)//2] + w[len(H)//2])
print('mu', mu)
# comparing H-----------------------------
H_eq = np.diag(w)
# energy basis to real basis
H_eq = v @ H_eq @ v.conj().T
print('comparing H_eq and H:\t', ham.check_equal(H_eq, H))
#verifying that v' * H * v is diagonal matrix
#only to order of 5 decimal places
#real space to energy basis
H_e = v.conj().T @ H @ v
print('energy basis diag ham:')
print(H_e.real[:5,:5])
print(w[:5])

#general way to find grand canonical rho---------------
rho_eq = np.zeros(H.shape, dtype=np.complex128)
for i in range(len(w)):
    rho_eq = rho_eq + np.outer(v[:,i], v[:,i].conj())*fermi_fun(w[i], 0, temp)

# quick way to find grand canonical rho-----------
e_exp = np.diag(fermi_fun(w, 0, temp))
#energy basis to real space
rho_v = v @ e_exp @ v.conj().T

#rho1 = calc_rho_vdv(w, v, temp)
#rho = rho_v


print('trace rho:\t', np.trace(rho_v), len(H))

print('comparing rho_eq and rho_v:\t', ham.check_equal(rho_eq, rho_v))

print('commutation of rho and H:\t', ham.check_comm(rho_v, H))
print('commutation of rho and H:\t', ham.check_comm(rho_eq, H))

