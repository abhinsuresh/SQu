import numpy as np
import matplotlib.pyplot as plt
from rcparams import rc_update
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MaxNLocator

val = dict()
val['x_plot']='x_plot';val['y_plot']='y_plot'
val['ylabel1']=''; val['xlabel1']=''
Gsd = dict();
Gsd['t']=0.9; Gsd['b']=0.2 
Gsd['l']=0.2; Gsd['r']=0.9
Gsd['w']=0.4; Gsd['h']=0.4

def plotfig(lat, ham, fig_name, values=dict(), espins=np.ones((9,3)), 
            curr=0, Gs=dict(), **kwargs):	
    """lat: Mn3Sn system 
       ham: hamiltonian object, import to plot cspins
    """	
    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    values = {**val, **values}
    a_w = 0.5
    if 'a_w' in kwargs.keys(): a_w = kwargs['a_w']
    #------------------------------------------------------------
    #print(plot_y.shape)
    pc = 0
    #------------------------------------------------------------
    #------------------------------------------------------------
    Gs = {**Gsd, **Gs}
    gs = gridspec.GridSpec(1, 1, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0])
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel1'], labelpad=1)
    ax1.set_ylabel(values['ylabel1'], labelpad=3)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax1.set_xticks([]); ax1.set_yticks([])

    #------------------------------------------------------------
    #plotting hopings and printing spin coupling number of Sn
    plot_Sn = 1
    plot_Mn = 1
    plot_spin = 1
    plot_inter = 1
    plot_numbers = 1
    plot_espins  = 0
    plot_anidir  = 0
    plot_curr = 0
    ax1.axis('off')
    if plot_Sn:
        #printing atom and numbers
        ax1.plot(lat.pos_Sn1[:,0], lat.pos_Sn1[:,1], marker='o', ls='', color='darkgray')
        if lat.layers==2: ax1.plot(lat.pos_Sn2[:,0], lat.pos_Sn2[:,1], marker='o', ls='', color='gray')
        if plot_numbers:
            for i, loc in enumerate(lat.pos_Sn1):
                ax1.text(loc[0]+0.02, loc[1]-0.08, str(i), color="k", fontsize=3)
            if lat.layers==2:
                for i, loc in enumerate(lat.pos_Sn2):
                    ax1.text(loc[0]+0.02, loc[1]-0.08, str(i+len(lat.pos_Sn1)), color="k", fontsize=3)
        #------------------------------------------------------------
        #printing connecting bonds x direction
        if lat.layers==2: hopx_list = np.split(lat.hopx_list,2)[0]
        if lat.layers==1: hopx_list = lat.hopx_list 
        for ind, hop in enumerate(hopx_list):
            ax1.plot([lat.pos_Sn1[hop[0],0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn1[hop[0],1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
            if lat.layers==2:
                ax1.plot([lat.pos_Sn2[hop[0],0], lat.pos_Sn2[hop[1],0]], 
                [lat.pos_Sn2[hop[0],1], lat.pos_Sn2[hop[1],1]], '--', color='silver')
            if plot_numbers:
                if pc: 
                    ax1.text(0.5*(lat.pos_Sn1[hop[0],0]+ lat.pos_Sn1[hop[1],0]), 
                    0.5*(lat.pos_Sn1[hop[0],1]+ lat.pos_Sn1[hop[1],1])+0.05, str(lat.x_cup[ind]), color='k', fontsize=3)
        #printing connecting bonds y1 direction
        if lat.layers==2: hopy1_list = np.split(lat.hopy1_list,2)[0] 
        if lat.layers==1: hopy1_list = lat.hopy1_list 
        for ind, hop in enumerate(hopy1_list):
            ax1.plot([lat.pos_Sn1[hop[0],0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn1[hop[0],1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
            if lat.layers==2: 
                ax1.plot([lat.pos_Sn2[hop[0],0], lat.pos_Sn2[hop[1],0]], 
                     [lat.pos_Sn2[hop[0],1], lat.pos_Sn2[hop[1],1]], '--', color='silver')
            if plot_numbers:
                if pc: 
                    ax1.text(0.5*(lat.pos_Sn1[hop[0],0]+ lat.pos_Sn1[hop[1],0]), 
                    0.5*(lat.pos_Sn1[hop[0],1]+ lat.pos_Sn1[hop[1],1])+0.05, str(lat.y1_cup[ind]), color='k', fontsize=3)
        #printing connecting bonds y2 direction
        if lat.layers==2: hopy2_list = np.split(lat.hopy2_list,2)[0] 
        if lat.layers==1: hopy2_list = lat.hopy2_list 
        for ind, hop in enumerate(hopy2_list):
            ax1.plot([lat.pos_Sn1[hop[0],0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn1[hop[0],1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
            if lat.layers==2:
                ax1.plot([lat.pos_Sn2[hop[0],0], lat.pos_Sn2[hop[1],0]], 
                     [lat.pos_Sn2[hop[0],1], lat.pos_Sn2[hop[1],1]], '--', color='silver')
            if plot_numbers:
                if pc:
                    ax1.text(0.5*(lat.pos_Sn1[hop[0],0]+ lat.pos_Sn1[hop[1],0]), 
                    0.5*(lat.pos_Sn1[hop[0],1]+ lat.pos_Sn1[hop[1],1])+0.05, str(lat.y2_cup[ind]), color='k', fontsize=3)
        #printing connecting bonds between two layers
        if lat.layers==2 and plot_inter:
            for ind, hop in enumerate(lat.hop_inter):
                ax1.plot([lat.pos_Sn2[hop[0]-len(lat.pos_Sn1),0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn2[hop[0]-len(lat.pos_Sn1),1], lat.pos_Sn1[hop[1],1]], '--', color='brown')
    #------------------------------------------------------------
    if plot_Mn:
        #printing atom and numbers
        ax1.plot(lat.pos_Mn[:,0], lat.pos_Mn[:,1], marker='o', ls='', color='mediumorchid')
        if plot_numbers:
            for i, loc in enumerate(lat.pos_Mn):
                ax1.text(loc[0]+0.02, loc[1]-0.08, str(i), color="purple", fontsize=3)
        #plotting hopings and printing spin coupling number of Mn
        cup_mn = []
        for site in lat.ribbon.sites:
            ind = lat.ribbon.id_by_site[site]
            #print(ind)
            #print(len(lat.ribbon.graph.out_neighbors(ind)))
            for neigh_ind in lat.ribbon.graph.out_neighbors(ind):
                cup_mn.append([ind, neigh_ind])
        cup_mn = np.array(cup_mn)
        for ind, hop in enumerate(cup_mn):
            ax1.plot([lat.pos_Mn[hop[0],0], lat.pos_Mn[hop[1],0]], 
                     [lat.pos_Mn[hop[0],1], lat.pos_Mn[hop[1],1]], ':', color='plum')

    #------------------------------------------------------------
    if plot_spin:
        x = lat.pos_Mn[:,0]
        y = lat.pos_Mn[:,1]
        ax1.quiver(lat.pos_Mn[:,0], lat.pos_Mn[:,1], 
           ham.cspins.s[:,0]/5, ham.cspins.s[:,1]/5, units = 'dots', 
           scale_units='x', width = a_w, scale = 1, pivot='mid',
           angles='xy', color='r', edgecolor='k', zorder=100,
            linewidth=0.1) 
    #------------------------------------------------------------
    if plot_espins:
        #x = lat.pos_Mn[:,0]
        #y = lat.pos_Mn[:,1]
        ax1.quiver(lat.pos_Mn[:,0], lat.pos_Mn[:,1], 
           espins[:,0]/6, espins[:,1]/6, units = 'dots', 
           scale_units='x', width = 0.6, scale = 1, pivot='mid',
           angles='xy', color='b', edgecolor='k', zorder=100,
            linewidth=0.1) 
    #------------------------------------------------------------
    ani_dir = np.array([ham.ani_func(i) for i in range(len(ham.cspins.s))])
    if plot_anidir:
        #x = lat.pos_Mn[:,0]
        #y = lat.pos_Mn[:,1]
        ax1.quiver(lat.pos_Mn[:,0], lat.pos_Mn[:,1], 
           ani_dir[:,0]/6, ani_dir[:,1]/6, units = 'dots', 
           scale_units='x', width = 0.6, scale = 1, pivot='mid',
           angles='xy', color='b', edgecolor='k', zorder=100,
            linewidth=0.1)
    #------------------------------------------------------------
    if plot_curr:
        #f is curr value
        #f = np.ones(len(lat.all_cup))
        N = 522
        f = curr[3,:,0][:N]
        mid = 5*np.median(f)
        x0 = lat.pos_Mn[lat.all_cup,0][:N]
        y0 = lat.pos_Mn[lat.all_cup,1][:N]
        x = f* np.array(lat.dir_bonds)[:,0][:N]
        y = f* np.array(lat.dir_bonds)[:,1][:N]
        ax1.quiver(x0, y0, x, y, units = 'dots', scale_units='x', 
           width = 0.6, scale = 1, pivot='mid', angles='xy', 
           color='C3', edgecolor='k', zorder=100, linewidth=0.1)
        ax1.quiver(0, 0, 0, mid, units = 'dots', scale_units='x', 
           width = 0.6, scale = 1, pivot='mid', angles='xy', 
           color='C3', edgecolor='k', zorder=100, linewidth=0.1)         
        ax1.text(0+0.1, 0, '$\mathrm{%0.2e\ (e\gamma/h)}$'%mid, color="k", fontsize=3)
    #------------------------------------------------------------
    #------------------------------------------------------------
    #if xlim is not None:
    #    ax1.set_xlim(xlim) 
    #if ylim is not None:
    #    ax1.set_ylim(ylim)
    #------------------------------------------------------------
    plt.savefig('results/fig_' + fig_name + '.pdf')

#------------------------------------------------------------
#------------------------------------------------------------
def plot_anim(lat, ham, fig_name, t_now, values=dict(), 
              Gs=dict(), **kwargs):		

    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    values = {**val, **values}
    Gs = {**Gsd, **Gs}
    #------------------------------------------------------------
    #print(plot_y.shape)
    pc = 0
    #------------------------------------------------------------
    #------------------------------------------------------------
    
    gs = gridspec.GridSpec(1, 1, 
                           top=Gs['t'], bottom=Gs['b'], 
                           left=Gs['l'], right=Gs['r'], 
                           wspace=Gs['w'], hspace=Gs['h'])
    ax1 = plt.subplot(gs[0,0])
    #------------------------------------------------------------
    ax1.set_xlabel(values['xlabel1'], labelpad=1)
    ax1.set_ylabel(values['ylabel1'], labelpad=3)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax1.set_xticks([]); ax1.set_yticks([])

    a_w = 10
    if 'a_w' in kwargs.keys(): a_w = kwargs['a_w']
    #------------------------------------------------------------
    #plotting hopings and printing spin coupling number of Sn
    plot_Sn = 1
    plot_Mn = 1
    plot_spin = 1
    plot_inter = 1
    ax1.axis('off')
    if plot_Sn:
        #printing atom and numbers
        ax1.plot(lat.pos_Sn1[:,0], lat.pos_Sn1[:,1], marker='o', ls='', color='darkgray')
        if lat.layers==2: ax1.plot(lat.pos_Sn2[:,0], lat.pos_Sn2[:,1], marker='o', ls='', color='gray')
        #------------------------------------------------------------
        #printing connecting bonds x direction
        if lat.layers==2: hopx_list = np.split(lat.hopx_list,2)[0]
        if lat.layers==1: hopx_list = lat.hopx_list 
        for ind, hop in enumerate(hopx_list):
            ax1.plot([lat.pos_Sn1[hop[0],0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn1[hop[0],1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
            if lat.layers==2:
                ax1.plot([lat.pos_Sn2[hop[0],0], lat.pos_Sn2[hop[1],0]], 
                [lat.pos_Sn2[hop[0],1], lat.pos_Sn2[hop[1],1]], '--', color='silver')
        #printing connecting bonds y1 direction
        if lat.layers==2: hopy1_list = np.split(lat.hopy1_list,2)[0] 
        if lat.layers==1: hopy1_list = lat.hopy1_list 
        for ind, hop in enumerate(hopy1_list):
            ax1.plot([lat.pos_Sn1[hop[0],0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn1[hop[0],1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
            if lat.layers==2: 
                ax1.plot([lat.pos_Sn2[hop[0],0], lat.pos_Sn2[hop[1],0]], 
                     [lat.pos_Sn2[hop[0],1], lat.pos_Sn2[hop[1],1]], '--', color='silver')
        #printing connecting bonds y2 direction
        if lat.layers==2: hopy2_list = np.split(lat.hopy2_list,2)[0] 
        if lat.layers==1: hopy2_list = lat.hopy2_list 
        for ind, hop in enumerate(hopy2_list):
            ax1.plot([lat.pos_Sn1[hop[0],0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn1[hop[0],1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
            if lat.layers==2:
                ax1.plot([lat.pos_Sn2[hop[0],0], lat.pos_Sn2[hop[1],0]], 
                     [lat.pos_Sn2[hop[0],1], lat.pos_Sn2[hop[1],1]], '--', color='silver')
        #printing connecting bonds between two layers
        if lat.layers==2 and plot_inter:
            for ind, hop in enumerate(lat.hop_inter):
                ax1.plot([lat.pos_Sn2[hop[0]-len(lat.pos_Sn1),0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn2[hop[0]-len(lat.pos_Sn1),1], lat.pos_Sn1[hop[1],1]], '--', color='brown')
    #------------------------------------------------------------
    if plot_Mn:
        #printing atom and numbers
        ax1.plot(lat.pos_Mn[:,0], lat.pos_Mn[:,1], marker='o', ls='', color='mediumorchid')
        #plotting hopings and printing spin coupling number of Mn
        cup_mn = []
        for site in lat.ribbon.sites:
            ind = lat.ribbon.id_by_site[site]
            #print(ind)
            #print(len(lat.ribbon.graph.out_neighbors(ind)))
            for neigh_ind in lat.ribbon.graph.out_neighbors(ind):
                cup_mn.append([ind, neigh_ind])
        cup_mn = np.array(cup_mn)
        for ind, hop in enumerate(cup_mn):
            ax1.plot([lat.pos_Mn[hop[0],0], lat.pos_Mn[hop[1],0]], 
                     [lat.pos_Mn[hop[0],1], lat.pos_Mn[hop[1],1]], ':', color='plum')

    #------------------------------------------------------------
    if plot_spin:
        x = lat.pos_Mn[:,0]
        y = lat.pos_Mn[:,1]
        ax1.quiver(lat.pos_Mn[:,0], lat.pos_Mn[:,1], 
           ham.cspins.s[:,0]/5, ham.cspins.s[:,1]/5, units = 'dots', 
           scale_units='x', width = a_w, scale = 1, pivot='mid',
           angles='xy', color='r', edgecolor='k', zorder=100,
            linewidth=0.1)
 
    ax1.text(1, 2.4, r'$\mathrm{t = '+str(t_now)+'\ fs}$', color="k", fontsize=5)

    #------------------------------------------------------------
    #------------------------------------------------------------
    #if xlim is not None:
    #    ax1.set_xlim(xlim) 
    #if ylim is not None:
    #    ax1.set_ylim(ylim)
    #------------------------------------------------------------
    plt.savefig('' + fig_name + '.png')

#------------------------------------------------------------
#------------------------------------------------------------
def plot_video(lat, ham, plot_x, plot_y, t_plot, fig_name, t_now, values=dict(), 
              Gs=dict(), **kwargs):		

    from matplotlib import rcParams
    rc_update(rcParams, np_v=2, np_h=1, minor=1, **kwargs)
    values = {**val, **values}
    Gs = {**Gsd, **Gs}
    #------------------------------------------------------------
    #print(plot_y.shape)
    pc = 0
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(3, 3, 
                           top=0.96, bottom=0.01, 
                           left=0.12, right=0.95, 
                           wspace=0.55, hspace=0.3,
                           height_ratios=[1,1,2])
    ax1 = plt.subplot(gs[0,0]) 
    ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[0,2])
    ax4 = plt.subplot(gs[1,0]) 
    ax5 = plt.subplot(gs[1,1])
    ax6 = plt.subplot(gs[1,2])
    ax7 = plt.subplot(gs[2,:])
    rm_ax = 1; nticks = 3; nyticks = 3
    t_ini = 0
    x1 = 0; x2 = 6
    #t_plot = -1
    #------------------------------------------------------------
    ax7.text(0.5,5.65, r'$\mathrm{Time ='+str(t_now)+'\ fs}$', color="k", fontsize=9)
    #ax1.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad=1)
    ax1.set_ylabel(r'$\mathrm{Light\ Pulse}$', labelpad=1)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax1.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax1.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    ax1.plot(plot_x, plot_y[:,0], color='C0', linewidth=0.4)
    ax1.axvline((t_plot+t_ini)/10000, color='C1', linewidth=0.4, alpha=1)
    #print(t_plot/10000)
    #print(t_plot, len(plot_x))
    #------------------------------------------------------------
    #ax2.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad=1);
    ax2.set_ylabel(r'$\mathrm{I_{Mn_3Sn\rightarrow Pt}\ (e\gamma/h)}$', labelpad=1)
    ax2.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax2.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax2.plot(plot_x[:t_plot], plot_y[:t_plot,1], color='k', linewidth=0.4)
    ax2.axvline((t_plot+t_ini)/10000, color='C1', linewidth=0.4, alpha=1)
    #------------------------------------------------------------
    #ax3.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad=1);
    ax3.set_ylabel(r'$\mathrm{I_{Mn_3Sn\rightarrow Pt}^{S_x}\ (e\gamma/h)}$', labelpad=1)
    ax3.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax3.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    ax3.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax3.plot(plot_x[:t_plot], plot_y[:t_plot,2], color='C2', linewidth=0.5)
    ax3.axvline((t_plot+t_ini)/10000, color='C1', linewidth=0.4, alpha=1)
    #------------------------------------------------------------
    ax4.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad=1);
    ax4.set_ylabel(r'$\mathrm{M^\alpha/N_{\rm LMM}}$', labelpad=15)
    ax4.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax4.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    ax4.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax4.plot(plot_x[:t_plot], plot_y[:t_plot,5], color='C2', linewidth=0.5)
    ax4.plot(plot_x[:t_plot], plot_y[:t_plot,6], color='C4', linewidth=0.5)
    ax4.plot(plot_x[:t_plot], plot_y[:t_plot,7], color='C3', linewidth=0.5)
    ax4.axvline((t_plot+t_ini)/10000, color='C1', linewidth=0.4, alpha=1)
    ax4.text(10,10, r'$\mathrm{\alpha}$', color="k", fontsize=9)
    #------------------------------------------------------------
    ax5.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad=1);
    ax5.set_ylabel(r'$\mathrm{I_{Mn_3Sn\rightarrow Pt}^{S_z}\ (e\gamma/h)}$', labelpad=1)
    ax5.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax5.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    ax5.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax5.plot(plot_x[:t_plot], plot_y[:t_plot,4], color='C3', linewidth=0.5)
    ax5.axvline((t_plot+t_ini)/10000, color='C1', linewidth=0.4, alpha=1)
    #------------------------------------------------------------
    ax6.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad=1);
    ax6.set_ylabel(r'$\mathrm{I_{\rm Mn_3Sn\rightarrow Pt}^{S_y}\ (e\gamma/h)}$', labelpad=1)
    ax6.xaxis.set_major_locator(MaxNLocator(nticks)) 
    ax6.yaxis.set_major_locator(MaxNLocator(nyticks)) 
    ax6.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    ax6.plot(plot_x[:t_plot], plot_y[:t_plot,3], color='C4', linewidth=0.5)
    ax6.axvline((t_plot+t_ini)/10000, color='C1', linewidth=0.4, alpha=1)
    #------------------------------------------------------------
    if rm_ax: 
        ax1.set_xticklabels([]); ax2.set_xticklabels([]); ax3.set_xticklabels([])
    ax1.set_xlim((x1,x2)); ax2.set_xlim((x1,x2)) 
    ax3.set_xlim((x1,x2)); ax4.set_xlim((x1,x2)) 
    ax5.set_xlim((x1,x2)); ax6.set_xlim((x1,x2)) 

    ax1.set_ylim((-0.13,0.13)); 
    ax2.set_ylim((0.001, 0.003)) 
    ax3.set_ylim((-0.004, 0.004)); 

    ax4.set_ylim((-0.1,0.1));

    ax5.set_ylim((-0.001,0.001)) 
    ax6.set_ylim((0, 0.005));

    #------------------------------------------------------------

    a_w = 10
    if 'a_w' in kwargs.keys(): a_w = kwargs['a_w']
    #------------------------------------------------------------
    #plotting hopings and printing spin coupling number of Sn
    plot_Sn = 1
    plot_Mn = 1
    plot_spin = 1
    plot_inter = 1
    ax7.axis('off')
    if plot_Sn:
        #printing atom and numbers
        ax7.plot(lat.pos_Sn1[:,0], lat.pos_Sn1[:,1], marker='o', ls='', color='darkgray')
        if lat.layers==2: ax7.plot(lat.pos_Sn2[:,0], lat.pos_Sn2[:,1], marker='o', ls='', color='gray')
        #------------------------------------------------------------
        #printing connecting bonds x direction
        if lat.layers==2: hopx_list = np.split(lat.hopx_list,2)[0]
        if lat.layers==1: hopx_list = lat.hopx_list 
        for ind, hop in enumerate(hopx_list):
            ax7.plot([lat.pos_Sn1[hop[0],0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn1[hop[0],1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
            if lat.layers==2:
                ax7.plot([lat.pos_Sn2[hop[0],0], lat.pos_Sn2[hop[1],0]], 
                [lat.pos_Sn2[hop[0],1], lat.pos_Sn2[hop[1],1]], '--', color='silver')
        #printing connecting bonds y1 direction
        if lat.layers==2: hopy1_list = np.split(lat.hopy1_list,2)[0] 
        if lat.layers==1: hopy1_list = lat.hopy1_list 
        for ind, hop in enumerate(hopy1_list):
            ax7.plot([lat.pos_Sn1[hop[0],0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn1[hop[0],1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
            if lat.layers==2: 
                ax7.plot([lat.pos_Sn2[hop[0],0], lat.pos_Sn2[hop[1],0]], 
                     [lat.pos_Sn2[hop[0],1], lat.pos_Sn2[hop[1],1]], '--', color='silver')
        #printing connecting bonds y2 direction
        if lat.layers==2: hopy2_list = np.split(lat.hopy2_list,2)[0] 
        if lat.layers==1: hopy2_list = lat.hopy2_list 
        for ind, hop in enumerate(hopy2_list):
            ax7.plot([lat.pos_Sn1[hop[0],0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn1[hop[0],1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
            if lat.layers==2:
                ax7.plot([lat.pos_Sn2[hop[0],0], lat.pos_Sn2[hop[1],0]], 
                     [lat.pos_Sn2[hop[0],1], lat.pos_Sn2[hop[1],1]], '--', color='silver')
        #printing connecting bonds between two layers
        if lat.layers==2 and plot_inter:
            for ind, hop in enumerate(lat.hop_inter):
                ax7.plot([lat.pos_Sn2[hop[0]-len(lat.pos_Sn1),0], lat.pos_Sn1[hop[1],0]], 
                     [lat.pos_Sn2[hop[0]-len(lat.pos_Sn1),1], lat.pos_Sn1[hop[1],1]], '--', color='silver')
    #------------------------------------------------------------
    if plot_Mn:
        #printing atom and numbers
        ax7.plot(lat.pos_Mn[:,0], lat.pos_Mn[:,1], marker='o', ls='', color='mediumorchid')
        #plotting hopings and printing spin coupling number of Mn
        cup_mn = []
        for site in lat.ribbon.sites:
            ind = lat.ribbon.id_by_site[site]
            #print(ind)
            #print(len(lat.ribbon.graph.out_neighbors(ind)))
            for neigh_ind in lat.ribbon.graph.out_neighbors(ind):
                cup_mn.append([ind, neigh_ind])
        cup_mn = np.array(cup_mn)
        for ind, hop in enumerate(cup_mn):
            ax7.plot([lat.pos_Mn[hop[0],0], lat.pos_Mn[hop[1],0]], 
                     [lat.pos_Mn[hop[0],1], lat.pos_Mn[hop[1],1]], ':', color='plum')

    #------------------------------------------------------------
    if plot_spin:
        x = lat.pos_Mn[:,0]
        y = lat.pos_Mn[:,1]
        ax7.quiver(lat.pos_Mn[:,0], lat.pos_Mn[:,1], 
           ham.cspins.s[:,0]/5, ham.cspins.s[:,1]/5, units = 'dots', 
           scale_units='x', width = a_w, scale = 1, pivot='mid',
           angles='xy', color='r', edgecolor='k', zorder=100,
            linewidth=0.1)
 

    #------------------------------------------------------------
    #------------------------------------------------------------
    #if xlim is not None:
    #    ax1.set_xlim(xlim) 
    #if ylim is not None:
    #    ax1.set_ylim(ylim)
    #------------------------------------------------------------
    plt.savefig('' + fig_name + '.png')
