import numpy as np
import sys
import kwant
from spins import Spins 
from constants import SIG_X, SIG_Y, SIG_Z, HBAR
from evolve_step import rk4_AbsBnd, spin_curr
sys.path.append('../')
from numba_wrapper import fermi_fun, calc_rho_vdv
sys.path.append('../../plot_manager/')
from plot2d import plotfig

#create the system
#-----------------
tf = 1000
dt = 0.1
temp    = 300
L       = 47
t       = 1
Jsd     = -1
omega   = 0.01/HBAR
bf      = 0
Lb      = 5
g_min   = 0.2
theta   = 45*np.pi/180
phi     = 0*np.pi/180
cspin = np.array([np.cos(phi)*np.sin(theta), np.sin(phi)*np.sin(theta), np.cos(theta)])
times = np.arange(0,tf,dt)

#spin system
sys = kwant.Builder()
lat = kwant.lattice.chain(1)
sys[lat(0)] = 4
sys = sys.finalized()
spin = Spins(sys,dt=dt, jsd_to_llg=-Jsd, bf=bf, bf_vec=(0.,0.,1.), g_lambda=0)
spin.s = cspin

#constructing the hamiltonian
Ht = np.diag(t*np.ones(L-1),1) + np.diag(t*np.ones(L-1),-1)
Ht = np.kron(Ht, np.eye(2))
Hsd = np.zeros(Ht.shape)
Hsd[L-1:L+1,L-1:L+1] = Jsd*(SIG_X*cspin[0] + SIG_Y*cspin[1] + SIG_Z*cspin[2])

H = Ht + Hsd

#finding initial state at half filling
w,v = np.linalg.eigh(H)
e_exp = np.diag(fermi_fun(w, 0, temp))
rho_eq = v @ e_exp @ v.conj().T
#rho_eq = np.outer(v[:,0],v[:,0].conj())
print('trace rho:', np.trace(rho_eq))
print(np.diag(rho_eq))
