import numpy as np
import sys
import kwant
from spins import Spins 
from constants import SIG_X, SIG_Y, SIG_Z, HBAR
from evolve_step import rk4_AbsBnd, spin_curr
sys.path.append('../')
from numba_wrapper import fermi_fun, calc_rho_vdv
sys.path.append('../../plot_manager/')
from plot2d import plotfig

#create the system
#-----------------
tf = 1000
dt = 0.1
temp    = 300
L       = 47
t       = 1
Jsd     = -1
omega   = 0.01/HBAR
bf      = 0
Lb      = 5
g_min   = 0.05
theta   = 45*np.pi/180
phi     = 0*np.pi/180
cspin = np.array([np.cos(phi)*np.sin(theta), np.sin(phi)*np.sin(theta), np.cos(theta)])
times = np.arange(0,tf,dt)

#spin system
sys = kwant.Builder()
lat = kwant.lattice.chain(1)
sys[lat(0)] = 4
sys = sys.finalized()
spin = Spins(sys,dt=dt, jsd_to_llg=-Jsd, bf=bf, bf_vec=(0.,0.,1.), g_lambda=0)
spin.s = cspin

#constructing the hamiltonian
Ht = np.diag(t*np.ones(L-1),1) + np.diag(t*np.ones(L-1),-1)
Ht = np.kron(Ht, np.eye(2))
Hsd = np.zeros(Ht.shape)
Hsd[L-1:L+1,L-1:L+1] = Jsd*(SIG_X*cspin[0] + SIG_Y*cspin[1] + SIG_Z*cspin[2])

H = Ht + Hsd

#finding initial state at half filling
w,v = np.linalg.eigh(H)
e_exp = np.diag(fermi_fun(w, 0, temp))
rho_eq = v @ e_exp @ v.conj().T
#rho_eq = np.outer(v[:,0],v[:,0].conj())
print('trace rho:', np.trace(rho_eq))

#finding dissipative terms
g_m = np.zeros(L)
g_m[0:Lb] = g_min*(Lb - np.arange(Lb))
g_m[L-Lb:] = g_min*(np.arange(Lb)+1)
g_m = np.kron(np.diag(g_m),np.eye(2))

G_M = 0.5*(g_m @ rho_eq + rho_eq @ g_m)
n,V = np.linalg.eigh(G_M)

rho = rho_eq
#default espins
save_curr = np.zeros((len(times),4))
for t_ind, t_now in enumerate(times):
    #verifications
    #if t_ind%100 == 0:
    #    print('trace:', np.trace(rho))    
    #measurements   
    save_curr[t_ind,:] = spin_curr(rho, 30, 31)
    #update classical
    phi = omega*t_now
    cspin = np.array([np.cos(phi)*np.sin(theta), np.sin(phi)*np.sin(theta), np.cos(theta)])
    #update quantum
    Hsd = np.zeros(Ht.shape)
    Hsd[L-1:L+1,L-1:L+1] = Jsd*(SIG_X*cspin[0] + SIG_Y*cspin[1] + SIG_Z*cspin[2])
    H = Ht + Hsd

    rho = rk4_AbsBnd(rho, H, g_m, G_M, dt)

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = save_curr[:,1:4]
values['xlabel1'] = r'$\mathrm{Time}$'
values['ylabel1'] = r'$\mathrm{I_{i\rightarrow j}^{S_\alpha}}$'
sf = np.array([[0,3]])
Cl = ['C0', 'C2', 'C3']
plotfig(data, 'spin_pump', values, sf=sf, Cl=Cl, fs=8, lw=0.4)
