import numpy as np
from constants import SIG_X, SIG_Y, SIG_Z
#normal rk4 closed system    
def evolve_rho(rho, Ht, g_m, G_M):
    rho_new = 1j*(rho @ Ht - Ht @ rho) - (g_m @ rho + rho @ g_m) + 2*G_M
    return rho_new

def rk4_AbsBnd(rho, Ht, g_m, G_M, dt):
    rho1 = evolve_rho(rho,            Ht, g_m, G_M)*dt
    rho2 = evolve_rho(rho + rho1*0.5, Ht, g_m, G_M)*dt
    rho3 = evolve_rho(rho + rho2*0.5, Ht, g_m, G_M)*dt
    rho4 = evolve_rho(rho + rho3,     Ht, g_m, G_M)*dt
    rho = rho + (rho1 + 2*rho2 + 2*rho3 + rho4)*(1/6)
    return rho

def spin_curr(rho, l, k):
    """find the bond spin current value from rho and H
    """
    #1st term
    rholk = rho[2*l:2*(l+1), 2*k:2*(k+1)]
    #2nd term
    rhokl = rho[2*k:2*(k+1), 2*l:2*(l+1)]

    curr_op = 1j*( rholk - rhokl )*2*np.pi
    cc = np.trace(curr_op)
    cx = np.trace(curr_op @ SIG_X)
    cy = np.trace(curr_op @ SIG_Y)
    cz = np.trace(curr_op @ SIG_Z)
    return np.array([cc, cx, cy, cz])
