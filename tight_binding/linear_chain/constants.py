import tinyarray as ta

HBAR = 0.6582119569       # Plancks constant [eV * fs]
KB = 8.6173324e-5        # Boltzman constant [eV / K]

MU_BOHR = 1#5.7883818060e-05   # Bohr magneton [eV / T]
GAMMA = 1#1.76085963023e-04   # Gyromagnetic ratio [rad / (fs*T)]

UNIT = ta.array([[1., 0.],
                 [0., 1.]])

SIG_X = 0.5*ta.array([[0, 1],
                  [1, 0]])

SIG_Y = 0.5*ta.array([[0., -1.j],
                  [1.j, 0.]])

SIG_Z = 0.5*ta.array([[1.0, 0.0],
                  [0.0, -1.0]])
