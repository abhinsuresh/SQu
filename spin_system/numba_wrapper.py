from numba import jit
import itertools
import numpy as np
from scipy.sparse import kron, eye
import sys
 
def create_tb_spinm(Lx, p0, a0, SIG_X, SIG_Y, SIG_Z, ex=1, sc=1):
    se = dict()
    on_s = np.zeros((Lx,Lx)); on_s[p0,p0] = 1
    se['sx0'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy0'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz0'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[p0+1,p0+1] = 1
    se['sx1'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy1'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz1'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[p0+2,p0+2] = 1
    se['sx2'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy2'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz2'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[p0+3,p0+3] = 1
    se['sx3'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy3'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz3'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()

    on_s = np.zeros((Lx,Lx)); on_s[a0,a0] = 1
    se['sx4'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy4'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz4'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[a0+1,a0+1] = 1
    se['sx5'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy5'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz5'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[a0+2,a0+2] = 1
    se['sx6'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy6'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz6'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[a0+3,a0+3] = 1
    se['sx7'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy7'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz7'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    
    return se

def create_tb_spinm_5(Lx, p0, a0, SIG_X, SIG_Y, SIG_Z, ex=1, sc=1):
    se = dict()
    on_s = np.zeros((Lx,Lx)); on_s[p0,p0] = 1
    se['sx0'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy0'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz0'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[p0+1,p0+1] = 1
    se['sx1'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy1'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz1'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[p0+2,p0+2] = 1
    se['sx2'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy2'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz2'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[p0+3,p0+3] = 1
    se['sx3'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy3'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz3'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[p0+4,p0+4] = 1
    se['sx4'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy4'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz4'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()

    on_s = np.zeros((Lx,Lx)); on_s[a0,a0] = 1
    se['sx5'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy5'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz5'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[a0+1,a0+1] = 1
    se['sx6'] = kron(kron(on_s, sc*0.6*SIG_X),eye(ex)).tocsr()
    se['sy6'] = kron(kron(on_s, sc*0.6*SIG_Y),eye(ex)).tocsr()
    se['sz6'] = kron(kron(on_s, sc*0.6*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[a0+2,a0+2] = 1
    se['sx7'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy7'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz7'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[a0+3,a0+3] = 1
    se['sx8'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy8'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz8'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    on_s = np.zeros((Lx,Lx)); on_s[a0+4,a0+4] = 1
    se['sx9'] = kron(kron(on_s, sc*0.5*SIG_X),eye(ex)).tocsr()
    se['sy9'] = kron(kron(on_s, sc*0.5*SIG_Y),eye(ex)).tocsr()
    se['sz9'] = kron(kron(on_s, sc*0.5*SIG_Z),eye(ex)).tocsr()
    
    return se

@jit(nopython=True)
def bitLenCount(int_type):        
    length = 0
    count = 0
    while (int_type):
        count += (int_type & 1)
        length += 1
        int_type >>= 1
    return count

@jit(nopython=True)
def get_states(L, N, dim_sys=4):
    """N is an integer 
    """   
    basisN = []
    for st_int in range(dim_sys**L):
        n = bitLenCount(st_int)
        if n==N:
            basisN.append(st_int)
    return basisN

@jit(nopython=True)
def get_mult_states(L, N, dim_sys=4):
    """N is (n1,n2,n3) 
    """   
    basisN = []
    for st_int in range(dim_sys**L):
        n = bitLenCount(st_int)
        if n == N[0] or n == N[1] or n == N[2]:
            basisN.append(st_int)
    return basisN

@jit(nopython=True)
def bitLen_Occu(int_type, length):    
    """This method finds the number of electrons and counts the 
    occupancy in method 1 
    """
    length = length//2    
    count   = 0
    ind     = 0
    occu    = np.zeros(length, dtype=np.int8)
    while (int_type):
        num = int_type & 1
        
        count += num
        int_type >>= 1
    
        occu[ind%length] += num
        ind += 1
    
    return count, occu

#@jit(nopython=True)
def get_states_occu(L, N, dc=True, dim_sys=4):   
    basisN = []
    for st_int in range(dim_sys**L):
        n, occu = bitLen_Occu(st_int, 2*L)
        if n == N:
            if 2 not in occu:
                #print(occu)           
                basisN.append(st_int)
    return basisN

@jit(nopython=True)
def int_to_bit(int_type):
    """This method is slower than bitLen_Occu by almost 4 times
    """ 
    result = ""
    while (int_type):
        result = result + str(int_type & 1)
        int_type >>= 1
    return result[::-1]

def combiner(zeros=1, ones=1):
    if ones == 0:
        yield '0'*zeros  
    for indices in itertools.combinations(range(zeros+ones), ones):
        item = ['0'] * (zeros+ones)
        for index in indices:
            item[index] = '1'
        yield ''.join(item)

#@jit(nopython=True)
def swap_bits(s,i,j):
    """swap bits starting from zero
    print(str(bin(204))[2:])
    print(str(bin(swap_bits(204,1,2)))[2:])
    """
    x = ( (s>>i)^(s>>j) ) & 1
    return s^( (x<<i)|(x<<j) )

def updn_list(N):
    """return list of Nup and Ndn for using in quspin
    """
    list1 = []
    N = 6
    for i in range(N//2 + 1):
        list1.append((i,N-i))
        if i != N-i:
            list1.append((N-i,i))
    return list1

@jit(nopython=True)
def calc_rho(E,V, temp=0, verbose=False):
    """Calculate the canonical gibbs density matrix
    """
    KB = 8.617333*1e-5   #eV K-1
    temp = temp+1e-15    #to avoid divide by zero error
    beta = 1/(KB*temp)
    E = E - E[0]
    rho = np.zeros((len(E),len(E)), dtype=np.complex128)
    for ind in np.arange(len(E)):
        rho = rho + np.exp( -beta*E[ind]  ) * \
                    np.conjugate(V[:,ind].reshape((len(E), 1))) @ \
                                 V[:,ind].reshape((1, len(E)))
    
    return rho/( np.trace(rho) )

#@jit(nopython=True)
def calc_rho_vdv(E,V, temp=0, verbose=False):
    """Calculate the canonical gibbs density matrix
    """
    KB = 8.617333*1e-5   #eV K-1
    temp = temp+1e-15    #to avoid divide by zero error
    beta = 1/(KB*temp)
    E = E - E[0]
    e_exp = np.diag(np.exp(-beta * E))
    rho = V @ e_exp @ np.transpose(np.conjugate(V))
    
    return rho/( np.trace(rho) )

def calc_rho0(E,V):
    """Calculate the canonical gibbs density matrix
    """
    beta = 0
    E = E - E[0]
    e_exp = np.diag(np.exp(-beta * E))
    rho = V @ e_exp @ np.transpose(np.conjugate(V))
    
    return rho/( np.trace(rho) )

#@jit(nopython=True)
def evolve_rho_gamma(rho, Ht, v=1, vd=1, Gamma=1, l=0):
    if not l:
        rho_new = 1j*(rho @ Ht - Ht @ rho)
    else:
        rho_new = 1j*(rho @ Ht - Ht @ rho) - \
                0.5*Gamma*(vd @ v @ rho + rho @ vd @ v -
                            2 * v @ rho @ vd) 
        
    return rho_new

#@jit(nopython=True)
def vmat(v, gamma, x_range, y_range, size): 
    vmat = np.zeros((size,size), dtype=np.complex128)
    for i in x_range:
        for j in y_range:
            vmat = vmat + np.sqrt(gamma[i,j])* v[:,i].reshape((size,1)) @ \
                   np.conjugate(v[:,j].reshape((1,size)))
            #vmat = vmat + np.sqrt(gamma[i,j]) * np.outer(v[:,i],np.conjugate(v[:,j]))
    return vmat

#@jit(nopython=True)
def sp_trace(a):
    row = a.tocoo().row
    col = a.tocoo().col
    L = len(row)
    trace = 0
    for i in range(L):
        if row[i] == col[i]:
            trace = trace + a.data[i]
    return trace

def check_hermitian(a, rtol=1e-05, atol=1e-08):
    return np.allclose(a, a.conj().T, rtol=rtol, atol=atol)

def check_equal(a, b, rtol=1e-05, atol=1e-05):
    return np.allclose(a, b, rtol=rtol, atol=atol)

def check_comm(a, b, rtol=1e-05, atol=1e-05):
    return np.allclose(a@b, b@a, rtol=rtol, atol=atol)
