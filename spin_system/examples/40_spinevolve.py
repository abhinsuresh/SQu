import sys
import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix

sys.path.append('../')
from spin_core import Spin
from evolve_step import Lindblad
from density_core import logarithmic_negativity
sys.path.append('../../plot_manager/')
from plot2d import plotfig_2p
float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
Lx  = 4
Ly  = 1
S   = 1/2
J   = -0.1
Jz  = J*1.005
tf  = 500
dt  = 0.1
#derived parameters
L   = Lx*Ly
start = time.time()
#x+-y+-z+-
gamma   = [0.1,0,0,0,0,0]
g       = [0,0,0,0,0,0]

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1, open_system=True)

#print('system created in ',time.time()-start)
Se = spin.op
#print(Se['sx0'].shape)
#print(Se.keys())
#print(spin.pop.keys())
H0   = spin.heisenberg(J=J,Jz=Jz)
w,v = la.eigh(H0.toarray())
up = np.array([1,0])
dn = np.array([0,1])
gs = np.kron(np.kron(np.kron(dn,up),up),up)
#gs = np.kron(np.kron(np.kron(np.kron(gs,up),up),up),up)
#gs = np.kron(np.kron(np.kron(dn,up),up),up)

rho = np.outer(gs.conj(),gs)
print(np.trace(rho @ Se['sz0']))
print(np.trace(rho @ Se['sz3']))

print('log neg:\t',logarithmic_negativity(rho,'B',m=2**2))

#rho_new = evolve.rk4_rho(rho)


#start time evolution
times = np.arange(0, tf+dt, dt)
result = np.zeros((len(times),3))

gf = 0.5
L  = gf*spin.pop['sxp0']
Ld = gf*spin.pop['sxm0']
Lp = Ld @ L
Lplus  = -1j*H0 - 0.5*Lp
Lminus = 1j*H0 - 0.5*Lp

evolve   = Lindblad(spin, gamma=gamma, g=g, dt=0.1,
                    Lplus=Lplus, Lminus=Lminus, L=L, Ld=Ld)

for t_ind, t_now in enumerate(times):
    
    result[t_ind,0] = np.trace(rho @ Se['sz0'])
    result[t_ind,1] = np.trace(rho @ Se['sz3'])
    result[t_ind,2] = logarithmic_negativity(rho,'B',m=2**2)
    #print(t_now)

    rho = evolve.rk4_rho(rho, H0)

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['xlabel2'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{\langle S^z_i\rangle}$'
values['ylabel2'] = r'$\mathrm{Log\ negativity}$'
sf = np.array([[0,2],[2,3]])
Cl = ['C0', 'C2', 'C3', 'k']
plotfig_2p(data, 'sz0', values, sf=sf, Cl=Cl)
    
print('run finished in:\t',time.time()-start)

