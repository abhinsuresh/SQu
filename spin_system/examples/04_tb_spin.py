import sys
import numpy as np
import time
import h5sparse
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix, eye, kron

sys.path.append('../')
from spin_core import Spin
from evolve_step import Lindblad
from density_core import logarithmic_negativity
from numba_wrapper import sp_trace, create_tb_spinm 
from constants import SIG_X, SIG_Y, SIG_Z

sys.path.append('../../squbit/')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian

sys.path.append('../../plot_manager/')
from plot2d import plotfig_2p, plotfig
data = dict();values = dict()
values['xlabel1'] = r'$\mathrm{Sites}$'
values['ylabel1'] = r'$\mathrm{\Psi}$'
sf = np.array([[0,1]])
float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
L  = 4
S   = 1/2
J   = -0.1
Jz  = J*1.005
Lx  = 110
kx = 0.1
dkx = 0.2
x0 = 40
Jsd = -0.5
tf = 70
dt = 0.1  #\hbar/gamma
p0 = 60
a0 = 68
#derived parameters
start = time.time()
verbose = 1
#x+-y+-z+-
gamma   = [0,0,0,0,0.01,0.01]
g       = [0,0,0,0,0,0]

#electron system
sys_e = Basis(L=Lx, Nup=1, Ndn=0,  model = 1, use_iterator=True)
ham = Hamiltonian(sys_e, periodic=0, Lx=Lx, Ly=1)
H_hop = ham.hubbard_sparse(t=-1, alpha = 0)
Ht = kron(H_hop,eye(2))
Ie = eye(Ht.shape[0]).tocsr()

#spin system creation with eDOF
spin_b = Spin(S=S, L=L)
if verbose: print('spin_b 4 created', '%0.3f'%(time.time()-start))
spin_o = Spin(S=S, L=2*L, kronOs=2*Lx, open_system=1)
Se = spin_o.op
if verbose: print('open spin_o 8 system created', '%0.3f'%(time.time()-start))
if verbose: print('spin mat sxpm0: ',spin_o.pop['sxpm0'].shape)
if verbose: print('spin mat sx0: ',spin_o.op['sx0'].shape)
evolve   = Lindblad(spin_o, gamma=gamma, g=g, dt=0.1)

#Heisenberg hamiltonian
Hs   = spin_b.heisenberg(J=J,Jz=Jz)    #4spin ham
Is = eye(Hs.shape[0]).tocsr()          #4spin size 16x16
HJ1 = kron(Ie,kron(Hs,Is))
HJ2 = kron(Ie,kron(Is,Hs))
if verbose: print('HJ1/2 shape:\t', HJ1.shape, HJ2.shape)

#total orbital hopping hamiltonian
H_E = kron(Ht,kron(Is,Is)).tocsr()    # 220x16x16
if verbose: print('e ham shape:\t', H_E.shape)
if verbose: print('e_ham nnz:\t', H_E.count_nonzero())

#constructing electron spin matrixes as a dict
se = create_tb_spinm(Lx, p0, a0, SIG_X, SIG_Y, SIG_Z, ex=Hs.shape[0]**2)
if verbose: print('e-spin matrix shape:\t', se['sx0'].shape)
#expading matrixes to larger space
HJSD = csr_matrix(H_E.shape, dtype=np.complex)
for ind in range(4):
    i = str(ind)
    HJSD = HJSD + Jsd * (se['sx'+i] @ kron(Ie,Se['sx'+i]) + 
                         se['sy'+i] @ kron(Ie,Se['sy'+i]) + 
                         se['sz'+i] @ kron(Ie,Se['sz'+i]))

for ind in range(4,8):
    i = str(ind)
    HJSD = HJSD + Jsd * (se['sx'+i] @ kron(Ie,Se['sx'+i]) + 
                         se['sy'+i] @ kron(Ie,Se['sy'+i]) + 
                         se['sz'+i] @ kron(Ie,Se['sz'+i]))

if verbose: print('HJSD shape:\t', HJSD.shape)
if verbose: print('HJSD nnz:\t', HJSD.count_nonzero())

#total hamiltonian
H_tot = H_E + HJ1 + HJ2 + HJSD
if verbose: print('H_tot nnz:\t', H_tot.count_nonzero())
#-------------------------------------------------------------------
#making the initial state
up = np.array([1,0])
dn = np.array([0,1])
gs1 = np.kron(np.kron(np.kron(up,up),up),up)
rho_p = np.outer(gs1.conj(), gs1)
gs2 = np.kron(np.kron(np.kron(dn,dn),dn),dn)
rho_a = np.outer(gs2.conj(), gs2)

rho_s = csr_matrix(kron(rho_p,rho_a))

#print(sp_trace(rho_s @ Se['sz0']))

cl = np.arange(Lx)
data['x_plot'] = cl
psi_orb = np.exp(1j*kx*cl - (dkx**2 * (cl - x0)**2)/4)
psi_orb = psi_orb

#unpolarized rho_e
rho_e = kron(np.outer(psi_orb,psi_orb.conj()),eye(2)).tocsr()
rho_e = rho_e/np.trace(rho_e.toarray())
#rho_new = evolve.rk4_rho(rho)

rho = kron(rho_e, rho_s).tocsr()
if verbose: print('rho nnz:\t', rho.count_nonzero())
#print('rho', time.time()-start)
#rho_new = evolve.rk3_rho(rho, kron(H0,Ht))
#print('t step', time.time()-start)

#start time evolution
times = np.arange(0, tf+dt, dt)
result = np.zeros((len(times),3))
if verbose: print(type(H_tot), type(rho))
for t_ind, t_now in enumerate(times):
    
    #result[t_ind,0] = np.trace(rho @ Se['sz0'])
    #result[t_ind,1] = np.trace(rho @ Se['sz7'])
    #result[t_ind,2] = logarithmic_negativity(rho,'B',m=2**4)
    with h5sparse.File('results/rho/rho_'+str(t_ind)+'.h5', 'w') as f:
        f.create_dataset('rho', data = rho,
                         dtype=np.complex64, compression="gzip")
    f.close()
    
    print('%0.3f'%t_now, 'in %0.3f'%(time.time()-start))

    rho = evolve.rk4_rho(rho, H_tot)

"""
data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['xlabel2'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{<Sz_i>}$'
values['ylabel2'] = r'$\mathrm{Log\ negativity}$'
sf = np.array([[0,2],[2,3]])
Cl = ['C0', 'C2', 'C3', 'k']
plotfig_2p(data, 'sz', values, sf=sf, Cl=Cl)
"""
if verbose: print('run finished in:\t',time.time()-start)
