#A code to print and check expectation values of spin, or any other
#operators to verify basic physics

import sys
import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix

sys.path.append('../')
from spin_core import Spin
from evolve_step import Lindblad
float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951

#parameters
Lx  = 4
Ly  = 1
S   = 1/2
J   = 1.0
Jz  = J
#derived parameters
L   = Lx*Ly
start = time.time()

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
#evolve   = Lindblad(spin, gamma=gamma, g=g, dt=0.1)

print('system created in ',time.time()-start)
#Se = spin.op
#print(Se['sx0'].shape)
#print(Se.keys())
#print(spin.pop.keys())
H0   = spin.heisenberg(J=J,Jz=Jz)
w,v = la.eigh(H0.toarray())

print(w[:5])

#rho = np.outer(v[:,0].conj(), v[:,0])

#print('rho outer ',time.time()-start)
#rho_new = evolve.rk4_rho(H0, rho)

#print('rho_new ',time.time()-start)

"""
w,v = la.eigh(H0.toarray())
print(w[:5])
s = 1
print(v[:,s].conj() @ Se['sz0'] @ v[:,s])
print(v[:,s].conj() @ Se['sz1'] @ v[:,s])
print(v[:,s].conj() @ Se['sz2'] @ v[:,s])
print(v[:,s].conj() @ Se['sz3'] @ v[:,s])
"""
