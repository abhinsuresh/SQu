import sys
import numpy as np, time, h5py
import qutip as qt

sys.path.append('../')
from spin_core import Spin
from evolve_step import Lindblad
from density_core import logarithmic_negativity
sys.path.append('../../plot_manager/')
from plot2d import plotfig_2p
HBAR = 0.658211951

#parameters
Lx  = 4
Ly  = 1
S   = 1/2
J   = -0.1
Jz  = J*1.005
tf  = 500
dt  = 0.1
#derived parameters
L   = Lx*Ly
start = time.time()

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1, open_system=True)

#print('system created in ',time.time()-start)
Se = spin.op
#print(Se['sx0'].shape)
#print(Se.keys())
#print(spin.pop.keys())
H0   = spin.heisenberg(J=J,Jz=Jz)
w,v = np.linalg.eigh(H0.toarray())
up = np.array([1,0])
dn = np.array([0,1])
gs = np.kron(np.kron(np.kron(dn,up),up),up)
#gs = np.kron(np.kron(np.kron(np.kron(gs,up),up),up),up)
#gs = np.kron(np.kron(np.kron(dn,up),up),up)

psi0 = gs.reshape((-1,1))
rho = np.outer(gs.conj(),gs)
print(np.trace(rho @ Se['sz0']))
print(np.trace(rho @ Se['sz3']))
print((psi0.conj().T @ Se['sz0'] @ psi0)[0,0] )
print('log neg:\t',logarithmic_negativity(rho,'B',m=2**2))
#start time evolution
times = np.arange(0, tf+dt, dt)
result = np.zeros((len(times),3))

gf = 0.5
L  = gf*spin.pop['sxp0']
Ld = gf*spin.pop['sxm0']
Lp = Ld @ L
print(H0.shape, Lp.shape)
Lplus  = -1j*H0 - 0.5*Lp
Lminus = 1j*H0 - 0.5*Lp

evolve   = Lindblad(spin, dt=0.1,
                    Lplus=Lplus, Lminus=Lminus, L=L, Ld=Ld)
psi = psi0
print(psi.shape)

I2 = np.eye(len(psi0))
Heff = H0 - 1j*0.5*Lp
Ntraj = 100
Ut = I2 -1j*Heff*dt

psi_new = (I2 - 1j*Heff*dt) @ psi
print(np.linalg.norm(psi_new))

"""
f = h5py.File('input/mat.h5', 'w')
f.create_dataset("Ut", data=Ut)
f.create_dataset("L", data=L.toarray())
f.create_dataset("Lp", data=Lp.toarray())
f.close()
"""
for n_ind in range(Ntraj):
    psi = psi0
    print(n_ind)
    for t_ind,t_now in enumerate(times): 
        
        r1 = np.random.rand(1)[0]
        dp = dt * (psi.conj().T @ Lp @ psi)[0,0]
        
        if r1 <= dp:
            psi = L @ psi
            psi = psi/np.linalg.norm(psi)
        else:
            psi = Ut @ psi
            psi = psi/np.linalg.norm(psi)

        result[t_ind,0] = result[t_ind,0] + (psi.conj().T @ Se['sz0'] @ psi)[0,0]
        result[t_ind,1] = result[t_ind,1] + (psi.conj().T @ Se['sz3'] @ psi)[0,0]
            


result = result/Ntraj

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['xlabel2'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{\langle Sz_i\rangle}$'
values['ylabel2'] = r'$\mathrm{Log\ negativity}$'
sf = np.array([[0,2],[2,3]])
Cl = ['C0', 'C2', 'C3', 'k']
plotfig_2p(data, 'szt2', values, sf=sf, Cl=Cl)
print('run finished in:\t',time.time()-start)
