import sys
import numpy as np
import time
import h5sparse
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix, eye, kron
from scipy.io import savemat, loadmat

sys.path.append('../')
from spin_core import Spin
from evolve_step import Lindblad
from density_core import logarithmic_negativity
from density_core import *
from numba_wrapper import sp_trace, create_tb_spinm 
from constants import SIG_X, SIG_Y, SIG_Z

sys.path.append('../../squbit/')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian

sys.path.append('../../plot_manager/')
from plot2d import plotfig_2p, plotfig
data = dict();values = dict()
values['xlabel1'] = r'$\mathrm{Sites}$'
values['ylabel1'] = r'$\mathrm{\Psi}$'
sf = np.array([[0,1]])
float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
L  = 4
S   = 1/2
J   = -0.1
Jz  = J*1.005
Lx  = 130
kx = np.pi/2
dkx = 0.2
x0 = 20
Jsd = -0.5
dt = 0.1  #\hbar/gamma
p0 = 44
a0 = 52
#derived parameters
start = time.time()
verbose = 1
#x+-y+-z+-

#electron system
sys_e = Basis(L=Lx, Nup=1, Ndn=0,  model = 1, use_iterator=True)
ham = Hamiltonian(sys_e, periodic=0, Lx=Lx, Ly=1)
H_hop = ham.hubbard_sparse(t=-1, alpha = 0)
Ht = kron(H_hop,eye(2))
w,v = la.eigh(Ht.toarray())
Ie = eye(Ht.shape[0]).tocsr()

#spin system creation with eDOF
spin_b = Spin(S=S, L=L, sc = 2)
#if verbose: print('spin_b 4 created', '%0.3f'%(time.time()-start))
spin_o = Spin(S=S, L=2*L, kronOs=2*Lx, open_system=1, sc=2)
Se = spin_o.op
#pop = spin_o.pop
#Heisenberg hamiltonian
Hs   = spin_b.heisenberg(J=J,Jz=Jz)    #4spin ham
Is = eye(Hs.shape[0]).tocsr()          #4spin size 16x16
HJ1 = kron(Ie,kron(Hs,Is))
HJ2 = kron(Ie,kron(Is,Hs))
w,v = la.eigh((kron(Hs,Is) + kron(Is,Hs).toarray()))

#total orbital hopping hamiltonian
H_E = kron(Ht,kron(Is,Is)).tocsr()    # 220x16x16

#constructing electron spin matrixes as a dict
se = create_tb_spinm(Lx, p0, a0, SIG_X, SIG_Y, SIG_Z, ex=Hs.shape[0]**2, sc=2)

HJSD = csr_matrix(H_E.shape, dtype=np.complex)
for ind in range(4):
    i = str(ind)
    HJSD = HJSD + Jsd * (se['sx'+i] @ kron(Ie,Se['sx'+i]) + 
                         se['sy'+i] @ kron(Ie,Se['sy'+i]) + 
                         se['sz'+i] @ kron(Ie,Se['sz'+i]))
for ind in range(4,8):
    i = str(ind)
    HJSD = HJSD + Jsd * (se['sx'+i] @ kron(Ie,Se['sx'+i]) + 
                         se['sy'+i] @ kron(Ie,Se['sy'+i]) + 
                         se['sz'+i] @ kron(Ie,Se['sz'+i]))

#total hamiltonian
H_tot = H_E + HJ1 + HJ2 + HJSD
if verbose: print('H_tot nnz:\t', H_tot.count_nonzero())
#print(type(H_tot))
#-------------------------------------------------------------------
#making the initial state
up = np.array([1,0])
dn = np.array([0,1])
gs1 = np.kron(np.kron(np.kron(up,up),up),up)
rho_p = np.outer(gs1, gs1.conj())
gs2 = np.kron(np.kron(np.kron(dn,dn),dn),dn)
rho_a = np.outer(gs2, gs2.conj())
#-------------------------------------------------------------------
#non zero temp
T_HJ1 = Hs - 0.01*(spin_b.op['sz0']+spin_b.op['sz1']+spin_b.op['sz2']+spin_b.op['sz3'])
T_HJ2 = Hs + 0.01*(spin_b.op['sz0']+spin_b.op['sz1']+spin_b.op['sz2']+spin_b.op['sz3'])
w1,v1 = np.linalg.eigh(T_HJ1.toarray())
w2,v2 = np.linalg.eigh(T_HJ2.toarray())
KB = 8.61733e-5
temp = 100
temp = temp + 1e-15
beta = 1/(KB*temp)
w1 = w1 - w1[0]
rho_d1 = np.diag(np.exp(-beta*w1))
#rho_d1[np.abs(rho_d1) < 1e-15] = 0
rho_1 = v1 @ rho_d1 @ v1.conj().transpose()
#rho_1[np.abs(rho_1) < 1e-15] = 0

w2 = w2 - w2[0]
rho_d2 = np.diag(np.exp(-beta*w2))
#rho_d2[np.abs(rho_d1) < 1e-15] = 0
rho_2 = v2 @ rho_d2 @ v2.conj().transpose()
#rho_2[np.abs(rho_2) < 1e-15] = 0
rho_s = csr_matrix(kron(rho_1,rho_2))
#rho_s[np.abs(rho_s) < 1e-15] = 0
rho_s = rho_s/np.trace(rho_s.toarray())
print("rho_s trace: ", np.trace(rho_s.toarray()))


cl = np.arange(Lx)
psi_orb = np.exp(1j*kx*cl - (dkx**2 * (cl - x0)**2)/4)
psi_orb = psi_orb

rho_e = kron(np.outer(psi_orb,psi_orb.conj()),eye(2)*0.5).tocsr()
rho_e = rho_e/np.trace(rho_e.toarray())
print("rho_e trace: ", np.trace(rho_e.toarray()))

#unpolarized rho_e
#rho_s = csr_matrix(kron(rho_p,rho_a))
#rho = kron(rho_e, rho_s).tocsr()

#if verbose: print('rho nnz:\t', rho.count_nonzero())
#if verbose: print(sp_trace(rho @ H_tot))
#if verbose: print(sp_trace(rho @ kron(Ie, Se['sz0'])))
#if verbose: print(sp_trace(rho @ kron(Ie, Se['sz7'])))

savemat('saved_mat/80/rho_e.mat',{'rho_e':rho_e}, do_compression=False)
savemat('saved_mat/80/rho_s.mat',{'rho_s':rho_s}, do_compression=False)
savemat('saved_mat/80/H.mat',{'H':H_tot}, do_compression=False)
#savemat('saved_mat/80/rho.mat',{'rho':rho}, do_compression=False)
#savemat('saved_mat/60/op.mat', Se, do_compression=True)
#savemat('saved_mat/60/pop.mat', spin_o.pop, do_compression=True)
print('saved all')


