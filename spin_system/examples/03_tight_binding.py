import sys
import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import csr_matrix
from scipy.sparse import eye, kron

sys.path.append('../')
from spin_core import Spin
from evolve_step import Lindblad
from density_core import logarithmic_negativity
from numba_wrapper import sp_trace

sys.path.append('../../squbit/')
from spinful_basis import Basis
from hamiltonian_core import Hamiltonian

sys.path.append('../../plot_manager/')
from plot2d import plotfig_2p, plotfig
data = dict();values = dict()
values['xlabel1'] = r'$\mathrm{Sites}$'
values['ylabel1'] = r'$\mathrm{\Psi}$'
sf = np.array([[0,1]])
float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
HBAR = 0.658211951
dim = '1d'

#parameters
L  = 8
S   = 1/2
J   = -0.1
Jz  = J*1.005
Lx  = 110
kx = 0.1
dkx = 0.2
x0 = 40
Jsd = -0.5
tf = 70
dt = 0.1  #\hbar/gamma
p0 = 60
a0 = 68
#derived parameters
start = time.time()
#x+-y+-z+-
gamma   = [0,0,0,0,0,0]
g       = [0,0,0,0,0,0]

#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1, open_system=0)
Se = spin.op
evolve   = Lindblad(spin, gamma=gamma, g=g, dt=0.1)
#electron system
sys_e = Basis(L=Lx, Nup=1, Ndn=0,  model = 1, use_iterator=True)
ham = Hamiltonian(sys_e, periodic=0, Lx=Lx, Ly=1)
H_hop = ham.hubbard_sparse(t=-1, alpha = 0)
Ht = kron(H_hop,eye(2))
Ie = eye(Ht.shape[0])

#print('system created in ',time.time()-start)
H0   = spin.heisenberg(J=J,Jz=Jz)
w,v = la.eigh(H0.toarray())
up = np.array([1,0])
dn = np.array([0,1])
gs1 = np.kron(np.kron(np.kron(up,up),up),up)
rho_p = np.outer(gs1.conj(), gs1)
gs2 = np.kron(np.kron(np.kron(dn,dn),dn),dn)
rho_a = np.outer(gs2.conj(), gs2)

rho_s = csr_matrix(kron(rho_p,rho_a))

#print(sp_trace(rho_s @ Se['sz0']))

cl = np.arange(Lx)
data['x_plot'] = cl
psi_orb = np.exp(1j*kx*cl - (dkx**2 * (cl - x0)**2)/4)
psi_orb = psi_orb

#unpolarized rho_e
rho_e = kron(np.outer(psi_orb,psi_orb.conj()),eye(2)).tocsr()
rho_e = rho_e/np.trace(rho_e.toarray())
#rho_new = evolve.rk4_rho(rho)

#rho = kron(rho_e, rho_s)
#rho = rho_e

print(rho_e.shape, Ht.shape)
#print('rho', time.time()-start)
#rho_new = evolve.rk3_rho(rho, kron(H0,Ht))
#print('t step', time.time()-start)

#start time evolution
times = np.arange(0, tf+dt, dt)
result = np.zeros((len(times),3))


for t_ind, t_now in enumerate(times):
    
    #result[t_ind,0] = np.trace(rho @ Se['sz0'])
    #result[t_ind,1] = np.trace(rho @ Se['sz7'])
    #result[t_ind,2] = logarithmic_negativity(rho,'B',m=2**4)
    n =  np.array([rho_e[2*i, 2*i] + rho_e[2*i+1, 2*i+1] \
            for i in range(Lx)])
    
    plot_y = n.reshape(len(n),1)
    data['y_plot'] = plot_y
    if t_ind%10 == 0:
        plotfig(data, 's01/fig_'+str(t_ind//10), values, 
                ylim=(0,0.1), sf=sf, fs=8, lw=0.2, ft='.png')
    
    #print(t_now)

    rho_e = evolve.rk4_rho(rho_e, Ht)
"""
data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['xlabel2'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{<Sz_i>}$'
values['ylabel2'] = r'$\mathrm{Log\ negativity}$'
sf = np.array([[0,2],[2,3]])
Cl = ['C0', 'C2', 'C3', 'k']
plotfig_2p(data, 'sz', values, sf=sf, Cl=Cl)
""" 
print('run finished in:\t',time.time()-start)
