import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
from scipy.sparse import eye
from scipy.sparse import kron

class Spin:
    """A class to add external quantum spins to the system
        can also create heisenberg hamiltonian
    """

    def __init__(self,
                 S = 1/2,   #spin value
                 L = 1,     #number of sites
                 sc = 1,    #spin scale (usually used for spin 1/2)
                 kronD = 1,
                 kronOs = 1,
                 open_system = False,
                 gamma = [0,0,0,0,0,0],
                 g = [0,0,0,0,0,0],
                 dtype = np.complex128,
                 names = ['sx', 'sy', 'sz', 'sp', 'sm']):
        self.S = S
        self.L = L
        self.SC = sc
        self.dim = int(2*self.S + 1)
        self.Ns = self.dim**(self.L)
        self.kronD = kronD
        self.kronOs = kronOs
        self.os = open_system
        self.dtype = dtype
        self.names = names
        self.gamma = gamma
        self.g     = g
        self.extended_op = self.extend_to_nsites()
        #made to call pop with refular op
        self.op = self.extended_op
        if self.os: 
            self.pop = self.master_eq_op()
            self.pop = self.kron_iden_pop(self.kronOs)
        self.op = self.kron_iden(self.kronD)

        return

    def basic_op(self):
        """creates sigma matrix
        """
        row_x = []; col_x = []; data_x = []
        row_y = []; col_y = []; data_y = []
        row_z = range(self.dim)
        col_z = range(self.dim)
        data_z = -1*np.arange(-self.S,self.S+1,1)
        self.Sz = coo_matrix((data_z, (row_z,col_z)), 
            shape=((self.dim,self.dim)), dtype=self.dtype)*self.SC
        for i in range(self.dim-1):
            row_x.append(i); row_x.append(i+1)
            col_x.append(i+1); col_x.append(i)
            data_x.append(0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
            data_x.append(0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
            data_y.append(-1j*0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
            data_y.append(1j*0.5*np.sqrt( (self.S+1)*2*(i+1) - (i+1)*(i+2)))
        self.Sx = coo_matrix((data_x, (row_x,col_x)), 
            shape=((self.dim,self.dim)), dtype=self.dtype)*self.SC
        self.Sy = coo_matrix((data_y, (row_x,col_x)), 
            shape=((self.dim,self.dim)), dtype=self.dtype)*self.SC

        self.Sxp = (-self.Sz + 1j*self.Sy)
        self.Sxm = (-self.Sz - 1j*self.Sy)
        
        self.Syp = (self.Sz + 1j*self.Sx)
        self.Sym = (self.Sz - 1j*self.Sx)
        
        self.Szp = (self.Sx + 1j*self.Sy)
        self.Szm = (self.Sx - 1j*self.Sy)
        return

    def extend_to_nsites(self):
        """extend spin matrix to n sites
        """
        result = dict()    
        sites = self.L
        I4 = eye(self.dim)
        self.basic_op() 
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sx)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sx)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sx'+str(ind)] = op.tocsr()

        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sy)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sy)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sy'+str(ind)] = op.tocsr()
            
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sz)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sz)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sz'+str(ind)] = op.tocsr()

        return result
    
    def master_eq_op(self):
        result = dict()    
        sites = self.L
        I4 = eye(self.dim)
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sxp)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sxp)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sxp'+str(ind)] = op.tocsr()
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sxm)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sxm)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sxm'+str(ind)] = op.tocsr()

        """
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Syp)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Syp)
                elif n_site > ind:
                    op = kron(op, I4)
            result['syp'+str(ind)] = op.tocsr()

        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Sym)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Sym)
                elif n_site > ind:
                    op = kron(op, I4)
            result['sym'+str(ind)] = op.tocsr()
        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Szp)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Szp)
                elif n_site > ind:
                    op = kron(op, I4)
            result['szp'+str(ind)] = op.tocsr()

        for ind in range(sites):
            op = eye(1)
            for n_site in range(sites):
                if n_site == 0 and ind == 0:
                    op = kron(op, self.Szm)
                elif n_site < ind:
                    op = kron(op, I4)
                elif n_site == ind:
                    op = kron(op, self.Szm)
                elif n_site > ind:
                    op = kron(op, I4)
            result['szm'+str(ind)] = op.tocsr()

        for ind in range(sites):
            result['sxpm'+str(ind)] = result['sxp'+str(ind)] @ \
                                      result['sxm'+str(ind)]
            result['sxmp'+str(ind)] = result['sxm'+str(ind)] @ \
                                      result['sxp'+str(ind)]
            result['sypm'+str(ind)] = result['syp'+str(ind)] @ \
                                      result['sym'+str(ind)]
            result['symp'+str(ind)] = result['sym'+str(ind)] @ \
                                      result['syp'+str(ind)]
            result['szpm'+str(ind)] = result['szp'+str(ind)] @ \
                                      result['szm'+str(ind)]
            result['szmp'+str(ind)] = result['szm'+str(ind)] @ \
                                      result['szp'+str(ind)]

        for ind in range(sites-1):
            result['sxpm'+str(ind)+str(ind+1)] = result['sxp'+str(ind)] @ \
                                                 result['sxm'+str(ind+1)]
            result['sxmp'+str(ind)+str(ind+1)] = result['sxm'+str(ind)] @ \
                                                 result['sxp'+str(ind+1)]
            result['sypm'+str(ind)+str(ind+1)] = result['syp'+str(ind)] @ \
                                                 result['sym'+str(ind+1)]
            result['symp'+str(ind)+str(ind+1)] = result['sym'+str(ind)] @ \
                                                 result['syp'+str(ind+1)]
            result['szpm'+str(ind)+str(ind+1)] = result['szp'+str(ind)] @ \
                                                 result['szm'+str(ind+1)]
            result['szmp'+str(ind)+str(ind+1)] = result['szm'+str(ind)] @ \
                                                 result['szp'+str(ind+1)]
        for ind in range(sites-1):
            result['sxpm'+str(ind+1)+str(ind)] = result['sxp'+str(ind)] @ \
                                                 result['sxm'+str(ind)]
            result['sxmp'+str(ind+1)+str(ind)] = result['sxm'+str(ind)] @ \
                                                 result['sxp'+str(ind)]
            result['sypm'+str(ind+1)+str(ind)] = result['syp'+str(ind)] @ \
                                                 result['sym'+str(ind)]
            result['symp'+str(ind+1)+str(ind)] = result['sym'+str(ind)] @ \
                                                 result['syp'+str(ind)]
            result['szpm'+str(ind+1)+str(ind)] = result['szp'+str(ind)] @ \
                                                 result['szm'+str(ind)]
            result['szmp'+str(ind+1)+str(ind)] = result['szm'+str(ind)] @ \
                                                 result['szp'+str(ind)]
        """
        return result
        

    def kron_iden(self, size):
        """Kron (electronic identity, spinDOF) 
        """
        result = dict()
        names = list(self.extended_op.keys())
        for key in names:
            result[key] = kron(eye(size),self.extended_op[key])
        return result

    def kron_iden_pop(self, size):
        """Kron (electronic identity, spinDOF) 
        """
        result = dict()
        names = list(self.pop.keys())
        for key in names:
            result[key] = kron(eye(size), self.pop[key])
        return result
    
    def heisenberg(self, J=0.1, Jz=0.1, periodic=0):
        #access the operators to create number operators
        OP = self.op
        NM = self.names[:3] # only getting x,y and z 
        L = OP[NM[0]+'0'].shape[0]
        H = coo_matrix((L,L),dtype=self.dtype)
        for i in range(self.L-1):
            for nm in NM[:2]:
                H = H + J*OP[nm+str(i)] @ OP[nm+str(i+1)]
            H = H + Jz*OP[NM[2]+str(i)] @ OP[NM[2]+str(i+1)]
        if periodic:
            H = H +  J*OP['sx0'] @ OP['sx'+str(self.L-1)]
            H = H +  J*OP['sy0'] @ OP['sy'+str(self.L-1)]
            H = H + Jz*OP['sz0'] @ OP['sz'+str(self.L-1)]
        return H
    
    def sd_ham(self, se, Jsd=0.1, periodic=0):
        """Make sure to have extended the system
        """
        #access the operators to create number operators
        L = se['sx0'].shape[0]
        H = coo_matrix((L,L),dtype=self.dtype)
        for i in range(self.L):
            for nm in self.names[:3]:
                H = H - Jsd*self.op[nm+str(i)] @ se[nm+str(i)]
                #H = H - Jsd*se[nm+str(i)] @ self.op[nm+str(i)]
        return H
 

    def check_hermitian(self, a, rtol=1e-05, atol=1e-08):
        return np.allclose(a, a.conj().T, rtol=rtol, atol=atol)

    def check_equal(self, a, b, rtol=1e-05, atol=1e-06):
        return np.allclose(a, b, rtol=rtol, atol=atol)

    def check_comm(self, a, b, rtol=1e-05, atol=1e-05):
        return np.allclose(a@b, b@a, rtol=rtol, atol=atol), '%3.2e'%np.sum(np.sum(np.abs(a@b -b@a)))


