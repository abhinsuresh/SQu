import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import identity as eye, csr_matrix
import sys
from numba import jit


HBAR = 0.6582119569
#HBAR = 1

class Lindblad:
    """Spin lindblad evolution implementation
    """
    def __init__(self,
                 spin,
                 gamma  = [0, 0, 0, 0, 0, 0],
                 g      = [0, 0, 0, 0, 0, 0],
                 dt = 0.1,
                 temp=0,
                 verbose=0,
                 H = 0,
                 Lplus = 0,
                 Lminus = 0,
                 L = 0,
                 Ld = 0,
                 ):
        self.spin = spin
        self.gamma = gamma
        self.g = g
        self.dt = dt
        self.temp = temp
        self.verbose = verbose
        self.H = H
        self.Lplus = Lplus
        self.Lminus = Lminus
        self.L = L
        self.Ld = Ld
        return

    #@jit(nopython=True)
    def rk4_rho(self, rho, H): 
        """a function to evolve the wave function in time
        H_dynamic should be a function of time if not []
        
        """
        rho1 = self.drho1(rho,            H)*self.dt
        rho2 = self.drho1(rho + rho1*0.5, H)*self.dt
        rho3 = self.drho1(rho + rho2*0.5, H)*self.dt
        rho4 = self.drho1(rho + rho3,     H)*self.dt

        rho = rho + (rho1 + 2*rho2 + 2*rho3 + rho4)*(1/6)
        return rho


    #@jit(nopython=True)
    def drho1(self, rho, Ht):
        #rho_new = 1j*(rho @ Ht - Ht @ rho) + self.lindblad_1(rho) + \
        #                                     self.lindblad_2(rho)
        rho_new = self.Lplus @ rho + rho @ self.Lminus + self.L@rho@self.Ld 
        if self.verbose: print('drho calculated')
        return rho_new


    def lindblad_1(self, rho):
        result = csr_matrix(rho.shape, dtype=np.complex64)
        if sum(self.gamma) == 0:
            return result
        op  = self.spin.op
        pop = self.spin.pop
        gamma_xp = self.gamma[0]; gamma_xm = self.gamma[1]
        gamma_yp = self.gamma[2]; gamma_ym = self.gamma[3]
        gamma_zp = self.gamma[4]; gamma_zm = self.gamma[5]
        for ind in range(self.spin.L):
            #print(i)
            si = str(ind)
            #x
            if gamma_xp != 0:
                result = result + gamma_xp*( pop['sxp'+si] @ rho @
                                             pop['sxm'+si] -
                                      0.5*(  pop['sxmp'+si] @ rho +
                                             rho @ pop['sxmp'+si] ) )
            if gamma_xm != 0:
                result = result + gamma_xm*( pop['sxm'+si] @ rho @
                                             pop['sxp'+si] -
                                      0.5*(  pop['sxpm'+si] @ rho +
                                             rho @ pop['sxpm'+si] ) )
            #y
            if gamma_yp != 0:
                result = result + gamma_yp*( pop['syp'+si] @ rho @
                                             pop['sym'+si] -
                                      0.5*(  pop['symp'+si] @ rho +
                                             rho @ pop['symp'+si] ) )
            if gamma_ym != 0:
                result = result + gamma_ym*( pop['sym'+si] @ rho @
                                             pop['syp'+si] -
                                      0.5*(  pop['sypm'+si] @ rho +
                                             rho @ pop['sypm'+si] ) )
            #z
            if gamma_zp != 0:
                result = result + gamma_zp*( pop['szp'+si] @ rho @
                                             pop['szm'+si] -
                                      0.5*(  pop['szmp'+si] @ rho +
                                             rho @ pop['szmp'+si] ) )
            if gamma_zm != 0:
                result = result + gamma_zm*( pop['szm'+si] @ rho @
                                             pop['szp'+si] -
                                      0.5*(  pop['szpm'+si] @ rho +
                                             rho @ pop['szpm'+si] ) )
            
        
        return result


    def lindblad_2(self, rho):
        result = csr_matrix(rho.shape, dtype=np.complex64)
        if sum(self.g) == 0:
            return result
        op  = self.spin.op
        pop = self.spin.pop
        g_xp = self.g[0]; g_xm = self.g[1]
        g_yp = self.g[2]; g_ym = self.g[3] 
        g_zp = self.g[4]; g_zm = self.g[5]
        for ind in range(self.spin.L-1):
            #print(i)
            si = str(ind)
            sj = str(ind+1)
            #x
            if g_xp != 0:
                result = result + g_xp*( pop['sxp'+sj] @ rho @
                                             pop['sxm'+si] -
                                      0.5*(  pop['sxmp'+si+sj] @ rho +
                                             rho @ pop['sxmp'+si+sj] ) )
            if g_xm != 0:
                result = result + g_xm*( pop['sxm'+sj] @ rho @
                                             pop['sxp'+si] -
                                      0.5*(  pop['sxpm'+si+sj] @ rho +
                                             rho @ pop['sxpm'+si+sj] ) )
            #y
            if g_yp != 0:
                result = result + g_yp*( pop['syp'+sj] @ rho @
                                             pop['sym'+si] -
                                      0.5*(  pop['symp'+si+sj] @ rho +
                                             rho @ pop['symp'+si+sj] ) )
            if g_ym != 0:
                result = result + g_ym*( pop['sym'+sj] @ rho @
                                             pop['syp'+si] -
                                      0.5*(  pop['sypm'+si+sj] @ rho +
                                             rho @ pop['sypm'+si+sj] ) )
            #z
            if g_zp != 0:
                result = result + g_zp*( pop['szp'+sj] @ rho @
                                             pop['szm'+si] -
                                      0.5*(  pop['szmp'+si+sj] @ rho +
                                             rho @ pop['szmp'+si+sj] ) )
            if g_zm != 0:
                result = result + g_zm*( pop['szm'+sj] @ rho @
                                             pop['szp'+si] -
                                      0.5*(  pop['szpm'+si+sj] @ rho +
                                             rho @ pop['szpm'+si+sj] ) )
        return result




#-------------------------------------------------------------------
#wavefunction evolution    
#-------------------------------------------------------------------
def evolve(H_static, H_dynamic, psi, dt, method='diag', time_dep=0):
    """a function to evolve the wave function in time
    if time independet, we return Ut and should be called outside
    the loop
    """
    size = H_static.shape[0]
    if method == 'gpu' or method == 'CN_gpu':
        import pycuda.gpuarray as gpuarray
        import pycuda.autoinit
        from skcuda import linalg
        from skcuda import misc
        linalg.init()
    
    if time_dep == 0:
        if method=='expm': 
            #Ut = la.expm(-1j*H_static.toarray()*dt/HBAR)
            Ut = la.expm(-1j*H_static*dt/HBAR)
            print('expm')
            return Ut
        elif method=='eig_vec':
            w,v = la.eigh(H_static.toarray())
            Ut = np.asarray([np.exp(-1j*w[i]*dt) for i in range(size)])
            return Ut
        elif method=='CN':
            Ut = sla.inv((eye(size) + 1j*dt*H_static/(2*HBAR)).tocsc()) @ \
                       (eye(size) - 1j*dt*H_static/(2*HBAR))
            return Ut
        elif method=='CN_spilu':
            B = sla.spilu(eye(size) + 1j*dt*H_static/(2*HBAR))
            return B
        elif method=='CN_gpu':
            mat      = (eye(size) + 1j*dt*H_static/(2*HBAR))
            a_gpu    = gpuarray.to_gpu(mat.toarray()) 
            ainv_gpu = linalg.inv(a_gpu, overwrite=True)
            mat_inv  = ainv_gpu.get()
            Ut       = mat_inv @ (eye(size) - 1j*dt*H_static/(2*HBAR))
            return Ut
    else: 
        if method=='expm': 
            Ht = H_static + H_dynamic
            Ut = la.expm(-1j*Ht*dt/HBAR)
            psi_new = Ut @ psi
            return psi

        elif method=='CN':
            Ht = H_static + H_dynamic
            Ut = la.inv(np.eye(size) + 1j*dt*Ht/(2*HBAR)) @ \
                        (np.eye(size) - 1j*dt*Ht/(2*HBAR))
            psi = Ut @ psi
            return psi
            
        if method=='eig_vec':
            Ht = H_static + H_dynamic
            w, v = la.eigh(Ht.toarray())
            Ut = np.asarray([np.exp(-1j*w[i]*dt) for i in range(2)])
            cn = [v[:,i].conj() @ psi for i in range(2)]
            psi = np.sum([cn[i] * (Ut[i] * v[:,i]) for i in range(2)], axis=0)
            return psi

        elif method=='CN_gpu':
            Ht = H_static + H_dynamic
            mat      = (eye(size) + 1j*dt*Ht/(2*HBAR))
            a_gpu    = gpuarray.to_gpu(mat.toarray()) 
            ainv_gpu = linalg.inv(a_gpu, overwrite=True)
            mat_inv  = ainv_gpu.get()
            Ut       = mat_inv @ (eye(size) - 1j*dt*Ht/(2*HBAR))
            psi = Ut @ psi
            return psi
