#!/bin/bash
#SBATCH --job-name='hphi_14'
#SBATCH --output=LOGFILE
#SBATCH --nodes=1
#SBATCH --mem=100G
#SBATCH --error=ERR
#SBATCH --time=01:00:00
#SBATCH --tasks-per-node=64
#SBATCH --partition=idle
export OMP_NUM_THREADS=1
vpkg_require hphi
vpkg_require openmpi/4.1.0
mpiexec -np 64 hphi -e namelist.def > a.out
