import numpy as np, sys, h5py
Ns = 8

#site 0 0
#site 1 1
#site 0 1
#site 1 0


tspan = np.arange(0,10000,20)
spin = np.zeros((len(tspan),3,Ns))
mz2exp = np.zeros(len(tspan))
#
curr_x = np.zeros(len(tspan))
curr_y = np.zeros(len(tspan))

for t_ind, t_now in enumerate(tspan): 

    green1 = np.loadtxt('output/zvo_cisajs_step%d.dat'%t_now)
    green2 = np.loadtxt('output/zvo_cisajscktalt_step%d.dat'%t_now)
    #print('green1 shape:\t', green1.shape) #4*L : 6


    for ind in range(Ns):
        spin[t_ind,0,ind] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) + \
                             (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/2

        spin[t_ind,1,ind] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) - \
                             (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/(2j)

        spin[t_ind,2,ind] = ((green1[4*ind+1,4]+1j*green1[4*ind+1,5]) - \
                             (green1[4*ind+0,4]+1j*green1[4*ind+0,5]))/2

    for ind in range(Ns):
        mz2exp[t_ind] += ((green1[4*ind+1,4]+1j*green1[4*ind+1,5]) + \
                          (green1[4*ind+0,4]+1j*green1[4*ind+0,5]) - \
                          (green2[ind,8]*2))

#print(spin.T)
#print(mz2exp/Ns)
result = np.zeros((len(tspan),3))
result[:,0] = spin[:,2,0] + spin[:,2,4]
result[:,1] = spin[:,2,1] + spin[:,2,5]
result[:,2] = mz2exp/Ns



f = h5py.File('results/figte2.hdf5', 'w')
f.create_dataset('y_plot', data = result)
f.create_dataset('x_plot', data = tspan*0.05)
f.close()
