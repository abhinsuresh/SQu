#!/bin/bash
#SBATCH --job-name='hphi_test'
#SBATCH --output=LOGFILE
#SBATCH --nodes=4
#SBATCH --error=ERR
#SBATCH --time=00-01:00
#SBATCH --tasks-per-node=64
#SBATCH --partition=standard
export OMP_NUM_THREADS=1
vpkg_require hphi
mpiexec -np 256 hphi -s stan2.in > a.out
