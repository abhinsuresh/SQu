import sys, numpy as np, h5py

sys.path.append('../../plot_manager')
from plot2d import plotfig_2p

data = dict();values=dict()

Ns = 8
J  = 0.1
#file1
var = np.arange(1,201,5)
spinexp = np.zeros((len(var),Ns,3))
mz2exp  = np.zeros(len(var))

result = np.zeros((len(var),3))

for Jz_ind, Jz in enumerate(var):
    green1 = np.loadtxt('dir_%02d'%Jz_ind+'/output/zvo_cisajs_eigen0.dat')
    green2 = np.loadtxt('dir_%02d'%Jz_ind+'/output/zvo_cisajscktalt_eigen0.dat')
    for ind in range(Ns):
        spinexp[Jz_ind,ind,0] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) + \
                              (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/2

        spinexp[Jz_ind,ind,1] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) - \
                              (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/(2j)

        spinexp[Jz_ind,ind,2] = ((green1[4*ind+1,4]+1j*green1[4*ind+1,5]) - \
                              (green1[4*ind+0,4]+1j*green1[4*ind+0,5]))/2

    for ind in range(Ns):
        mz2exp[Jz_ind] += ((green1[4*ind+1,4]+1j*green1[4*ind+1,5]) + \
                           (green1[4*ind+0,4]+1j*green1[4*ind+0,5]) - \
                           (green2[ind,8]*2))

result[:,0] = spinexp[:,0,2] + spinexp[:,4,2]
result[:,1] = spinexp[:,1,2] + spinexp[:,5,2]
result[:,2] = mz2exp/Ns

data['x_plot'] = var; data['y_plot'] = result
#print(spinexp[:,1,2], len(var))
values['xlabel1'] = r'$\mathrm{J_z/J}$'; 
values['xlabel2'] = r'$\mathrm{J_z/J}$'; 
values['ylabel1'] = r'$\mathrm{\left<  \hat{s}_i^z \right>}$'
values['ylabel2'] = r'$\mathrm{\left<  \hat{m}_z^2 \right>}$'
Cl = ['k','C2','C3']*2
Ls = ['-','--','-']*2
Gs = dict()
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.12; Gs['r']=0.98
Gs['w']=0.3; Gs['h']=0.4

sf = np.array([[0,2],[2,3]])
#plotfig_2p(data, 'x', values, Cl=Cl, Ls=Ls, lw=0.4, minor=1, sf=sf, Gs=Gs)


f = h5py.File('results/fig10.hdf5', 'w')
f.create_dataset('y_plot', data = data['y_plot'][:])
f.create_dataset('x_plot', data = data['x_plot'][:])
f.close()
