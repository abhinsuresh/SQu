#!/bin/bash

#create and array
v_i=1
v_f=205
v_s=5

Jz=($(seq $v_i $v_s $v_f))

#print all elements in an array
ind=0

#checking the existence of directores
if [ -d "dir_00" ]
then
    echo "Directories exist! Quiting the script"
    read -p "Do you want to delete current dirs [Y/n]:" del_flag
    if [ $del_flag == Y ]
    then
        rm -rf dir_*
    elif [ $del_flag == n ]
    then
        echo $del_flag
    else
        echo "unknown input! exiting code"
    fi
    exit 1
else
    echo "Making directories and creating input files into dirs"
    #looping to make directores
    for i in ${Jz[@]}
    do
        val=$(printf "%0*d\n" 2 $ind)
        mkdir dir_$val
        python make_input.py $i $val
        let "ind = $ind + 1"
        #running hphi code
        cd dir_$val
        hphi -e namelist.def > a.out
        cd ..
    done
    #plotting and saving hdf5 using python
    python plotfig $v_i $v_s $v_f
fi
