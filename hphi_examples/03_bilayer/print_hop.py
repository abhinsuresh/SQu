import numpy as np
from index_core import hop_list

W = 3       #(along x)
L = 3       #(along y)
U = 0.0
t = 0.0
Jexc = 1.0
Jsd = 1.0
Bx  = 0
nm = ''
Sz = 0
tf = 10
dt = 0.01
Ntf = int(tf/dt)

Ns = W*L
Nele = Ns; Nspin = Ns
Ntot = Nele + Nspin
Ncond = Nele

k = 1; kh = 1
hopx_list, hopy_list = hop_list(W, L)
Ntrans = (len(hopx_list) + len(hopy_list))
print(np.array(hopx_list))
print(np.array(hopy_list

