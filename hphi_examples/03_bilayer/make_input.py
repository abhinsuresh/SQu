import numpy as np
from index_core import hop_list

W = 2       #(along x)
L = 2       #(along y)
U = 0.0
t = 1.0
Jexc = 0.0
Jsd = 0.0
Bx  = 0
nm = ''
Sz = 0
tf = 10
dt = 0.01
Ntf = int(tf/dt)

Ns = W*L*2
Nele = 4; Nspin = 0
Ntot = Ns + Nspin
Ncond = Nele

k = 1; kh = 1
hopx_list, hopy_list, hopz_list = hop_list(W, L, PBC=1, bi=1)
Ntrans = (len(hopx_list) + len(hopy_list) + len(hopz_list))
print(Ntrans)
#print(np.array(hopx_list))
#print(np.array(hopy_list))


#to edit with change
#------------------------------------------------------------
calc_mod = 0       #(0: Hubbard, 3:HubbardGC)
calc_type   = 3     #(3;CG, 4:TE, 2:FullDiag, 1:TPQ, 0:Lancoz)
eigout   = 0
eigin    = 0
kh_te1      = 0
#------------------------------------------------------------
k_locspn    = k
k_hop       = k
k_ex        = 0
k_cInter    = 0
k_hund      = 0

kh_ex       = 0
kh_mag      = 0
kh_cInter   = 0
kh_hund     = 0
l_max       = 2000              # l_max have relation with max time step
exp_c       = 10
if kh:
    if L==1: LV = 2*t + 1.125*Jsd + 0.5*U + 2.25*Jexc
    else: LV = 4*t + 1.125*Jsd + 2*U + 4.5*Jexc

nmls = ""
nmls = nmls + "ModPara\tmodpara.def\n"
nmls = nmls + "CalcMod\tcalcmod.def\n"
nmls = nmls + "LocSpin\tlocspn.def\n"
#nmls = nmls + "CoulombInter\tcoulombinter.def\n"
#nmls = nmls + "Hund\thund.def\n"
#nmls = nmls + "Exchange\texchange.def\n"
nmls = nmls + "Trans\ttrans.def\n"
#if sp: nmls = nmls + "OneBodyG\tgreenone.def\n"
if kh_te1: nmls = nmls + "TEOneBody\t teone.def\n"
if kh_te1: nmls = nmls + "SpectrumVec\t zvo_eigenvec_0"
with open(nm+'namelist.def', 'w') as f:
    f.write(nmls)
f.close()
#if (sp)nmls = nmls + "TwoBodyG\tgreentwo.def\n"
#nmls = nmls + "PairExcitation\tpair.def\n"
#nmls = nmls + "SpectrumVec\tzvo_eigenvec_0\n"

#------------------------------------------------------------------
calc = ""
calc = calc + "#CalcType = 0:Lanczos, 1:TPQCalc, 2:FullDiag, 3:CG, 4:Time-evolution\n"
calc = calc + "#CalcModel = 0:Hubbard, 1:Spin, 2:Kondo, 3:HubbardGC, 4:SpinGC, 5:KondoGC\n"
calc = calc + "#Restart = 0:None, 1:Save, 2:Restart&Save, 3:Restart\n"
calc = calc + "#CalcSpec = 0:None, 1:Normal, 2:No H*Phi, 3:Save, 4:Restart, 5:Restart&Save\n"
calc = calc + "CalcType   %d\n"%calc_type
calc = calc + "CalcModel   %d\n"%calc_mod
calc = calc + "ReStart   0\n"
calc = calc + "CalcSpec   0\n"
calc = calc + "CalcEigenVec   0\n"
calc = calc + "InitialVecType   0\n"
calc = calc + "InputEigenVec   %d\n"%eigin
calc = calc + "OutputEigenVec   %d\n"%eigout
calc = calc + "InputHam   0\n"
calc = calc + "OutputHam   0\n"
calc = calc + "OutputExVec   0"
calc = calc.expandtabs(4)
with open(nm+'calcmod.def', 'w') as f:
    f.write(calc)
f.close()

#------------------------------------------------------------------
mod = ""
mod = mod + "--------------------\n"
mod = mod + "Model_Parameters   0\n"
mod = mod + " --------------------\n"
mod = mod + "HPhi_Cal_Parameters\n"
mod = mod + "--------------------\n"
mod = mod + "CDataFileHead  zvo\n"
mod = mod + "CParaFileHead  zqp\n"
mod = mod + "--------------------\n"
mod = mod + "Nsite          %d\n"%Ntot
if (calc_mod != 3 ): mod = mod + "2Sz            %d\n"%Sz
if (calc_mod != 3 ): mod = mod + "Ncond          %d\n"%Ncond
mod = mod + "Lanczos_max    %d\n"%l_max
mod = mod + "initial_iv     -1\n" 
mod = mod + "exct           1\n"  
mod = mod + "LanczosEps     14\n"   
mod = mod + "LanczosTarget  2\n"   
mod = mod + "LargeValue     %1.15e\n"%LV
mod = mod + "NumAve         5\n"
mod = mod + "ExpecInterval  20\n"
mod = mod + "NOmega         200\n"
mod = mod + "OmegaMax       %1.15e"%(LV*Ntot)+"     "+"%1.15e\n"%(0.01*LV)
mod = mod + "OmegaMin       %1.15e"%(-LV*Ntot)+"     "+"%1.15e\n"%(0.01*LV)
mod = mod + "OmegaOrg       0.000000000000000e+00     0.000000000000000e+00\n"
if (calc_type == 4): mod = mod + "ExpandCoef     %d\n"%exp_c
mod = mod.expandtabs(4)
with open(nm+'modpara.def', 'w') as f:
    f.write(mod)
f.close()

#------------------------------------------------------------------
if k_locspn:
    """locspin file
    """
    locsp = "="*24 + '\nNlocalSpin\t' + str(0) +"\n" + \
            "="*24 + "\n" + "="*3 + "i_1LocSpn_0IterElc" + "="*3 + "\n" + "="*24
    for ele in range(Ns):
        locsp = locsp + "\n" + \
        "\t" + str(ele) + "\t\t" + str(0)
    locsp = locsp.expandtabs(4)
    with open(nm+'locspn.def', 'w') as f:
        f.write(locsp)
    f.close()
    
if k_hop:
    """Spin independent kinetic hopping between the given list
    """
    trans = "="*24 + '\nNTransfer\t' + str(Ntrans*4 + 2) +"\n" + \
            "="*24 + "\n" + "="*7 + "i_j_s_tijs" + "="*7 + "\n" + "="*24
    for ele in hopx_list + hopy_list + hopz_list:
        trans = trans + "\n" + \
        "\t" + str(ele[1]) + "\t0\t" + str(ele[0]) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%t + "%1.15f"%0 + "\n" + \
        "\t" + str(ele[0]) + "\t0\t" + str(ele[1]) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%t + "%1.15f"%0 + "\n" + \
        "\t" + str(ele[1]) + "\t1\t" + str(ele[0]) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%t + "%1.15f"%0 + "\n" + \
        "\t" + str(ele[0]) + "\t1\t" + str(ele[1]) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%t + "%1.15f"%0
    trans = trans.expandtabs(4)
    with open(nm+'trans.def', 'w') as f:
        f.write(trans)
    f.close()

    
if k_ex:
    """Kondo exchange part
    """
    Num = Ns
    if kh_ex: Num = Num + Ntrans
    exchange = "="*45 + '\nNExchange\t' + str(Num) +"\n" + \
            "="*45 + "\n" + "="*9 + " ExchangeCoupling coupling " + "="*9 + "\n" + "="*45
    for ele in range(Ns):
        exchange = exchange + "\n" + \
        "\t" + str(ele+Ns) + "\t" + str(ele) + "\t\t" + "%1.15f"%(-Jsd/2)
    exchange = exchange.expandtabs(4)
    with open(nm+'exchange.def', 'w') as f:
        f.write(exchange)
    f.close()

if k_cInter:
    """Kondo couloumb inter part
    """
    Num = Ns
    if kh_cInter: Num = Num + Ntrans
    coulomb = "="*45 + '\nNCoulombInter\t' + str(Num) +"\n" + \
            "="*45 + "\n" + "="*16 + " CoulombInter " + "="*16 + "\n" + "="*45
    for ele in range(Ns):
        coulomb = coulomb + "\n" + \
        "\t" + str(ele+Ns) + "\t" + str(ele) + "\t\t" + "%1.15f"%(-Jsd/4)
    coulomb = coulomb.expandtabs(4)
    with open(nm+'coulombinter.def', 'w') as f:
        f.write(coulomb)
    f.close()

if k_hund:
    """Spin hund coupling
    """
    Num = Ns
    if kh_hund: Num = Num + Ntrans
    hund = "="*45 + '\nNHund\t' + str(Num) +"\n" + \
            "="*45 + "\n" + "="*14 + " Hund Coupling " + "="*14 + "\n" + "="*45
    for ele in range(Ns):
        hund = hund + "\n" + \
        "\t" + str(ele+Ns) + "\t" + str(ele) + "\t\t" + "%1.15f"%(-Jsd/2)
    hund = hund.expandtabs(4)
    with open(nm+'hund.def', 'w') as f:
        f.write(hund)
    f.close()

if kh_ex:
    """Spin exchange part
    """
    exchange = ""
    for ele in hopx_list + hopy_list:
        exchange = exchange + "\n" + \
        "\t" + str(ele[0]) + "\t" + str(ele[1]) + "\t\t" + "%1.15f"%(-Jexc/2)
    exchange = exchange.expandtabs(4)
    with open(nm+'exchange.def', 'a') as f:
        f.write(exchange)
    f.close()

if kh_cInter:
    """Spin couloumb part
    """
    coulomb = ""
    for ele in hopx_list + hopy_list:
        coulomb = coulomb + "\n" + \
        "\t" + str(ele[0]) + "\t" + str(ele[1]) + "\t\t" + "%1.15f"%(-Jexc/4)
    coulomb = coulomb.expandtabs(4)
    with open(nm+'coulombinter.def', 'a') as f:
        f.write(coulomb)
    f.close()

if kh_hund:
    """Spin hund coupling
    """
    hund = ""
    for ele in hopx_list + hopy_list:
        hund = hund + "\n" + \
        "\t" + str(ele[0]) + "\t" + str(ele[1]) + "\t\t" + "%1.15f"%(-Jexc/2)
    hund = hund.expandtabs(4)
    with open(nm+'hund.def', 'a') as f:
        f.write(hund)
    f.close()

if kh_te1:
    """time dependent part of the hamiltonian
    """
    te1 = "="*38 + '\nAllTimeStep\t' + str(Ntf) +"\n" + \
            "="*38 + "\n" + "="*7 + " OneBody Time Evolution " + "="*7 + "\n" + "="*38
    for t_now in np.arange(0,tf,dt):
        te1 = te1 + "\n" + "%0.6f\t"%t_now + str(2*Ns)
        for ele in range(Ns):
            te1 = te1 + "\n" + \
            "\t" + str(ele) + "\t0\t" + str(ele) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%0 + "%1.15f"%0 + "\n" + \
            "\t" + str(ele) + "\t1\t" + str(ele) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%0 + "%1.15f"%0
    te1 = te1.expandtabs(4)
    with open(nm+'teone.def', 'w') as f:
        f.write(te1)
    f.close()
