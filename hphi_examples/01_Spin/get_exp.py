import numpy as np, sys
Ns = 8

#site 0 0
#site 1 1
#site 0 1
#site 1 0

green1 = np.loadtxt('output/zvo_cisajs_eigen0.dat')
print(green1.shape) #4*L : 6

spin = np.zeros((3,Ns))

for ind in range(Ns):
    spin[0,ind] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) + \
                   (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/2

    spin[1,ind] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) - \
                   (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/(2j)

    spin[2,ind] = ((green1[4*ind+1,4]+1j*green1[4*ind+1,5]) - \
                   (green1[4*ind+0,4]+1j*green1[4*ind+0,5]))/2

print(spin.T)
