import numpy as np, sys
sys.path.append('../../plot_manager/')
from plot2d import plotfig
Ns = 8

#site 0 0
#site 1 1
#site 0 1
#site 1 0


times = np.arange(0,10,0.2)
result = np.zeros((len(times),3)) 

for t_ind, t_now in enumerate(times): 
    green1 = np.loadtxt('output/zvo_cisajs_step%d.dat'%(t_now*100))
    #print(green1.shape) #4*L : 6
    spin = np.zeros((3,Ns))
    for ind in range(Ns):
        spin[0,ind] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) + \
                       (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/2

        spin[1,ind] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) - \
                       (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/(2j)

        spin[2,ind] = ((green1[4*ind+1,4]+1j*green1[4*ind+1,5]) - \
                       (green1[4*ind+0,4]+1j*green1[4*ind+0,5]))/2
    result[t_ind, 0:3] = spin[0,0:3]


data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{<Sx_i>}$'
sf = np.array([[0,3]])
Cl = ['C0', 'C2', 'C3', 'k']
plotfig(data, 'sz_l', values, sf=sf, Cl=Cl)

