import sys, numpy as np

sys.path.append('../../plot_manager')
from plot2d import plotfig

data = dict();values=dict()

Ns = 8
J  = 0.1
#file1
Jz_vals = np.arange(1,298,8)
spinexp = np.zeros((len(Jz_vals),Ns,3))

for Jz_ind, Jz in enumerate(np.arange(1,298,8)):
    green1 = np.loadtxt('dir_%02d'%Jz_ind+'/output/zvo_cisajs_eigen0.dat')
    for ind in range(Ns):
        spinexp[Jz_ind,ind,0] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) + \
                              (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/2

        spinexp[Jz_ind,ind,1] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) - \
                              (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/(2j)

        spinexp[Jz_ind,ind,2] = ((green1[4*ind+1,4]+1j*green1[4*ind+1,5]) - \
                              (green1[4*ind+0,4]+1j*green1[4*ind+0,5]))/2



data['x_plot'] = Jz_vals; data['y_plot'] = spinexp[:,1,2]
print(spinexp[:,1,2], len(Jz_vals))
values['xlabel1'] = r'$\mathrm{J_z/J}$'; 
values['xlabel2'] = r'$\mathrm{J_z/J}$'; 
values['ylabel1'] = r'$\mathrm{\left<  \hat{s}_z \right>}$'
values['ylabel2'] = r'$\mathrm{Entanglement~Entropy}$'
Cl = ['k','C2','C3']*2
Ls = ['-','--',':']*2
Gs = dict()
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.12; Gs['r']=0.98
Gs['w']=0.3; Gs['h']=0.4

sf = np.array([[0,3],[3,6]])
plotfig(data, '3', values, Cl=Cl, Ls=Ls, lw=0.4, minor=1)
