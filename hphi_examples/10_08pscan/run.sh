#!/bin/bash

#create and array
Jz=($(seq 1 3 101))

#print all elements in an array
ind=0

#checking the existence of directores
if [ -d "dir_01" ]
then
    echo "Directories exist! Quiting the script"
    read -p "Do you want to delete current dirs [Y/n]:" del_flag
    if [ $del_flag == Y ]
    then
        rm -rf dir_*
    elif [ $del_flag == n ]
    then
        echo $del_flag
    else
        echo "unknown input! exiting code"
    fi
    exit 1
else
    echo "Making directories and creating input files into dirs"
    #looping to make directores
    for i in ${Jz[@]}
    do
        val=$(printf "%0*d\n" 2 $ind)
        mkdir dir_$val
        python make_input.py $i $val
        let "ind = $ind + 1"
        #running hphi code
        cp submit.qs dir_$val/.
        cd dir_$val
        #hphi -e namelist.def > a.out
        sbatch submit.qs
        cd ..
    done
fi
