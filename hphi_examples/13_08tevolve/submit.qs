#!/bin/bash
#SBATCH --job-name='hphi_13'
#SBATCH --output=LOGFILE
#SBATCH --nodes=16
#SBATCH --mem=400G
#SBATCH --error=ERR
#SBATCH --time=2-0
#SBATCH --tasks-per-node=64
#SBATCH --partition=standard
export OMP_NUM_THREADS=1
vpkg_require hphi
vpkg_require openmpi/4.1.0
mpiexec -np 1024 hphi -e namelist.def > a.out
