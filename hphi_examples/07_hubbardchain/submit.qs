#!/bin/bash
#SBATCH --job-name='hphi_test'
#SBATCH --output=LOGFILE
#SBATCH --nodes=1
#SBATCH --error=ERR
#SBATCH --time=02:00:00
#SBATCH --tasks-per-node=64
#SBATCH --partition=idle
export OMP_NUM_THREADS=1
vpkg_require hphi
mpiexec -np 64 hphi -e namelist.def > a.out
