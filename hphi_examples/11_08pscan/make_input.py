import numpy as np, sys
from index_core import hop_orb_list

W       = 4       #(along x)
L       = 2       #(along y)
t       = 1.0
U       = 8.0
Jexc    = 0.1
Jexc_z  = Jexc*int(sys.argv[1])
nm      = sys.argv[2]
Jz      = Jexc_z - Jexc
JH      = 1.0
Uprime  = U - 2*JH
Mu      = (3*U - 5*JH)/2
gJH     = 1
Bz      = 1e-3
orb     = 2
tso     = 0.0

Ns = W*L*orb
Nele = Ns
Ntot = Ns
Ncond = Nele

hopx_list, hopy_list = hop_orb_list(W, L, PBC=1)
Ntrans = (len(hopx_list) + len(hopy_list))
#print(np.array(hopx_list))
#print(np.array(hopy_list))

#to edit with change
#------------------------------------------------------------
calc_mod = 0       #(0: Hubbard, 3:HubbardGC)
calc_type   = 3     #(3;CG, 4:TE, 2:FullDiag, 1:TPQ, 0:Lancoz)
eigout   = 0
eigin    = 0
kh_te1      = 0
#------------------------------------------------------------
h = 1
locspn      = h
hop         = h
rso         = h
cIntra      = h
#Jexc  
ex          = h
cInter      = h
hund        = h
Jising      = 1
#Uprime and JH
interall    = 1
#mu
mu          = 1
#gJH
gamma_JH    = 1
#spin expectation values
one_body    = 1
two_body    = 1
#magnetic field
mag         = 1

l_max       = 2000              # l_max have relation with max time step
exp_c       = 10

#if L==1: LV = 2*t + 0.5*U + 2.25*Jexc
#else: LV = 4*t + 2*U + 1.125*Jexc
LV = 0

nmls = ""
nmls = nmls + "ModPara \t\tmodpara.def\n"
nmls = nmls + "CalcMod \t\tcalcmod.def\n"
nmls = nmls + "LocSpin \t\tlocspn.def\n"
nmls = nmls + "Trans \t\t\ttrans.def\n"
nmls = nmls + "CoulombIntra\tcoulombintra.def\n"
nmls = nmls + "Exchange \t\texchange.def\n"
nmls = nmls + "CoulombInter\tcoulombinter.def\n"
nmls = nmls + "Hund \t\t\thund.def\n"
if Jising: nmls = nmls + "Ising \t\t\tising.def\n"
if interall: nmls = nmls + "InterAll \t\tzinterall.def\n"
if gamma_JH: nmls = nmls + "PairHop \t\tpairhop.def\n"
if one_body: nmls = nmls + "OneBodyG \t\tgreenone.def\n"
if two_body: nmls = nmls + "TwoBodyG \t\tgreentwo.def\n"
if kh_te1: nmls = nmls + "TEOneBody \t\tteone.def\n"
if kh_te1: nmls = nmls + "SpectrumVec \t\tzvo_eigenvec_0"
nmls = nmls.expandtabs(4)
with open('dir_'+nm+'/namelist.def', 'w') as f:
    f.write(nmls)
f.close()
#if (sp)nmls = nmls + "TwoBodyG\tgreentwo.def\n"
#nmls = nmls + "PairExcitation\tpair.def\n"
#nmls = nmls + "SpectrumVec\tzvo_eigenvec_0\n"

#------------------------------------------------------------------
calc = ""
calc = calc + "#CalcType = 0:Lanczos, 1:TPQCalc, 2:FullDiag, 3:CG, 4:Time-evolution\n"
calc = calc + "#CalcModel = 0:Hubbard, 1:Spin, 2:Kondo, 3:HubbardGC, 4:SpinGC, 5:KondoGC\n"
calc = calc + "#Restart = 0:None, 1:Save, 2:Restart&Save, 3:Restart\n"
calc = calc + "#CalcSpec = 0:None, 1:Normal, 2:No H*Phi, 3:Save, 4:Restart, 5:Restart&Save\n"
calc = calc + "CalcType   %d\n"%calc_type
calc = calc + "CalcModel   %d\n"%calc_mod
calc = calc + "ReStart   0\n"
calc = calc + "CalcSpec   0\n"
calc = calc + "CalcEigenVec   0\n"
calc = calc + "InitialVecType   0\n"
calc = calc + "InputEigenVec   %d\n"%eigin
calc = calc + "OutputEigenVec   %d\n"%eigout
calc = calc + "InputHam   0\n"
calc = calc + "OutputHam   0\n"
calc = calc + "OutputExVec   0"
calc = calc.expandtabs(4)
with open('dir_'+nm+'/calcmod.def', 'w') as f:
    f.write(calc)
f.close()

#--------------------------------------------------------------------
if locspn:
    """locspin file
    """
    locsp = "="*24 + '\nNlocalSpin\t' + str(0) +"\n" + \
            "="*24 + "\n" + "="*3 + "i_1LocSpn_0IterElc" + "="*3 + "\n" + "="*24
    for ele in range(Ns):
        locsp = locsp + "\n" + \
        "\t" + str(ele) + "\t\t" + str(0)
    locsp = locsp.expandtabs(4)
    with open('dir_'+nm+'/locspn.def', 'w') as f:
        f.write(locsp)
    f.close()
    
#--------------------------------------------------------------------
if hop:
    """Spin independent kinetic hopping between the given list
    """
    #Large value
    LV = LV + (4*Ntrans)*t

    Num = Ntrans*4
    if mu: Num = Num + W*L*orb*2
    if mag: Num = Num + 2
    if rso: Num = Num + Ntrans*4
    
    
    trans = "="*24 + '\nNTransfer\t' + str(Num) +"\n" + \
            "="*24 + "\n" + "="*7 + "i_j_s_tijs" + "="*7 + "\n" + "="*24
    for ele in hopx_list + hopy_list:
        trans = trans + "\n" + \
        "\t" + str(ele[1]) + "\t0\t" + str(ele[0]) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%t + "%1.15f"%0 + "\n" + \
        "\t" + str(ele[0]) + "\t0\t" + str(ele[1]) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%t + "%1.15f"%0 + "\n" + \
        "\t" + str(ele[1]) + "\t1\t" + str(ele[0]) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%t + "%1.15f"%0 + "\n" + \
        "\t" + str(ele[0]) + "\t1\t" + str(ele[1]) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%t + "%1.15f"%0
    trans = trans.expandtabs(4)
    with open('dir_'+nm+'/trans.def', 'w') as f:
        f.write(trans)
    f.close()
    
#--------------------------------------------------------------------
if cIntra:
    """Coulomb interaction on all sites
    """
    #Large value
    LV = LV + (Ns)*U

    coulomb = "="*45 + '\nNCoulombIntra\t' + str(Ns) +"\n" + \
            "="*45 + "\n" + "="*18 + " CoulombIntra " + "="*18 + "\n" + "="*45
    for ele in range(Ns):
        coulomb = coulomb + "\n" + "\t" + str(ele) + "\t\t" + "%1.15f\t\t"%U
    coulomb = coulomb.expandtabs(4)
    with open('dir_'+nm+'/coulombintra.def', 'w') as f:
        f.write(coulomb)
    f.close()

#--------------------------------------------------------------------
if ex:
    """exchange part 1
    """
    #Large value
    LV = LV + (Ntrans)*(Jexc/2)

    Num = Ntrans
    if gamma_JH: Num = Num + W*L
    
    exchange = "="*45 + '\nNExchange\t' + str(Num) +"\n" + \
            "="*45 + "\n" + "="*9 + " ExchangeCoupling coupling " + "="*9 + "\n" + "="*45
    for ele in hopx_list + hopy_list:
        exchange = exchange + "\n" + "\t" + str(ele[0]) + "\t" + str(ele[1]) + "\t\t" + "%1.15f"%(-Jexc/2)
    exchange = exchange.expandtabs(4)
    with open('dir_'+nm+'/exchange.def', 'w') as f:
        f.write(exchange)
    f.close()

if cInter:
    """exchange part 2
    """
    #Large value
    LV = LV + (Ntrans)*(Jexc/4)

    coulomb = "="*45 + '\nNCoulombInter\t' + str(Ntrans) +"\n" + \
            "="*45 + "\n" + "="*16 + " CoulombInter " + "="*16 + "\n" + "="*45
    for ele in hopx_list + hopy_list:
        coulomb = coulomb + "\n" + "\t" + str(ele[0]) + "\t" + str(ele[1]) + "\t\t" + "%1.15f"%(-Jexc/4)
    coulomb = coulomb.expandtabs(4)
    with open('dir_'+nm+'/coulombinter.def', 'w') as f:
        f.write(coulomb)
    f.close()

if hund:
    """exchange part 3
    """
    #Large value
    LV = LV + (Ntrans)*Jexc/2

    Num = Ntrans
    hund = "="*45 + '\nNHund\t' + str(Num) +"\n" + \
            "="*45 + "\n" + "="*14 + " Hund Coupling " + "="*14 + "\n" + "="*45
    for ele in hopx_list + hopy_list:
        hund = hund + "\n" + "\t" + str(ele[0]) + "\t" + str(ele[1]) + "\t\t" + "%1.15f"%(-Jexc/2)
    hund = hund.expandtabs(4)
    with open('dir_'+nm+'/hund.def', 'w') as f:
        f.write(hund)
    f.close()

if Jising:
    """exchange ising coupling
    """
    #Large value
    LV = LV + (Ntrans)*Jz

    ising = "="*45 + '\nNIsing\t' + str(Ntrans) +"\n" + \
            "="*45 + "\n" + "="*14 + " Ising " + "="*14 + "\n" + "="*45
    for ele in hopx_list + hopy_list:
        ising = ising + "\n" + \
        "\t" + str(ele[0]) + "\t" + str(ele[1]) + "\t\t" + "%1.15f"%(Jz)
    ising = ising.expandtabs(4)
    with open('dir_'+nm+'/ising.def', 'w') as f:
        f.write(ising)
    f.close()

#--------------------------------------------------------------------
if interall:
    """InterAll file for Uprime and JH part
    """
    #Large value
    LV = LV + (W*L*2)*(Uprime-JH)
    LV = LV + (W*L*2)*(Uprime)

    N_2d = W*L
    Num = N_2d*4
    #if gamma_JH: Num = Num + N_2d*4
    inter = "="*24 + '\nNInterAll\t' + str(Num) +"\n" + \
            "="*24 + "\n" + "="*7 + "zInterAll" + "="*7 + "\n" + "="*24
    for ele in range(N_2d):
        inter = inter + "\n" + \
        "\t" + str(ele) + "\t0\t" + str(ele) + "\t0\t" + str(ele+N_2d) + "\t" + "0\t" + str(ele+N_2d) + "\t" + "0"  + "\t\t" + "%1.15f\t\t"%(Uprime-JH) + "%1.15f"%0 + "\n" + \
        "\t" + str(ele) + "\t1\t" + str(ele) + "\t1\t" + str(ele+N_2d) + "\t" + "1\t" + str(ele+N_2d) + "\t" + "1"  + "\t\t" + "%1.15f\t\t"%(Uprime-JH) + "%1.15f"%0 + "\n" + \
        "\t" + str(ele) + "\t0\t" + str(ele) + "\t0\t" + str(ele+N_2d) + "\t" + "1\t" + str(ele+N_2d) + "\t" + "1"  + "\t\t" + "%1.15f\t\t"%Uprime + "%1.15f"%0 + "\n" + \
        "\t" + str(ele) + "\t1\t" + str(ele) + "\t1\t" + str(ele+N_2d) + "\t" + "0\t" + str(ele+N_2d) + "\t" + "0"  + "\t\t" + "%1.15f\t\t"%Uprime + "%1.15f"%0
    inter = inter.expandtabs(4)
    with open('dir_'+nm+'/zinterall.def', 'w') as f:
        f.write(inter)
    f.close()

#--------------------------------------------------------------------
if mu:
    """onsite energy added onto transfile
    """
    #Large value
    LV = LV + (2*Ns)*Mu

    trans = ""
    for ele in range(Ns):
        trans = trans + "\n" + \
        "\t" + str(ele) + "\t0\t" + str(ele) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%Mu + "%1.15f"%0 + "\n" + \
        "\t" + str(ele) + "\t1\t" + str(ele) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%Mu + "%1.15f"%0
    trans = trans.expandtabs(4)
    with open('dir_'+nm+'/trans.def', 'a') as f:
        f.write(trans)
    f.close()

#--------------------------------------------------------------------
if gamma_JH:
    """gamma gJH part 1 of the Hamiltonian
    """
    #Large value
    LV = LV + (W*L + W*L)*(gJH)

    Num = W*L
    pairhop = "="*45 + '\nNPairhop\t' + str(Num) +"\n" + \
            "="*45 + "\n" + "="*14 + " Pairhop " + "="*14 + "\n" + "="*45
    for ele in range(Num):
        pairhop = pairhop + "\n" + "\t" + str(ele) + "\t" + str(ele + Num) + "\t\t" + "%1.15f"%(gJH)
    pairhop = pairhop.expandtabs(4)
    with open('dir_'+nm+'/pairhop.def', 'w') as f:
        f.write(pairhop)
    f.close()
    
    exchange = ""
    for ele in range(Num):
        exchange = exchange + "\n" + "\t" + str(ele) + "\t" + str(ele + Num) + "\t\t" + "%1.15f"%(gJH)
    exchange = exchange.expandtabs(4)
    with open('dir_'+nm+'/exchange.def', 'a') as f:
        f.write(exchange)
    f.close()

#--------------------------------------------------------------------
if mag:
    """to inlcude magnetic field at sites
    """
    #Large value
    LV = LV + (1*2)*(0.5*Bz)

    trans = ""
    for ele in [0]:
        trans = trans + "\n" + \
        "\t" + str(ele) + "\t0\t" + str(ele) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%(0.5*Bz) + "%1.15f"%0 + "\n" + \
        "\t" + str(ele) + "\t1\t" + str(ele) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%(-0.5*Bz) + "%1.15f"%0
    trans = trans.expandtabs(4)
    with open('dir_'+nm+'/trans.def', 'a') as f:
        f.write(trans)
    f.close()

#--------------------------------------------------------------------
if rso:
    """Spin independent kinetic hopping between the given list
    """
    #Large value
    LV = LV + (Ntrans*4)*(tso)

    trans = ""
    for ele in hopy_list:
        trans = trans + "\n" + \
        "\t" + str(ele[0]) + "\t0\t" + str(ele[1]) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%0 + "%1.15f"%(0.5*tso) + "\n" + \
        "\t" + str(ele[1]) + "\t1\t" + str(ele[0]) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%0 + "%1.15f"%(-0.5*tso) + "\n" + \
        "\t" + str(ele[0]) + "\t1\t" + str(ele[1]) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%0 + "%1.15f"%(0.5*tso) + "\n" + \
        "\t" + str(ele[1]) + "\t0\t" + str(ele[0]) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%0 + "%1.15f"%(-0.5*tso)
    for ele in hopx_list:
        trans = trans + "\n" + \
        "\t" + str(ele[0]) + "\t0\t" + str(ele[1]) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%(-0.5*tso) + "%1.15f"%0 + "\n" + \
        "\t" + str(ele[1]) + "\t1\t" + str(ele[0]) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%(-0.5*tso) + "%1.15f"%0 + "\n" + \
        "\t" + str(ele[0]) + "\t1\t" + str(ele[1]) + "\t"+"0"+"\t\t" + "%1.15f\t\t"%(0.5*tso) + "%1.15f"%0 + "\n" + \
        "\t" + str(ele[1]) + "\t0\t" + str(ele[0]) + "\t"+"1"+"\t\t" + "%1.15f\t\t"%(0.5*tso) + "%1.15f"%0
    trans = trans.expandtabs(4)
    with open('dir_'+nm+'/trans.def', 'a') as f:
        f.write(trans)
    f.close()
#--------------------------------------------------------------------
if one_body:
    """One body greens function
    """
    g1 = "="*24 + '\nNCisAjs\t' + str(Ns*4) +"\n" + \
            "="*24 + "\n" + "="*3 + " Green functions " + "="*3 + "\n" + "="*24
    for ele in range(Ns):
        g1 = g1 + "\n" + \
        "\t" + str(ele) + "\t0\t" + str(ele) + "\t"+"0\n" + \
        "\t" + str(ele) + "\t1\t" + str(ele) + "\t"+"1\n" + \
        "\t" + str(ele) + "\t0\t" + str(ele) + "\t"+"1\n" + \
        "\t" + str(ele) + "\t1\t" + str(ele) + "\t"+"0"
    g1 = g1.expandtabs(4)
    with open('dir_'+nm+'/greenone.def', 'w') as f:
        f.write(g1)
    f.close()
#--------------------------------------------------------------------
if two_body:
    """Two body greens function
    """
    g2 = "="*47 + '\nNCisAjsCktAltDC\t' + str(Ns) + "\n" + "="*47 + "\n"\
          + "="*8 + " Green functions for Sq AND Nq " + "="*8 + "\n" + "="*47
    for ele in range(Ns):
        g2 = g2 + "\n" + \
        "\t" + str(ele) + "\t0\t" + str(ele) + "\t0\t" + str(ele) + "\t1\t" + str(ele) + "\t"+"1"
    g2 = g2.expandtabs(4)
    with open('dir_'+nm+'/greentwo.def', 'w') as f:
        f.write(g2)
    f.close()
#------------------------------------------------------------------
LV = LV/(Ns)
#------------------------------------------------------------------
mod = ""
mod = mod + "--------------------\n"
mod = mod + "Model_Parameters   0\n"
mod = mod + " --------------------\n"
mod = mod + "HPhi_Cal_Parameters\n"
mod = mod + "--------------------\n"
mod = mod + "CDataFileHead  zvo\n"
mod = mod + "CParaFileHead  zqp\n"
mod = mod + "--------------------\n"
mod = mod + "Nsite          %d\n"%Ntot
#if (calc_mod != 3 ): mod = mod + "2Sz            %d\n"%Sz
if (calc_mod != 3 ): mod = mod + "Ncond          %d\n"%Ncond
mod = mod + "Lanczos_max    %d\n"%l_max
mod = mod + "initial_iv     -1\n" 
mod = mod + "exct           1\n"  
mod = mod + "LanczosEps     14\n"   
mod = mod + "LanczosTarget  2\n"   
mod = mod + "LargeValue     %1.15e\n"%LV
mod = mod + "NumAve         5\n"
mod = mod + "ExpecInterval  20\n"
mod = mod + "NOmega         200\n"
mod = mod + "OmegaMax       %1.15e"%(LV*Ntot)+"     "+"%1.15e\n"%(0.01*LV)
mod = mod + "OmegaMin       %1.15e"%(-LV*Ntot)+"     "+"%1.15e\n"%(0.01*LV)
mod = mod + "OmegaOrg       0.000000000000000e+00     0.000000000000000e+00\n"
if (calc_type == 4): mod = mod + "ExpandCoef     %d\n"%exp_c
mod = mod.expandtabs(4)
with open('dir_'+nm+'/modpara.def', 'w') as f:
    f.write(mod)
f.close()
