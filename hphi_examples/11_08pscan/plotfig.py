import sys, numpy as np, h5py

sys.path.append('../../plot_manager')
from plot2d import plotfig_2p

data = dict();values=dict()

Ns = 16
J  = 0.1
#file1
Jz_vals = np.arange(1,101,3)
spinexp = np.zeros((len(Jz_vals),Ns,3))
mz2exp  = np.zeros(len(Jz_vals))
gs_e  = np.zeros(len(Jz_vals))

result = np.zeros((len(Jz_vals),3))

for Jz_ind, Jz in enumerate(Jz_vals):
    green1 = np.loadtxt('dir_%02d'%Jz_ind+'/output/zvo_cisajs_eigen0.dat')
    green2 = np.loadtxt('dir_%02d'%Jz_ind+'/output/zvo_cisajscktalt_eigen0.dat')
    f_e = open('dir_%02d'%Jz_ind+'/output/zvo_energy.dat','r')
    gs_e[Jz_ind] = f_e.read().split()[3]
    f_e.close()
    #print(gs_e[Jz_ind])
    for ind in range(Ns):
        spinexp[Jz_ind,ind,0] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) + \
                              (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/2

        spinexp[Jz_ind,ind,1] = ((green1[4*ind+3,4]+1j*green1[4*ind+3,5]) - \
                              (green1[4*ind+2,4]+1j*green1[4*ind+2,5]))/(2j)

        spinexp[Jz_ind,ind,2] = ((green1[4*ind+1,4]+1j*green1[4*ind+1,5]) - \
                              (green1[4*ind+0,4]+1j*green1[4*ind+0,5]))/2

    for ind in range(Ns):
        mz2exp[Jz_ind] += ((green1[4*ind+1,4]+1j*green1[4*ind+1,5]) + \
                           (green1[4*ind+0,4]+1j*green1[4*ind+0,5]) - \
                           (green2[ind,8]*2))

result[:,0] = -1*(spinexp[:,0,2] + spinexp[:,8,2] - \
             (spinexp[:,1,2] + spinexp[:,9,2]) + \
             (spinexp[:,2,2] + spinexp[:,10,2]) - \
             (spinexp[:,3,2] + spinexp[:,11,2]) - \
             (spinexp[:,4,2] + spinexp[:,12,2]) + \
             (spinexp[:,5,2] + spinexp[:,13,2]) - \
             (spinexp[:,6,2] + spinexp[:,14,2]) + \
             (spinexp[:,7,2] + spinexp[:,15,2]))
#print(result[:,0])
result[:,1] = mz2exp/Ns
result[:,2] = gs_e

data['x_plot'] = Jz_vals; data['y_plot'] = result
#print(spinexp[:,1,2], len(Jz_vals))
"""
values['xlabel1'] = r'$\mathrm{J_z/J}$'; 
values['xlabel2'] = r'$\mathrm{J_z/J}$'; 
values['ylabel1'] = r'$\mathrm{\left<  \hat{s}_i^z \right>}$'
values['ylabel2'] = r'$\mathrm{\left<  \hat{m}_z^2 \right>}$'
Cl = ['k','C2','C3']*2
Ls = ['-','--','-']*2
Gs = dict()
Gs['t']=0.9; Gs['b']=0.2; Gs['l']=0.12; Gs['r']=0.98
Gs['w']=0.3; Gs['h']=0.4

sf = np.array([[0,2],[2,3]])
plotfig_2p(data, '3', values, Cl=Cl, Ls=Ls, lw=0.4, minor=1, sf=sf, Gs=Gs)
"""
f = h5py.File('results/fig11.hdf5', 'w')
f.create_dataset('y_plot', data = data['y_plot'][:])
f.create_dataset('x_plot', data = data['x_plot'][:])
f.close()
