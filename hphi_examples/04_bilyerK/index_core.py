def flatten_list(_2d_list):
    """flatten a 2d list
    """
    flat_list = []
    for element in _2d_list:
        if type(element) is list:
            for item in element:
                flat_list.append(item)
        else:
            flat_list.append(element)
    return flat_list

def hop_list(W, L, PBC=1, bi=1):
    _hopy = []
    _hopx = []
    _hopz = []
    for i in range(W):
        _hopx.append(range(L*i, L*i + L, 1))
    for i in range(L):
        _hopy.append(range(i, L*(W-1) + i + 1, L))
    hopx_list = []
    hopy_list = []
    hopz_list = []
    for ele in _hopy:
        hopx_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
        if (PBC==1 and W!=1): hopx_list.append( [[ele[0], ele[-1]]])
    if bi:
        for ele in _hopy:
            hopx_list.append( [[ele[i]+W*L, ele[i+1]+W*L] for i in range(len(ele)-1)]  )
            if (PBC==1 and W!=1): hopx_list.append( [[ele[0]+W*L, ele[-1]+W*L]])
    hopx_list = flatten_list(hopx_list)
    for ele in _hopx:
        hopy_list.append( [[ele[i], ele[i+1]] for i in range(len(ele)-1)]  )
        if (PBC==1 and L!=1): hopy_list.append( [[ele[0], ele[-1]]])
    if bi:
        for ele in _hopx:
            hopy_list.append( [[ele[i]+W*L, ele[i+1]+W*L] for i in range(len(ele)-1)]  )
            if (PBC==1 and L!=1): hopy_list.append( [[ele[0]+W*L, ele[-1]+W*L]])
    hopy_list = flatten_list(hopy_list)
    if bi:
        for ele in range(W*L):
            hopz_list.append([[ele, ele+W*L]])
    hopz_list = flatten_list(hopz_list)
    if bi:
        return hopx_list, hopy_list, hopz_list
    else:
        return hopx_list, hopy_list
        
