import sys, numpy as np, time, h5py
import scipy.linalg as la
from scipy.io import savemat, loadmat

#sys.path.append('../')
from spin_core import Spin
from evolve_step import Lindblad
from master_equation import BathSpectral, calc_rho_vdv
from density_core import *
sys.path.append('../plot_manager/')
from plot2d import plotfig
from plot2d import plotfig_2p
#from numba_wrapper import sp_trace
HBAR = 0.658211951
KB = 8.6173324e-5

#parameters
N       = 8
S       = 1/2
sc      = 2
J       = 0.1
Jz      = 1.005*J

gamma   = 0.05
Lambda  = 8 * J
omega0  = 2 * J
T       = 300*KB

tf  = 100
dt  = 0.05
start = time.time()
#spin system creation with eDOF
spin = Spin(S=S, L = N, sc = 2)
evolve   = Lindblad(spin, gamma=1, dt=dt)
Se = spin.op

H0   = -1*spin.heisenberg(J=J,Jz=Jz) 

w,v = la.eigh(H0.toarray())
print(v[:,0].conj() @ Se['sz0'] @ v[:,0])
print(v[:,0].conj() @ Se['sz3'] @ v[:,0])
print(w[:5])



rho_spin = loadmat('rho_t070')["rho_spin"]
rho_f = partial_trace_A(rho_spin,2,256)

rho_spin = loadmat('rho_t001')["rho_spin"]
rho_i = partial_trace_A(rho_spin,2,256)

#print(np.trace(rho_f @ H0))
#print(np.trace(rho_i @ H0))
print("Initial entanglement entropy", entanglement_entropy(rho_i, True))
print("Final entanglement entropy", entanglement_entropy(rho_f, True))
print("Initial negativity", logarithmic_negativity(rho_i, m=16))
#print(entanglement_entropy(rho_f))

def e_basis(mat, v):
    return v.conj().T @ mat @ v
def s_basis(mat, v):
    return v @ mat @ v.conj().T

#jump operator
Xs = Se['sx7']
Xe = e_basis(Xs, v)

Ediff = (w[0] - w).reshape(-1,1)
for ind in range(1,len(w)):
    Ediff = np.append(Ediff,(w[ind]-w).reshape(-1,1),axis=1)
#limiting case
Ediff = Ediff + 1e-14
 
L1a = np.sqrt(2*np.pi*gamma*BathSpectral(Ediff,omega0,T,Lambda))
#print(La[:5,:5])
L1b = Xe
L1e = L1a * L1b
L = s_basis(L1e, v)
Ld = L.T.conj()
Lp = Ld @ L
#print("\n", np.round(L[:5,:5],3))

#rho = np.outer(v[:,0], v[:,0].conj())

times = np.arange(0, tf+dt, dt)
#m_op = spin.field_op(1,'sz')/N
results = np.zeros((len(times),5))

#print("initial m: ",np.trace(rho @ m_op))
#rho_new = evolve.rk4_rho(rho)
#start time evolution
rho = rho_f
for t_ind, t_now in enumerate(times):
     
    results[t_ind,0] = entanglement_entropy(rho, True)
    rhoA = partial_trace(rho,m=16,n=16)
    rhoB = partial_trace_A(rho,m=16,n=16)
    results[t_ind,1] = entanglement_entropy(rhoA, True)
    results[t_ind,2] = entanglement_entropy(rhoB, True)
    results[t_ind,3] = results[t_ind,1] + results[t_ind,2] - results[t_ind,0]
    results[t_ind,4] = logarithmic_negativity(rho, m=16)
    #print(t_ind)
    
    rho = evolve.rk4_rho(rho, H0, L,Ld,Lp)

#f = h5py.File('results/02_100k.h5', 'w')
#f.create_dataset('results', data=results)
#f.close()

#results = h5py.File("results/02ule.h5","r")["results"][:]
#results = np.loadtxt("res_darwin.txt")
times = np.arange(0,100+0.05,0.05)

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = results
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['xlabel2'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{Entanglement~Entropy}$'
values['ylabel2'] = r'$\mathrm{Log~Negativity}$'
sf = np.array([[0,1],[4,5]])
#sf = np.array([[0,1],[1,2]])
#Cl = ['C0', 'C2', 'C3', 'k']
plotfig_2p(data, '300k', values, sf=sf, fs=8)
print('run finished in:\t',time.time()-start)

"""
data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{m}$'
plotfig(data, 'm', values, ylim=[0,1])
print("final m: ",np.trace(rho @ m_op))
print("gibbs m:", np.trace(calc_rho_vdv(w,v,T) @ m_op))
"""
