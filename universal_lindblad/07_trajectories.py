import sys, numpy as np, time
import scipy.linalg as la

from spin_core import Spin
from evolve_step import Lindblad, psi_qj
from master_equation import *
sys.path.append('../plot_manager/')
from plot2d import plotfig
#from numba_wrapper import sp_trace
HBAR = 0.658211951
dim = '1d'

#parameters
N       = 4
S       = 1/2
eta     = 1
gamma   = 0.02 * eta
Bz      = 8 * eta
omega0  = 2 * eta
T       = 5 * eta
Lambda  = 100
verbose = True

start = time.time()
#spin system creation with eDOF
spin = Spin(S=S, L = N, kronD=1, sc = 1)
Se = spin.op

H0   = -1*spin.heisenberg(J=1*eta,Jz=1*eta) + -1*spin.field_op(Bz,'sz')

L, Ld, Lp = get_Lexact(H0, Se['sx3'], gamma, BathSpectral, omega0, T, Lambda)

#jump operator
Xs = Se['sx3']

w,v = la.eigh(H0.toarray())
rho = np.outer(v[:,0], v[:,0].conj())
tf  = 200
dt  = 0.05
times = np.arange(0, tf+dt, dt)
evolve   = Lindblad(spin, gamma=1, dt=dt)
m_op = spin.field_op(1,'sz')/N
result = np.zeros(len(times))
#if verbose: print("initial m: ",np.trace(rho @ m_op))
#start time evolution
psi_0 = v[:,0].reshape(-1,1)

print("exp value: ", np.real(psi_0.T.conj() @ m_op @ psi_0))

Heff = H0 - 1j*Lp
Ntraj = 10

for traj in np.arange(Ntraj):
    psi = psi_0
    for t_ind, t_now in enumerate(times):
        #drawing a random number
        r1 = np.random.rand(1)
        psi_new, dp = psi_qj(Heff, psi, dt)
        if r1 > dp:
            psi_p = psi_new/(np.sqrt(1-dp))
        else:
            psi_p = (L @ psi)/(np.sqrt(dp/dt))
        psi = psi_p
        rho = psi @ np.conjugate(psi.transpose())
        #adding all values to single time index
        result[t_ind] = result[t_ind] + np.real(psi.T.conj() @ m_op @ psi)

result = result/Ntraj


data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{m}$'
plotfig(data, 'm3', values)
if verbose: print("final m: ",np.trace(rho @ m_op))
if verbose: print("gibbs m:", np.trace(calc_rho_vdv(w,v,T) @ m_op))
if verbose: print('run finished in:\t',time.time()-start)
