import numpy as np
import scipy.linalg as la
import scipy.sparse.linalg as sla
#from numba import jit

def partial_trace(rho, sub_sys='B', m=2, n=2, verbose=False):
    """A function to find the partial trace over
    sub_sys, m and n are dim of A and B
    """
    
    L = m*n
    if not (sub_sys == 'B' or sub_sys == 'A'):
        raise ValueError('Sub_sys should be "A" or "B"')
    if not rho.shape[0] == L:
        raise ValueError('Product of subsystem dim should match rho dim')
    
    if sub_sys == 'B':
        rho_A = np.zeros((m,m), dtype=np.complex128)
        if verbose: print('tracing over B',m,n)
        for i in range(m):
            for j in range(m):
                rho_A[i, j] = \
                np.sum([rho[n*i+k ,n*j+k] \
                        for k in range(n)])
        return rho_A
    
    elif sub_sys == 'A':
        rho_B = np.zeros((n,n), dtype=np.complex128)
        if verbose: print('tracing over A',m,n)
        for i in range(n):
            for j in range(n):
                rho_B[i,j] = \
                np.sum([rho[i+n*k,j+n*k]\
                        for k in range(m)])
        return rho_B

#@jit(nopython=True)
def partial_trace_A(rho, m=2, n=2): 
    L = m*n
    rho_B = np.zeros((n,n), dtype=np.complex128)
    for i in range(n):
        for j in range(n):
            m_sum = 0
            for k in range(m):
                m_sum = m_sum + rho[i+n*k,j+n*k]
            rho_B[i,j] = m_sum
    return rho_B

#rho = np.arange(16).reshape((4,4))
#print(rho)
#rhoa = partial_trace(rho,'B',2,2)
#print(rhoa)
def partial_psi_trace(psi, m = 2):
    """Computes partial trace density matrix from
    psi tracing over A in A kron B
    """
    dim = int(len(psi)//m)
    rho_B = np.zeros((dim,dim), dtype=np.complex128)
    for i in range(int(m)):
        rho_B = rho_B + np.outer(psi[i*dim:(i+1)*dim],
                        np.conjugate(psi[i*dim:(i+1)*dim]))
    return rho_B

#psi = np.arange(1,10)*3/2
#print(psi)
#rho = np.outer(psi,psi)
#print(rho)
#rhoa = partial_trace(rho,'A',3,3)
#print(rhoa)
#rhoa = partial_psi_trace(psi,3)
#print(rhoa)

def partial_transpose(rho, sub_sys='B', m=2):
    """Only partial transpose over sys B and
    m = 2
    """
    L = m*m
    rho_tb = np.zeros((L,L), dtype=np.complex128)
    if not (sub_sys == 'B'):
        raise ValueError('Sub_sys should be "A" or "B"')
    if not len(rho) == L:
        raise ValueError('Product of subsystem dim should match rho dim')
    
    for i in range(m):
        for j in range(m):
            rho_tb[m*i:m*(i+1), m*j:m*(j+1)] = \
            np.transpose(rho[m*i:m*(i+1), m*j:m*(j+1)])
    return rho_tb
    
#rho = np.arange(81).reshape((9,9))
#print(rho)
#rhoa = partial_transpose(rho,'B',3)
#print(rhoa)

def logarithmic_negativity(rho, sub_sys='B',m=2):
    """Computes the logarithmic negativity of the 
    half of the subsystem divides the system into two
    """
    rho_tb = partial_transpose(rho,'B',m)

    #negativity
    w_neg = la.eigvals(rho_tb)
    neg = np.sum(w_neg[np.where(w_neg<0)])
    
    
    #norm_rho = np.transpose(np.conjugate(rho_tb)) @ rho_tb
    norm_rho = rho_tb @ np.transpose(np.conjugate(rho_tb)) 
    w = la.eigvals(norm_rho)
    log_neg = np.log(np.sum(np.sqrt(w)))

    return log_neg
    
def entanglement_entropy(rho, full=False):
    if not full:
        l_rho = len(rho)
        m = int(np.sqrt(l_rho))
        rho_a = partial_trace(rho, m = m, n = m)
    else:
        rho_a = rho
    w = la.eigvals(rho_a)
    eig = np.real(w[np.where(w>1e-8)])
    S = -np.sum([eig[i]*np.log(eig[i]) for i in range(len(eig))])
    return S
    
    
     


    
