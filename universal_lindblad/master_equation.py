import numpy as np, sys
HBAR = 1#0.6582119569       # Plancks constant [eV * fs]
KB = 1#8.6173324e-5        # Boltzman constant [eV / K]
MU_BOHR = 1#5.7883818060e-05   # Bohr magneton [eV / T]

sys.path.append('../plot_manager/')
from plot2d import plotfig

#temp = 300
#Lambda = 50*temp



def BathSpectral(w, w0, T, Lambda):
    return (1/w0) * (w * np.exp(-w**2/(2*Lambda**2)))/(1 - np.exp(-w/T))


def OmicSpectral(w, w0, Lambda):
    return w * np.exp(-w**2/(2*Lambda**2))/w0

def get_adHs(Hs, O, n):
    if n == 0:
        return O
    else:
        Op = Hs@O - O@Hs
        for i in range(1,n):
            Op = Hs@Op - Op@Hs
        return Op


def calc_rho_vdv(E,V, temp=0, verbose=False):
    """Calculate the canonical gibbs density matrix
    """
    temp = temp+1e-15    #to avoid divide by zero error
    beta = 1/(KB*temp)
    E = E - E[0]
    e_exp = np.diag(np.exp(-beta * E))
    rho = V @ e_exp @ np.transpose(np.conjugate(V))
    
    return rho/( np.trace(rho) )

def get_cn(Jw, omega, t_span, n_trunc=10, ctype=np.complex64, plot=False):
    N = len(omega)
    Nt = len(t_span)
    dw = omega[1] - omega[0]
    dt = t_span[1] - t_span[0]

    Gt = np.zeros(Nt, dtype=ctype)
    Jt = np.zeros(Nt, dtype=ctype)
    for t_ind, t_now in enumerate(t_span):
        Gt[t_ind] = dw/np.sqrt(2*np.pi) * ( np.sqrt(Jw) @ np.exp(-1j*omega*t_now) )
        Jt[t_ind] = dw/(2*np.pi) * ( Jw @ np.exp(-1j*omega*t_now) )
    
    cn = np.zeros(n_trunc, dtype=ctype)
    for n_val in range(n_trunc):
        kn = ((-1j)**n_val)/(np.math.factorial(n_val))
        cn[n_val] = kn * dt * (Gt @ (t_span**n_val))
    
    print("broder abs values of jump correlator: ", np.abs(Gt[0]), np.abs(Gt[-1]))
    print("Max abs values of jump correlator: ", np.abs(Gt[Nt//2]), t_span[Nt//2])
    print("last 3 coefficients abs: ",np.abs(cn)[-3:])
    if plot:
        result = np.append(Gt.real.reshape(-1,1), Gt.imag.reshape(-1,1), axis=1)
        result = np.append(result, np.abs(Gt).reshape(-1,1),axis=1)
        result = np.append(result, np.abs(Jt).reshape(-1,1),axis=1)

        data = dict();values = dict()
        data['x_plot'] = omega; data['y_plot'] = Jw
        values['xlabel1'] = r'$\mathrm{\omega}$'; values['ylabel1'] = r'$\mathrm{J(\omega)}$'
        plotfig(data, 'ule_jw', values)
        data['x_plot'] = t_span; data['y_plot'] = result
        values['xlabel1'] = r'$\mathrm{Time}$'; values['ylabel1'] = r'$\mathrm{g(t)}$'
        plotfig(data, 'ule_gt', values, sf=np.array([[0,3]]), xlim=[-0.2,0.2])
        values['xlabel1'] = r'$\mathrm{Time}$'; values['ylabel1'] = r'$\mathrm{log|g(t)|}$'
        plotfig(data, 'ule_glabs', values, sf=np.array([[2,4]]), Ls=['-','--']*2,xlim=[-0.5,0.5], ylog=1, ylim=[1e-6,1e3])
        data['x_plot'] = np.arange(n_trunc); data['y_plot'] = np.abs(cn)
        values['xlabel1'] = r'$\mathrm{n}$'; values['ylabel1'] = r'$\mathrm{|cn|}$'
        plotfig(data, 'ule_cn', values, ylog=1)#, ylim=[1e-18,1e2])

    return cn

def get_Lexact(H0, X, gamma, BathSpectral, omega0, T, Lambda):
    w,v = np.linalg.eigh(H0.toarray())
    def e_basis(mat, v):
        return v.conj().T @ mat @ v
    def s_basis(mat, v):
        return v @ mat @ v.conj().T
    #jump operator
    Xs = X
    Xe = e_basis(Xs, v)
    Ediff = (w[0] - w).reshape(-1,1)
    for ind in range(1,len(w)):
        Ediff = np.append(Ediff,(w[ind]-w).reshape(-1,1),axis=1)
    #limiting case
    Ediff = Ediff + 1e-14
    L1a = np.sqrt(2*np.pi*gamma*BathSpectral(Ediff,omega0,T,Lambda))
    #print(La[:5,:5])
    L1b = Xe
    L1e = L1a * L1b
    L = s_basis(L1e, v)
    print("L trace", np.trace(L))
    Ld = L.T.conj()
    Lp = Ld @ L
    return L, Ld, Lp


def sp_trace(a):
    row = a.tocoo().row
    col = a.tocoo().col
    L = len(row)
    trace = 0
    for i in range(L):
        if row[i] == col[i]:
            trace = trace + a.data[i]
    return trace

