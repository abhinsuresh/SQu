import numpy as np, sys, h5py
sys.path.append('../plot_manager/')
from plot2d import plotfig_2p


results = h5py.File("results/02ule.h5","r")["results"][:]
#results = np.loadtxt("res_darwin.txt")
times = np.arange(0,100+0.05,0.05)

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = results
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['xlabel2'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{Entanglement~Entropy}$'
values['ylabel2'] = r'$\mathrm{Log~Negativity}$'
sf = np.array([[0,1],[4,5]])
#sf = np.array([[0,1],[1,2]])
#Cl = ['C0', 'C2', 'C3', 'k']
plotfig_2p(data, 'ent_02', values, sf=sf, fs=8)
