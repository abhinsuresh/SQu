import sys, numpy as np, time
import scipy.linalg as la
np.set_printoptions(linewidth=np.inf)

#sys.path.append('../')
from spin_core import Spin
from master_equation import BathSpectral
#from numba_wrapper import sp_trace
HBAR = 0.658211951
dim = '1d'

#parameters
L       = 4
S       = 1/2
eta     = 1
gamma   = 0.02 * eta
Bz      = 8 * eta
Lambda  = 8 * eta
omega0  = 2 * eta
T       = 20 * eta

tf  = 100
dt  = 0.1
start = time.time()
#spin system creation with eDOF
spin = Spin(S=S, L = L, kronD=1, sc = 1)
#evolve   = Lindblad(spin, gamma=gamma, g=g, dt=0.1)
Se = spin.op

H0   = -1*spin.heisenberg(J=1*eta,Jz=1*eta) + -1*spin.field_op(Bz,'sz')

w,v = la.eigh(H0.toarray())
print(v[:,0].conj() @ Se['sz0'] @ v[:,0])
print(v[:,0].conj() @ Se['sz3'] @ v[:,0])
print(w[:5])

def e_basis(mat, v):
    return v.conj().T @ mat @ v
def s_basis(mat, v):
    return v @ mat @ v.conj().T

Xs = Se['sx3']
Xe = e_basis(Xs, v)

Ediff = (w[0] - w).reshape(-1,1)
for ind in range(1,len(w)):
    Ediff = np.append(Ediff,(w[ind]-w).reshape(-1,1),axis=1)

#to get the correct limit
Ediff = Ediff + 1e-14
#print("\n",Ediff[:5,:5])

Ed = np.zeros(Ediff.shape)
for m in range(len(w)):
    for n in range(len(w)):
        #Ed[m,n] = w[m] - w[n]
        Ed[m,n] = w[n] - w[m]
#print("\n",Ed[:5,:5])

#ebasis method
st = time.time()
La = np.sqrt(2*np.pi*gamma*BathSpectral(Ediff,omega0,T,Lambda))
#print(La[:5,:5])
Lb = Xe
Le = La * Lb
L = s_basis(Le, v)
print("\n", np.round(L[:5,:5],3))
print(time.time()-st)        

#brute force
st = time.time()
Ls = np.zeros(Ediff.shape)
for m in range(len(w)):
    for n in range(len(w)):
        Ls = Ls + np.sqrt(2*np.pi*gamma*BathSpectral(w[n]-w[m]+1e-14,omega0,T,Lambda)) * (v[:,m].conj() @ Xs @v[:,n]) * (np.outer(v[:,m],v[:,n].conj()))
print("\n", np.round(Ls[:5,:5],3))        
print(time.time()-st)        


"""
#jump operator
X = op['sx7']
L = np.zeros(X.shape)
for m in spin.Ns:
    for n in spin.Ns:
        L += np.sqrt(2*np.pi*gamma*BathSpectral(w[n]-w[m])) 

up = np.array([1,0])
dn = np.array([0,1])
gs = np.kron(np.kron(np.kron(dn,up),up),up)
gs = np.kron(np.kron(np.kron(np.kron(gs,up),up),up),up)
#gs = np.kron(np.kron(np.kron(dn,up),up),up)

rho = csr_matrix(np.outer(gs.conj(),gs))
print(sp_trace(rho @ Se['sz0']))
print(sp_trace(rho @ Se['sz3']))

print('log neg:\t',logarithmic_negativity(rho.toarray(),'B',m=2**4))

#rho_new = evolve.rk4_rho(rho)
#start time evolution
times = np.arange(0, tf+dt, dt)
result = np.zeros((len(times),3))


for t_ind, t_now in enumerate(times):
    
    result[t_ind,0] = sp_trace(rho @ Se['sz0'])
    result[t_ind,1] = sp_trace(rho @ Se['sz1'])
    result[t_ind,2] = logarithmic_negativity(rho.toarray(),'B',m=2**4)
    #print(t_now)

    rho = evolve.rk4_rho(rho, H0)

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['xlabel2'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{<Sz_i>}$'
values['ylabel2'] = r'$\mathrm{Log\ negativity}$'
sf = np.array([[0,2],[2,3]])
Cl = ['C0', 'C2', 'C3', 'k']
plotfig_2p(data, 'sz_l', values, sf=sf, Cl=Cl)
    
print('run finished in:\t',time.time()-start)
"""
