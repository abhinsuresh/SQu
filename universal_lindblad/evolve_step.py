import numpy as np
import time
import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.sparse import identity as eye, csr_matrix
import sys
#from numba import jit


#HBAR = 0.6582119569
HBAR = 1

class Lindblad:
    """Spin lindblad evolution implementation
    """
    def __init__(self,
                 spin,
                 gamma  = 0,
                 dt = 0.1,
                 temp=0,
                 Lambda=0,
                 verbose=0,
                 Ht = 0,
                 ):
        self.spin = spin
        self.gamma = gamma
        self.dt = dt
        self.Lambda = Lambda
        self.temp = temp
        self.verbose = verbose
        self.Ht = Ht
        return

    def rk4_rho(self, rho, Ht, L,Ld,Lp): 
        """a function to evolve the wave function in time
        H_dynamic should be a function of time if not []
        
        """
        rho1 = self.drho(rho,            Ht, L,Ld,Lp)*self.dt
        rho2 = self.drho(rho + rho1*0.5, Ht, L,Ld,Lp)*self.dt
        rho3 = self.drho(rho + rho2*0.5, Ht, L,Ld,Lp)*self.dt
        rho4 = self.drho(rho + rho3,     Ht, L,Ld,Lp)*self.dt

        rho = rho + (rho1 + 2*rho2 + 2*rho3 + rho4)*(1/6)
        return rho
    def drho(self, rho, Ht, L,Ld,Lp):
        rho_new = 1j*(rho @ Ht - Ht @ rho) + self.lindblad(rho,L,Ld,Lp) 
        return rho_new
    def lindblad(self, rho, L, Ld, Lp):
        """Lp = Ld*L
        """
        result = np.zeros(rho.shape, dtype=np.complex64)
        if self.gamma == 0:
            return result
        result = result + L @ rho @ Ld - 0.5*(Lp @ rho + rho @ Lp ) 
        return result


 
    def rk4_rho2(self, rho, Ht, L1,L1d, L2,L2d, L3,L3d, L4,L4d, Lpsum): 
        """a function to evolve the wave function in time
        H_dynamic should be a function of time if not [] 
        """
        rho1 = self.drho2(rho,            Ht, L1,L1d, L2,L2d, L3,L3d, L4,L4d, Lpsum)*self.dt
        rho2 = self.drho2(rho + rho1*0.5, Ht, L1,L1d, L2,L2d, L3,L3d, L4,L4d, Lpsum)*self.dt
        rho3 = self.drho2(rho + rho2*0.5, Ht, L1,L1d, L2,L2d, L3,L3d, L4,L4d, Lpsum)*self.dt
        rho4 = self.drho2(rho + rho3,     Ht, L1,L1d, L2,L2d, L3,L3d, L4,L4d, Lpsum)*self.dt
        rho = rho + (rho1 + 2*rho2 + 2*rho3 + rho4)*(1/6)
        return rho

    def drho2(self, rho, Ht, L1,L1d, L2,L2d, L3,L3d, L4,L4d, Lpsum):
        rho_new = 1j*(rho @ Ht - Ht @ rho) + self.lindblad2(rho, L1,L1d, L2,L2d, L3,L3d, L4,L4d, Lpsum) 
        return rho_new

    def lindblad2(self, rho, L1,L1d, L2,L2d, L3,L3d, L4,L4d, Lpsum):
        return L1@rho@L1d  + L2@rho@L2d  + L3@rho@L3d + L4@rho@L4d - 0.5*(Lpsum @ rho + rho @ Lpsum ) 



def psi_qj(Heff, psi, dt):
    """Evolve using quantum trajectories
    """
    I2 = np.eye(len(Heff))
    psi_new = (I2 - 1j*Heff*dt) @ psi
    dp = 1 - la.norm(psi_new)
    
    return psi_new, dp

#-------------------------------------------------------------------
#wavefunction evolution    
#-------------------------------------------------------------------
def evolve(H_static, H_dynamic, psi, dt, method='diag', time_dep=1):
    """a function to evolve the wave function in time
    if time independet, we return Ut and should be called outside
    the loop
    """
    size = H_static.shape[0]
    if method == 'gpu' or method == 'CN_gpu':
        import pycuda.gpuarray as gpuarray
        import pycuda.autoinit
        from skcuda import linalg
        from skcuda import misc
        linalg.init()
    
    if time_dep == 0:
        if method=='expm': 
            Ut = la.expm(-1j*H_static.toarray()*dt/HBAR)
            print('expm')
            return Ut
        elif method=='eig_vec':
            w,v = la.eigh(H_static.toarray())
            Ut = np.asarray([np.exp(-1j*w[i]*dt) for i in range(size)])
            return Ut
        elif method=='CN':
            Ut = sla.inv((eye(size) + 1j*dt*H_static/(2*HBAR)).tocsc()) @ \
                       (eye(size) - 1j*dt*H_static/(2*HBAR))
            return Ut
        elif method=='CN_spilu':
            B = sla.spilu(eye(size) + 1j*dt*H_static/(2*HBAR))
            return B
        elif method=='CN_gpu':
            mat      = (eye(size) + 1j*dt*H_static/(2*HBAR))
            a_gpu    = gpuarray.to_gpu(mat.toarray()) 
            ainv_gpu = linalg.inv(a_gpu, overwrite=True)
            mat_inv  = ainv_gpu.get()
            Ut       = mat_inv @ (eye(size) - 1j*dt*H_static/(2*HBAR))
            return Ut
    else: 
        if method=='expm': 
            Ht = H_static + H_dynamic
            Ut = la.expm(-1j*Ht*dt/HBAR)
            psi_new = Ut @ psi
            return psi

        elif method=='CN':
            Ht = H_static + H_dynamic
            Ut = la.inv(np.eye(size) + 1j*dt*Ht/(2*HBAR)) @ \
                        (np.eye(size) - 1j*dt*Ht/(2*HBAR))
            psi = Ut @ psi
            return psi
            
        if method=='eig_vec':
            Ht = H_static + H_dynamic
            w, v = la.eigh(Ht.toarray())
            Ut = np.asarray([np.exp(-1j*w[i]*dt) for i in range(2)])
            cn = [v[:,i].conj() @ psi for i in range(2)]
            psi = np.sum([cn[i] * (Ut[i] * v[:,i]) for i in range(2)], axis=0)
            return psi

        elif method=='CN_gpu':
            Ht = H_static + H_dynamic
            mat      = (eye(size) + 1j*dt*Ht/(2*HBAR))
            a_gpu    = gpuarray.to_gpu(mat.toarray()) 
            ainv_gpu = linalg.inv(a_gpu, overwrite=True)
            mat_inv  = ainv_gpu.get()
            Ut       = mat_inv @ (eye(size) - 1j*dt*Ht/(2*HBAR))
            psi = Ut @ psi
            return psi
