import sys, numpy as np, time
import scipy.linalg as la

#sys.path.append('../')
from spin_core import Spin
from evolve_step import Lindblad
from master_equation import BathSpectral, calc_rho_vdv
sys.path.append('../plot_manager/')
from plot2d import plotfig
#from numba_wrapper import sp_trace
HBAR = 0.658211951
dim = '1d'
KB = 8.6173324e-5
#parameters
N       = 4
S       = 1/2
eta     = 1
J       = 0.1
Jzm     = 1.005
Jz      = J * Jzm 
gamma   = 0.9 * eta
Bz      = Jzm/500
omega0  = 2 * J

T       = 1
Lambda  = 50*T #100 * eta

print("T and Lambda", T, Lambda)

tf  = 5000
dt  = 0.1

#real temperatire calculation, vdv part KB set to 1, but passing T=KBtemp
#temp    = 300
#T       = temp * KB
#Lambda  = 50*T

start = time.time()
#spin system creation with eDOF
spin = Spin(S=S, L = N, kronD=1, sc = 1)
evolve   = Lindblad(spin, gamma=gamma, dt=dt)
Se = spin.op

H0   = -1*spin.heisenberg(J=J,Jz=Jz) + -1*spin.field_op(Bz,'sz')

w,v = la.eigh(H0.toarray())
#for i in range(10):
#    print(v[:,i].conj() @ Se['sz0'] @ v[:,i])
print(v[:,0].conj() @ Se['sz0'] @ v[:,0], v[:,0].conj() @ Se['sz1'] @ v[:,0],
      v[:,0].conj() @ Se['sz2'] @ v[:,0], v[:,0].conj() @ Se['sz3'] @ v[:,0])
print(w[:5])
print("gibbs m:", np.trace(calc_rho_vdv(w,v,T) @ Se['sz0']))


def e_basis(mat, v):
    return v.conj().T @ mat @ v
def s_basis(mat, v):
    return v @ mat @ v.conj().T

#jump operator
Xs = Se['sx3']
Xe = e_basis(Xs, v)

Ediff = (w[0] - w).reshape(-1,1)
for ind in range(1,len(w)):
    Ediff = np.append(Ediff,(w[ind]-w).reshape(-1,1),axis=1)
#limiting case
Ediff = Ediff + 1e-14
 
L1a = np.sqrt(2*np.pi*gamma*BathSpectral(Ediff,omega0,T,Lambda))
#print(La[:5,:5])
L1b = Xe
L1e = L1a * L1b
L = s_basis(L1e, v)

print("L trace", np.trace(L))
Ld = L.T.conj()
Lp = Ld @ L
#print("\n", np.round(L[:5,:5],3))

rho = np.outer(v[:,0], v[:,0].conj())

times = np.arange(0, tf+dt, dt)
m_op = spin.field_op(1,'sz')/N
result = np.zeros(len(times))

print("initial m: ",np.trace(rho @ m_op))
#rho_new = evolve.rk4_rho(rho)
#start time evolution

for t_ind, t_now in enumerate(times):
    
    result[t_ind] = np.trace(rho @ m_op)
    #print(t_ind, result[t_ind], np.trace(rho).real)
    rho = evolve.rk4_rho(rho, H0, L,Ld,Lp)

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{m}$'
plotfig(data, 'm', values)#, ylim=[-0.5,1])
print('run finished in:\t',time.time()-start)
print("final m: ",np.trace(rho @ m_op))
print("gibbs m:", np.trace(calc_rho_vdv(w,v,T) @ m_op))
