import sys, numpy as np, time
import scipy.linalg as la
from scipy.io import savemat

from spin_core import Spin
from evolve_step import Lindblad
from master_equation import *
sys.path.append('../plot_manager/')
from plot2d import plotfig
#from numba_wrapper import sp_trace
HBAR = 0.658211951
dim = '1d'
KB = 8.6173324e-5

#parameters
N       = 4
S       = 1/2
eta     = 1
gamma   = 0.02 * eta
Bz      = 1 * eta
omega0  = 2 * eta
temp    = 400
T       = 1
Lambda  = 100
verbose = True

start = time.time()
#spin system creation with eDOF
spin = Spin(S=S, L = N, kronD=1, sc = 1)
Se = spin.op

H0   = -1*spin.heisenberg(J=1*eta,Jz=1*eta) + -1*spin.field_op(Bz,'sz')

#L, Ld, Lp = get_Lexact(H0, Se['sx3'], gamma, BathSpectral, omega0, T, Lambda)
w,v = la.eigh(H0.toarray())
m_op = spin.field_op(1,'sz')/N
if verbose: print("gibbs m:", np.trace(calc_rho_vdv(w,v,T) @ m_op))

#jump operator
Xs = Se['sx3']

#function
omega = np.arange(-10*Lambda,10*Lambda,0.01)
Sw = OmicSpectral(omega, omega0, Lambda)
Jw = BathSpectral(omega, omega0, T, Lambda)

#----------------------------
#summation
t_span = np.arange(-0.5,0.5,0.001)
n_trunc = 10
cn  = get_cn(Jw, omega, t_span, n_trunc=n_trunc, ctype=np.complex64, plot=True)

L = np.zeros(Xs.shape, dtype=np.complex64)
for ind in range(n_trunc):
    L = L + cn[ind] * get_adHs(H0, Xs, ind)

#if verbose: print('L finished in:\t',time.time()-start)
L = L * np.sqrt(gamma)
if verbose: print("trace of L", np.trace(L))
Ld = L.T.conj()
Lp = Ld @ L
#print("L mat:\n", np.round(L[:5,:5],3))

#----------------------------
"""
#full diag
def e_basis(mat, v):
    return v.conj().T @ mat @ v
def s_basis(mat, v):
    return v @ mat @ v.conj().T

Xe = e_basis(Xs, v)
Ediff = (w[0] - w).reshape(-1,1)
for ind in range(1,len(w)):
    Ediff = np.append(Ediff,(w[ind]-w).reshape(-1,1),axis=1)
#limiting case
Ediff = Ediff + 1e-14
L1a = np.sqrt(2*np.pi*gamma*BathSpectral(Ediff,omega0,T,Lambda))
L1b = Xe
L1e = L1a * L1b
L = s_basis(L1e, v)
Ld = L.T.conj()
Lp = Ld @ L
"""
#----------------------------



w,v = la.eigh(H0.toarray())
#if verbose: print(v[:,0].conj() @ Se['sz0'] @ v[:,0])
#if verbose: print(v[:,0].conj() @ Se['sz3'] @ v[:,0])
#if verbose: print(w[:5])
rho = np.outer(v[:,0], v[:,0].conj())
tf  = 1000
dt  = 0.1
times = np.arange(0, tf+dt, dt)
evolve   = Lindblad(spin, gamma=1, dt=dt)
m_op = spin.field_op(1,'sz')/N
result = np.zeros((len(times),2))

#if verbose: print("initial m: ",np.trace(rho @ m_op))

#savemat("mat_16/rho.mat",{"rho":rho})
#savemat("mat_16/H.mat",{"H":H0})
#savemat("mat_16/op.mat",{"op":Se})
#savemat("mat_16/m.mat",{"m":m_op})
#savemat("mat_16/L.mat",{"L":L,"Ld":Ld,"Lp":Lp})

#start time evolution
for t_ind, t_now in enumerate(times):
    result[t_ind,0] = np.trace(rho @ m_op)
    #print(t_ind, result[t_ind], np.trace(rho).real)
    rho = evolve.rk4_rho(rho, H0, L,Ld,Lp)

gibbs_m = np.trace(calc_rho_vdv(w,v,T) @ m_op)
result[:,1] = np.ones(len(times))*gibbs_m

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = result
values['xlabel1'] = r'$\mathrm{Time\ (fs)}$'
values['ylabel1'] = r'$\mathrm{m}$'
sf = np.array([[0,2]])
plotfig(data, 'm16', values, sf=sf)
if verbose: print("final m: ",np.trace(rho @ m_op))
if verbose: print("gibbs m:", np.trace(calc_rho_vdv(w,v,T) @ m_op))
if verbose: print('run finished in:\t',time.time()-start)
