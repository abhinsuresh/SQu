import sys, numpy as np, time, h5py
import scipy.linalg as la
from scipy.io import savemat, loadmat

#sys.path.append('../')
from spin_core import Spin
from evolve_step import Lindblad
from master_equation import BathSpectral, calc_rho_vdv
from density_core import *
HBAR = 0.658211951
dim = '1d'

#parameters
N       = 8
S       = 1/2
sc      = 2
J       = 0.1
Jz      = 1.005*J

gamma   = 0.02 * J
Lambda  = 8 * J
omega0  = 2 * J
T       = 10 * J

tf  = 100
dt  = 0.05
start = time.time()
#spin system creation with eDOF
spin = Spin(S=S, L = N, sc = 2)
evolve   = Lindblad(spin, gamma=gamma, dt=dt)
Se = spin.op

H0   = -1*spin.heisenberg(J=J,Jz=Jz) 

w,v = la.eigh(H0.toarray())
print(w[:5])

rho_spin = loadmat('rho_t070')["rho_spin"]
rho_f = partial_trace_A(rho_spin,2,256)

#rho_spin = loadmat('rho_t001')["rho_spin"]
#rho_i = partial_trace_A(rho_spin,2,256)

print("energy:", np.trace(rho_f @ H0))

def e_basis(mat, v):
    return v.conj().T @ mat @ v
def s_basis(mat, v):
    return v @ mat @ v.conj().T

#jump operator
Xs = Se['sx7']
Xe = e_basis(Xs, v)

Ediff = (w[0] - w).reshape(-1,1)
for ind in range(1,len(w)):
    Ediff = np.append(Ediff,(w[ind]-w).reshape(-1,1),axis=1)
#limiting case
Ediff = Ediff + 1e-14
 
L1a = np.sqrt(2*np.pi*gamma*BathSpectral(Ediff,omega0,T,Lambda))
L1b = Xe
L1e = L1a * L1b
L = s_basis(L1e, v)
Ld = L.T.conj()
Lp = Ld @ L


rho = rho_f

print(logarithmic_negativity(rho, m=16))
print(entanglement_entropy(rho, True))

savemat("mat_256/rho.mat",{"rho":rho})
savemat("mat_256/H.mat",{"H":H0})
savemat("mat_256/op.mat",{"op":Se})
savemat("mat_256/L.mat",{"L":L,"Ld":Ld,"Lp":Lp})
