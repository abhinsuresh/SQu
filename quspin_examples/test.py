import numpy as np # general math functions
import time
import h5py
import matplotlib.pyplot as plt
from scipy import sparse

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

from quspin.operators import hamiltonian # operators
from quspin.basis import spin_basis_general # spin basis constructor
from quspin.basis import spin_basis_1d # spin basis constructor
from function_wrapper import calc_rho

no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)
np.set_printoptions(formatter={'float': lambda x: "{0:1.2e}".format(x)})

def spin_op(basis, site=0, s='z'):
    sigma = [[s, [[1.0, site]]]]
    return hamiltonian(sigma,[],dtype=np.float64,basis=basis,**no_checks)
#parameters
L = 10
Jzz=0.1

#deriverd parameters
Jxy = Jzz*np.logspace(-3, 0, num=20, endpoint=True)[::-1]
lb2 = L//2
s1 = '10'*lb2
s2 = '01'*lb2
subsystem=[i for i in range(L//2)]

start = time.time()
#setting the basis
basis = spin_basis_1d(L)
print('size:', basis.Ns)
ind_1 = basis.Ns - int(s1,2) - 1
ind_2 = basis.Ns - int(s2,2) - 1

#measurables
state_1 = np.zeros((basis.Ns,basis.Ns)); state_1[ind_1,ind_1]=1
state_2 = np.zeros((basis.Ns,basis.Ns)); state_2[ind_2,ind_2]=1
sz0 = spin_op(basis,0,'z')

st_1    = np.zeros(len(Jxy))
st_2    = np.zeros(len(Jxy))
ent     = np.zeros(len(Jxy))
spin_0  = np.zeros(len(Jxy))

for J_ind, J in enumerate(Jxy):
    
    J_xy=[[J,   i, (i+1)%L] for i in range(L)] # PBC
    J_zz=[[Jzz, i, (i+1)%L] for i in range(L)] # PBC
    #print(len(J_zz))

    static=[["xx",J_xy],["yy",J_xy],["zz",J_zz]]

    H=hamiltonian(static,[],dtype=np.float64,basis=basis,**no_checks)

    #E,V= H.eigh()
    #print('Ei using eigh: ',E[:5])
    linalg.init()
    a_gpu = gpuarray.to_gpu(H.toarray()) 
    vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
    E = w_gpu.get()
    V = vr_gpu.get().transpose()
    #V = vr_gpu.get().transpose()

    rho = calc_rho(E, V, 200)
    #rho = np.eye(len(E))

    st_1[J_ind]   = np.trace(rho @ state_1)
    st_2[J_ind]   = np.trace(rho @ state_2)
    #ent[J_ind]    = basis.ent_entropy(rho, sub_sys_A=subsystem)['Sent_A']
    #spin_0[J_ind] = sz0.expt_value(rho)

    
with h5py.File('results/spinchain_1d.hdf5', 'w') as f:
    dset0 = f.create_dataset("JxybyJzz", 
             data = Jxy , compression="gzip")
    dset1 = f.create_dataset("st_1", 
             data = st_1 , compression="gzip")
    dset2 = f.create_dataset("st_2", 
             data = st_2 , compression="gzip")
    dset3 = f.create_dataset("ent", 
             data = ent , compression="gzip")
    dset4 = f.create_dataset("spin_0", 
             data = spin_0 , compression="gzip")
f.close()

print('time taken: ', time.time()- start)

#plt.plot(Jxy, st_1)
#plt.xscale('log')
#plt.savefig('results/fig.png')

#print('Energy values: ',w_gpu[:5])
#print(type(a_gpu), type(vr_gpu), type(a_gpu.get()))
#print('eigen vector test: ', V[0,:] @ H.toarray() @ V[0,:], E[1])
#print(np.sum(V[:,0]))
#print("%0.3e"%spin_op(basis,0).expt_value(V[0,:]),"%0.3e"%spin_op(basis,0).expt_value(V[1,:]))
#print("%0.3e"%spin_op(basis,1).expt_value(V[0,:]),"%0.3e"%spin_op(basis,1).expt_value(V[1,:]))
#print("%0.3e"%spin_op(basis,2).expt_value(V[0,:]),"%0.3e"%spin_op(basis,2).expt_value(V[1,:]))





