import numpy as np # general math functions
import time
import h5py
import matplotlib.pyplot as plt

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

from quspin.operators import hamiltonian # operators
from quspin.basis import spin_basis_general # spin basis constructor
from function_wrapper import calc_rho

no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)
np.set_printoptions(formatter={'float': lambda x: "{0:1.2e}".format(x)})

def spin_op(basis, site=0, s='z'):
    sigma = [[s, [[1.0, site]]]]
    return hamiltonian(sigma,[],dtype=np.float64,basis=basis,**no_checks)
#parameters
Lx,Ly = 4,2
Jzz=0.1
Jxy=0.1

#deriverd parameters
_beta = np.linspace(0,100,20) 
N2d = Lx*Ly
s = np.arange(N2d)
x = s%Lx 
y = s//Lx 
T_x = (x+1)%Lx + Lx*y
T_y = x +Lx*((y+1)%Ly)

lb2 = Lx//2
s1 = '10'*lb2 + '01'*lb2
s2 = '01'*lb2 + '10'*lb2
subsystem=[i for i in range(N2d//2)]

start = time.time()
#setting the basis
basis = spin_basis_general(N2d)
print('size:', basis.Ns)
ind_1 = basis.Ns - int(s1,2) - 1
ind_2 = basis.Ns - int(s2,2) - 1

#measurables
state_1 = np.zeros((basis.Ns,basis.Ns)); state_1[ind_1,ind_1]=1
state_2 = np.zeros((basis.Ns,basis.Ns)); state_2[ind_2,ind_2]=1
sz0 = spin_op(basis,0,'z')

st_1    = np.zeros(len(_beta))
st_2    = np.zeros(len(_beta))
ent     = np.zeros(len(_beta))
spin_0  = np.zeros(len(_beta))

for b_ind, beta in enumerate(_beta):
    
    J_xy=[[Jxy,i,T_x[i]] for i in range(N2d)] + [[Jxy,i,T_y[i]] for i in range(N2d)] #PBC
    J_zz=[[Jzz,i,T_x[i]] for i in range(N2d)] + [[Jzz,i,T_y[i]] for i in range(N2d)] #PBC
    #print(len(J_zz))

    static=[["xx",J_xy],["yy",J_xy],["zz",J_zz]]

    H=hamiltonian(static,[],dtype=np.float64,basis=basis,**no_checks)

    #E,V= H.eigh()
    #print('Ei using eigh: ',E[:5])
    linalg.init()
    a_gpu = gpuarray.to_gpu(H.toarray()) 
    vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
    E = w_gpu.get()
    V = vr_gpu.get().transpose()

    rho = calc_rho(E, V, beta)

    st_1[b_ind]   = np.trace(rho @ state_1)
    st_2[b_ind]   = np.trace(rho @ state_2)
    ent[b_ind]    = basis.ent_entropy(rho, sub_sys_A=subsystem)['Sent_A']
    spin_0[b_ind] = sz0.expt_value(rho)
    print("step: ", b_ind)
    
with h5py.File('results/spin_2d_8_t.hdf5', 'w') as f:
    dset0 = f.create_dataset("beta", 
             data = _beta , compression="gzip")
    dset1 = f.create_dataset("st_1", 
             data = st_1 , compression="gzip")
    dset2 = f.create_dataset("st_2", 
             data = st_2 , compression="gzip")
    dset3 = f.create_dataset("ent", 
             data = ent , compression="gzip")
    dset4 = f.create_dataset("spin_0", 
             data = spin_0 , compression="gzip")
f.close()

print('time taken: ', time.time()- start)

#plt.plot(Jxy, st_1)
#plt.xscale('log')
#plt.savefig('results/fig2.png')

#print('Energy values: ',w_gpu[:5])
#print(type(a_gpu), type(vr_gpu), type(a_gpu.get()))
#print('eigen vector test: ', V[0,:] @ H.toarray() @ V[0,:], E[1])
#print(np.sum(V[:,0]))
#print("%0.3e"%spin_op(basis,0).expt_value(V[0,:]),"%0.3e"%spin_op(basis,0).expt_value(V[1,:]))
#print("%0.3e"%spin_op(basis,1).expt_value(V[0,:]),"%0.3e"%spin_op(basis,1).expt_value(V[1,:]))
#print("%0.3e"%spin_op(basis,2).expt_value(V[0,:]),"%0.3e"%spin_op(basis,2).expt_value(V[1,:]))





