import numpy as np
import time

from quspin.basis import spinful_fermion_basis_general
from quspin.basis import spinful_fermion_basis_1d
from quspin.operators import hamiltonian
from function_wrapper import updn_list

#model parameters
Lx, Ly = 4, 1
N_2d = Lx*Ly
Nlist = updn_list(N_2d)
#print(Nlist)

J=1.0 # hopping matrix element
U=2.0 # onsite interaction
mu=0.5 # chemical potential

s = np.arange(N_2d) # sites [0,1,2,...,N_2d-1] in simple notation
x = s%Lx # x positions for sites
y = s//Lx # y positions for sites
T_x = (x+1)%Lx + Lx*y # translation along x-direction
T_y = x + Lx*((y+1)%Ly) # translation along y-direction
S = -(s+1) # fermion spin inversion in the simple case
start = time.time()
basis_2d=spinful_fermion_basis_general(N_2d,Nf=Nlist,
                                       kxblock=(T_x,0))
basis_1d=spinful_fermion_basis_1d(N_2d,Nf=Nlist,a=2,kblock=0)
#basis_1d=spinful_fermion_basis_1d(N_2d,Nf=Nlist, double_occupancy=False)
#basis_1d=spinful_fermion_basis_1d(N_2d,Nf=Nlist, pblock=1)
                                     #  kyblock=(T_y,0),
                                     #  sblock=(S,0))

print(basis_1d)
#print(T_x)
#print(T_y)
end = time.time()

print('Time taken: ', '%0.3e'%(end-start),' s')



