import numpy as np
import scipy
import cupy
from numba import jit

from quspin.operators import hamiltonian

no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)

def updn_list(N):
    """return list of Nup and Ndn for using in quspin
    """
    list1 = []
    for i in range(N//2 + 1):
        list1.append((i,N-i))
        if i != N-i:
            list1.append((N-i,i))
    return list1

def fermi_fun(E, E0=0, beta=1e6):
    """Calculate fermi function
    """    
    _x = -1*beta*(E - E0)
    return scipy.special.expit(_x)

@jit(nopython=True, parallel=False)
def calc_rho(E,V, beta=100, verbose=False):
    """Calculate the canonical gibbs density matrix
    """
    rho = np.zeros((len(E),len(E)), dtype=np.complex128)
    E = E - E[0]
    for ind in np.arange(len(E)):
        rho = rho + np.exp(-beta*E[ind])*\
                np.outer(V[:,ind].conjugate(),V[:,ind])
    
    return rho/( np.trace(rho) )

def calc_cupy_rho(E,V, beta=100, verbose=False):
    """Calculate the canonical gibbs density matrix
    """
    rho = cupy.zeros((len(E),len(E)))
    #out = cupy.zeros((len(E),len(E)))
    E = E - E[0]
    for ind in range(len(E)):
        rho = rho + cupy.exp( -beta*E[ind])*\
            cupy.outer(cupy.conjugate(V[:,ind]), V[:,ind])
    
    return rho/( cupy.trace(rho) )

def spin_op(site, sigma, basis):
    """Obtain spin operators from quspin
    """
    val = 1.0
    if sigma == 'x':
        s = [['+|-', [[val, site, site]]]]
        updn = hamiltonian(s,[],dtype=np.float64,basis=basis,  **no_checks)
        dnup = updn.getH()
        op = 0.5*(updn + dnup)
    elif sigma == 'y':
        s = [['+|-', [[val, site, site]]]]
        updn = hamiltonian(s,[],dtype=np.float64,basis=basis,  **no_checks)
        dnup = updn.getH()
        op = -0.5*1j*(updn - dnup)
    elif sigma == 'z':
        s = [['+-|', [[val, site, site]]]]
        nup = hamiltonian(s,[],dtype=np.float64,basis=basis,  **no_checks)
        s = [['|+-', [[val, site, site]]]]
        ndn = hamiltonian(s,[],dtype=np.float64,basis=basis,  **no_checks)
        op = 0.5*(nup - ndn)
    return op
