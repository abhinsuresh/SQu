import numpy as np
import time

from quspin.basis import spinful_fermion_basis_general
from quspin.operators import hamiltonian
from function_wrapper import updn_list

#model parameters
Lx, Ly = 4, 4
N_2d = Lx*Ly
Nlist = updn_list(N_2d)

J=1.0 # hopping matrix element
U=2.0 # onsite interaction
mu=0.5 # chemical potential

s = np.arange(N_2d) # sites [0,1,2,...,N_2d-1] in simple notation
x = s%Lx # x positions for sites
y = s//Lx # y positions for sites
T_x = (x+1)%Lx + Lx*y # translation along x-direction
T_y = x + Lx*((y+1)%Ly) # translation along y-direction
S = -(s+1) # fermion spin inversion in the simple case
start = time.time()
basis_2d=spinful_fermion_basis_general(N_2d,Nf=Nlist,
                                       kxblock=(T_x,0),
                                       kyblock=(T_y,0),
                                       sblock=(S,0))

print(basis_2d.Ns)

hopping_left =[[-J,i,T_x[i]] for i in range(N_2d)] +\
              [[-J,i,T_y[i]] for i in range(N_2d)]
hopping_right=[[+J,i,T_x[i]] for i in range(N_2d)] +\
              [[+J,i,T_y[i]] for i in range(N_2d)]
potential=[[-mu,i] for i in range(N_2d)]
interaction=[[U,i,i] for i in range(N_2d)]
#
static=[["+-|",hopping_left], # spin up hops to left
        ["-+|",hopping_right], # spin up hops to right
        ["|+-",hopping_left], # spin down hopes to left
        ["|-+",hopping_right], # spin up hops to right
        ["n|",potential], # onsite potenial, spin up
        ["|n",potential], # onsite potential, spin down
        ["n|n",interaction]] # spin up-spin down interaction
# build hamiltonian
H=hamiltonian(static,[],basis=basis_2d,dtype=np.float64)
# diagonalise H
E=H.eigsh()

end = time.time()

print('Time taken: ', '%0.3e'%(end-start),' s')



