import numpy as np; import sys
import time; import cupy
import matplotlib.pyplot as plt
import h5py; import scipy.linalg as la

from quspin.basis import spinful_fermion_basis_general
from quspin.operators import hamiltonian

from function_wrapper import calc_rho
from function_wrapper import calc_cupy_rho
from function_wrapper import spin_op 
from function_wrapper import updn_list
sys.path.append('../plot_manager')

from plot2d import plotfig

no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)
np.set_printoptions(formatter={'float': lambda x: "{0:1.2e}".format(x)})
KB = 8.6173324e-5

#parameters
Lx,Ly = 2,2
gs_beta = 1e5
fig_name = 'tj_temp'
Jzz = 0.1
Jxy = 0.1
t   = 0.0
err = 0.0
gpu = 0
temp = np.linspace(1e-14, 20, 5)

#deriverd parameters
ent = np.zeros(len(temp))
if gpu == 1:
    import pycuda.gpuarray as gpuarray
    import pycuda.autoinit
    from skcuda import linalg
    from skcuda import misc
N2d = Lx*Ly
_beta = 1/(KB*temp)
s = np.arange(N2d)
x = s%Lx 
y = s//Lx 
T_x = (x+1)%Lx + Lx*y
T_y = x +Lx*((y+1)%Ly)

start = time.time()
#setting the basis
b = spinful_fermion_basis_general(N2d, Nf = updn_list(N2d))
print('basis size:', b.Ns)

hop_left =[[+t,i,T_x[i]] for i in range(N2d)] + [[+t,i,T_y[i]] for i in range(N2d)]
hop_right=[[-t,i,T_x[i]] for i in range(N2d)] + [[-t,i,T_y[i]] for i in range(N2d)]

static=[
        ['+-|',hop_left],  # up hops left
        ['-+|',hop_right], # up hops right
        ['|+-',hop_left],  # down hops left
        ['|-+',hop_right], # down hops right
                                ]
#build kinetic Hamiltonian
Ht=hamiltonian(static,[],basis=b,dtype=np.float64,**no_checks)
#build defect 
defect = -err * spin_op(0,'z',b)

for ind, beta in enumerate(_beta):
    #build heisenberg hamiltonian
    s = [['+|I', [[0.0, 0, 0]]]]
    Hj = hamiltonian(s,[],dtype=np.float64,basis=b,**no_checks)
    print('beta:','%1.2e'%beta)
    for coup in hop_right:
        Hj = Hj + Jzz* spin_op(coup[1],'z',b) * spin_op(coup[2],'z',b)
        Hj = Hj + Jxy* spin_op(coup[1],'y',b) * spin_op(coup[2],'y',b)
        Hj = Hj + Jxy* spin_op(coup[1],'x',b) * spin_op(coup[2],'x',b)

    #adding both t and j part with defect
    H = Ht + Hj + defect

    #print(H.toarray().dtype)
    if gpu == 1:
        linalg.init()
        a_gpu = gpuarray.to_gpu(H.toarray()) 
        vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
        E = w_gpu.get()
        V = vr_gpu.get().transpose()
        cw = cupy.array(E)
        cv = cupy.array(V)
        rho = calc_cupy_rho(cw, cv, beta)
        rho_cpu = rho.get()
        ent[ind] = spin_op(0,'z',b).expt_value(rho_cpu).real
    elif gpu == 2:
        hg = cupy.array(H.toarray())
        cw, cv = cupy.linalg.eigh(hg,'L')
        rho = calc_cupy_rho(cw, cv, beta)
        rho_cpu = rho.get()
        ent[ind] = spin_op(0,'z',b).expt_value(rho_cpu).real
        #print(cw[:5])
    else:
        E, V = la.eigh(H.toarray())
        #print('%1.3e'%spin_op(0,'z',b).expt_value(V[:,0]).real)
        #print('%1.3e'%spin_op(1,'z',b).expt_value(V[:,0]).real)
        rho = calc_rho(E, V, beta)
        #spin_exp[ind] = spin_op(0,'z',b).expt_value(rho).real
        ent[ind] = b.ent_entropy(rho, sub_sys_A=(range(N2d//2), range(N2d//2)), 
        alpha=1.0, density=False)["Sent_A"]

data = dict();values = dict()
data['temp'] = temp; data['ent'] = ent
values['x_plot'] = 'temp'; values['y_plot'] = 'ent'
values['xlabel'] = r'$\mathrm{Temperature\ (K)}$'
values['ylabel'] = r'$\mathrm{Half\ entropy}$'
#plt.plot(J_xy,spin_exp)
#plt.savefig("results/11.pdf")
plotfig(data, values, fig_name, fs=8)


print('time taken: ', time.time()- start)
