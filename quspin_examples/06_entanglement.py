from quspin.basis import spinful_fermion_basis_1d
from quspin.operators import hamiltonian
import numpy as np

L = 6
t = -1.0

basis = spinful_fermion_basis_1d(L,Nf=(L//2,L//2))
print(basis.Ns)

hop_right=[[-t,i,(i+1)%L] for i in range(L)] #PBC
hop_left= [[+t,i,(i+1)%L] for i in range(L)] #PBC

static=[
        ['+-|',hop_left],  # up hops left
        ['-+|',hop_right], # up hops right
        ['|+-',hop_left],  # down hops left
        ['|-+',hop_right], # down hops right
                                ]
dynamic=[]
# build Hamiltonian
no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)
H=hamiltonian(static,dynamic,basis=basis,dtype=np.float64,**no_checks)

E,V = H.eigh()
#E,V=H.eigsh(k=5, which='SA')
E[:5]

for i in range(1,4):
    print(basis.ent_entropy(V[:,0], sub_sys_A=(range(i),range(i)), 
                enforce_pure=True, alpha=1.0, density=False)["Sent_A"])


