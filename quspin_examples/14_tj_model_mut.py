import numpy as np
import time
import cupy
import matplotlib.pyplot as plt
import h5py
import scipy.linalg as la

from quspin.basis import spinful_fermion_basis_general
from quspin.operators import hamiltonian

from function_wrapper import calc_rho
from function_wrapper import calc_cupy_rho
from function_wrapper import spin_op 
from function_wrapper import updn_list


no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)
np.set_printoptions(formatter={'float': lambda x: "{0:1.2e}".format(x)})
KB = 8.6173324e-5

#parameters
Lx,Ly = 2,2
gs_beta = 1e20
Jzz = 0.1
Jxy = 0.0
t   = 0.0
err = 1e-10
temp = np.linspace(20, 400, 5)
gpu = 0

#deriverd parameters
if gpu == 1:
    import pycuda.gpuarray as gpuarray
    import pycuda.autoinit
    from skcuda import linalg
    from skcuda import misc
_beta = 1/(KB*temp)
N2d = Lx*Ly
s = np.arange(N2d)
x = s%Lx 
y = s//Lx 
T_x = (x+1)%Lx + Lx*y
T_y = x +Lx*((y+1)%Ly)

start = time.time()
#setting the basis
b = spinful_fermion_basis_general(N2d, Nf = updn_list(N2d))
print('basis size:', b.Ns)

hop_left =[[+t,i,T_x[i]] for i in range(N2d)] + [[+t,i,T_y[i]] for i in range(N2d)]
hop_right=[[-t,i,T_x[i]] for i in range(N2d)] + [[-t,i,T_y[i]] for i in range(N2d)]

static=[
        ['+-|',hop_left],  # up hops left
        ['-+|',hop_right], # up hops right
        ['|+-',hop_left],  # down hops left
        ['|-+',hop_right], # down hops right
                                ]
#build kinetic Hamiltonian
Ht=hamiltonian(static,[],basis=b,dtype=np.float64,**no_checks)

#build heisenberg hamiltonian
s = [['+|I', [[0.0, 0, 0]]]]
Hj = hamiltonian(s,[],dtype=np.float64,basis=b,**no_checks)
for coup in hop_right:
    Hj = Hj + Jzz* spin_op(coup[1],'z',b) * spin_op(coup[2],'z',b)
    Hj = Hj + Jxy* spin_op(coup[1],'y',b) * spin_op(coup[2],'y',b)
    Hj = Hj + Jxy* spin_op(coup[1],'x',b) * spin_op(coup[2],'x',b)

defect = -err * spin_op(0,'z',b)
Hj = Hj + defect 

#adding both t and j part with defect
H = Ht + Hj

#print(H.toarray().dtype)
if gpu == 1:
    linalg.init()
    a_gpu = gpuarray.to_gpu(H.toarray()) 
    vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
    print(w_gpu[:5])
    E = w_gpu.get()
    V = vr_gpu.get().transpose()
    cw = cupy.array(E)
    cv = cupy.array(V)
elif gpu == 2:
    hg = cupy.array(H.toarray())
    cw, cv = cupy.linalg.eigh(hg,'L')
    print(cw[:5])
else:
    E, V = la.eigh(Hj.toarray())
    print('%1.3e'%spin_op(0,'z',b).expt_value(V[:,0]).real)
    print('%1.3e'%spin_op(1,'z',b).expt_value(V[:,0]).real)
    print('%1.3e'%spin_op(0,'z',b).expt_value(V[:,1]).real)
    print('%1.3e'%spin_op(1,'z',b).expt_value(V[:,1]).real)
    print(E[:8])
    rho = calc_rho(E, V, gs_beta)
    print('%1.3e'%spin_op(0,'z',b).expt_value(rho).real)
    
"""
mut = np.zeros(len(_beta))
for i, beta in enumerate(_beta):
    rho = calc_cupy_rho(cw, cv, beta)
    rho_cpu = rho.get()

    #print('%1.3e'%spin_op(0,'z',b).expt_value(rho_cpu).real)
    #print('%1.3e'%spin_op(1,'z',b).expt_value(rho_cpu).real)
    #print('%1.3e'%spin_op(N2d-1,'z',b).expt_value(rho_cpu).real)


    s1 = b.ent_entropy(rho_cpu, sub_sys_A=(range(1), range(1)), 
        alpha=1.0, density=False)["Sent_A"]
    sN = b.ent_entropy(rho_cpu, sub_sys_A=(range(N2d-1, N2d),range(N2d-1,N2d)), 
        alpha=1.0, density=False)["Sent_A"]
    s1N = b.ent_entropy(rho_cpu, sub_sys_A=((0, N2d-1),(0, N2d-1)), 
        alpha=1.0, density=False)["Sent_A"]

    mut[i] =  s1 + sN - s1N

with h5py.File('results/mut.hdf5', 'w') as f:
    dset0 = f.create_dataset("beta", 
             data = _beta , compression="gzip")
    dset1 = f.create_dataset("mut", 
             data = mut , compression="gzip")
f.close()
plt.plot(_beta,mut)
plt.savefig("results/mut.png")
"""

print('time taken: ', time.time()- start)
