import numpy as np # general math functions
import time
import h5py

from quspin.operators import hamiltonian # operators
from quspin.basis import spin_basis_general # spin basis constructor
from quspin.basis import spin_basis_1d # spin basis constructor

no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)
np.set_printoptions(formatter={'float': lambda x: "{0:0.2e}".format(x)})

def spin_op(basis, site=0, s='z'):
    sigma = [[s, [[1.0, site]]]]
    return hamiltonian(sigma,dynamic,
                       dtype=np.float64,basis=basis, **no_checks)

Jzz=0.1
L = 12
start = time.time()


#setting the basis
basis = spin_basis_1d(L)
print('size:', basis.Ns)

J_zz=[[Jzz, i, (i+1)%L] for i in range(L)] # PBC
J_xy=[[Jzz  , i, (i+1)%L] for i in range(L)] # PBC

static=[["xx",J_xy],["yy",J_xy],["zz",J_zz]]
dynamic = []

H=hamiltonian(static,dynamic,dtype=np.float64,
              basis=basis, **no_checks)
es = time.time()
E,V=H.eigh()
ee = time.time()
print('eigh time: ', ee-es)
print('Energy values: ',np.round(E[:5],2))
print('time taken: ', time.time()- start)





