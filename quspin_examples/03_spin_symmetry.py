from quspin.operators import hamiltonian # operators
from quspin.basis import spin_basis_general # spin basis constructor
from quspin.basis import spin_basis_1d # spin basis constructor
import numpy as np # general math functions

J=1.0 # spin=spin interaction
g=0.5 # magnetic field strength
Lx, Ly = 4, 2 # linear dimension of spin 1 2d lattice
N_2d = Lx*Ly # number of sites for spin 1

s = np.arange(N_2d) # sites [0,1,2,....]
x = s%Lx # x positions for sites
y = s//Lx # y positions for sites
T_x = (x+1)%Lx + Lx*y # translation along x-direction
T_y = x +Lx*((y+1)%Ly) # translation along y-direction
P_x = x + Lx*(Ly-y-1) # reflection about x-axis
P_y = (Lx-x-1) + Lx*y # reflection about y-axis
Z   = -(s+1) # spin inversion

print(T_x)
print(T_y)
print(P_y)
print(P_x)
print(Z)

basis_2d = spin_basis_general(N_2d,k1block=(T_x,0), kyblock=(T_y,0))#,
basis_1d = spin_basis_1d(N_2d,kblock=0)#,
                              #  kyblock=(T_y,0),
                              #  pxblock=(P_x,0),
                              #  pyblock=(P_y,0),
                              #  zblock=(Z,0))
#print(basis_2d)
