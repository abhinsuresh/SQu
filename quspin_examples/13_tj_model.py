import numpy as np
import time
import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc
import cupy

from quspin.basis import spinful_fermion_basis_general
from quspin.operators import hamiltonian

from function_wrapper import calc_rho
from function_wrapper import calc_cupy_rho
from function_wrapper import spin_op 
from function_wrapper import updn_list


no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)
np.set_printoptions(formatter={'float': lambda x: "{0:1.2e}".format(x)})

#parameters
Lx,Ly = 2,2
t   = 0.0
Jzz = -0.1
Jxy = 0.0
err = 0.0

#deriverd parameters
#_beta = np.linspace(0,100,20) 
N2d = Lx*Ly
s = np.arange(N2d)
x = s%Lx 
y = s//Lx 
T_x = (x+1)%Lx + Lx*y
T_y = x +Lx*((y+1)%Ly)
#lb2 = Lx//2
#s1 = '10'*lb2 + '01'*lb2
#s2 = '01'*lb2 + '10'*lb2
#subsystem=[i for i in range(N2d//2)]

start = time.time()
#setting the basis
b = spinful_fermion_basis_general(N2d, Nf = updn_list(N2d))
print('basis size:', b.Ns)

hop_left =[[+t,i,T_x[i]] for i in range(N2d)] + [[+t,i,T_y[i]] for i in range(N2d)]
hop_right=[[-t,i,T_x[i]] for i in range(N2d)] + [[-t,i,T_y[i]] for i in range(N2d)]

static=[
        ['+-|',hop_left],  # up hops left
        ['-+|',hop_right], # up hops right
        ['|+-',hop_left],  # down hops left
        ['|-+',hop_right], # down hops right
                                ]
#build kinetic Hamiltonian
Ht=hamiltonian(static,[],basis=b,dtype=np.float64,**no_checks)

#build heisenberg hamiltonian
s = [['+|I', [[0.0, 0, 0]]]]
Hj = hamiltonian(s,[],dtype=np.float64,basis=b,**no_checks)
for coup in hop_right:
    Hj = Hj + Jzz*coup[0]* spin_op(coup[1],'z',b) * spin_op(coup[2],'z',b)
    Hj = Hj + Jxy*coup[0]* spin_op(coup[1],'y',b) * spin_op(coup[2],'y',b)
    Hj = Hj + Jxy*coup[0]* spin_op(coup[1],'x',b) * spin_op(coup[2],'x',b)

defect = -err * spin_op(0,'z',b)
Hj = Hj 

#adding both t and j part with defect
H = Hj 

#print(H.toarray().dtype)

linalg.init()
a_gpu = gpuarray.to_gpu(H.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
print(w_gpu[:5])
E = w_gpu.get()
V = vr_gpu.get().transpose()

#hg = cupy.array(H.toarray())
#cw, cv = cupy.linalg.eigh(hg,'L')
#V = cv.get()
#print(cw[:5])

cw = cupy.array(E)
cv = cupy.array(V)
rho = calc_cupy_rho(cw, cv, 1e6)

#rho_np = rho.ndarray

#print(type(rho.get()))
#print(V.dtype)
#rho = calc_rho(E,V, 100)

#print(len(er))
#gg = cupy.outer(vr[:,0],vr[:,0])
#print(gg.get().shape)

#E,V = H.eigh()
print('%1.3e'%spin_op(0,'z',b).expt_value(rho.get()).real)
print('%1.3e'%spin_op(1,'z',b).expt_value(rho.get()).real)
print('%1.3e'%spin_op(2,'z',b).expt_value(rho.get()).real)
print('%1.3e'%spin_op(3,'z',b).expt_value(rho.get()).real)

print('%1.3e'%spin_op(0,'z',b).expt_value(V[:,0]).real)
print('%1.3e'%spin_op(1,'z',b).expt_value(V[:,0]).real)
print('%1.3e'%spin_op(2,'z',b).expt_value(V[:,0]).real)
print('%1.3e'%spin_op(3,'z',b).expt_value(V[:,0]).real)


#E,V=H.eigsh(k=5, which='SA')
#E[:5]


#for i in range(1,4):
#    print(basis.ent_entropy(V[:,0], sub_sys_A=(range(i),range(i)), 
#                enforce_pure=True, alpha=1.0, density=False)["Sent_A"])


print('time taken: ', time.time()- start)
