import numpy as np # general math functions
import time
import h5py

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
from skcuda import linalg
from skcuda import misc

from quspin.operators import hamiltonian # operators
from quspin.basis import spin_basis_general # spin basis constructor
from quspin.basis import spin_basis_1d # spin basis constructor

no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)
np.set_printoptions(formatter={'float': lambda x: "{0:0.2e}".format(x)})

def spin_op(basis, site=0, s='z'):
    sigma = [[s, [[1.0, site]]]]
    return hamiltonian(sigma,dynamic,
                       dtype=np.float64,basis=basis, **no_checks)
Jxy=0.1
Jzz=0.2
L = 6
start = time.time()


#setting the basis
basis = spin_basis_1d(L)
print('size:', basis.Ns)

J_xy=[[Jxy, i, (i+1)%L] for i in range(L)] # PBC
J_zz=[[Jzz, i, (i+1)%L] for i in range(L)] # PBC
print(len(J_zz))

static=[["xx",J_xy],["yy",J_xy],["zz",J_zz]]
dynamic = []

H=hamiltonian(static,dynamic,dtype=np.float64,
              basis=basis, **no_checks)

#E,V= H.eigh()
#print('Ei using eigh: ',E[:5])
linalg.init()
a_gpu = gpuarray.to_gpu(H.toarray()) 
vr_gpu, w_gpu = linalg.eig(a_gpu, 'N', 'V')
E = w_gpu.get()
V = vr_gpu.get().transpose()


#print('Energy values: ',w_gpu[:5])
#print(type(a_gpu), type(vr_gpu), type(a_gpu.get()))
print('eigen vector test: ', V[:,0] @ H.toarray() @ V[:,0], E[0])
e = np.sort(E)
#print(np.sum(V[:,0]))
print("%0.3e"%spin_op(basis,0).expt_value(V[0,:]),"%0.3e"%spin_op(basis,0).expt_value(V[1,:]))
print("%0.3e"%spin_op(basis,1).expt_value(V[0,:]),"%0.3e"%spin_op(basis,1).expt_value(V[1,:]))
print("%0.3e"%spin_op(basis,2).expt_value(V[0,:]),"%0.3e"%spin_op(basis,2).expt_value(V[1,:]))
print('time taken: ', time.time()- start)





