import numpy as np # general math functions
import time
import h5py

from quspin.operators import hamiltonian # operators
from quspin.basis import spin_basis_general # spin basis constructor
from quspin.basis import spin_basis_1d # spin basis constructor

no_checks = dict(check_pcon=False,check_symm=False,check_herm=False)
np.set_printoptions(formatter={'float': lambda x: "{0:0.2e}".format(x)})

def spin_op(basis, site=0, s='z'):
    sigma = [[s, [[1.0, site]]]]
    return hamiltonian(sigma,dynamic,
                       dtype=np.float64,basis=basis, **no_checks)

Jzz=0.1
L = 14

Jxy = Jzz*np.logspace(-1,-5,10)
#Jxy = Jzz*np.linspace(1e-7, 1e-5, 10)
#Jxy = np.append(Jxy,0)
sz_l = np.zeros((len(Jxy),2))
ent_l = np.zeros(len(Jxy))

#setting the basis
basis = spin_basis_1d(L)
subsystem=[i for i in range(L//2)]

for J_ind, J in enumerate(Jxy):
    #setting the hamiltonian
    J_zz=[[Jzz, i, (i+1)%L] for i in range(L)] # PBC
    J_xy=[[J  , i, (i+1)%L] for i in range(L)] # PBC

    static=[["xx",J_xy],["yy",J_xy],["zz",J_zz]]
    dynamic = []

    H=hamiltonian(static,dynamic,dtype=np.float64,
                  basis=basis, **no_checks)

    E,V=H.eigh()
    #print(np.round(E[:3],3))
    sz_l[J_ind,0] = spin_op(basis,0).expt_value(V[:,0])
    sz_l[J_ind,1] = spin_op(basis,1).expt_value(V[:,0])
    ent_l[J_ind] = basis.ent_entropy(V[:,0],
                                    sub_sys_A=subsystem)['Sent_A']/L

with h5py.File('spin_info_l14.hdf5', 'w') as f:
    dset1 = f.create_dataset("Jxy_by_Jzz", 
             data = Jxy/Jzz , compression="gzip")
    dset2 = f.create_dataset("sz_l0", 
             data = sz_l[:,0] , compression="gzip")
    dset3 = f.create_dataset("sz_l1", 
             data = sz_l[:,1] , compression="gzip")
    dset4 = f.create_dataset("half_ent", 
             data = ent_l , compression="gzip")
f.close()
#print('Jxy/Jz:\n',Jxy/Jzz)
#print('GS sz_0:\n',sz_l[:,0])
#print('GS sz_1:\n',sz_l[:,1])
#print('GS half_ent:\n',ent_l)





