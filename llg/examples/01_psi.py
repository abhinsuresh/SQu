#!/usr/bin/env python3 
import sys
sys.path.append('../')
from kwant_systems import make_ribbon
from spins import Spins
from constants import SIG_X, SIG_Y, SIG_Z, UNIT, KB, HBAR
import numpy as np
import numpy.random as rand
import h5py
from scipy import linalg
sys.path.append('../../plot_manager')
from plot2d import plotfig
def main():
    #reading input parameters------------------------------
    N	    = 1
    tf	    = 500
    dt      = 0.1
    jexc    = 0
    g_l     = 0.01
    angle   = 150*np.pi/180
    s_conf  = (np.sin(angle), 0, np.cos(angle))
    print('s_conf', s_conf)

    #building the system-----------------------------------
    ribbon = make_ribbon(length=N)
    cspins = Spins(ribbon, jexc=jexc, g_lambda=g_l, 
                   dt = dt, bf_vec=(0,0,1),
                   bf = 1, spin_config=s_conf, 
                   temperature= 0)
    print(cspins.s) 
    
    with open('ref_0.txt', "w") as fref:
        out_str = '#reference data printed'		 
        out_str += '\n'
        fref.write(out_str)
    fref.close()
    espins = np.zeros(cspins.s.shape)

    #propagating-------------------------------------------
    times = np.arange(0, tf + cspins.dt, cspins.dt)
    save = np.zeros((3,len(times)))
    for t_index, time in enumerate(times):
        # evolving LLG
        
        #printing reference output to check
        save[:,t_index] = cspins.s
        #evolution
        cspins.llg(espins, time)
    T = 300
    avg = np.zeros(len(save[2,:]))
    for i in range(len(save[2,:])):
        if i < (len(save[2,:]) - T):
            avg[i] = np.sum(save[2,i:i+T])/T
        else:
            avg[i] = np.sum(save[2,i-T:i])/T

    for i in range(len(avg)):
        with open('ref_1.txt', "a") as fref:
                out_str = '{:5.2e} '.format(time)	   
                out_str += '{0: 1.3e} '.format(avg[i])			 
                out_str += '\n'
                fref.write(out_str)
        fref.close()

    data = dict();values = dict()
    data['alpha'] = times; data['energy'] = avg#save[2,:]#avg
    values['x_plot'] = 'alpha'; values['y_plot'] = 'energy'
    values['xlabel'] = r'$\mathrm{Time}$'
    values['ylabel'] = r'$\mathrm{M_z}$'
    #plt.plot(J_xy,spin_exp)
    #plt.savefig("results/11.pdf")
    plotfig(data, 'mz', values, xlim=(0,500), ylim=(-1,1),fs=8, lw=0.5) 

if __name__ == main():
	main()
