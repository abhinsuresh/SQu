import numpy as np
import h5py
import sys

sys.path.append('../../plot_manager/')
from plot2d import plotfig
scale_spin = 2

data = dict();values = dict()

def avg(sdata, T = 100):
    L = len(sdata) - 1
    l = L//T
    avg = np.zeros(l)
    for i in range(l):
        avg[i] = np.sum(sdata[i*T: (i+1)*T])
    return avg/T

fig_name  = 'h'

dset1  = np.loadtxt(sys.argv[1], unpack=True)
#dset2  = np.loadtxt(sys.argv[2], unpack=True)
#dset3  = np.loadtxt(sys.argv[3], unpack=True)
_time = np.arange(0,500+0.1,0.1)
s1  = dset1
#s2  = dset2
#s3  = dset3[:-1]

#s1 = avg(s1, 100)
#s2 = avg(s2, 10)
#s3 = avg(s3, 100)
#print(s1.shape)
#print(s2.shape)
#print(s3.shape)
#_time = np.arange(0,500,0.1)


L = len(s1)
plot_y = s1.reshape(L,1)
#plot_y = np.concatenate((plot_y, s2.reshape(L,1)), axis=1)
#plot_y = np.concatenate((plot_y, s3.reshape(L,1)), axis=1)
plot_y = np.concatenate((plot_y,0.9*np.ones((L,1))), axis=1)
plot_y = np.concatenate((plot_y, 0.3130*np.ones((L,1))), axis=1)
#spinp = pulse[N1:N2]
#plot = np.concatenate((spinp.reshape(L,1),pulse.reshape(L,1)), axis = 1)
plot_x = _time; 
#values['ylabel'] = r'$\mathrm{{\langle {\hat{S}^x}_6 \rangle} - {\langle \hat{S}^x_2 \rangle}}$'
values['ylabel'] = r'$\mathrm{m_z}$'
values['xlabel'] = r'$\mathrm{Time\ (fs)}$'

    
data['x_plot'] = plot_x; data['y_plot'] = plot_y
Cl = ['k', 'b', 'C3', 'grey', 'grey']
Ls = ['-', '-', '-', '--', '--']
Mk = ['-', '-', '-', '--', '--']
plotfig(data, fig_name, values,
        xlim=(0,_time[-1]), ylim=(-1,1), text=1, 
        Cl=Cl, Ls =Ls, Mk=Mk,fs=8, lw=0.4, minor_y=1, mks=0.1)
