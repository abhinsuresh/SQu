#!/usr/bin/env python3 
import sys
sys.path.append('../')
from kwant_systems import make_ribbon
from spins import Spins
from constants import SIG_X, SIG_Y, SIG_Z, UNIT, KB, HBAR
import numpy as np
import numpy.random as rand
import h5py
import time
from scipy import linalg
sys.path.append('../../plot_manager')
from plot2d import plotfig
def main():
    #reading input parameters------------------------------
    nm      = '06'
    nm      = nm + ''
    N	    = 4
    tf	    = 120
    dt      = 0.1
    jexc    = 0.01
    g_l     = 0.0
    Bz      = -0.06
    Nconf   = 400
    T       = 100
    nm      = nm + str(T) + '_'
    espins = np.zeros((4,3))
    start = time.time()
    #building the system-----------------------------------
    def bf_func(time, site):
        if site.pos[0] == 0:
            return np.array([0,0,Bz*(-1)**(site.pos[0])])
        else:
            return np.array([0,0,Bz*(-1)**(site.pos[0])])
    ribbon = make_ribbon(length=N)
    times = np.arange(0, tf + dt, dt)
    csave  = np.zeros((3,len(times)))
    for nc in range(Nconf): 
        cspins = Spins(ribbon, jexc=jexc, g_lambda=g_l, 
                       dt = dt, bf_func=bf_func,
                       temperature= T, seed = nc)
        for i in np.arange(N):
            cspins.s[i] = [0,0,(-1)**i]
        for t_index, ti in enumerate(times):
            csave[:,t_index] =  csave[:, t_index] + cspins.s[1,:]
            #evolution
            cspins.llg(espins, ti)

    #time averaging
    csave = csave/Nconf

    with h5py.File('results/'+nm+'.hdf5', 'w') as f:
        dset = f.create_dataset("cspin", 
                 data = csave, 
                 dtype=np.float64, compression="gzip")
    f.close()
    #saving output to results
    with open('results/'+nm+'out.txt', "w") as fref:
        out_str = str(time.time()-start)
        fref.write(out_str)
    fref.close()
    """
    data = dict();values = dict()
    data['alpha'] = times; data['energy'] = csave[2,:]#avg
    values['x_plot'] = 'alpha'; values['y_plot'] = 'energy'
    values['xlabel'] = r'$\mathrm{Time}$'
    values['ylabel'] = r'$\mathrm{M_z}$'
    #plt.plot(J_xy,spin_exp)
    #plt.savefig("results/11.pdf")
    plotfig(data, 'mzc', values, xlim=(0,500),
            ylim=(-1,1),fs=8, lw=0.5) 
    """
if __name__ == main():
	main()
