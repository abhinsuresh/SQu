#!/usr/bin/env python3 
import sys
sys.path.append('../')
from kwant_systems import make_ribbon
from spins import Spins
from constants import SIG_X, SIG_Y, SIG_Z, UNIT, KB, HBAR
import numpy as np
import numpy.random as rand
import h5py
import time
from scipy import linalg
sys.path.append('../../plot_manager')
from plot2d import plotfig
def main():
    #reading input parameters------------------------------
    nm      = '04'
    nm      = nm + ''
    N	    = 1
    tf	    = 500
    dt      = 0.1
    jexc    = 0
    g_l     = 0.01
    Nconf   = 400
    T       = 0.1
    angle   = 150*np.pi/180
    s_conf  = (np.sin(angle), 0, np.cos(angle))
    espins = np.zeros(3)
    print(s_conf)
    start = time.time()
    #building the system-----------------------------------
    ribbon = make_ribbon(length=N)
    times = np.arange(0, tf + dt, dt)
    csave  = np.zeros((3,len(times)))
    with open('results/'+nm+str(T)+'.txt', "w") as fref:
        out_str = '#calculation for llg noise at T='+str(T)		 
        out_str += '\n'
        fref.write(out_str)
    fref.close()
    for nc in range(Nconf): 
        cspins = Spins(ribbon, jexc=jexc, g_lambda=g_l, 
                       dt = dt, bf_vec=(0,0,1),
                       bf = 1, spin_config=s_conf, 
                       temperature= T, seed = nc)
        for t_index, ti in enumerate(times):
            csave[:,t_index] = csave[:,t_index] + cspins.s
            #evolution
            cspins.llg(espins, ti)

    #time averaging
    csave = csave/Nconf
    #saving output to results
    for i in range(len(times)):
        with open('results/'+nm+str(T)+'.txt', "a") as f:
            out_str = ''
            out_str += '{0: 1.3e} '.format(csave[2,i])			 
            out_str += '\n'
            f.write(out_str)
        f.close()
    with open('results/'+nm+'out.txt', "w") as fref:
        out_str = str(time.time()-start)
        fref.write(out_str)
    fref.close()
    """
    data = dict();values = dict()
    data['alpha'] = times; data['energy'] = csave[2,:]#avg
    values['x_plot'] = 'alpha'; values['y_plot'] = 'energy'
    values['xlabel'] = r'$\mathrm{Time}$'
    values['ylabel'] = r'$\mathrm{M_z}$'
    #plt.plot(J_xy,spin_exp)
    #plt.savefig("results/11.pdf")
    plotfig(data, 'mzc', values, xlim=(0,500),
            ylim=(-1,1),fs=8, lw=0.5) 
    """
if __name__ == main():
	main()
