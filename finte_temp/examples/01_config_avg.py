import sys, time
sys.path.append('../')
from kwant_systems import make_ribbon
from spins import Spins
from constants import SIG_X, SIG_Y, SIG_Z, UNIT, KB, HBAR
import numpy as np
import numpy.random as rand
import h5py
import time
from scipy import linalg
sys.path.append('../../plot_manager')
from plot2d import plotfig
start = time.time()
#reading input parameters------------------------------
N	= 1
tf	= 500
dt      = 0.1
jexc    = 0
g_l     = 0.01
Nconf   = 400
T       = 0.1
angle   = 150*np.pi/180
s_conf  = (np.sin(angle), 0, np.cos(angle))
espins = np.zeros(3)

#building the system-----------------------------------
start = time.time()
ribbon = make_ribbon(length=N)
times = np.arange(0, tf + dt, dt)
csave  = np.zeros((len(times),3))

print(s_conf)
for nc in range(Nconf): 
    cspins = Spins(ribbon, jexc=jexc, g_lambda=g_l, dt = dt, 
                   bf_vec=(0,0,1), bf = 1, spin_config=s_conf, 
                   temperature=T)
    for t_index, ti in enumerate(times):
        csave[t_index,:] = csave[t_index,:] + cspins.s
        #evolution
        cspins.llg(espins, ti)

data = dict();values = dict()
data['x_plot'] = times; data['y_plot'] = csave[:,2:3]/Nconf
values['xlabel'] = r'$\mathrm{Time}$'
values['ylabel'] = r'$\mathrm{M_z}$'
plotfig(data, '1', values, ylim=(-1.2,1.2),fs=8, lw=0.5)
print('time taken: ',time.time()-start)
"""
with h5py.File('results/'+nm+'.hdf5', 'w') as f:
    dset = f.create_dataset("cspin", 
             data = csave, 
             dtype=np.float64, compression="gzip")
f.close()
with open('results/'+nm+'out.txt', "w") as fref:
    out_str = str(time.time()-start)
    fref.write(out_str)
fref.close()
"""
