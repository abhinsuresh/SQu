import numpy as np
import kwant
import matplotlib
import matplotlib.pyplot as plt
import warnings
from constants import HBAR, SIG_X, SIG_Y, SIG_Z, UNIT

warnings.filterwarnings("ignore")
matplotlib.use('Agg')

def make_ribbon(length=2, width=1, onsite=0, gamma=1.,
				tsoc=0., plot_sys=False, spin=False):

	if spin:
            unit = np.array([[1, 0], [0, 1]])
            sigmax = np.array([[0, 1], [1, 0]])
            sigmay = np.array([[0, -1j], [1j, 0]])
            sigmaz = np.array([[1, 0], [0, 1]])
	else:
            unit = 1

	def onsite_f(site, v0, p1):
            x, y = site.pos
            return unit * onsite

	def onsite_lead(site, v0, p1):
            x, y = site.pos
            return unit * onsite

	def hopping_fx(site1, site2, v0, p1):
            if spin:
                    res = -unit*gamma - 1j*tsoc*sigmay
            else:
                    res = -unit*gamma
            return res

	def hopping_fy(site1, site2, v0, p1):
            if spin:
                    res = -unit*gamma + 1j*tsoc*sigmax
            else:
                    res = -unit*gamma
            return res

	def hopping_f(site1, site2, v0, p1):
            return -unit*gamma

	sys = kwant.Builder()
	lat = kwant.lattice.square()
	for i in range(length):
            for j in range(width):
                sys[lat(i, j)] = onsite_f
                if j > 0:
                    sys[lat(i, j), lat(i, j-1)] = hopping_fy
                if i > 0:
                    sys[lat(i, j), lat(i-1, j)] = hopping_fx

	sym_left_lead = kwant.TranslationalSymmetry((-1, 0))
	left_lead = kwant.Builder(sym_left_lead)

	for j in range(width):
            left_lead[lat(0, j)] = onsite_f
            if j > 0:
                left_lead[lat(0, j), lat(0, j-1)] = hopping_f
            left_lead[lat(1, j), lat(0, j)] = hopping_f

	sys.attach_lead(left_lead)
	sys.attach_lead(left_lead.reversed())

	sysf = sys.finalized()
	return sysf

